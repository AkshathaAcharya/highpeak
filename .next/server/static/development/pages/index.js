module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./Forms/images.js":
/*!*************************!*\
  !*** ./Forms/images.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

const Image = props => {
  return __jsx("img", {
    className: props.class || "",
    src: props.source,
    alt: props.altText
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Image);

/***/ }),

/***/ "./Forms/input.js":
/*!************************!*\
  !*** ./Forms/input.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

const Input = props => {
  const {
    onBlur,
    type,
    classname,
    placeholder,
    value,
    onChange
  } = props;
  return __jsx("input", {
    type: type,
    className: classname,
    placeholder: placeholder,
    value: value,
    onChange: onChange,
    onBlur: onBlur,
    required: true
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Input);

/***/ }),

/***/ "./SVG/svg.js":
/*!********************!*\
  !*** ./SVG/svg.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function SVG(props) {
  return __jsx("svg", {
    width: props.width || '21px',
    height: props.height || '32px',
    viewBox: "0 0 21 32",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    xmlnsXlink: "http://www.w3.org/1999/xlink"
  }, props.children, __jsx("g", {
    stroke: "none",
    "stroke-width": "1",
    fill: "none",
    "fill-rule": "evenodd"
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (SVG);

/***/ }),

/***/ "./components/Footer/footer.css":
/*!**************************************!*\
  !*** ./components/Footer/footer.css ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/Footer/footer.js":
/*!*************************************!*\
  !*** ./components/Footer/footer.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/values */ "./node_modules/@babel/runtime-corejs2/core-js/object/values.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-responsive-modal */ "react-responsive-modal");
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-ga */ "react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _HitUsUp_hitUsUp__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../HitUsUp/hitUsUp */ "./components/HitUsUp/hitUsUp.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../constants/constants */ "./constants/constants.js");
/* harmony import */ var _Footer_footer_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Footer/footer.css */ "./components/Footer/footer.css");
/* harmony import */ var _Footer_footer_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_Footer_footer_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _styles_index_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../styles/index.css */ "./styles/index.css");
/* harmony import */ var _styles_index_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_styles_index_css__WEBPACK_IMPORTED_MODULE_9__);



var __jsx = react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement;








class Footer extends react__WEBPACK_IMPORTED_MODULE_2___default.a.Component {
  constructor(props) {
    super(props);

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(this, "onToggleHitUsUpModal", () => {
      this.setState({
        open: !this.state.open
      });
    });

    this.state = {
      open: false
    };
  }

  render() {
    const {
      open
    } = this.state;
    const footer = _constants_constants__WEBPACK_IMPORTED_MODULE_7__["NAVIGATION_ARRAY"].map((navObj, index) => navObj.title !== "" && navObj.title !== "Home" && (this.props.homeFooter ? __jsx("a", {
      className: "footerRemoveLines",
      key: index,
      onClick: e => {
        this.props.changePage(index);
      }
    }, navObj.title) : __jsx(next_link__WEBPACK_IMPORTED_MODULE_5___default.a, {
      as: "/",
      key: index,
      href: {
        pathname: "/home",
        query: {
          section: index
        }
      },
      target: "_blank"
    }, navObj.title)));
    return __jsx(react__WEBPACK_IMPORTED_MODULE_2___default.a.Fragment, null, this.props.footer && __jsx("div", {
      className: "footerWrap"
    }, __jsx("div", {
      className: "footerNavigationLinks"
    }, footer, __jsx("a", {
      href: "/about",
      target: "_self"
    }, "Who are we?")), __jsx("div", {
      className: "CTAButtonsFooter"
    }, __jsx("a", {
      href: "/portfolio",
      target: "_blank",
      className: "ourStuffButtonFooter"
    }, "Our stuff"), __jsx("button", {
      className: "hitUsUpLink menuHitUsUpLink",
      onClick: this.onToggleHitUsUpModal
    }, __jsx("b", {
      className: "footerHoverLine footerHitUsUp"
    }, "Hit us up!"))), __jsx("div", {
      className: "modalTransition"
    }, __jsx(react_responsive_modal__WEBPACK_IMPORTED_MODULE_3___default.a, {
      open: open,
      onClose: this.onToggleHitUsUpModal,
      showCloseIcon: true,
      center: true,
      styles: {
        modal: {
          outline: "none",
          animation: "0.2s zoomIn"
        }
      }
    }, __jsx(_HitUsUp_hitUsUp__WEBPACK_IMPORTED_MODULE_6__["default"], {
      togglehitusup: this.onToggleHitUsUpModal,
      headerName: "Hit Us Up!"
    }))), __jsx("div", {
      className: "weAreEveryWhere"
    }, "We are everywhere. Come, say hi!"), __jsx("div", {
      className: "footerFaIconsWrap"
    }, _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default()(_constants_constants__WEBPACK_IMPORTED_MODULE_7__["FOOTER_ICONS"]).map((item, index) => __jsx(react_ga__WEBPACK_IMPORTED_MODULE_4___default.a.OutboundLink, {
      className: "iconLinks",
      key: index,
      eventLabel: item.label,
      to: item.navigationURL,
      target: "_blank"
    }, __jsx("img", {
      src: item.img
    })))), __jsx("div", {
      className: "horizontalDiv"
    }, __jsx("span", {
      className: "copyRights"
    }, "Copyright 2020, High Peak Software. All rights reserved"), __jsx("span", {
      className: "PrivacyPolicyTermsConditions"
    }, __jsx("a", {
      href: "/termsConditions",
      target: "_self",
      className: "termsConditions"
    }, "Terms and Conditions"), __jsx("a", {
      href: "/privacyPolicy",
      target: "_self"
    }, "Privacy Policy")))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./components/GetFreeAI/getFreeAI.css":
/*!********************************************!*\
  !*** ./components/GetFreeAI/getFreeAI.css ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/GetFreeAI/getFreeAI.js":
/*!*******************************************!*\
  !*** ./components/GetFreeAI/getFreeAI.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-ga */ "react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Forms_images__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Forms/images */ "./Forms/images.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../constants/constants */ "./constants/constants.js");
/* harmony import */ var _GetFreeAI_getFreeAI_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../GetFreeAI/getFreeAI.css */ "./components/GetFreeAI/getFreeAI.css");
/* harmony import */ var _GetFreeAI_getFreeAI_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_GetFreeAI_getFreeAI_css__WEBPACK_IMPORTED_MODULE_5__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






const GetFreeAI = () => {
  return __jsx("div", {
    className: "component fifthComponent"
  }, __jsx("div", {
    className: "fifthCompMble"
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, null, __jsx("title", null, "High Peak Software - Home"), __jsx("meta", {
    name: "description",
    content: "High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
  }), __jsx("meta", {
    name: "keywords",
    content: "latest technology blogs, artificial intelligence blogs, technology blogs, automation blogs, blogs about technology, marketing tech blog, IT blogs, technology ebooks, product case study, project case study"
  }), __jsx("meta", {
    property: "og:title",
    content: "High Peak Software - Home"
  }), __jsx("meta", {
    property: "og:site_name",
    content: "High Peak Software"
  }), __jsx("meta", {
    property: "og:description",
    content: "High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
  }), __jsx("meta", {
    property: "og:url",
    content: "https://highpeaksw.com"
  }), __jsx("meta", {
    property: "og:image",
    content: "../assets/logo-color.png"
  })), __jsx("div", {
    className: "getFreeAIStuff"
  }, "Get ", __jsx("strong", null, "free"), " AI stuff", __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_3__["default"], {
    class: "weHaveProofTitleImage",
    source: "../assets/titleelement.png",
    altText: "title element"
  })), __jsx("div", {
    className: "freeImageWrap"
  }, _constants_constants__WEBPACK_IMPORTED_MODULE_4__["FREE_AI_IMG"].map((val, index) => __jsx(react_ga__WEBPACK_IMPORTED_MODULE_1___default.a.OutboundLink, {
    className: "horizontal",
    eventLabel: val.label,
    to: val.url,
    target: "_blank"
  }, __jsx("div", {
    key: index,
    className: "verticalDiv"
  }, __jsx("div", {
    className: `${val.className} ${index % 2 === 0 ? "" : "hideMobile"}`
  }), __jsx("div", {
    className: "titleDiv"
  }, __jsx("strong", {
    className: val.titleClass
  }, val.title), __jsx("span", {
    className: val.subtitleClass
  }, val.subTitle)), index % 2 == 1 && __jsx("div", {
    className: `${val.className} showMobile`
  })))))));
};

/* harmony default export */ __webpack_exports__["default"] = (GetFreeAI);

/***/ }),

/***/ "./components/HitUsUp/hitUsUp.css":
/*!****************************************!*\
  !*** ./components/HitUsUp/hitUsUp.css ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/HitUsUp/hitUsUp.js":
/*!***************************************!*\
  !*** ./components/HitUsUp/hitUsUp.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_popupbox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-popupbox */ "react-popupbox");
/* harmony import */ var react_popupbox__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_popupbox__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Forms_input__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../Forms/input */ "./Forms/input.js");
/* harmony import */ var _Forms_images__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../Forms/images */ "./Forms/images.js");
/* harmony import */ var _constants_hitUsUpNewConstants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../constants/hitUsUpNewConstants */ "./constants/hitUsUpNewConstants.js");
/* harmony import */ var _constants_uploadFileAction__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../constants/uploadFileAction */ "./constants/uploadFileAction.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../constants/constants */ "./constants/constants.js");
/* harmony import */ var _constants_googleAnalyticsConstants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../constants/googleAnalyticsConstants */ "./constants/googleAnalyticsConstants.js");
/* harmony import */ var _constants_gtmScripts__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../constants/gtmScripts */ "./constants/gtmScripts.js");
/* harmony import */ var _utils_analytics__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../utils/analytics */ "./utils/analytics.js");
/* harmony import */ var _HitUsUp_hitUsUp_css__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../HitUsUp/hitUsUp.css */ "./components/HitUsUp/hitUsUp.css");
/* harmony import */ var _HitUsUp_hitUsUp_css__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_HitUsUp_hitUsUp_css__WEBPACK_IMPORTED_MODULE_12__);


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;












class HitUsUp extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {
  constructor() {
    super();

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "handleNameChange", event => {
      this.setState({
        name: event && event.target && event.target.value ? event.target.value : null
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "handleEmailChange", event => {
      this.setState({
        email: event && event.target && event.target.value ? event.target.value : null,
        validEmail: /\s/.test(event.target.value) === false && _constants_constants__WEBPACK_IMPORTED_MODULE_8__["EMAIL_REGEX"].regexObject.test(event.target.value) && true
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "handleCommentsChange", event => {
      this.setState({
        comments: event && event.target && event.target.value ? event.target.value : null
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "handleCheckboxChange", () => {
      this.setState({
        isChecked: !this.state.isChecked
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "onToggleMenu", () => {
      this.setState(prevState => ({
        openMenu: !prevState.openMenu
      }));
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "uploadFile", event => {
      const formData = new FormData();
      formData.append("file", event.target.files[0]);
      this.setState({
        uploadResume: this.props.store,
        fileData: formData
      });
      return true;
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "handleSubmit", async event => {
      event.preventDefault();
      const headerVariable = this.props.headerName.replace(/[^a-zA-Z0-9]/g, "").replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
        return index == 0 ? word.toLowerCase() : word.toUpperCase();
      }).replace(/\s+/g, "");
      const filteredAnalytics = _constants_googleAnalyticsConstants__WEBPACK_IMPORTED_MODULE_9__["GOOGLE_ANALYTICS_CONSTANTS"][headerVariable];
      Object(_utils_analytics__WEBPACK_IMPORTED_MODULE_11__["Event"])(filteredAnalytics.event, filteredAnalytics.details, filteredAnalytics.page);

      if (this.state.validEmail && this.state.name && this.state.isChecked && this.state.comments) {
        this.setState({
          beforesubmit: false,
          submitted: true,
          data: {
            Name: this.state.name,
            Email: this.state.email,
            Comments: this.state.comments,
            Filename: this.props.store === undefined ? "noFile" : this.props.store.filename,
            Path: this.props.store === undefined ? "noPath" : this.props.store.path
          }
        });
        return false;
      } else {
        const content = __jsx("p", {
          className: "quotes displaymessage redColorContainer"
        }, "* Please fill all fields!!.");

        react_popupbox__WEBPACK_IMPORTED_MODULE_2__["PopupboxManager"].open({
          content
        });
      }
    });

    this.state = {
      name: null,
      email: null,
      comments: null,
      isChecked: false,
      validName: false,
      validEmail: false,
      data: null,
      submitted: false,
      beforesubmit: true,
      openMenu: false,
      open: false,
      joinus: false,
      file: null,
      fileData: null,
      uploadResume: null
    };
  }

  componentWillMount() {
    Object(_constants_gtmScripts__WEBPACK_IMPORTED_MODULE_10__["GA_SCRIPT"])();
  }

  render() {
    const fieldFills = this.state.validEmail && this.state.name && this.state.isChecked && this.state.comments && "Filled";
    const submitCheck = this.props.headerName === "Join Us!" ? this.state.fileData && fieldFills : fieldFills;

    if (this.state.data) {
      this.props.sendData(this.state.data);
      this.state.data = null;
    }

    this.props.store !== this.state.uploadResume ? null : this.state.fileData && this.props.uploadData(this.state.fileData);
    return __jsx("div", {
      className: "hitUsUpWrap"
    }, __jsx("div", {
      className: "hitUsUpTitle"
    }, __jsx("span", null, this.props.headerName), __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_5__["default"], {
      class: "hitusupTitleelement",
      source: "/assets/titleelement.png",
      altText: "title-element"
    })), __jsx("div", {
      className: "mobileFlex"
    }, __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_5__["default"], {
      class: "iconSize",
      source: "/assets/user.svg"
    }), __jsx("span", {
      className: "redColorContainer"
    }, " * "), __jsx(_Forms_input__WEBPACK_IMPORTED_MODULE_4__["default"], {
      value: this.state.name,
      onChange: this.handleNameChange,
      type: "text",
      name: "name",
      classname: "hitUsUpInputOld",
      placeholder: "Tell us your name",
      required: true
    })), __jsx("div", {
      className: "mobileFlex"
    }, __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_5__["default"], {
      class: "iconSize",
      source: "/assets/envelope.svg"
    }), __jsx("span", {
      className: "redColorContainer"
    }, " * "), __jsx(_Forms_input__WEBPACK_IMPORTED_MODULE_4__["default"], {
      value: this.state.email,
      onChange: this.handleEmailChange,
      type: "email",
      name: "email",
      classname: "hitUsUpInputOld",
      placeholder: "We don't write much",
      required: true
    })), !this.state.validEmail && this.state.email !== null && __jsx("div", {
      className: "hpsError redColorContainer displaymessage"
    }, "Invalid e-mail"), this.props.headerName === "Join Us!" ? __jsx("div", {
      className: "mobileFlex"
    }, __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_5__["default"], {
      class: "iconSize",
      source: "/assets/edit.svg"
    }), __jsx("span", {
      className: "redColorContainer"
    }, " * "), __jsx(_Forms_input__WEBPACK_IMPORTED_MODULE_4__["default"], {
      value: this.state.comments,
      onChange: this.handleCommentsChange,
      classname: "hitUsUpInputOld",
      name: "comments",
      placeholder: "Applying for the position of",
      required: true
    })) : __jsx("div", {
      className: "mobileFlex"
    }, __jsx("span", {
      className: "moveIconTop"
    }, __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_5__["default"], {
      class: "iconSize",
      source: "/assets/edit.svg"
    }), __jsx("span", {
      className: "redColorContainer"
    }, " * ")), __jsx("textarea", {
      value: this.state.comments,
      onChange: this.handleCommentsChange,
      className: "hitUsUpTextArea",
      name: "comments",
      placeholder: "Drop us a note! \n\n(So we know what you want to talk about. Otherwise, we like rambling a lot)",
      required: true
    })), this.props.headerName === "Join Us!" && __jsx("div", {
      className: "widthContainer mobileFlex"
    }, __jsx("span", {
      className: "iconsUpdate"
    }, __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_5__["default"], {
      class: "iconSize",
      source: "/assets/file.svg"
    }), __jsx("span", {
      className: "redColorContainer"
    }, " * ")), __jsx("input", {
      type: "file",
      name: "file",
      className: "fileStyle",
      onChange: this.uploadFile,
      required: true
    })), __jsx("div", {
      className: "AgreeTerms"
    }, __jsx("input", {
      className: "hitusupCheckbox",
      type: "checkbox",
      onChange: this.handleCheckboxChange,
      checked: this.state.isChecked,
      required: true
    }), __jsx("span", {
      className: "redColorContainer"
    }, "*"), __jsx("span", {
      className: "termsPrivacy"
    }, "I agree to the", __jsx("a", {
      href: "/termsConditions",
      target: "_blank",
      className: "termsLink"
    }, "terms and conditions"), "and", __jsx("a", {
      href: "/privacyPolicy",
      target: "_blank",
      className: "termsLink"
    }, "privacypolicy"))), this.state.submitted ? __jsx("input", {
      disabled: submitCheck && this.state.submitted,
      type: "submit",
      value: "Submitted!",
      onClick: event => this.handleSubmit(event),
      className: "disablesubmit buttonContainer"
    }) : __jsx("input", {
      type: "submit",
      value: "Submit",
      onClick: event => this.handleSubmit(event),
      className: this.state.beforesubmit && submitCheck === "Filled" ? "hitUsUpSubmit buttonContainer" : "disablesubmit buttonContainer"
    }), submitCheck !== "Filled" && __jsx(react_popupbox__WEBPACK_IMPORTED_MODULE_2__["PopupboxContainer"], null));
  }

}

const dispachToProps = dispatch => {
  return {
    sendData: (...test) => dispatch(Object(_constants_hitUsUpNewConstants__WEBPACK_IMPORTED_MODULE_6__["sendData"])(...test)),
    uploadData: (...test) => dispatch(Object(_constants_uploadFileAction__WEBPACK_IMPORTED_MODULE_7__["uploadData"])(...test))
  };
};

const stateToProps = state => {
  return {
    store: state.SendMailReducer.store,
    sentMail: state.SendMailReducer.sentMail
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["connect"])(stateToProps, dispachToProps)(HitUsUp));

/***/ }),

/***/ "./components/HomeLanding/homeLanding.css":
/*!************************************************!*\
  !*** ./components/HomeLanding/homeLanding.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/HomeLanding/homeLanding.js":
/*!***********************************************!*\
  !*** ./components/HomeLanding/homeLanding.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Forms_images__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Forms/images */ "./Forms/images.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../constants/constants */ "./constants/constants.js");
/* harmony import */ var _HomeLanding_homeLanding_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../HomeLanding/homeLanding.css */ "./components/HomeLanding/homeLanding.css");
/* harmony import */ var _HomeLanding_homeLanding_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_HomeLanding_homeLanding_css__WEBPACK_IMPORTED_MODULE_4__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





const HomeLanding = () => {
  return __jsx("div", {
    className: "component firstComponent"
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, null, __jsx("title", null, "High Peak Software - Home")), __jsx("div", {
    className: "weAreHighAI"
  }, "We Are", __jsx("section", {
    className: "rotatingWordsMargin"
  }, "High-", __jsx("span", {
    class: "rotatingWords rotatingWordsOne homepageRwwords"
  }, _constants_constants__WEBPACK_IMPORTED_MODULE_3__["ROTATING_WORDS"].map((words, index) => __jsx("span", {
    key: index
  }, words.homePage)))), "AI", __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_2__["default"], {
    class: "titleImage",
    source: "../assets/titleelement.png",
    altText: "title-element"
  }), __jsx("a", {
    href: "/portfolio",
    target: "_blank",
    className: "checkOutOurStuff"
  }, "Check out our stuff")));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeLanding);

/***/ }),

/***/ "./components/WeDeliverFreeAI/weDeliverFreeAI.css":
/*!********************************************************!*\
  !*** ./components/WeDeliverFreeAI/weDeliverFreeAI.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/WeDeliverFreeAI/weDeliverFreeAI.js":
/*!*******************************************************!*\
  !*** ./components/WeDeliverFreeAI/weDeliverFreeAI.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_contactUs_contactUs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/contactUs/contactUs */ "./components/contactUs/contactUs.js");
/* harmony import */ var _Footer_footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Footer/footer */ "./components/Footer/footer.js");
/* harmony import */ var _WeDeliverFreeAI_weDeliverFreeAI_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../WeDeliverFreeAI/weDeliverFreeAI.css */ "./components/WeDeliverFreeAI/weDeliverFreeAI.css");
/* harmony import */ var _WeDeliverFreeAI_weDeliverFreeAI_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_WeDeliverFreeAI_weDeliverFreeAI_css__WEBPACK_IMPORTED_MODULE_4__);


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;




class WeDeliverFreeAI extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {
  constructor(props) {
    super(props);

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "componentDidMount", () => {
      this.props.footer === false && this.setState({
        footer: false
      });
    });

    this.state = {
      footer: true,
      homeFooter: this.props.homeFooter || false
    };
  }

  render() {
    return __jsx("div", {
      className: "mainDivdlvestf"
    }, __jsx(_components_contactUs_contactUs__WEBPACK_IMPORTED_MODULE_2__["default"], null), __jsx(_Footer_footer__WEBPACK_IMPORTED_MODULE_3__["default"], {
      homeFooter: this.state.homeFooter,
      footer: this.state.footer,
      navigationArray: this.props.navigationArray,
      changePage: this.props.changePage
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (WeDeliverFreeAI);

/***/ }),

/***/ "./components/WeMakeCoolAI/weMakeCoolAI.css":
/*!**************************************************!*\
  !*** ./components/WeMakeCoolAI/weMakeCoolAI.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/WeMakeCoolAI/weMakeCoolAI.js":
/*!*************************************************!*\
  !*** ./components/WeMakeCoolAI/weMakeCoolAI.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_awesome_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-awesome-slider */ "react-awesome-slider");
/* harmony import */ var react_awesome_slider__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_awesome_slider__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_awesome_slider_dist_autoplay__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-awesome-slider/dist/autoplay */ "react-awesome-slider/dist/autoplay");
/* harmony import */ var react_awesome_slider_dist_autoplay__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_awesome_slider_dist_autoplay__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Forms_images__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../Forms/images */ "./Forms/images.js");
/* harmony import */ var _constants_constants_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../constants/constants.js */ "./constants/constants.js");
/* harmony import */ var _WeMakeCoolAI_weMakeCoolAI_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../WeMakeCoolAI/weMakeCoolAI.css */ "./components/WeMakeCoolAI/weMakeCoolAI.css");
/* harmony import */ var _WeMakeCoolAI_weMakeCoolAI_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_WeMakeCoolAI_weMakeCoolAI_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_awesome_slider_dist_styles_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-awesome-slider/dist/styles.css */ "./node_modules/react-awesome-slider/dist/styles.css");
/* harmony import */ var react_awesome_slider_dist_styles_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_awesome_slider_dist_styles_css__WEBPACK_IMPORTED_MODULE_7__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







const AutoplaySlider = react_awesome_slider_dist_autoplay__WEBPACK_IMPORTED_MODULE_2___default()(react_awesome_slider__WEBPACK_IMPORTED_MODULE_1___default.a);

class WeMakeCoolAI extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return __jsx("div", {
      className: "component thirdComponent"
    }, __jsx("div", {
      className: "thirdCompMobile"
    }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_3___default.a, null, __jsx("title", null, "High Peak Software - Home"), __jsx("meta", {
      name: "description",
      content: "High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
    }), __jsx("meta", {
      name: "keywords",
      content: "AI products, ML products, ML solutions. Web applications. Mobile applications. Emerging technologies, Saas products, "
    }), __jsx("meta", {
      property: "og:title",
      content: "High Peak Software - Home"
    }), __jsx("meta", {
      property: "og:site_name",
      content: "High Peak Software"
    }), __jsx("meta", {
      property: "og:description",
      content: "High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
    }), __jsx("meta", {
      property: "og:url",
      content: "https://highpeaksw.com"
    }), __jsx("meta", {
      property: "og:image",
      content: "../assets/logo-color.png"
    }), __jsx("meta", {
      name: "viewport",
      content: "width=device-width,height=device-height,initial-scale=1.0"
    })), __jsx("div", {
      className: "columnWise"
    }, __jsx("div", {
      className: "thirdComponentSlider"
    }, __jsx(AutoplaySlider, {
      play: true,
      infinite: true,
      cancelOnInteraction: false,
      interval: 3000
    }, _constants_constants_js__WEBPACK_IMPORTED_MODULE_5__["PROJECTS_ARRAY"].map((proj, index) => __jsx("div", {
      key: index,
      className: "groupContainer"
    }, __jsx("div", {
      className: "ImageWrapCoolAI"
    }, __jsx("a", {
      href: proj.url
    }, __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_4__["default"], {
      source: proj.projScreen,
      altText: "project-screenshots"
    }))), __jsx("div", {
      className: "slideProjTitle"
    }, __jsx("strong", null, proj.title), __jsx("div", {
      className: "projSubTitle"
    }, proj.subTitle)), __jsx("div", {
      className: "responsiveNumber"
    }, proj.responsiveNumber))))), __jsx("div", {
      className: "ourIntraAtWrk"
    }, "Our", __jsx("div", {
      class: "rotatingWords rotatingWordsOne rwIntratwork"
    }, _constants_constants_js__WEBPACK_IMPORTED_MODULE_5__["ROTATING_WORDS"].map((words, index) => __jsx("span", {
      key: index
    }, __jsx("pre", null, words.weMakeCoolAI)))), __jsx("section", null, "at work")))), __jsx("div", {
      className: "weMakeCool"
    }, __jsx("span", null, "We make ", __jsx("strong", null, "cool"), " AI stuff"), __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_4__["default"], {
      class: "weMakeCoolTitleImage",
      source: "../assets/titleelement.png",
      altText: "title-element"
    })));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (WeMakeCoolAI);

/***/ }),

/***/ "./components/WeSolveToughAI/weSolveToughAI.css":
/*!******************************************************!*\
  !*** ./components/WeSolveToughAI/weSolveToughAI.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/WeSolveToughAI/weSolveToughAI.js":
/*!*****************************************************!*\
  !*** ./components/WeSolveToughAI/weSolveToughAI.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Forms_images__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Forms/images */ "./Forms/images.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../constants/constants */ "./constants/constants.js");
/* harmony import */ var _WeSolveToughAI_weSolveToughAI_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../WeSolveToughAI/weSolveToughAI.css */ "./components/WeSolveToughAI/weSolveToughAI.css");
/* harmony import */ var _WeSolveToughAI_weSolveToughAI_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_WeSolveToughAI_weSolveToughAI_css__WEBPACK_IMPORTED_MODULE_5__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





/* harmony default export */ __webpack_exports__["default"] = (() => {
  return __jsx("div", {
    className: "component secondComponent"
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, null, __jsx("title", null, "High Peak Software - Home"), __jsx("meta", {
    name: "description",
    content: "High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
  }), __jsx("meta", {
    name: "keywords",
    content: "AI solutions, Machine Learning, Predictive Analytics, Deep Learning, Data Science, Natural Language Processing, NLP, ML, DL, Artificial Intelligence, Digital Marketing, UX/UI design"
  }), __jsx("meta", {
    property: "og:title",
    content: "High Peak Software - Home"
  }), __jsx("meta", {
    property: "og:site_name",
    content: "High Peak Software"
  }), __jsx("meta", {
    property: "og:description",
    content: "High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
  }), __jsx("meta", {
    property: "og:url",
    content: "https://highpeaksw.com"
  }), __jsx("meta", {
    property: "og:image",
    content: "../assets/logo-color.png"
  })), __jsx("div", {
    className: "wesolveMobileView"
  }, __jsx("div", {
    className: "weSolveTough"
  }, __jsx("span", null, "We solve ", __jsx("strong", null, "tough"), " AI stuff"), __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_3__["default"], {
    class: "titleImageSolve",
    source: "../assets/titleelement.png",
    altText: "title-element"
  })), __jsx("div", {
    className: "rightDiv"
  }, __jsx("div", {
    className: "tableStyle"
  }, _constants_constants__WEBPACK_IMPORTED_MODULE_4__["WE_SOLVE_TOUGH_AI"].map((title, index) => __jsx("div", {
    key: index,
    className: "solveAI"
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    prefetch: true,
    as: `/services/${title.url}`,
    href: {
      pathname: "/services/[solution.js]",
      query: {
        solution: title.title
      }
    }
  }, __jsx("a", {
    className: "weSolveAnchor"
  }, __jsx("img", {
    src: title.img,
    altText: "images",
    className: "solutionsImages",
    onMouseOver: e => e.currentTarget.src = title.hoverImage,
    onMouseOut: e => e.currentTarget.src = title.img
  }), __jsx("div", {
    className: "serviceTitle"
  }, title.title)))))), __jsx("div", {
    className: "weBuildBetter"
  }, "We build", __jsx("div", {
    className: "rotatingWords rotatingWordsOne rwBuildbetter"
  }, _constants_constants__WEBPACK_IMPORTED_MODULE_4__["ROTATING_WORDS"].map((words, index) => __jsx("span", {
    key: index
  }, words.weSolveToughAI))), __jsx("section", null, "AI solutions")))));
});

/***/ }),

/***/ "./components/contactUs/contactUs.css":
/*!********************************************!*\
  !*** ./components/contactUs/contactUs.css ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/contactUs/contactUs.js":
/*!*******************************************!*\
  !*** ./components/contactUs/contactUs.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _contactUs_contactUs_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contactUs/contactUs.css */ "./components/contactUs/contactUs.css");
/* harmony import */ var _contactUs_contactUs_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_contactUs_contactUs_css__WEBPACK_IMPORTED_MODULE_1__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const ContactUs = () => {
  return __jsx("div", {
    className: "contactUs"
  }, __jsx("div", {
    className: "mapDiv columnContainer"
  }, __jsx("span", null, "Drop in for "), __jsx("span", null, "some ", __jsx("span", {
    className: "boldStyle"
  }, " tea ")), __jsx("span", null, " or "), __jsx("span", null, "a game of"), __jsx("span", {
    className: "boldStyle"
  }, " TT!")), __jsx("div", {
    className: "addressWrap columnContainer"
  }, __jsx("div", {
    className: "addressBar"
  }, __jsx("span", {
    className: "addressHeading"
  }, "Atlanta Office"), __jsx("span", null, "6055 Southard Trace, Cumming, GA, USA")), __jsx("div", {
    className: "addressBar"
  }, __jsx("span", {
    className: "addressHeading"
  }, "Development center"), __jsx("span", null, "4, 17th Cross, K R Road, BSK Stage II,"), __jsx("span", null, "Bengaluru, Karnataka, India - 560070 ")), __jsx("div", {
    className: "addressBar"
  }, __jsx("span", {
    className: "addressHeading"
  }, "E-mail"), __jsx("span", null, "marketing@highpeaksw.com")), __jsx("div", {
    className: "addressBar"
  }, __jsx("span", {
    className: "addressHeading"
  }, "Phone"), __jsx("span", null, "+91 80 2676 0510"))));
};

/* harmony default export */ __webpack_exports__["default"] = (ContactUs);

/***/ }),

/***/ "./components/homePageNormalScroll/homePageScroll.js":
/*!***********************************************************!*\
  !*** ./components/homePageNormalScroll/homePageScroll.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-responsive-modal */ "react-responsive-modal");
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _HomeLanding_homeLanding__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../HomeLanding/homeLanding */ "./components/HomeLanding/homeLanding.js");
/* harmony import */ var _WeSolveToughAI_weSolveToughAI__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../WeSolveToughAI/weSolveToughAI */ "./components/WeSolveToughAI/weSolveToughAI.js");
/* harmony import */ var _WeMakeCoolAI_weMakeCoolAI__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../WeMakeCoolAI/weMakeCoolAI */ "./components/WeMakeCoolAI/weMakeCoolAI.js");
/* harmony import */ var _GetFreeAI_getFreeAI__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../GetFreeAI/getFreeAI */ "./components/GetFreeAI/getFreeAI.js");
/* harmony import */ var _Forms_images__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../Forms/images */ "./Forms/images.js");
/* harmony import */ var _menu_svg__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../menu_svg */ "./components/menu_svg.js");
/* harmony import */ var _WeDeliverFreeAI_weDeliverFreeAI__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../WeDeliverFreeAI/weDeliverFreeAI */ "./components/WeDeliverFreeAI/weDeliverFreeAI.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../constants/constants */ "./constants/constants.js");
/* harmony import */ var _constants_gtmScripts__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../constants/gtmScripts */ "./constants/gtmScripts.js");
/* harmony import */ var _styles_about_css__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../styles/about.css */ "./styles/about.css");
/* harmony import */ var _styles_about_css__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_styles_about_css__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _Footer_footer_css__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../Footer/footer.css */ "./components/Footer/footer.css");
/* harmony import */ var _Footer_footer_css__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_Footer_footer_css__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _styles_homePage_css__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../styles/homePage.css */ "./styles/homePage.css");
/* harmony import */ var _styles_homePage_css__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_styles_homePage_css__WEBPACK_IMPORTED_MODULE_14__);


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;














class HomePageScroll extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {
  constructor(props) {
    super(props);

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "onToggleHitUsUpModal", () => {
      this.setState(prevState => ({
        open: !prevState.open
      }));
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "onToggleMenu", () => {
      this.setState(prevState => ({
        openMenu: !prevState.openMenu
      }));
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "componentDidMount", () => {
      Object(_constants_gtmScripts__WEBPACK_IMPORTED_MODULE_11__["GA_SCRIPT"])();
      const keyToScroll = this.props.querysection;
      const key = String(keyToScroll).charAt(0);

      if (key == 1) {
        this.focusScrollMethod("", this.WeSolveToughAIToFocus);
      }

      if (key == 2) {
        this.focusScrollMethod("", this.weMakeCoolAIToFocus);
      }

      if (key == 3) {
        this.focusScrollMethod("", this.GetFreeAIToFocus);
      }

      if (key == 4) {
        this.focusScrollMethod("", this.DropInToFocus);
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "footerScroll", event => {
      if (event == 1) {
        this.focusScrollMethod("", this.WeSolveToughAIToFocus);
      }

      if (event == 2) {
        this.focusScrollMethod("", this.weMakeCoolAIToFocus);
      }

      if (event == 3) {
        this.focusScrollMethod("", this.GetFreeAIToFocus);
      }

      if (event == 4) {
        this.focusScrollMethod("", this.DropInToFocus);
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "focusScrollMethod", (event, reference) => {
      this.state.openMenu && this.setState({
        openMenu: false
      });

      if (reference["current"]) {
        reference["current"].scrollIntoView({
          top: reference["offsetTop"],
          behavior: "auto",
          block: "center",
          inline: "center"
        });
      }
    });

    this.state = {
      open: false,
      openMenu: false,
      homePages: "homePageLinks"
    };
    this.homeDivToFocus = react__WEBPACK_IMPORTED_MODULE_1___default.a.createRef();
    this.WeSolveToughAIToFocus = react__WEBPACK_IMPORTED_MODULE_1___default.a.createRef();
    this.weMakeCoolAIToFocus = react__WEBPACK_IMPORTED_MODULE_1___default.a.createRef();
    this.GetFreeAIToFocus = react__WEBPACK_IMPORTED_MODULE_1___default.a.createRef();
    this.DropInToFocus = react__WEBPACK_IMPORTED_MODULE_1___default.a.createRef();
  }

  render() {
    const navigationArray = [{
      menu: "Home",
      reference: this.homeDivToFocus
    }, {
      menu: "We Solve Tough AI Stuff",
      reference: this.WeSolveToughAIToFocus
    }, {
      menu: "We Make Cool AI Stuff",
      reference: this.weMakeCoolAIToFocus
    }, {
      menu: "Get Free AI Stuff",
      reference: this.GetFreeAIToFocus
    }, {
      menu: "Drop In",
      reference: this.DropInToFocus
    }];
    const {
      open
    } = this.state;
    return __jsx("div", null, __jsx("div", {
      className: "mobileHeaderSticky"
    }, __jsx("a", {
      href: "/",
      target: "_self"
    }, __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_7__["default"], {
      class: "logoPosition",
      source: "/assets/logo-color.png",
      altText: "logo"
    })), __jsx("div", {
      className: "leftNavigationBar",
      onClick: this.onToggleMenu
    }, __jsx(_menu_svg__WEBPACK_IMPORTED_MODULE_8__["default"], {
      navigationColor: "black"
    })), __jsx("div", {
      className: "navigationSlider"
    }, __jsx(react_responsive_modal__WEBPACK_IMPORTED_MODULE_2___default.a, {
      open: this.state.openMenu,
      onClose: this.onToggleMenu,
      styles: {
        overlay: {
          padding: "0px"
        },
        modal: {
          maxWidth: "1500px",
          width: "0%",
          marginRight: "0%",
          float: "right",
          height: "100%",
          fontSize: "24px",
          animation: "slide-in 0.5s forwards",
          outline: "none"
        }
      }
    }, __jsx("ul", {
      className: "ulRight"
    }, __jsx("li", {
      className: "liMenuSlide"
    }, navigationArray.map((navigation, array) => __jsx("a", {
      onClick: e => {
        this.focusScrollMethod(e, navigation.reference);
      }
    }, navigation.menu)), __jsx("a", {
      href: "/about",
      className: "menulink"
    }, "Who Are We?")))))), __jsx("div", {
      className: "homelanding",
      ref: this.homeDivToFocus
    }, __jsx(_HomeLanding_homeLanding__WEBPACK_IMPORTED_MODULE_3__["default"], null)), __jsx("div", {
      className: "wesolvetoughAI",
      ref: this.WeSolveToughAIToFocus
    }, __jsx(_WeSolveToughAI_weSolveToughAI__WEBPACK_IMPORTED_MODULE_4__["default"], null)), __jsx("div", {
      className: "wemakecoolAI",
      ref: this.weMakeCoolAIToFocus
    }, __jsx(_WeMakeCoolAI_weMakeCoolAI__WEBPACK_IMPORTED_MODULE_5__["default"], null)), __jsx("div", {
      className: "getfreeAI",
      ref: this.GetFreeAIToFocus
    }, __jsx(_GetFreeAI_getFreeAI__WEBPACK_IMPORTED_MODULE_6__["default"], null)), __jsx("div", {
      className: "wedeliverfreeAI",
      ref: this.DropInToFocus
    }, __jsx(_WeDeliverFreeAI_weDeliverFreeAI__WEBPACK_IMPORTED_MODULE_9__["default"], {
      footer: true,
      navigation_Array: _constants_constants__WEBPACK_IMPORTED_MODULE_10__["NAVIGATION_ARRAY"],
      changePage: this.footerScroll,
      homeFooter: true
    })));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (HomePageScroll);

/***/ }),

/***/ "./components/homePages/homePage.js":
/*!******************************************!*\
  !*** ./components/homePages/homePage.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-responsive-modal */ "react-responsive-modal");
/* harmony import */ var react_responsive_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_responsive_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _pageScroller__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../pageScroller */ "./components/pageScroller.js");
/* harmony import */ var _HomeLanding_homeLanding__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../HomeLanding/homeLanding */ "./components/HomeLanding/homeLanding.js");
/* harmony import */ var _WeMakeCoolAI_weMakeCoolAI__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../WeMakeCoolAI/weMakeCoolAI */ "./components/WeMakeCoolAI/weMakeCoolAI.js");
/* harmony import */ var _WeSolveToughAI_weSolveToughAI__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../WeSolveToughAI/weSolveToughAI */ "./components/WeSolveToughAI/weSolveToughAI.js");
/* harmony import */ var _GetFreeAI_getFreeAI__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../GetFreeAI/getFreeAI */ "./components/GetFreeAI/getFreeAI.js");
/* harmony import */ var _scroll_svg__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../scroll_svg */ "./components/scroll_svg.js");
/* harmony import */ var _menu_svg__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../menu_svg */ "./components/menu_svg.js");
/* harmony import */ var _HitUsUp_hitUsUp__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../HitUsUp/hitUsUp */ "./components/HitUsUp/hitUsUp.js");
/* harmony import */ var _WeDeliverFreeAI_weDeliverFreeAI__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../WeDeliverFreeAI/weDeliverFreeAI */ "./components/WeDeliverFreeAI/weDeliverFreeAI.js");
/* harmony import */ var _Forms_images__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../Forms/images */ "./Forms/images.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../constants/constants */ "./constants/constants.js");
/* harmony import */ var _constants_googleAnalyticsConstants__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../constants/googleAnalyticsConstants */ "./constants/googleAnalyticsConstants.js");
/* harmony import */ var _constants_gtmScripts__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../constants/gtmScripts */ "./constants/gtmScripts.js");
/* harmony import */ var _utils_analytics__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../utils/analytics */ "./utils/analytics.js");
/* harmony import */ var _styles_index_css__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../styles/index.css */ "./styles/index.css");
/* harmony import */ var _styles_index_css__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_styles_index_css__WEBPACK_IMPORTED_MODULE_17__);


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

















class HomePage extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {
  constructor(props) {
    super(props);

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "onToggleHitUsUpModal", () => {
      this.setState(prevState => ({
        open: !prevState.open
      }));
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "onToggleMenu", () => {
      this.setState(prevState => ({
        openMenu: !prevState.openMenu
      }));
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "changePage", index => {
      if (index) {
        const pagesAnalytics = _constants_googleAnalyticsConstants__WEBPACK_IMPORTED_MODULE_14__["GOOGLE_ANALYTICS_CONSTANTS"][this.state.pageName][index];
        Object(_utils_analytics__WEBPACK_IMPORTED_MODULE_16__["Event"])(pagesAnalytics.event, pagesAnalytics.page, pagesAnalytics.details);
      }

      this.setState({
        active: this.state.active === index ? null : index,
        currentPage: index,
        openMenu: false
      }, this.scrollToPage);
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "scrollToPage", () => {
      const {
        currentPage
      } = this.state;

      this._pageScroller.goToPage(currentPage);
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "navColor", position => {
      const {
        active,
        currentPage
      } = this.state;

      if (active === position || position === currentPage) {
        return "2px solid #FECE2B";
      }

      return " ";
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "pageOnChange", number => {
      this.setState({
        currentPage: number
      });
      this.changePage(number - 1);
      this.navColor(number - 1);
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "componentDidMount", () => {
      const keyToScroll = this.props.querysection;
      const one = String(keyToScroll).charAt(0);
      this.changePage(Number(one));
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "navMenuMouseOver", (event, index) => {
      this.setState({
        hoveredIndex: index
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "navMenuMouseLeave", () => {
      this.setState({
        hoveredIndex: ""
      });
    });

    this.state = {
      currentPage: 0,
      active: null,
      open: false,
      openMenu: false,
      hoveredIndex: "",
      sectionScroll: 0,
      scrollPageStart: false,
      pageName: "home"
    };
    this._pageScroller = null;
  }

  componentWillMount() {
    Object(_constants_gtmScripts__WEBPACK_IMPORTED_MODULE_15__["GA_SCRIPT"])();
  }

  render() {
    const {
      open
    } = this.state;
    const renderedArray = _constants_constants__WEBPACK_IMPORTED_MODULE_13__["NAVIGATION_ARRAY"][this.state.currentPage];

    const logo = __jsx(_Forms_images__WEBPACK_IMPORTED_MODULE_12__["default"], {
      class: "logoPosition",
      source: renderedArray.colorImage,
      altText: "logo"
    });

    return __jsx("div", null, __jsx("div", {
      className: "mainDiv"
    }, __jsx("div", {
      className: "footerNavigation"
    }, __jsx("div", {
      className: "vl"
    }, " "), __jsx("div", {
      style: {
        height: "100%"
      }
    }, __jsx("ul", {
      className: "OrderNavigation"
    }, _constants_constants__WEBPACK_IMPORTED_MODULE_13__["NAVIGATION_ARRAY"].map((navObj, index) => navObj.title != "" && __jsx("div", {
      key: index,
      className: "eachListDiv",
      onClick: e => {
        this.changePage(index);
      },
      onMouseOver: e => {
        this.navMenuMouseOver(e, index);
      },
      onMouseLeave: this.navMenuMouseLeave
    }, __jsx("li", {
      className: "NavList"
    }, __jsx("a", {
      href: navObj.hashurl,
      style: {
        borderLeft: this.navColor(index)
      },
      className: "anchorNavigation"
    }, this.state.hoveredIndex === index && navObj.title)))))), __jsx("div", {
      className: "bottomVlDiv"
    }, __jsx("div", {
      className: "bottomVl"
    }))), renderedArray.scroll && __jsx("div", {
      className: "scrollButtonHome",
      onClick: e => {
        this.changePage(this.state.currentPage + 1);
      }
    }, __jsx(_scroll_svg__WEBPACK_IMPORTED_MODULE_8__["default"], {
      rectangleColor: renderedArray.stateScrollRectangle,
      circleColor: renderedArray.stateScrollCircle
    }), __jsx("div", {
      style: {
        marginLeft: "-3px",
        fontSize: "11px",
        color: renderedArray.scrollButtonColor
      }
    }, "Scroll")), __jsx("div", {
      className: "topNavigation"
    }, __jsx("a", {
      href: "/",
      target: "_self"
    }, logo), __jsx("a", {
      className: renderedArray.classNameOurStuffButton,
      href: "/portfolio",
      target: "_blank"
    }, "Our stuff"), __jsx("button", {
      onClick: this.onToggleHitUsUpModal,
      className: renderedArray.classNameHitUsUp
    }, "Hit us up!"), __jsx("div", {
      className: "leftNavigationBar",
      onClick: this.onToggleMenu
    }, __jsx(_menu_svg__WEBPACK_IMPORTED_MODULE_9__["default"], {
      navigationColor: renderedArray.navigationColor
    }))), __jsx("div", {
      className: "modalTransition"
    }, __jsx(react_responsive_modal__WEBPACK_IMPORTED_MODULE_2___default.a, {
      open: open,
      onClose: this.onToggleHitUsUpModal,
      showCloseIcon: true,
      center: true,
      styles: {
        modal: {
          outline: "none",
          animation: "0.2s zoomIn"
        }
      }
    }, __jsx(_HitUsUp_hitUsUp__WEBPACK_IMPORTED_MODULE_10__["default"], {
      headerName: "Hit Us Up!",
      togglehitusup: this.onToggleHitUsUpModal
    }))), __jsx("div", {
      className: "navigationSlider"
    }, __jsx(react_responsive_modal__WEBPACK_IMPORTED_MODULE_2___default.a, {
      open: this.state.openMenu,
      onClose: this.onToggleMenu,
      styles: {
        overlay: {
          padding: "0px"
        },
        modal: {
          maxWidth: "1500px",
          width: "25%",
          marginRight: "0%",
          float: "right",
          height: "94%",
          fontSize: "24px",
          animation: "slide 0.5s forwards",
          outline: "none"
        }
      }
    }, __jsx("ul", {
      className: "ulRight"
    }, _constants_constants__WEBPACK_IMPORTED_MODULE_13__["NAVIGATION_ARRAY"].map((navObj, index) => __jsx("li", {
      key: index,
      className: "liMenuSlide"
    }, __jsx("a", {
      className: this.state.currentPage === index ? "menulink activeMenu" : "menulink",
      onClick: e => {
        this.changePage(index);
      }
    }, navObj.title))), __jsx("li", {
      className: "blackColor"
    }, __jsx("a", {
      href: "/about",
      target: "_self"
    }, "Who Are We?"))))), __jsx(_pageScroller__WEBPACK_IMPORTED_MODULE_3__["default"], {
      ref: c => this._pageScroller = c,
      pageOnChange: this.pageOnChange
    }, __jsx(_HomeLanding_homeLanding__WEBPACK_IMPORTED_MODULE_4__["default"], null), __jsx(_WeSolveToughAI_weSolveToughAI__WEBPACK_IMPORTED_MODULE_6__["default"], null), __jsx(_WeMakeCoolAI_weMakeCoolAI__WEBPACK_IMPORTED_MODULE_5__["default"], null), __jsx(_GetFreeAI_getFreeAI__WEBPACK_IMPORTED_MODULE_7__["default"], null), __jsx(_WeDeliverFreeAI_weDeliverFreeAI__WEBPACK_IMPORTED_MODULE_11__["default"], {
      navigation_Array: _constants_constants__WEBPACK_IMPORTED_MODULE_13__["NAVIGATION_ARRAY"],
      changePage: this.changePage,
      homeFooter: true
    }))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (HomePage);

/***/ }),

/***/ "./components/menu_svg.js":
/*!********************************!*\
  !*** ./components/menu_svg.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SVG_svg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SVG/svg */ "./SVG/svg.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const MenuSvg = props => {
  return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx(_SVG_svg__WEBPACK_IMPORTED_MODULE_1__["default"], {
    width: "42px"
  }, __jsx("g", {
    id: "01",
    transform: "translate(-1269.000000, -55.000000)",
    fill: props.navigationColor,
    "fill-rule": "nonzero"
  }, __jsx("g", {
    id: "menu(1)",
    transform: "translate(1269.000000, 55.000000)"
  }, __jsx("path", {
    d: "M38.7500292,0 L1.24997078,0 C0.55994119,0 0,0.44804655 0,1 C0,1.55195345 0.560045095,2 1.24997078,2 L38.7500292,2 C39.4400588,2 40,1.55195345 40,1 C40,0.44804655 39.4400588,0 38.7500292,0 Z",
    id: "Path"
  }), __jsx("path", {
    d: "M38.7727551,9 L14.2272449,9 C13.5497608,9 13,9.44804655 13,10 C13,10.5520366 13.5498628,11 14.2272449,11 L38.7727551,11 C39.4502392,11 40,10.5519534 40,10 C40,9.44796342 39.4502392,9 38.7727551,9 Z",
    id: "Path"
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (MenuSvg);

/***/ }),

/***/ "./components/pageScroller.js":
/*!************************************!*\
  !*** ./components/pageScroller.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ReactPageScroller; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/symbol */ "./node_modules/@babel/runtime-corejs2/core-js/symbol.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);


var __jsx = react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement;




if (!global._babelPolyfill) {
  __webpack_require__(/*! babel-polyfill */ "babel-polyfill");
}

const previousTouchMove = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const scrolling = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const wheelScroll = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const touchMove = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const keyPress = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const onWindowResized = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const addNextComponent = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const scrollWindowUp = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const scrollWindowDown = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const setRenderComponents = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const _isMounted = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const _isBodyScrollEnabled = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const disableScroll = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const enableScroll = _babel_runtime_corejs2_core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default()();

const ANIMATION_TIMER = 200;
const KEY_UP = 38;
const KEY_DOWN = 40;
const DISABLED_CLASS_NAME = "rps-scroll--disabled";
class ReactPageScroller extends react__WEBPACK_IMPORTED_MODULE_2___default.a.Component {
  constructor(props) {
    super(props);

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "componentDidMount", () => {
      this[_isMounted] = true;
      window.addEventListener("resize", this[onWindowResized]);

      document.ontouchmove = event => {
        event.preventDefault();
      };

      this._pageContainer.addEventListener("touchmove", this[touchMove]);

      this._pageContainer.addEventListener("keydown", this[keyPress]);

      let componentsToRenderLength = 0;

      if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(this.props.children[this.state.componentIndex])) {
        componentsToRenderLength++;
      } else {
        componentsToRenderLength++;
      }

      this[addNextComponent](componentsToRenderLength);
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "componentWillUnmount", () => {
      this[_isMounted] = false;
      window.removeEventListener("resize", this[onWindowResized]);

      document.ontouchmove = e => {
        return true;
      };

      this._pageContainer.removeEventListener("touchmove", this[touchMove]);

      this._pageContainer.removeEventListener("keydown", this[keyPress]);
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "goToPage", number => {
      const {
        pageOnChange,
        children
      } = this.props;
      const {
        componentIndex,
        componentsToRenderLength
      } = this.state;
      let newComponentsToRenderLength = componentsToRenderLength;

      if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isEqual(componentIndex, number)) {
        if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(this["container_" + number]) && !this[scrolling]) {
          this[scrolling] = true;
          this._pageContainer.style.transform = `translate3d(0, ${number * -100}%, 0)`;

          if (pageOnChange) {
            pageOnChange(number + 1);
          }

          if (lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(this["container_" + (number + 1)]) && !lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(children[number + 1])) {
            newComponentsToRenderLength++;
          }

          setTimeout(() => {
            this.setState({
              componentIndex: number,
              componentsToRenderLength: newComponentsToRenderLength
            }, () => {
              this[scrolling] = false;
              this[previousTouchMove] = null;
            });
          }, this.props.animationTimer + ANIMATION_TIMER);
        } else if (!this[scrolling] && !lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(children[number])) {
          for (let i = componentsToRenderLength; i <= number; i++) {
            newComponentsToRenderLength++;
          }

          if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(children[number + 1])) {
            newComponentsToRenderLength++;
          }

          this[scrolling] = true;
          this.setState({
            componentsToRenderLength: newComponentsToRenderLength
          }, () => {
            this._pageContainer.style.transform = `translate3d(0, ${number * -100}%, 0)`;

            if (pageOnChange) {
              pageOnChange(number + 1);
            }

            setTimeout(() => {
              this.setState({
                componentIndex: number
              }, () => {
                this[scrolling] = false;
                this[previousTouchMove] = null;
              });
            }, this.props.animationTimer + ANIMATION_TIMER);
          });
        }
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, disableScroll, () => {
      if (this[_isBodyScrollEnabled]) {
        this[_isBodyScrollEnabled] = false;
        window.scrollTo({
          left: 0,
          top: 0,
          behavior: "smooth"
        });
        document.body.classList.add(DISABLED_CLASS_NAME);
        document.documentElement.classList.add(DISABLED_CLASS_NAME);
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, enableScroll, () => {
      if (!this[_isBodyScrollEnabled]) {
        this[_isBodyScrollEnabled] = true;
        document.body.classList.remove(DISABLED_CLASS_NAME);
        document.documentElement.classList.remove(DISABLED_CLASS_NAME);
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, wheelScroll, event => {
      if (event.deltaY < 0) {
        this[scrollWindowUp]();
      } else {
        this[scrollWindowDown]();
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, touchMove, event => {
      if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNull(this[previousTouchMove])) {
        if (event.touches[0].clientY > this[previousTouchMove]) {
          this[scrollWindowUp]();
        } else {
          this[scrollWindowDown]();
        }
      } else {
        this[previousTouchMove] = event.touches[0].clientY;
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, keyPress, event => {
      if (lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isEqual(event.keyCode, KEY_UP)) {
        this[scrollWindowUp]();
      }

      if (lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isEqual(event.keyCode, KEY_DOWN)) {
        this[scrollWindowDown]();
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, onWindowResized, () => {
      this.forceUpdate();
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, addNextComponent, componentsToRenderOnMountLength => {
      let componentsToRenderLength = 0;

      if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(componentsToRenderOnMountLength)) {
        componentsToRenderLength = componentsToRenderOnMountLength;
      }

      componentsToRenderLength = Math.max(componentsToRenderLength, this.state.componentsToRenderLength);

      if (componentsToRenderLength <= this.state.componentIndex + 1) {
        if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(this.props.children[this.state.componentIndex + 1])) {
          componentsToRenderLength++;
        }
      }

      this.setState({
        componentsToRenderLength
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, setRenderComponents, () => {
      const newComponentsToRender = [];

      for (let i = 0; i < this.state.componentsToRenderLength; i++) {
        if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(this.props.children[i])) {
          newComponentsToRender.push(__jsx("div", {
            key: i,
            ref: c => this["container_" + i] = c,
            style: {
              height: "100%",
              width: "100%"
            }
          }, this.props.children[i]));
        } else {
          break;
        }
      }

      return newComponentsToRender;
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, scrollWindowUp, () => {
      if (!this[scrolling] && !this.props.blockScrollUp) {
        if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(this["container_" + (this.state.componentIndex - 1)])) {
          this[disableScroll]();
          this[scrolling] = true;
          this._pageContainer.style.transform = `translate3d(0, ${(this.state.componentIndex - 1) * -100}%, 0)`;

          if (this.props.pageOnChange) {
            this.props.pageOnChange(this.state.componentIndex);
          }

          setTimeout(() => {
            this[_isMounted] && this.setState(prevState => ({
              componentIndex: prevState.componentIndex - 1
            }), () => {
              this[scrolling] = false;
              this[previousTouchMove] = null;
            });
          }, this.props.animationTimer + ANIMATION_TIMER);
        } else {
          this[enableScroll]();

          if (this.props.scrollUnavailable) {
            this.props.scrollUnavailable();
          }
        }
      }
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, scrollWindowDown, () => {
      if (!this[scrolling] && !this.props.blockScrollDown) {
        if (!lodash__WEBPACK_IMPORTED_MODULE_4___default.a.isNil(this["container_" + (this.state.componentIndex + 1)])) {
          this[disableScroll]();
          this[scrolling] = true;
          this._pageContainer.style.transform = `translate3d(0, ${(this.state.componentIndex + 1) * -100}%, 0)`;

          if (this.props.pageOnChange) {
            this.props.pageOnChange(this.state.componentIndex + 2);
          }

          setTimeout(() => {
            this[_isMounted] && this.setState(prevState => ({
              componentIndex: prevState.componentIndex + 1
            }), () => {
              this[scrolling] = false;
              this[previousTouchMove] = null;
              this[addNextComponent]();
            });
          }, this.props.animationTimer + ANIMATION_TIMER);
        } else {
          this[enableScroll]();

          if (this.props.scrollUnavailable) {
            this.props.scrollUnavailable();
          }
        }
      }
    });

    this.state = {
      componentIndex: 0,
      componentsToRenderLength: 0
    };
    this[previousTouchMove] = null;
    this[scrolling] = false;
    this[_isMounted] = false;
    this[_isBodyScrollEnabled] = true;
  }

  render() {
    const {
      animationTimer,
      transitionTimingFunction,
      containerHeight,
      containerWidth
    } = this.props;
    return __jsx("div", {
      style: {
        height: containerHeight,
        width: containerWidth,
        overflow: "hidden"
      }
    }, __jsx("div", {
      ref: c => this._pageContainer = c,
      onWheel: this[wheelScroll],
      style: {
        height: "100%",
        width: "100%",
        transition: `transform ${animationTimer}ms ${transitionTimingFunction}`
      },
      tabIndex: 0
    }, this[setRenderComponents]()));
  }

}

Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(ReactPageScroller, "propTypes", {
  animationTimer: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  transitionTimingFunction: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string,
  pageOnChange: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,
  scrollUnavailable: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,
  containerHeight: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number, prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string]),
  containerWidth: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number, prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string]),
  blockScrollUp: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  blockScrollDown: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool
});

Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(ReactPageScroller, "defaultProps", {
  animationTimer: 1000,
  transitionTimingFunction: "ease-in-out",
  containerHeight: "100vh",
  containerWidth: "100vw",
  blockScrollUp: false,
  blockScrollDown: false
});

/***/ }),

/***/ "./components/scroll_svg.js":
/*!**********************************!*\
  !*** ./components/scroll_svg.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SVG_svg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SVG/svg */ "./SVG/svg.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const ScrollSvg = props => {
  return __jsx(_SVG_svg__WEBPACK_IMPORTED_MODULE_1__["default"], null, __jsx("g", {
    id: "01",
    transform: "translate(-673.000000, -656.000000)"
  }, __jsx("g", {
    id: "Group-8",
    transform: "translate(668.000000, 656.000000)"
  }, __jsx("g", {
    id: "Group-6",
    transform: "translate(5.000000, 0.000000)"
  }, __jsx("rect", {
    id: "Rectangle",
    x: "0",
    y: "0",
    width: "21.0000014",
    height: "31.5000021",
    rx: "10.5000007",
    className: props.rectangleColor
  }), __jsx("circle", {
    id: "Oval",
    cx: "10.5000007",
    cy: "7.70000052",
    r: "2.80000019",
    className: props.circleColor
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (ScrollSvg);

/***/ }),

/***/ "./constants/constants.js":
/*!********************************!*\
  !*** ./constants/constants.js ***!
  \********************************/
/*! exports provided: PROJECTS_ARRAY, WE_SOLVE_TOUGH_AI, FREE_AI_IMG, FOOTER_ICONS, ROTATING_WORDS, NAVIGATION_ARRAY, ABOUT_NAVIGATION_ARRAY, EMAIL_REGEX */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PROJECTS_ARRAY", function() { return PROJECTS_ARRAY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WE_SOLVE_TOUGH_AI", function() { return WE_SOLVE_TOUGH_AI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FREE_AI_IMG", function() { return FREE_AI_IMG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FOOTER_ICONS", function() { return FOOTER_ICONS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROTATING_WORDS", function() { return ROTATING_WORDS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NAVIGATION_ARRAY", function() { return NAVIGATION_ARRAY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ABOUT_NAVIGATION_ARRAY", function() { return ABOUT_NAVIGATION_ARRAY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EMAIL_REGEX", function() { return EMAIL_REGEX; });
const PROJECTS_ARRAY = [{
  projScreen: "../assets/scarlet.png",
  title: "Scarlet",
  url: "/products/scarlet",
  subTitle: "Intelligent Document Processing",
  responsiveNumber: "01/07"
}, {
  projScreen: "../assets/IIOT.png",
  title: "IIOT",
  url: "/products/iiot",
  subTitle: "Secure Remote Access Software",
  responsiveNumber: "02/07"
}, {
  projScreen: "../assets/almanac.png",
  title: "Almanac",
  url: "/products/almanac",
  subTitle: "Smart Project Management Tool",
  responsiveNumber: "03/07"
}, {
  projScreen: "../assets/peakcruit.png",
  title: "PeakCruit",
  url: "/products/peakcruit",
  subTitle: "Smart Recruitment Application",
  responsiveNumber: "04/07"
}, {
  projScreen: "../assets/sharpic.png",
  title: "Sharpic",
  url: "/products/sharpic",
  subTitle: "Image Upscaling Tool",
  responsiveNumber: "05/07"
}, {
  projScreen: "../assets/peakspeak.png",
  title: "PeakSpeak",
  url: "/products/peakspeak",
  subTitle: "Anonymous Chat Application",
  responsiveNumber: "06/07"
}, {
  projScreen: "../assets/lendstash.png",
  title: "LendStash",
  url: "/products/lendstash",
  subTitle: "Secure Rental Platform",
  responsiveNumber: "07/07"
}];
const WE_SOLVE_TOUGH_AI = [{
  img: "../assets/AI_home.svg",
  hoverImage: "../assets/AI_hover.svg",
  title: "Artificial Intelligence",
  url: "artificial-intelligence"
}, {
  img: "../assets/MachineLearning_home.svg",
  hoverImage: "../assets/MachineLearning_hover.svg",
  title: "Machine Learning",
  url: "machine-learning"
}, {
  img: "../assets/DataScience_home.svg",
  hoverImage: "../assets/DataScience_hover.svg",
  title: "Data Science",
  url: "data-science"
}, {
  img: "../assets/ProductDevelopment_home.svg",
  hoverImage: "../assets/ProductDevelopment_hover.svg",
  title: "Product Development",
  url: "product-development"
}, {
  img: "../assets/UXDesign_home.svg",
  hoverImage: "../assets/UXDesign_hover.svg",
  title: "UI & UX Design",
  url: "ui-&-ux-design"
}, {
  img: "../assets/DigitalMarketing_home.svg",
  hoverImage: "../assets/DigitalMarketing_hover.svg",
  title: "Marketing",
  url: "marketing"
}];
const FREE_AI_IMG = [{
  className: "freeAiImages aiBlog",
  url: "/blogs",
  title: "AI Blog",
  titleClass: "TitleAi GetfreeAITitle ",
  subtitleClass: "freeAiTitle GetfreeAITitle ",
  subTitle: "Our research and ramblings",
  label: "AiBlogsImage"
}, {
  className: "freeAiImages caseStudies",
  url: "/portfolio",
  title: "Case studies",
  titleClass: "TitleAi GetfreeAITitle ",
  subtitleClass: "freeAiTitle GetfreeAITitle ",
  subTitle: "This is why you should give us your business",
  label: "AiCaseStudies"
}, {
  className: "freeAiImages eBooks",
  url: "https://medium.com/high-peak-ai",
  title: "E-Books",
  titleClass: "TitleAi GetfreeAITitle ",
  subtitleClass: "freeAiTitle GetfreeAITitle ",
  subTitle: "Our research and ramblings but with more words",
  label: "AiEbooks"
}];
const FOOTER_ICONS = {
  facebook: {
    img: "../assets/facebook.svg",
    navigationURL: "https://www.facebook.com/highpeaksw/",
    label: "facebookIcon"
  },
  twitter: {
    img: "../assets/twitter.svg",
    navigationURL: "https://twitter.com/highpeaksw",
    label: "twitterIcon"
  },
  linkedin: {
    img: "../assets/linkedin.svg",
    navigationURL: "https://www.linkedin.com/company/high-peak-software-inc./",
    label: "linkedinIcon"
  }
};
const ROTATING_WORDS = [{
  weSolveToughAI: "better",
  homePage: "Performance",
  weMakeCoolAI: "intrapreneurs"
}, {
  weSolveToughAI: "smarter",
  homePage: "Precision",
  weMakeCoolAI: "AI innovators"
}, {
  weSolveToughAI: "efficient",
  homePage: "Impact",
  weMakeCoolAI: "AI experts"
}];
const NAVIGATION_ARRAY = [{
  title: "Home",
  logo: true,
  colorImage: "../assets/logo-color.png",
  navigationColor: "#222222",
  classNameOurStuffButton: "topButtons ourStuffHome",
  classNameHitUsUp: "topButtons hitUsUpHome hitUsUpBackground",
  scroll: true,
  stateScrollRectangle: "scrollHome",
  stateScrollCircle: "scrollHomeCircle",
  scrollButton: {
    rectangleColor: "#B2B2B2",
    circleColor: "#ffffff"
  },
  scrollButtonColor: "#B2B2B2",
  autoHideShowHeader: "topNavigation active activeBackground-home"
}, {
  title: "We Solve Tough AI Stuff",
  logo: true,
  colorImage: "../assets/logo-white.png",
  classNameOurStuffButton: "topButtons ourStuffWeSolveToughAI",
  classNameHitUsUp: "topButtons hitUsUpWeSolveToughAI hitUsUpBackground",
  scroll: true,
  stateScrollRectangle: "scrollWeSolveToughAI",
  stateScrollCircle: "scrollWeSolveToughAICircle",
  navigationColor: "#ffffff",
  scrollButton: {
    rectangleColor: "#565656",
    circleColor: "#2a2a2a"
  },
  scrollButtonColor: "#565656",
  autoHideShowHeader: "topNavigation active activeBackground-weSolveToughAI"
}, {
  title: "We Make Cool AI Stuff",
  logo: true,
  colorImage: "../assets/logo-white.png",
  classNameOurStuffButton: "topButtons ourStuffWeMAkeCoolAI",
  classNameHitUsUp: "topButtons hitUsUpWeMakeCoolAI hitUsUpBackground",
  scroll: true,
  stateScrollRectangle: "scrollWeMAkeCoolAI",
  stateScrollCircle: "scrollWeMAkeCoolAICircle",
  navigationColor: "#ffffff",
  scrollButton: {
    rectangleColor: "#7169cc",
    circleColor: "#2f2f8a"
  },
  scrollButtonColor: "#7169cc",
  autoHideShowHeader: "topNavigation active activeBackground-weMakeCoolAI"
}, {
  title: "Get Free AI Stuff",
  logo: true,
  colorImage: "../assets/logo-color.png",
  classNameOurStuffButton: "topButtons ourStuffGetFreeAI",
  classNameHitUsUp: "topButtons hitUsUpFreeAI hitUsUpBackground",
  scroll: true,
  stateScrollRectangle: "scrollFreeAI",
  stateScrollCircle: "scrollFreeAICircle",
  navigationColor: "#222222",
  scrollButton: {
    rectangleColor: "#B2B2B2",
    circleColor: "#2a2a2a"
  },
  scrollButtonColor: "B2B2B2",
  autoHideShowHeader: "topNavigation active activeBackground-getFreeAI"
}, {
  title: "Drop In!",
  logo: true,
  colorImage: "../assets/logo-color.png",
  classNameOurStuffButton: "topButtons ourStuffGetFreeAI",
  classNameHitUsUp: "topButtons hitUsUpFreeAI hitUsUpBackground",
  scroll: false,
  stateScrollRectangle: "scrollFooter",
  stateScrollCircle: "scrollFooterCircle",
  navigationColor: "#222222",
  scrollButton: {
    rectangleColor: "antiquewhite",
    circleColor: "antiquewhite"
  },
  scrollButtonColor: "antiquewhite",
  autoHideShowHeader: "topNavigation active activeBackground-weDeliverFreeAI"
}, {
  title: "",
  logo: true,
  colorImage: "../assets/logo-color.png",
  classNameOurStuffButton: "topButtonsAbout ourStuffGetFreeAI",
  classNameHitUsUp: "topButtonsAbout hitUsUpFreeAI hitUsUpBackground",
  scroll: false,
  stateScrollRectangle: "scrollFooter",
  stateScrollCircle: "scrollFooterCircle",
  navigationColor: "#222222",
  scrollButton: {
    rectangleColor: "antiquewhite",
    circleColor: "antiquewhite"
  },
  scrollButtonColor: "antiquewhite",
  autoHideShowHeader: "topNavigation active activeBackground-weDeliverFreeAI"
}];
const ABOUT_NAVIGATION_ARRAY = [{
  title: "Who we are ?",
  hashurl: "#we",
  logo: true,
  colorImage: "../assets/logo-color.png",
  navigationColor: "#222222",
  ourStuffClassName: "topButtonsAbout ourStuffButtonHome fontContainer",
  hitUsUpClassName: "topButtonsAbout hitUsUpHome dynamichitUsUp fontContainer",
  scroll: true,
  stateScrollRectangle: "scrollHome",
  stateScrollCircle: "scrollHomeCircle",
  scrollButton: {
    rectangleColor: "#B2B2B2",
    circleColor: "#ffffff"
  },
  scrollButtonColor: "black",
  autoHideShowHeader: "topNavigation active activeBackground-Who_Are_We"
}, {
  title: "We are your partner",
  hashurl: "#partner",
  logo: true,
  colorImage: "../assets/logo-white.png",
  navigationColor: "#FFFFFF",
  ourStuffClassName: "topButtonsAbout ourStuffButtonWeArePartners fontContainer",
  hitUsUpClassName: "topButtonsAbout hitUsUpWestay dynamichitUsUp fontContainer",
  scroll: true,
  stateScrollRectangle: "scrollHome",
  stateScrollCircle: "scrollHomeCircle",
  scrollButton: {
    rectangleColor: "#B2B2B2",
    circleColor: "#ffffff"
  },
  scrollButtonColor: "white",
  autoHideShowHeader: "topNavigation active activeBackground-we_Are_Partner"
}, {
  title: "Our Resident ?",
  hashurl: "#resident",
  logo: true,
  colorImage: "../assets/logo-color.png",
  navigationColor: "#222222",
  ourStuffClassName: "topButtonsAbout ourStuffButtonHome fontContainer",
  hitUsUpClassName: "topButtonsAbout hitUsUpHome dynamichitUsUp fontContainer",
  scroll: true,
  stateScrollRectangle: "scrollHome",
  stateScrollCircle: "scrollHomeCircle",
  scrollButton: {
    rectangleColor: "#B2B2B2",
    circleColor: "#ffffff"
  },
  scrollButtonColor: "black",
  autoHideShowHeader: "topNavigation active activeBackground-Our_Resident"
}, {
  title: "Drop In!",
  hashurl: "#dropIn",
  logo: true,
  colorImage: "../assets/logo-color.png",
  navigationColor: "#222222",
  ourStuffClassName: "topButtonsAbout ourStuffButtonHome fontContainer",
  hitUsUpClassName: "topButtonsAbout hitUsUpHome dynamichitUsUp fontContainer",
  scroll: false,
  stateScrollRectangle: "scrollHome",
  stateScrollCircle: "scrollHomeCircle",
  scrollButton: {
    rectangleColor: "#B2B2B2",
    circleColor: "#ffffff"
  },
  scrollButtonColor: "black",
  autoHideShowHeader: "topNavigation active activeBackground-Our_Resident"
}, {
  title: "",
  hashurl: "",
  logo: false,
  colorImage: "",
  ourStuffClassName: "",
  hitUsUpClassName: "",
  scroll: false,
  stateScrollRectangle: "",
  stateScrollCircle: "",
  navigationColor: "",
  stateScroll: {
    rectangleColor: "",
    circleColor: ""
  },
  scrollColor: "",
  autoHideShowHeader: ""
}];
const EMAIL_REGEX = {
  regexObject: /[\w-]+@([\w-]+\.)+[\w-]+/i
};

/***/ }),

/***/ "./constants/googleAnalyticsConstants.js":
/*!***********************************************!*\
  !*** ./constants/googleAnalyticsConstants.js ***!
  \***********************************************/
/*! exports provided: GOOGLE_ANALYTICS_CONSTANTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GOOGLE_ANALYTICS_CONSTANTS", function() { return GOOGLE_ANALYTICS_CONSTANTS; });
const GOOGLE_ANALYTICS_CONSTANTS = {
  hitUsUp: {
    event: "HITUSUP_SUBMIT",
    page: "HITUSUP",
    details: "HITUSUP_PAGE"
  },
  demo: {
    event: "REQUEST FOR DEMO",
    page: "DEMO_SUBMIT",
    details: "DEMO_PAGE"
  },
  joinUs: {
    event: "JOINUS_SUBMIT",
    page: "UPLOAD RESUME",
    details: "JOINUS_PAGE"
  },
  home: {
    0: {
      event: "HOME_PAGE_SCROLL",
      page: "HOMELANDING",
      details: "HOME_PAGE"
    },
    1: {
      event: "HOME_PAGE_SCROLL",
      page: "WE_SOLVE_TOUGH_AI",
      details: "HOME_PAGE"
    },
    2: {
      event: "HOME_PAGE_SCROLL",
      page: "WE_MAKE_COOL_AI",
      details: "HOME_PAGE"
    },
    3: {
      event: "HOME_PAGE_SCROLL",
      page: "GET_FREE_AI",
      details: "HOME_PAGE"
    },
    4: {
      event: "HOME_PAGE_SCROLL",
      page: "DROP_IN",
      details: "HOME_PAGE"
    }
  },
  aboutUs: {
    0: {
      event: "ABOUT_PAGE_SCROLL",
      page: "WHO_ARE_WE",
      details: "ABOUT_PAGE"
    },
    1: {
      event: "ABOUT_PAGE_SCROLL",
      page: "WE_ARE_PARTNERS",
      details: "ABOUT_PAGE"
    },
    2: {
      event: "ABOUT_PAGE_SCROLL",
      page: "OUR_RESIDENT",
      details: "ABOUT_PAGE"
    },
    3: {
      event: "ABOUT_PAGE_SCROLL",
      page: "DROP_IN",
      details: "ABOUT_PAGE"
    }
  }
};

/***/ }),

/***/ "./constants/gtmScripts.js":
/*!*********************************!*\
  !*** ./constants/gtmScripts.js ***!
  \*********************************/
/*! exports provided: GTM_SCRIPT_TAG, GTM_NO_SCRIPT, GA_SCRIPT, GA_WINDOW */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GTM_SCRIPT_TAG", function() { return GTM_SCRIPT_TAG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GTM_NO_SCRIPT", function() { return GTM_NO_SCRIPT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GA_SCRIPT", function() { return GA_SCRIPT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GA_WINDOW", function() { return GA_WINDOW; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_analytics__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/analytics */ "./utils/analytics.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

const GTM_SCRIPT_TAG = {
  tag: __jsx("script", null, function (w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
      "gtm.start": new Date().getTime(),
      event: "gtm.js"
    });
    var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != "dataLayer" ? "&l=" + l : "";
    j.async = true;
    j.src = "https://www.googletagmanager.com/gtm.js?id=" + i + dl;
    f.parentNode.insertBefore(j, f);
  }, "(window,document,'script','dataLayer','GTM-PM5VGBL');")
};
const GTM_NO_SCRIPT = () => {
  return __jsx("noscript", null, __jsx("iframe", {
    src: "https://www.googletagmanager.com/ns.html?id=GTM-PM5VGBL",
    height: "0",
    width: "0",
    style: {
      display: "none",
      visibility: "hidden"
    }
  }));
};
const GA_SCRIPT = () => {
  const script = document.createElement("script");
  script.src = "//js.hs-scripts.com/7251170.js";
  script.id = "hs-script-loader";
  script.type = "text/javascript";
  script.async = true;
  document.body.appendChild(script);
  const iframescript = document.createElement("iframe");
  iframescript.src = "https://www.googletagmanager.com/ns.html?id=GTM-PM5VGBL";
  iframescript.height = "0";
  iframescript.width = "0";
  iframescript.style.visibility = "hidden";
  iframescript.style.display = "none";
};
const GA_WINDOW = () => {
  if (!window.GA_INITIALIZED) {
    Object(_utils_analytics__WEBPACK_IMPORTED_MODULE_1__["initGA"])();
    window.GA_INITIALIZED = true;
  }

  Object(_utils_analytics__WEBPACK_IMPORTED_MODULE_1__["logPageView"])();
};

/***/ }),

/***/ "./constants/hitUsUpNewConstants.js":
/*!******************************************!*\
  !*** ./constants/hitUsUpNewConstants.js ***!
  \******************************************/
/*! exports provided: SEND_DATA_TO_SERVER, sendData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SEND_DATA_TO_SERVER", function() { return SEND_DATA_TO_SERVER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendData", function() { return sendData; });
const SEND_DATA_TO_SERVER = "SEND_DATA_TO_SERVER";
const sendData = data => {
  return {
    type: SEND_DATA_TO_SERVER,
    payload: data
  };
};

/***/ }),

/***/ "./constants/uploadFileAction.js":
/*!***************************************!*\
  !*** ./constants/uploadFileAction.js ***!
  \***************************************/
/*! exports provided: UPLOAD_FILE_DATA, uploadData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPLOAD_FILE_DATA", function() { return UPLOAD_FILE_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadData", function() { return uploadData; });
const UPLOAD_FILE_DATA = "UPLOAD_FILE_DATA";
const uploadData = data => {
  return {
    type: UPLOAD_FILE_DATA,
    payload: data
  };
};

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/map.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/map.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/map */ "core-js/library/fn/map");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/assign.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/assign */ "core-js/library/fn/object/assign");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/create.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/create.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/create */ "core-js/library/fn/object/create");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "core-js/library/fn/object/define-property");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js":
/*!*******************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-descriptor */ "core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js":
/*!********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/keys */ "core-js/library/fn/object/keys");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/values.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/values.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/values */ "core-js/library/fn/object/values");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/promise.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/promise.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/promise */ "core-js/library/fn/promise");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/symbol.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/symbol.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/symbol */ "core-js/library/fn/symbol");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/weak-map.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/weak-map.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/weak-map */ "core-js/library/fn/weak-map");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/extends.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/extends.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$assign = __webpack_require__(/*! ../core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

function _extends() {
  module.exports = _extends = _Object$assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$getOwnPropertyDescriptor = __webpack_require__(/*! ../core-js/object/get-own-property-descriptor */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js");

var _Object$defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var _WeakMap = __webpack_require__(/*! ../core-js/weak-map */ "./node_modules/@babel/runtime-corejs2/core-js/weak-map.js");

function _getRequireWildcardCache() {
  if (typeof _WeakMap !== "function") return null;
  var cache = new _WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};

  if (obj != null) {
    var hasPropertyDescriptor = _Object$defineProperty && _Object$getOwnPropertyDescriptor;

    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? _Object$getOwnPropertyDescriptor(obj, key) : null;

        if (desc && (desc.get || desc.set)) {
          _Object$defineProperty(newObj, key, desc);
        } else {
          newObj[key] = obj[key];
        }
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "./node_modules/next/dist/client/link.js":
/*!***********************************************!*\
  !*** ./node_modules/next/dist/client/link.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = void 0;

var _map = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/map */ "./node_modules/@babel/runtime-corejs2/core-js/map.js"));

var _url = __webpack_require__(/*! url */ "url");

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "react"));

var _propTypes = _interopRequireDefault(__webpack_require__(/*! prop-types */ "prop-types"));

var _router = _interopRequireDefault(__webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js"));

var _rewriteUrlForExport = __webpack_require__(/*! ../next-server/lib/router/rewrite-url-for-export */ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js");

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "./node_modules/next/dist/next-server/lib/utils.js");
/* global __NEXT_DATA__ */


function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new _map.default();
var IntersectionObserver = false ? undefined : null;

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: formatUrl(href),
        as: asHref ? formatUrl(asHref) : asHref
      };
    });

    this.linkClicked = e => {
      // @ts-ignore target exists on currentTarget
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (true) {
      if (props.prefetch) {
        console.warn('Next.js auto-prefetches automatically based on viewport. The prefetch attribute is no longer needed. More: https://err.sh/zeit/next.js/prefetch-true-deprecated');
      }
    }

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      this.cleanUpListeners = listenToIntersections(ref, () => {
        this.prefetch();
      });
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch() {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var {
      pathname
    } = window.location;
    var {
      href: parsedHref
    } = this.formatUrls(this.props.href, this.props.as);
    var href = (0, _url.resolve)(pathname, parsedHref);

    _router.default.prefetch(href);
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch();
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) {}

    return _react.default.cloneElement(child, props);
  }

}

Link.propTypes = void 0;

if (true) {
  var warn = (0, _utils.execOnce)(console.error); // This module gets removed by webpack.IgnorePlugin

  var exact = __webpack_require__(/*! prop-types-exact */ "prop-types-exact");

  Link.propTypes = exact({
    href: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]).isRequired,
    as: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
    prefetch: _propTypes.default.bool,
    replace: _propTypes.default.bool,
    shallow: _propTypes.default.bool,
    passHref: _propTypes.default.bool,
    scroll: _propTypes.default.bool,
    children: _propTypes.default.oneOfType([_propTypes.default.element, (props, propName) => {
      var value = props[propName];

      if (typeof value === 'string') {
        warn("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  });
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "./node_modules/next/dist/client/router.js":
/*!*************************************************!*\
  !*** ./node_modules/next/dist/client/router.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _defineProperty = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router2 = _interopRequireWildcard(__webpack_require__(/*! ../next-server/lib/router/router */ "./node_modules/next/dist/next-server/lib/router/router.js"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(/*! ../next-server/lib/router-context */ "./node_modules/next/dist/next-server/lib/router-context.js");

exports.RouterContext = _routerContext.RouterContext;

var _withRouter = _interopRequireDefault(__webpack_require__(/*! ./with-router */ "./node_modules/next/dist/client/with-router.js"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

(0, _defineProperty.default)(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  (0, _defineProperty.default)(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = (0, _extends2.default)({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "./node_modules/next/dist/client/with-router.js":
/*!******************************************************!*\
  !*** ./node_modules/next/dist/client/with-router.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = withRouter;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router = __webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js");

function withRouter(ComposedComponent) {
  class WithRouteWrapper extends _react.default.Component {
    constructor() {
      super(...arguments);
      this.context = void 0;
    }

    render() {
      return _react.default.createElement(ComposedComponent, (0, _extends2.default)({
        router: this.context
      }, this.props));
    }

  }

  WithRouteWrapper.displayName = void 0;
  WithRouteWrapper.getInitialProps = void 0;
  WithRouteWrapper.contextType = _router.RouterContext;
  WithRouteWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouteWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (true) {
    var name = ComposedComponent.displayName || ComposedComponent.name || 'Unknown';
    WithRouteWrapper.displayName = "withRouter(" + name + ")";
  }

  return WithRouteWrapper;
}

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/mitt.js":
/*!********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/mitt.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

var _Object$create = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/create */ "./node_modules/@babel/runtime-corejs2/core-js/object/create.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = _Object$create(null);

  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router-context.js":
/*!******************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router-context.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  result["default"] = mod;
  return result;
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const React = __importStar(__webpack_require__(/*! react */ "react"));

exports.RouterContext = React.createContext(null);

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function rewriteUrlForNextExport(url) {
  const [pathname, hash] = url.split('#'); // tslint:disable-next-line

  let [path, qs] = pathname.split('?');
  path = path.replace(/\/$/, ''); // Append a trailing slash if this path does not have an extension

  if (!/\.[^/]+\/?$/.test(path)) path += `/`;
  if (qs) path += '?' + qs;
  if (hash) path += '#' + hash;
  return path;
}

exports.rewriteUrlForNextExport = rewriteUrlForNextExport;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/router.js":
/*!*****************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/router.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Promise = __webpack_require__(/*! @babel/runtime-corejs2/core-js/promise */ "./node_modules/@babel/runtime-corejs2/core-js/promise.js");

var _Object$assign = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");

const mitt_1 = __importDefault(__webpack_require__(/*! ../mitt */ "./node_modules/next/dist/next-server/lib/mitt.js"));

const utils_1 = __webpack_require__(/*! ../utils */ "./node_modules/next/dist/next-server/lib/utils.js");

const rewrite_url_for_export_1 = __webpack_require__(/*! ./rewrite-url-for-export */ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js");

const route_matcher_1 = __webpack_require__(/*! ./utils/route-matcher */ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js");

const route_regex_1 = __webpack_require__(/*! ./utils/route-regex */ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js");

const is_dynamic_1 = __webpack_require__(/*! ./utils/is-dynamic */ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js");

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription
  }) {
    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state.options && e.state.options.fromExternal) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (true) {
        if (typeof url === 'undefined' || typeof as === 'undefined') {
          console.warn('`popstate` event triggered but `event.state` did not have `url` or `as` https://err.sh/zeit/next.js/popstate-state-empty');
        }
      }

      this.replace(url, as, options);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented
    // @ts-ignore backwards compatibility

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.nextExport ? pathname : as;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    return rewrite_url_for_export_1.rewriteUrlForNextExport(url);
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = _Object$assign({}, data, {
      Component
    });

    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new _Promise((resolve, reject) => {
      // marking route changes as a navigation start entry
      if (utils_1.SUPPORTS_PERFORMANCE_USER_TIMING) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      const url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as; // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      } // @ts-ignore pathname is always a string


      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const rr = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(rr)(asPathname);

        if (!routeMatch) {
          const error = 'The provided `as` value is incompatible with the `href` value. This is invalid. https://err.sh/zeit/next.js/incompatible-href-as';

          if (true) {
            throw new Error(error);
          } else {}

          return resolve(false);
        } // Merge params into `query`, overwriting any specified in search


        _Object$assign(query, routeMatch);
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result
      // @ts-ignore pathname is always a string

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);
        const hash = window.location.hash.substring(1);

        if (true) {
          const appComp = this.components['/_app'].Component;
          window.next.isPrerendered = appComp.getInitialProps === appComp.origGetInitialProps && !routeInfo.Component.getInitialProps;
        } // @ts-ignore pathname is always defined


        this.set(route, pathname, query, as, _Object$assign({}, routeInfo, {
          hash
        }));

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (true) {
      if (typeof window.history === 'undefined') {
        console.error(`Warning: window.history is not available.`);
        return;
      } // @ts-ignore method should always exist on history


      if (typeof window.history[method] === 'undefined') {
        console.error(`Warning: window.history.${method} is not available`);
        return;
      }
    }

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      // @ts-ignore method should always exist on history
      window.history[method]({
        url,
        as,
        options
      }, null, as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return _Promise.resolve(cachedRouteInfo);
    }

    return new _Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(Component => resolve({
        Component
      }), reject);
    }).then(routeInfo => {
      const {
        Component
      } = routeInfo;

      if (true) {
        const {
          isValidElementType
        } = __webpack_require__(/*! react-is */ "./node_modules/next/node_modules/react-is/index.js");

        if (!isValidElementType(Component)) {
          throw new Error(`The default export is not a React Component in page: "${pathname}"`);
        }
      }

      return new _Promise((resolve, reject) => {
        // we provide AppTree later so this needs to be `any`
        this.getInitialProps(Component, {
          pathname,
          query,
          asPath: as
        }).then(props => {
          routeInfo.props = props;
          this.components[route] = routeInfo;
          resolve(routeInfo);
        }, reject);
      });
    }).catch(err => {
      return new _Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR') {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(Component => {
          const routeInfo = {
            Component,
            err
          };
          return new _Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }));
      });
    });
  }

  set(route, pathname, query, as, data) {
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch `page` code, you may wait for the data during `page` rendering.
   * This feature only works in production!
   * @param url of prefetched `page`
   */


  prefetch(url) {
    return new _Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (true) return; // @ts-ignore pathname is always defined

      const route = toRoute(pathname);
      this.pageLoader.prefetch(route).then(resolve, reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    const Component = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return Component;
  }

  async getInitialProps(Component, ctx) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    const {
      Component: App
    } = this.components['/_app'];
    let props;

    if ( // @ts-ignore workaround for dead-code elimination
    (self.__HAS_SPR || "development" !== 'production') && Component.__NEXT_SPR) {
      let status; // pathname should have leading slash

      let {
        pathname
      } = url_1.parse(ctx.asPath || ctx.pathname);
      pathname = !pathname || pathname === '/' ? '/index' : pathname;
      props = await fetch( // @ts-ignore __NEXT_DATA__
      `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`).then(res => {
        if (!res.ok) {
          status = res.status;
          throw new Error('failed to load prerender data');
        }

        return res.json();
      }).catch(err => {
        console.error(`Failed to load data`, status, err);
        window.location.href = pathname;
        return new _Promise(() => {});
      });
    } else {
      const AppTree = this._wrapApp(App);

      ctx.AppTree = AppTree;
      props = await utils_1.loadGetInitialProps(App, {
        AppTree,
        Component,
        router: this,
        ctx
      });
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    if (cancelled) {
      const err = new Error('Loading initial props cancelled');
      err.cancelled = true;
      throw err;
    }

    return props;
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

Router.events = mitt_1.default();
exports.default = Router;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js":
/*!***************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string


const TEST_ROUTE = /\/\[[^\/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js":
/*!******************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$keys = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const params = {};

    _Object$keys(groups).forEach(slugName => {
      const m = routeMatch[groups[slugName]];

      if (m !== undefined) {
        params[slugName] = decodeURIComponent(m);
      }
    });

    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js":
/*!****************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-regex.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^\/]+?)\\\](?=\/|$)/g, (_, $1) => (groups[$1 // Un-escape key
  .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1')] = groupIndex++, '/([^/]+?)'));
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/utils.js":
/*!*********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/utils.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$keys = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  return (...args) => {
    if (!used) {
      used = true;
      fn.apply(this, args);
    }
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(Component, ctx) {
  if (true) {
    if (Component.prototype && Component.prototype.getInitialProps) {
      const message = `"${getDisplayName(Component)}.getInitialProps()" is defined as an instance method - visit https://err.sh/zeit/next.js/get-initial-props-as-an-instance-method for more information.`;
      throw new Error(message);
    }
  } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!Component.getInitialProps) {
    return {};
  }

  const props = await Component.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(Component)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (true) {
    if (_Object$keys(props).length === 0 && !ctx.ctx) {
      console.warn(`${getDisplayName(Component)} returned an empty object from \`getInitialProps\`. This de-optimizes and prevents automatic static optimization. https://err.sh/zeit/next.js/empty-object-getInitialProps`);
    }
  }

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (true) {
    if (url !== null && typeof url === 'object') {
      _Object$keys(url).forEach(key => {
        if (exports.urlObjectKeys.indexOf(key) === -1) {
          console.warn(`Unknown key passed via urlObject into url.format: ${key}`);
        }
      });
    }
  }

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SUPPORTS_PERFORMANCE = typeof performance !== 'undefined';
exports.SUPPORTS_PERFORMANCE_USER_TIMING = exports.SUPPORTS_PERFORMANCE && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "./node_modules/next/link.js":
/*!***********************************!*\
  !*** ./node_modules/next/link.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/client/link */ "./node_modules/next/dist/client/link.js")


/***/ }),

/***/ "./node_modules/next/node_modules/react-is/cjs/react-is.development.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/next/node_modules/react-is/cjs/react-is.development.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.8.6
 * react-is.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */





if (true) {
  (function() {
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var hasSymbol = typeof Symbol === 'function' && Symbol.for;

var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace;
var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;

function isValidElementType(type) {
  return typeof type === 'string' || typeof type === 'function' ||
  // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
  type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE);
}

/**
 * Forked from fbjs/warning:
 * https://github.com/facebook/fbjs/blob/e66ba20ad5be433eb54423f2b097d829324d9de6/packages/fbjs/src/__forks__/warning.js
 *
 * Only change is we use console.warn instead of console.error,
 * and do nothing when 'console' is not supported.
 * This really simplifies the code.
 * ---
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var lowPriorityWarning = function () {};

{
  var printWarning = function (format) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var argIndex = 0;
    var message = 'Warning: ' + format.replace(/%s/g, function () {
      return args[argIndex++];
    });
    if (typeof console !== 'undefined') {
      console.warn(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };

  lowPriorityWarning = function (condition, format) {
    if (format === undefined) {
      throw new Error('`lowPriorityWarning(condition, format, ...args)` requires a warning ' + 'message argument');
    }
    if (!condition) {
      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        args[_key2 - 2] = arguments[_key2];
      }

      printWarning.apply(undefined, [format].concat(args));
    }
  };
}

var lowPriorityWarning$1 = lowPriorityWarning;

function typeOf(object) {
  if (typeof object === 'object' && object !== null) {
    var $$typeof = object.$$typeof;
    switch ($$typeof) {
      case REACT_ELEMENT_TYPE:
        var type = object.type;

        switch (type) {
          case REACT_ASYNC_MODE_TYPE:
          case REACT_CONCURRENT_MODE_TYPE:
          case REACT_FRAGMENT_TYPE:
          case REACT_PROFILER_TYPE:
          case REACT_STRICT_MODE_TYPE:
          case REACT_SUSPENSE_TYPE:
            return type;
          default:
            var $$typeofType = type && type.$$typeof;

            switch ($$typeofType) {
              case REACT_CONTEXT_TYPE:
              case REACT_FORWARD_REF_TYPE:
              case REACT_PROVIDER_TYPE:
                return $$typeofType;
              default:
                return $$typeof;
            }
        }
      case REACT_LAZY_TYPE:
      case REACT_MEMO_TYPE:
      case REACT_PORTAL_TYPE:
        return $$typeof;
    }
  }

  return undefined;
}

// AsyncMode is deprecated along with isAsyncMode
var AsyncMode = REACT_ASYNC_MODE_TYPE;
var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
var ContextConsumer = REACT_CONTEXT_TYPE;
var ContextProvider = REACT_PROVIDER_TYPE;
var Element = REACT_ELEMENT_TYPE;
var ForwardRef = REACT_FORWARD_REF_TYPE;
var Fragment = REACT_FRAGMENT_TYPE;
var Lazy = REACT_LAZY_TYPE;
var Memo = REACT_MEMO_TYPE;
var Portal = REACT_PORTAL_TYPE;
var Profiler = REACT_PROFILER_TYPE;
var StrictMode = REACT_STRICT_MODE_TYPE;
var Suspense = REACT_SUSPENSE_TYPE;

var hasWarnedAboutDeprecatedIsAsyncMode = false;

// AsyncMode should be deprecated
function isAsyncMode(object) {
  {
    if (!hasWarnedAboutDeprecatedIsAsyncMode) {
      hasWarnedAboutDeprecatedIsAsyncMode = true;
      lowPriorityWarning$1(false, 'The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
    }
  }
  return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
}
function isConcurrentMode(object) {
  return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
}
function isContextConsumer(object) {
  return typeOf(object) === REACT_CONTEXT_TYPE;
}
function isContextProvider(object) {
  return typeOf(object) === REACT_PROVIDER_TYPE;
}
function isElement(object) {
  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
}
function isForwardRef(object) {
  return typeOf(object) === REACT_FORWARD_REF_TYPE;
}
function isFragment(object) {
  return typeOf(object) === REACT_FRAGMENT_TYPE;
}
function isLazy(object) {
  return typeOf(object) === REACT_LAZY_TYPE;
}
function isMemo(object) {
  return typeOf(object) === REACT_MEMO_TYPE;
}
function isPortal(object) {
  return typeOf(object) === REACT_PORTAL_TYPE;
}
function isProfiler(object) {
  return typeOf(object) === REACT_PROFILER_TYPE;
}
function isStrictMode(object) {
  return typeOf(object) === REACT_STRICT_MODE_TYPE;
}
function isSuspense(object) {
  return typeOf(object) === REACT_SUSPENSE_TYPE;
}

exports.typeOf = typeOf;
exports.AsyncMode = AsyncMode;
exports.ConcurrentMode = ConcurrentMode;
exports.ContextConsumer = ContextConsumer;
exports.ContextProvider = ContextProvider;
exports.Element = Element;
exports.ForwardRef = ForwardRef;
exports.Fragment = Fragment;
exports.Lazy = Lazy;
exports.Memo = Memo;
exports.Portal = Portal;
exports.Profiler = Profiler;
exports.StrictMode = StrictMode;
exports.Suspense = Suspense;
exports.isValidElementType = isValidElementType;
exports.isAsyncMode = isAsyncMode;
exports.isConcurrentMode = isConcurrentMode;
exports.isContextConsumer = isContextConsumer;
exports.isContextProvider = isContextProvider;
exports.isElement = isElement;
exports.isForwardRef = isForwardRef;
exports.isFragment = isFragment;
exports.isLazy = isLazy;
exports.isMemo = isMemo;
exports.isPortal = isPortal;
exports.isProfiler = isProfiler;
exports.isStrictMode = isStrictMode;
exports.isSuspense = isSuspense;
  })();
}


/***/ }),

/***/ "./node_modules/next/node_modules/react-is/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/next/node_modules/react-is/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (false) {} else {
  module.exports = __webpack_require__(/*! ./cjs/react-is.development.js */ "./node_modules/next/node_modules/react-is/cjs/react-is.development.js");
}


/***/ }),

/***/ "./node_modules/react-awesome-slider/dist/styles.css":
/*!***********************************************************!*\
  !*** ./node_modules/react-awesome-slider/dist/styles.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./pages/home.js":
/*!***********************!*\
  !*** ./pages/home.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FullPage; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_homePageNormalScroll_homePageScroll__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/homePageNormalScroll/homePageScroll */ "./components/homePageNormalScroll/homePageScroll.js");
/* harmony import */ var _components_homePages_homePage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/homePages/homePage */ "./components/homePages/homePage.js");
/* harmony import */ var _constants_gtmScripts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../constants/gtmScripts */ "./constants/gtmScripts.js");
/* harmony import */ var _styles_index_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../styles/index.css */ "./styles/index.css");
/* harmony import */ var _styles_index_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_index_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _styles_homePage_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../styles/homePage.css */ "./styles/homePage.css");
/* harmony import */ var _styles_homePage_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_styles_homePage_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _styles_dynamiccls_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../styles/dynamiccls.css */ "./styles/dynamiccls.css");
/* harmony import */ var _styles_dynamiccls_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_styles_dynamiccls_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _styles_animations_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../styles/animations.css */ "./styles/animations.css");
/* harmony import */ var _styles_animations_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_styles_animations_css__WEBPACK_IMPORTED_MODULE_9__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;









class FullPage extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {
  constructor() {
    super();

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "updateWindowDimensions", () => {
      this.setState({
        Responsivewidth: window.innerWidth
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "componentDidMount", () => {
      Object(_constants_gtmScripts__WEBPACK_IMPORTED_MODULE_5__["GA_SCRIPT"])();
      this.updateWindowDimensions();
      window.addEventListener("resize", this.updateWindowDimensions);
      Object(_constants_gtmScripts__WEBPACK_IMPORTED_MODULE_5__["GA_WINDOW"])();
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "componentWillUnmount", () => {
      window.removeEventListener("resize", this.updateWindowDimensions);
    });

    this.state = {
      Responsivewidth: 0
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind();
  }

  static getInitialProps({
    query
  }) {
    return {
      query
    };
  }

  render() {
    return __jsx("div", null, __jsx(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, null, _constants_gtmScripts__WEBPACK_IMPORTED_MODULE_5__["GTM_SCRIPT_TAG"].tag, __jsx("link", {
      href: "https://fonts.googleapis.com/css?family=Montserrat",
      rel: "stylesheet"
    }), __jsx("meta", {
      name: "viewport",
      content: "width=device-width, initial-scale=1.0"
    }), __jsx("meta", {
      charSet: "utf-8"
    }), __jsx("meta", {
      name: "viewport",
      content: "initial-scale=1, width=device-width, height=device-height, viewport-fit=cover"
    }), __jsx("title", null, "High Peak Software - Home"), __jsx("meta", {
      name: "description",
      content: "High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
    }), __jsx("meta", {
      name: "keywords",
      content: "High Peak Software, High Peak, AI solutions provider, Product outsourcing, ML solutions, AI products, Bangalore AI services provider, Data science solutions provider, Deep learning services provider, IT software outsourcing"
    }), __jsx("meta", {
      property: "og:title",
      content: "High Peak Software - Home"
    }), __jsx("meta", {
      property: "og:site_name",
      content: "High Peak Software"
    }), __jsx("meta", {
      property: "og:description",
      content: "High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
    }), __jsx("meta", {
      property: "og:url",
      content: "https://highpeaksw.com"
    }), __jsx("meta", {
      property: "og:image",
      content: "../assets/logo-color.png"
    })), __jsx(_constants_gtmScripts__WEBPACK_IMPORTED_MODULE_5__["GTM_NO_SCRIPT"], null), this.state.Responsivewidth > 1000 && this.state.Responsivewidth !== 0 && __jsx("div", null, __jsx(_components_homePages_homePage__WEBPACK_IMPORTED_MODULE_4__["default"], {
      querysection: this.props.query != undefined ? this.props.query.section : 0
    })), this.state.Responsivewidth < 1000 && this.state.Responsivewidth !== 0 && __jsx("div", null, __jsx(_components_homePageNormalScroll_homePageScroll__WEBPACK_IMPORTED_MODULE_3__["default"], {
      querysection: this.props.query != undefined ? this.props.query.section : 0
    })));
  }

}

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home */ "./pages/home.js");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const IndexPage = () => {
  return __jsx(_home__WEBPACK_IMPORTED_MODULE_1__["default"], null);
};

/* harmony default export */ __webpack_exports__["default"] = (IndexPage);

/***/ }),

/***/ "./styles/about.css":
/*!**************************!*\
  !*** ./styles/about.css ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/animations.css":
/*!*******************************!*\
  !*** ./styles/animations.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/dynamiccls.css":
/*!*******************************!*\
  !*** ./styles/dynamiccls.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/homePage.css":
/*!*****************************!*\
  !*** ./styles/homePage.css ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/index.css":
/*!**************************!*\
  !*** ./styles/index.css ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./utils/analytics.js":
/*!****************************!*\
  !*** ./utils/analytics.js ***!
  \****************************/
/*! exports provided: initGA, logPageView, logEvent, logException, Event */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initGA", function() { return initGA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logPageView", function() { return logPageView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logEvent", function() { return logEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logException", function() { return logException; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Event", function() { return Event; });
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-ga */ "react-ga");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_0__);

const initGA = () => {
  react_ga__WEBPACK_IMPORTED_MODULE_0___default.a.initialize("UA-128048260-1");
};
const logPageView = () => {
  react_ga__WEBPACK_IMPORTED_MODULE_0___default.a.set({
    page: window.location.pathname
  });
  react_ga__WEBPACK_IMPORTED_MODULE_0___default.a.pageview(window.location.pathname);
};
const logEvent = (category = "", action = "") => {
  if (category && action) {
    react_ga__WEBPACK_IMPORTED_MODULE_0___default.a.event({
      category,
      action
    });
  }
};
const logException = (description = "", fatal = false) => {
  if (description) {
    react_ga__WEBPACK_IMPORTED_MODULE_0___default.a.exception({
      description,
      fatal
    });
  }
};
const Event = (category, action, label) => {
  react_ga__WEBPACK_IMPORTED_MODULE_0___default.a.event({
    category: category,
    action: action,
    label: label
  });
};

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/akshatham/NewFolder/highpeak/pages/index.js */"./pages/index.js");


/***/ }),

/***/ "babel-polyfill":
/*!*********************************!*\
  !*** external "babel-polyfill" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),

/***/ "core-js/library/fn/map":
/*!*****************************************!*\
  !*** external "core-js/library/fn/map" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/map");

/***/ }),

/***/ "core-js/library/fn/object/assign":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/assign" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "core-js/library/fn/object/create":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/create" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/create");

/***/ }),

/***/ "core-js/library/fn/object/define-property":
/*!************************************************************!*\
  !*** external "core-js/library/fn/object/define-property" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-descriptor":
/*!************************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-descriptor" ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "core-js/library/fn/object/keys":
/*!*************************************************!*\
  !*** external "core-js/library/fn/object/keys" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "core-js/library/fn/object/values":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/values" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/values");

/***/ }),

/***/ "core-js/library/fn/promise":
/*!*********************************************!*\
  !*** external "core-js/library/fn/promise" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/promise");

/***/ }),

/***/ "core-js/library/fn/symbol":
/*!********************************************!*\
  !*** external "core-js/library/fn/symbol" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/symbol");

/***/ }),

/***/ "core-js/library/fn/weak-map":
/*!**********************************************!*\
  !*** external "core-js/library/fn/weak-map" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/weak-map");

/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "prop-types-exact":
/*!***********************************!*\
  !*** external "prop-types-exact" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types-exact");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-awesome-slider":
/*!***************************************!*\
  !*** external "react-awesome-slider" ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-awesome-slider");

/***/ }),

/***/ "react-awesome-slider/dist/autoplay":
/*!*****************************************************!*\
  !*** external "react-awesome-slider/dist/autoplay" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-awesome-slider/dist/autoplay");

/***/ }),

/***/ "react-ga":
/*!***************************!*\
  !*** external "react-ga" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-ga");

/***/ }),

/***/ "react-popupbox":
/*!*********************************!*\
  !*** external "react-popupbox" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-popupbox");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "react-responsive-modal":
/*!*****************************************!*\
  !*** external "react-responsive-modal" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-responsive-modal");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map