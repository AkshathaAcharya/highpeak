module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./constants/getBlogsActions.js":
/*!**************************************!*\
  !*** ./constants/getBlogsActions.js ***!
  \**************************************/
/*! exports provided: getBlogs, getCategories, storeCategories, groupedByCategories, setFlag */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBlogs", function() { return getBlogs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCategories", function() { return getCategories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "storeCategories", function() { return storeCategories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "groupedByCategories", function() { return groupedByCategories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setFlag", function() { return setFlag; });
/* harmony import */ var _getBlogsConstants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./getBlogsConstants */ "./constants/getBlogsConstants.js");

const getBlogs = page => {
  return {
    type: _getBlogsConstants__WEBPACK_IMPORTED_MODULE_0__["GET_BLOGS"],
    page: page
  };
};
const getCategories = () => {
  return {
    type: _getBlogsConstants__WEBPACK_IMPORTED_MODULE_0__["GET_CATEGORY"]
  };
};
const storeCategories = data => {
  return {
    type: _getBlogsConstants__WEBPACK_IMPORTED_MODULE_0__["STORE_CATEGORIES"],
    payload: data
  };
};
const groupedByCategories = data => {
  return {
    type: _getBlogsConstants__WEBPACK_IMPORTED_MODULE_0__["GROUPED_BY_CATEGORY"],
    payload: data
  };
};
const setFlag = data => {
  return {
    type: _getBlogsConstants__WEBPACK_IMPORTED_MODULE_0__["SET_FLAG"],
    payload: data
  };
};

/***/ }),

/***/ "./constants/getBlogsConstants.js":
/*!****************************************!*\
  !*** ./constants/getBlogsConstants.js ***!
  \****************************************/
/*! exports provided: GET_BLOGS, GET_CATEGORY, STORE_CATEGORIES, GROUPED_BY_CATEGORY, SET_FLAG */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_BLOGS", function() { return GET_BLOGS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_CATEGORY", function() { return GET_CATEGORY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STORE_CATEGORIES", function() { return STORE_CATEGORIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GROUPED_BY_CATEGORY", function() { return GROUPED_BY_CATEGORY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_FLAG", function() { return SET_FLAG; });
const GET_BLOGS = "GET_BLOGS";
const GET_CATEGORY = "GET_CATEGORY";
const STORE_CATEGORIES = "STORE_CATEGORIES";
const GROUPED_BY_CATEGORY = "GROUPED_BY_CATEGORY";
const SET_FLAG = "SET_FLAG";

/***/ }),

/***/ "./constants/hitUsUpNewConstants.js":
/*!******************************************!*\
  !*** ./constants/hitUsUpNewConstants.js ***!
  \******************************************/
/*! exports provided: SEND_DATA_TO_SERVER, sendData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SEND_DATA_TO_SERVER", function() { return SEND_DATA_TO_SERVER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendData", function() { return sendData; });
const SEND_DATA_TO_SERVER = "SEND_DATA_TO_SERVER";
const sendData = data => {
  return {
    type: SEND_DATA_TO_SERVER,
    payload: data
  };
};

/***/ }),

/***/ "./constants/uploadFileAction.js":
/*!***************************************!*\
  !*** ./constants/uploadFileAction.js ***!
  \***************************************/
/*! exports provided: UPLOAD_FILE_DATA, uploadData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPLOAD_FILE_DATA", function() { return UPLOAD_FILE_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadData", function() { return uploadData; });
const UPLOAD_FILE_DATA = "UPLOAD_FILE_DATA";
const uploadData = data => {
  return {
    type: UPLOAD_FILE_DATA,
    payload: data
  };
};

/***/ }),

/***/ "./lib/store/reducers/getBlogsReducer.js":
/*!***********************************************!*\
  !*** ./lib/store/reducers/getBlogsReducer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_reducerUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../utils/reducerUtils */ "./utils/reducerUtils.js");
/* harmony import */ var _constants_getBlogsConstants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../constants/getBlogsConstants */ "./constants/getBlogsConstants.js");


const initialState = {
  categories: [],
  groupedByCategories: [],
  isFetch: true
};
const BlogsReducer = Object(_utils_reducerUtils__WEBPACK_IMPORTED_MODULE_0__["default"])([], {
  STORE_CATEGORIES: (state, action) => {
    const allCategory = [{
      id: 0,
      name: 'All'
    }];
    action.payload.forEach(category => {
      allCategory.push(category);
    });
    return {
      groupedByCategories: state.groupedByCategories,
      categories: allCategory,
      isFetch: state.isFetch
    };
  },
  GROUPED_BY_CATEGORY: (state, action) => {
    let groupedByCategories = {
      posts: []
    };
    let postList = [];
    state.categories.forEach((category, index) => {
      let groupedByCategoriesObj = {
        blogs: [],
        totalBlogs: 0
      };
      action.payload.forEach(post => {
        let postObj = {};
        postObj['postId'] = post.id;
        postObj['htmlContent'] = post.content.rendered;
        postObj['title'] = post.title.rendered;
        postObj['status'] = post.status;
        postObj['excerpt'] = post.excerpt.rendered;
        postObj['featureImageId'] = post.featured_media;
        postObj['redirectLink'] = post.link;
        postObj['featuredMedia'] = post.featured_media !== 0 && post._embedded['wp:featuredmedia'][0];
        postObj['featuredImage'] = post.featured_media !== 0 && post._embedded['wp:featuredmedia'][0].source_url;
        postObj['obj'] = post;
        postObj['catgories'] = post.categories;

        if (index === 0) {
          state.groupedByCategories ? state.groupedByCategories.posts.push(postObj) : postList.push(postObj);
        }

        if (post.categories.indexOf(category.id) === 0) {
          if (state.groupedByCategories) {
            state.groupedByCategories[category.id].blogs.push(postObj);
          } else {
            groupedByCategoriesObj.blogs.push(postObj);
          }
        }
      });

      if (state.groupedByCategories) {
        state.groupedByCategories[category.id].totalBlogs = state.groupedByCategories[category.id].blogs.length;
      } else {
        groupedByCategoriesObj.totalBlogs = groupedByCategoriesObj.blogs.length;
        groupedByCategories[category.id] = groupedByCategoriesObj;
        groupedByCategories['posts'] = postList;
      }
    });
    return {
      groupedByCategories: state.groupedByCategories ? state.groupedByCategories : groupedByCategories,
      categories: state.categories,
      isFetch: state.isFetch
    };
  },
  SET_FLAG: (state, action) => {
    return {
      groupedByCategories: state.groupedByCategories,
      categories: state.categories,
      isFetch: action.payload
    };
  }
});
/* harmony default export */ __webpack_exports__["default"] = (BlogsReducer);

/***/ }),

/***/ "./lib/store/reducers/reducerIndex.js":
/*!********************************************!*\
  !*** ./lib/store/reducers/reducerIndex.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _sendMailReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sendMailReducer */ "./lib/store/reducers/sendMailReducer.js");
/* harmony import */ var _getBlogsReducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./getBlogsReducer */ "./lib/store/reducers/getBlogsReducer.js");



const Reducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  SendMailReducer: _sendMailReducer__WEBPACK_IMPORTED_MODULE_1__["default"],
  BlogsReducer: _getBlogsReducer__WEBPACK_IMPORTED_MODULE_2__["default"]
});
/* harmony default export */ __webpack_exports__["default"] = (Reducer);

/***/ }),

/***/ "./lib/store/reducers/sendMailReducer.js":
/*!***********************************************!*\
  !*** ./lib/store/reducers/sendMailReducer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_reducerUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../utils/reducerUtils */ "./utils/reducerUtils.js");

const initialState = {
  store: []
};
const SendMailReducer = Object(_utils_reducerUtils__WEBPACK_IMPORTED_MODULE_0__["default"])([], {
  RESPONSE_TO_REDUCER: (state, action) => {
    return {
      store: action.payload
    };
  }
});
/* harmony default export */ __webpack_exports__["default"] = (SendMailReducer);

/***/ }),

/***/ "./lib/store/saga/HitUsUpSaga.js":
/*!***************************************!*\
  !*** ./lib/store/saga/HitUsUpSaga.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return hitUsUpSaga; });
/* harmony import */ var _utils_apiUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../utils/apiUtils */ "./utils/apiUtils.js");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constants_hitUsUpNewConstants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../constants/hitUsUpNewConstants */ "./constants/hitUsUpNewConstants.js");




function* sendDataToServer(action) {
  try {
    const {
      payload
    } = action;
    const response = yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["call"])(_utils_apiUtils__WEBPACK_IMPORTED_MODULE_0__["default"], `${window.location.origin}/send`, payload, "post");
    return false;
  } catch (error) {}
}

function* sendMail() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_constants_hitUsUpNewConstants__WEBPACK_IMPORTED_MODULE_2__["SEND_DATA_TO_SERVER"], sendDataToServer);
}

function* hitUsUpSaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([sendMail()]);
}

/***/ }),

/***/ "./lib/store/saga/JoinUsSaga.js":
/*!**************************************!*\
  !*** ./lib/store/saga/JoinUsSaga.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return JoinUsSaga; });
/* harmony import */ var _utils_apiUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../utils/apiUtils */ "./utils/apiUtils.js");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constants_uploadFileAction__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../constants/uploadFileAction */ "./constants/uploadFileAction.js");



const RESPONSE_TO_REDUCER = "RESPONSE_TO_REDUCER";

function* uploadDataToServer(action) {
  try {
    const {
      payload
    } = action;
    var response = yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["call"])(_utils_apiUtils__WEBPACK_IMPORTED_MODULE_0__["default"], `${window.location.origin}/upload`, payload, "post");
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])({
      type: RESPONSE_TO_REDUCER,
      payload: response.data
    });
    return false;
  } catch (error) {}
}

function* UploadResume() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_constants_uploadFileAction__WEBPACK_IMPORTED_MODULE_2__["UPLOAD_FILE_DATA"], uploadDataToServer);
}

function* JoinUsSaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([UploadResume()]);
}

/***/ }),

/***/ "./lib/store/saga/getBlogsSaga.js":
/*!****************************************!*\
  !*** ./lib/store/saga/getBlogsSaga.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getBlogsSaga; });
/* harmony import */ var _utils_apiUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../utils/apiUtils */ "./utils/apiUtils.js");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constants_getBlogsConstants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../constants/getBlogsConstants */ "./constants/getBlogsConstants.js");
/* harmony import */ var _constants_getBlogsActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../constants/getBlogsActions */ "./constants/getBlogsActions.js");




const url = 'https://blogs.highpeaksw.com/wp-json/wp/v2';

function* getBlogsFromWordpress(action) {
  try {
    const response = yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["call"])(_utils_apiUtils__WEBPACK_IMPORTED_MODULE_0__["default"], `${url}/posts?_embed&per_page=${action.page.perPage}&page=${action.page.pageNo}`, "get");
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_constants_getBlogsActions__WEBPACK_IMPORTED_MODULE_3__["groupedByCategories"])(response.data));
    return false;
  } catch (error) {
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_constants_getBlogsActions__WEBPACK_IMPORTED_MODULE_3__["setFlag"])(false));
  }
}

function* getCategoriesFromWP() {
  try {
    const response = yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["call"])(_utils_apiUtils__WEBPACK_IMPORTED_MODULE_0__["default"], `${url}/categories`, "get");
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(Object(_constants_getBlogsActions__WEBPACK_IMPORTED_MODULE_3__["storeCategories"])(response.data));
    return false;
  } catch (error) {
    console.log(error);
  }
}

function* getBlogsSaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeEvery"])(_constants_getBlogsConstants__WEBPACK_IMPORTED_MODULE_2__["GET_BLOGS"], getBlogsFromWordpress);
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_constants_getBlogsConstants__WEBPACK_IMPORTED_MODULE_2__["GET_CATEGORY"], getCategoriesFromWP);
}

/***/ }),

/***/ "./lib/store/saga/sagaIndex.js":
/*!*************************************!*\
  !*** ./lib/store/saga/sagaIndex.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return rootsaga; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _saga_JoinUsSaga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../saga/JoinUsSaga */ "./lib/store/saga/JoinUsSaga.js");
/* harmony import */ var _saga_HitUsUpSaga__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../saga/HitUsUpSaga */ "./lib/store/saga/HitUsUpSaga.js");
/* harmony import */ var _saga_getBlogsSaga__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../saga/getBlogsSaga */ "./lib/store/saga/getBlogsSaga.js");




function* rootsaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["all"])([Object(_saga_JoinUsSaga__WEBPACK_IMPORTED_MODULE_1__["default"])(), Object(_saga_HitUsUpSaga__WEBPACK_IMPORTED_MODULE_2__["default"])(), Object(_saga_getBlogsSaga__WEBPACK_IMPORTED_MODULE_3__["default"])()]);
}

/***/ }),

/***/ "./lib/store/store.js":
/*!****************************!*\
  !*** ./lib/store/store.js ***!
  \****************************/
/*! exports provided: initializeStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initializeStore", function() { return initializeStore; });
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga */ "redux-saga");
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _reducers_reducerIndex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reducers/reducerIndex */ "./lib/store/reducers/reducerIndex.js");
/* harmony import */ var _saga_sagaIndex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./saga/sagaIndex */ "./lib/store/saga/sagaIndex.js");




const initialState = {};

const bindMiddleware = middleware => {
  if (true) {
    const {
      composeWithDevTools
    } = __webpack_require__(/*! redux-devtools-extension */ "redux-devtools-extension");

    return composeWithDevTools(Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(...middleware));
  }

  return Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(...middleware);
};

const initializeStore = (preloadedState = initialState) => {
  const sagaMiddleware = redux_saga__WEBPACK_IMPORTED_MODULE_1___default()();
  const store = Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(_reducers_reducerIndex__WEBPACK_IMPORTED_MODULE_2__["default"], preloadedState, bindMiddleware([sagaMiddleware]));
  store.sagaTask = sagaMiddleware.run(_saga_sagaIndex__WEBPACK_IMPORTED_MODULE_3__["default"]);
  return store;
};

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/assign.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/assign */ "core-js/library/fn/object/assign");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "core-js/library/fn/object/define-property");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js":
/*!********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/keys */ "core-js/library/fn/object/keys");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/promise.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/promise.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/promise */ "core-js/library/fn/promise");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/asyncToGenerator.js":
/*!*************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/asyncToGenerator.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Promise = __webpack_require__(/*! ../core-js/promise */ "./node_modules/@babel/runtime-corejs2/core-js/promise.js");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    _Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new _Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

module.exports = _asyncToGenerator;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/extends.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/extends.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$assign = __webpack_require__(/*! ../core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

function _extends() {
  module.exports = _extends = _Object$assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/next/app.js":
/*!**********************************!*\
  !*** ./node_modules/next/app.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/pages/_app */ "./node_modules/next/dist/pages/_app.js")


/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/utils.js":
/*!*********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/utils.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$keys = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  return (...args) => {
    if (!used) {
      used = true;
      fn.apply(this, args);
    }
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(Component, ctx) {
  if (true) {
    if (Component.prototype && Component.prototype.getInitialProps) {
      const message = `"${getDisplayName(Component)}.getInitialProps()" is defined as an instance method - visit https://err.sh/zeit/next.js/get-initial-props-as-an-instance-method for more information.`;
      throw new Error(message);
    }
  } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!Component.getInitialProps) {
    return {};
  }

  const props = await Component.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(Component)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (true) {
    if (_Object$keys(props).length === 0 && !ctx.ctx) {
      console.warn(`${getDisplayName(Component)} returned an empty object from \`getInitialProps\`. This de-optimizes and prevents automatic static optimization. https://err.sh/zeit/next.js/empty-object-getInitialProps`);
    }
  }

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (true) {
    if (url !== null && typeof url === 'object') {
      _Object$keys(url).forEach(key => {
        if (exports.urlObjectKeys.indexOf(key) === -1) {
          console.warn(`Unknown key passed via urlObject into url.format: ${key}`);
        }
      });
    }
  }

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SUPPORTS_PERFORMANCE = typeof performance !== 'undefined';
exports.SUPPORTS_PERFORMANCE_USER_TIMING = exports.SUPPORTS_PERFORMANCE && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "./node_modules/next/dist/pages/_app.js":
/*!**********************************************!*\
  !*** ./node_modules/next/dist/pages/_app.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.Container = Container;
exports.createUrl = createUrl;
exports.default = void 0;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _asyncToGenerator2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/asyncToGenerator.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "./node_modules/next/dist/next-server/lib/utils.js");

exports.AppInitialProps = _utils.AppInitialProps;
/**
* `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.
* This allows for keeping state between navigation, custom error handling, injecting additional data.
*/

function appGetInitialProps(_x) {
  return _appGetInitialProps.apply(this, arguments);
}

function _appGetInitialProps() {
  _appGetInitialProps = (0, _asyncToGenerator2.default)(function* (_ref) {
    var {
      Component,
      ctx
    } = _ref;
    var pageProps = yield (0, _utils.loadGetInitialProps)(Component, ctx);
    return {
      pageProps
    };
  });
  return _appGetInitialProps.apply(this, arguments);
}

class App extends _react.default.Component {
  // Kept here for backwards compatibility.
  // When someone ended App they could call `super.componentDidCatch`.
  // @deprecated This method is no longer needed. Errors are caught at the top level
  componentDidCatch(error, _errorInfo) {
    throw error;
  }

  render() {
    var {
      router,
      Component,
      pageProps
    } = this.props;
    var url = createUrl(router);
    return _react.default.createElement(Component, (0, _extends2.default)({}, pageProps, {
      url: url
    }));
  }

}

exports.default = App;
App.origGetInitialProps = appGetInitialProps;
App.getInitialProps = appGetInitialProps;
var warnContainer;
var warnUrl;

if (true) {
  warnContainer = (0, _utils.execOnce)(() => {
    console.warn("Warning: the `Container` in `_app` has been deprecated and should be removed. https://err.sh/zeit/next.js/app-container-deprecated");
  });
  warnUrl = (0, _utils.execOnce)(() => {
    console.error("Warning: the 'url' property is deprecated. https://err.sh/zeit/next.js/url-deprecated");
  });
} // @deprecated noop for now until removal


function Container(p) {
  if (true) warnContainer();
  return p.children;
}

function createUrl(router) {
  // This is to make sure we don't references the router object at call time
  var {
    pathname,
    asPath,
    query
  } = router;
  return {
    get query() {
      if (true) warnUrl();
      return query;
    },

    get pathname() {
      if (true) warnUrl();
      return pathname;
    },

    get asPath() {
      if (true) warnUrl();
      return asPath;
    },

    back: () => {
      if (true) warnUrl();
      router.back();
    },
    push: (url, as) => {
      if (true) warnUrl();
      return router.push(url, as);
    },
    pushTo: (href, as) => {
      if (true) warnUrl();
      var pushRoute = as ? href : '';
      var pushUrl = as || href;
      return router.push(pushRoute, pushUrl);
    },
    replace: (url, as) => {
      if (true) warnUrl();
      return router.replace(url, as);
    },
    replaceTo: (href, as) => {
      if (true) warnUrl();
      var replaceRoute = as ? href : '';
      var replaceUrl = as || href;
      return router.replace(replaceRoute, replaceUrl);
    }
  };
}

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/app */ "./node_modules/next/app.js");
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-redux-wrapper */ "next-redux-wrapper");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _lib_store_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../lib/store/store */ "./lib/store/store.js");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






class HPSApp extends next_app__WEBPACK_IMPORTED_MODULE_2___default.a {
  static async getInitialProps({
    Component,
    ctx
  }) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    return {
      pageProps
    };
  }

  render() {
    const {
      Component,
      pageProps,
      store
    } = this.props;
    return __jsx(react_redux__WEBPACK_IMPORTED_MODULE_1__["Provider"], {
      store: store
    }, __jsx(Component, pageProps));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3___default()(_lib_store_store__WEBPACK_IMPORTED_MODULE_4__["initializeStore"])(HPSApp));

/***/ }),

/***/ "./utils/apiUtils.js":
/*!***************************!*\
  !*** ./utils/apiUtils.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/promise */ "./node_modules/@babel/runtime-corejs2/core-js/promise.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_promise__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);



const axiosCall = (url, data, method = "get", headers = {}, responseType = "json", props) => {
  return new _babel_runtime_corejs2_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a((resolve, reject) => {
    if (headers["headers"]) headers = headers["headers"];
    axios__WEBPACK_IMPORTED_MODULE_1___default()({
      url: `${url}`,
      method,
      data,
      // headers: {
      //   // Authorization: localStorage.getItem("auth_token"),
      //   'Access-Control-Allow-Origin' : '*'
      // },
      responseType
    }).then(response => {
      resolve(response);
    }).catch(error => {
      reject(error);
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (axiosCall);

/***/ }),

/***/ "./utils/reducerUtils.js":
/*!*******************************!*\
  !*** ./utils/reducerUtils.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
* Returns a reducer function
* @param reducerFunctions {object} an object where keys are action types and values are functions
@param initialState {}
*/
function getReducerFromObject(initialState, handlers) {
  return function reducer(state = initialState, action) {
    if (handlers.hasOwnProperty(action.type)) {
      return handlers[action.type](state, action);
    } else {
      return state;
    }
  };
}

/* harmony default export */ __webpack_exports__["default"] = (getReducerFromObject);

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi private-next-pages/_app.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.js */"./pages/_app.js");


/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "core-js/library/fn/object/assign":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/assign" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "core-js/library/fn/object/define-property":
/*!************************************************************!*\
  !*** external "core-js/library/fn/object/define-property" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "core-js/library/fn/object/keys":
/*!*************************************************!*\
  !*** external "core-js/library/fn/object/keys" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "core-js/library/fn/promise":
/*!*********************************************!*\
  !*** external "core-js/library/fn/promise" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/promise");

/***/ }),

/***/ "next-redux-wrapper":
/*!*************************************!*\
  !*** external "next-redux-wrapper" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-devtools-extension":
/*!*******************************************!*\
  !*** external "redux-devtools-extension" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension");

/***/ }),

/***/ "redux-saga":
/*!*****************************!*\
  !*** external "redux-saga" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga");

/***/ }),

/***/ "redux-saga/effects":
/*!*************************************!*\
  !*** external "redux-saga/effects" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga/effects");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=_app.js.map