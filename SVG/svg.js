function SVG(props){
    return(
        <svg
          width={props.width || '21px'}
          height={props.height || '32px'}
          viewBox="0 0 21 32"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
        >
            {props.children}
          <g
            stroke="none"
            stroke-width="1"
            fill="none"
            fill-rule="evenodd"
          ></g>
        </svg>
    )
}

export default SVG