import Modal from "react-responsive-modal";
import Head from "next/head";
import Link from "next/link";
import { PopupboxManager, PopupboxContainer } from "react-popupbox";
import { connect } from "react-redux";
import Input from "../Forms/input";
import Images from "../Forms/images";
import MenuSvg from "../components/menu_svg";
import { sendData } from "../constants/hitUsUpNewConstants";
import { EMAIL_REGEX, NAVIGATION_ARRAY } from "../constants/constants";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_SCRIPT,
  GA_WINDOW
} from "../constants/gtmScripts";
import { Event } from "../utils/analytics";
import "../styles/hitUsUpNew.css";

class HitUsUpNew extends React.Component {
  constructor() {
    super();
    this.state = {
      name: null,
      email: null,
      comments: null,
      isChecked: false,
      validName: false,
      validEmail: false,
      submitted: false,
      beforesubmit: true,
      data: null,
      openMenu: false
    };
  }
  componentDidMount = () => {
    GA_SCRIPT();
    GA_WINDOW();
  };
  handleNameChange = event => {
    this.setState({
      name:
        event && event.target && event.target.value ? event.target.value : null
    });
  };

  handleEmailChange = event => {
    this.setState({
      email:
        event && event.target && event.target.value ? event.target.value : null,
      validEmail:
        /\s/.test(event.target.value) === false &&
        EMAIL_REGEX.regexObject.test(event.target.value) &&
        true
    });
  };
  handleCommentsChange = event => {
    this.setState({
      comments:
        event && event.target && event.target.value ? event.target.value : null
    });
  };
  handleCheckboxChange = () => {
    this.setState({ isChecked: !this.state.isChecked });
  };
  onToggleMenu = () => {
    this.setState(prevState => ({
      openMenu: !prevState.openMenu
    }));
  };

  handleSubmit = async event => {
    event.preventDefault();
    Event("HITUSUP_NEW", "HITUSUP_NEW SUBMIT", "HITUSUP_NEW_PAGE");
    const commentsData = this.state.comments;
    if (
      this.state.validEmail &&
      this.state.name &&
      this.state.isChecked &&
      commentsData
    ) {
      this.setState({
        beforesubmit: false,
        submitted: true,
        data: {
          Name: this.state.name,
          Email: this.state.email,
          Comments: this.state.comments
        }
      });
    } else {
      const content = (
        <p className="quotesNew errorContainer"> * Please fill all fields!!.</p>
      );
      PopupboxManager.open({ content });
    }
  };
  render() {
    const submitCheck =
      this.state.validEmail &&
      this.state.name &&
      this.state.isChecked &&
      this.state.comments &&
      "Filled";
    this.state.data && this.props.sendData(this.state.data);
    return (
      <div className="hitUsUpDiv middleContainer">
        <Head>{GTM_SCRIPT_TAG.tag}</Head>
        <GTM_NO_SCRIPT />
        <div className="middleContainerColumnSpaceBetween middleContainerRowHead">
          <div className="topNavigationLeft">
            <a href="/" target="_self">
              <Images
                source="../assets/logo-white.png"
                altText="logo"
                class="logoPositionition"
              />
            </a>
            <a
              href="/portfolio"
              target="_blank"
              className="topButtonsHitUsUp hitUsUpOurStuff hideInMobile"
            >
              Our stuff
            </a>
          </div>

          <div className="leftNavigationBarHitUsUp" onClick={this.onToggleMenu}>
            <MenuSvg navigationColor="#ffffff" />
          </div>
        </div>

        <div className="navigationSlider">
          <Modal
            open={this.state.openMenu}
            onClose={this.onToggleMenu}
            styles={{
              overlay: { padding: "0px" },
              modal: {
                maxWidth: "1500px",
                width: "25%",
                marginRight: "0%",
                float: "right",
                height: "94%",
                fontSize: "24px",
                animation: "slide 0.5s forwards",
                outline: "none"
              }
            }}
          >
            <ul className="ulRight">
              {NAVIGATION_ARRAY.map((navObj, index) => (
                <li key={index} className="liMenuSlide">
                  <Link
                    href={{
                      pathname: "./home",
                      query: { section: index }
                    }}
                  >
                    <a
                      className={
                        this.state.currentPage === index
                          ? "menuLink activeMenu"
                          : "menuLink"
                      }
                    >
                      {navObj.title}
                    </a>
                  </Link>
                </li>
              ))}
              <li className="blackColor">
                <a href="/about" target="_self">
                  Who Are We?
                </a>
              </li>
            </ul>
          </Modal>
        </div>
        <div className="setWidth middleContainer">
          <div className="middleContainerColumn">
            <span className="hitUsUpHeading">Hit us up! </span>
            <span>
              <Images
                class="hitUsUpImage"
                source="../assets/titleelement.png"
                altText="title element"
              />
            </span>
          </div>
          <div className="middleContainerrow">
            <div className="middleContainer">
              <span className="titleContainer ">
                Tell us your name <span className="sizeContainer"> * </span>
              </span>
              <Input
                value={this.state.name}
                onChange={this.handleNameChange}
                type="text"
                name="name"
                classname="inputContainer "
                placeholder="Enter full name"
                required
              />
            </div>
            <div className="middleContainer">
              <span className="titleContainer ">
                We don’t write much. We promise!
                <span className="sizeContainer"> * </span>
              </span>
              <Input
                value={this.state.email}
                onChange={this.handleEmailChange}
                type="email"
                name="email"
                classname="inputContainer"
                placeholder="Enter email ID"
                required
              />
            </div>
          </div>
          {this.state.email === null ||
            (!this.state.validEmail && (
              <div className="hitUsUpErrorNew errorContainer">
                Invalid e-mail
              </div>
            ))}
          <div className="middleContainer">
            <span className="titleContainer">
              Drop us a note! <span className="sizeContainer">*</span>
            </span>
            <textarea
              value={this.state.comments}
              onChange={this.handleCommentsChange}
              className="backgroundContainer "
              name="comments"
              placeholder="Message"
              required
            ></textarea>
          </div>
          <div className="termsMarginNew titleContainer">
            <span>
              <input
                className="hitusupCheckboxNew"
                type="checkbox"
                onChange={this.handleCheckboxChange}
                checked={this.state.isChecked}
                required
              />
            </span>
            <span className="sizeContainer"> * </span>
            <span className="termContainer">
              I agree to the
              <a
                href="/termsConditions"
                target="_blank"
                className="termsLinkNew"
              >
                terms & conditions
              </a>
              and
              <a href="/privacyPolicy" target="_blank" className="termsLinkNew">
                privacy policy
              </a>
            </span>
          </div>
          <div className="buttonSubmit">
            <span>
              <input
                type="submit"
                value="Submit"
                onClick={event => this.handleSubmit(event)}
                className="hitUsUpSubmitNew"
              ></input>
            </span>
            <span>{submitCheck !== "Filled" && <PopupboxContainer />}</span>
          </div>
        </div>
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {
    sendData: (...test) => dispatch(sendData(...test))
  };
};

export default connect(null, dispachToProps)(HitUsUpNew);
