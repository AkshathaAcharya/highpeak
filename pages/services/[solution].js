import Modal from "react-responsive-modal";
import Head from "next/head";
import Images from "../../Forms/images";
import HitUsUp from "../../components/HitUsUp/hitUsUp";
import Footer from "../../components/Footer/footer";
import Sixcards from "../../components/solutionsCards/sixCards";
import Fivecards from "../../components/solutionsCards/fiveCards";
import Fourcards from "../../components/solutionsCards/fourCards";
import { Event } from "../../utils/analytics";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_SCRIPT,
  GA_WINDOW
} from "../../constants/gtmScripts";
import { SOLUTIONS } from "../../constants/solutionsConstants";
import "../../styles/index.css";
import "../../components/HitUsUp/hitUsUp.css";
import "../../styles/solutions.css";

class Solutions extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false,
      isFlipped: false,
      indexToFlip: null
    };
  }

  static getInitialProps({ query }) {
    return { query };
  }

  componentDidMount = () => {
    GA_SCRIPT();
    GA_WINDOW();
  };

  onToggleHitUsUpModal = () => {
    this.setState(prevState => ({
      open: !prevState.open
    }));
  };

  handleClick = (e, ind) => {
    Event(
      SOLUTIONS[this.props.query.solution].flipCards[ind].frontCard,
      SOLUTIONS[this.props.query.solution].solutionsName,
      "SOLUTION_PAGE_FLIP"
    );
    this.state.indexToFlip === ind
      ? this.setState({
          isFlipped: false,
          indexToFlip: null
        })
      : this.setState(prevState => ({
          isFlipped: !prevState.isFlipped,
          indexToFlip: ind
        }));
  };

  render() {
    const filteredSolutions = SOLUTIONS[this.props.query.solution];
    const Includecomponent =
      filteredSolutions.componentToInclude === "Sixcards"
        ? Sixcards
        : filteredSolutions.componentToInclude === "Fivecards"
        ? Fivecards
        : Fourcards;
    const { open } = this.state;

    return (
      <div>
        <Head>
          {GTM_SCRIPT_TAG.tag}

          <link
            href="https://fonts.googleapis.com/css?family=Montserrat"
            rel="stylesheet"
          />
          <title>{filteredSolutions.metaTitle}</title>

          <meta
            name="description"
            content={filteredSolutions.metaDescription}
          ></meta>
          <meta name="keywords" content={filteredSolutions.keywords}></meta>
          <meta
            property="og:title"
            content={filteredSolutions.metaTitle}
          ></meta>
          <meta
            property="og:description"
            content={filteredSolutions.metaDescription}
          ></meta>
          <meta property="og:url" content={filteredSolutions.ogURL}></meta>
          <meta property="og:site_name" content="High Peak Software" />
          <meta property="og:image" content={filteredSolutions.ogImage}></meta>
        </Head>
        <GTM_NO_SCRIPT />
        <div className="solutionsHeader">
          <a href="/" target="_self">
            <Images
              class="titleImageSolutions"
              source="../assets/logo-white.png"
              altText="title-element"
            />
          </a>
          <div className="columnDiv">
            <span className="marketing">
              {filteredSolutions.solutionsName}
              <Images
                class="titleImageSolutionsPage"
                source="../assets/titleelement.png"
                altText="title-element"
              />
            </span>

            <a href="/portfolio" target="_blank" className="viewProjects">
              View projects
            </a>
            <button
              onClick={this.onToggleHitUsUpModal}
              className="hitusUp"
              style={{ float: "right" }}
            >
              Hit us up!
            </button>
          </div>
        </div>

        <div className="modalTransition">
          <Modal
            open={open}
            onClose={this.onToggleHitUsUpModal}
            showCloseIcon={true}
            center
            styles={{
              modal: {
                outline: "none",
                animation: "0.2s zoomIn",
                width: "628px",
                borderRadius: "5px"
              }
            }}
          >
            <HitUsUp
              togglehitusup={this.onToggleHitUsUpModal}
              headerName="Hit Us Up!"
            />
          </Modal>
        </div>

        <Includecomponent
          filteredSolutions={filteredSolutions}
          indexToFlip={this.state.indexToFlip}
          handleClick={this.handleClick}
        />

        <div className="footerSolutionsWrap">
          <Footer footer={true} />
        </div>
      </div>
    );
  }
}

export default Solutions;
