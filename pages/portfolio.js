import Modal from "react-responsive-modal";
import "@babel/polyfill";
import Link from "next/link";
import Head from "next/head";
import MenuSvg from "../components/menu_svg";
import Images from "../Forms/images";
import Footer from "../components/Footer/footer";
import { Event } from "../utils/analytics";
import { NAVIGATION_ARRAY } from "../constants/constants";
import { PORTFOLIO_CONSTANTS, FILTERS } from "../constants/portfolioConstants";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_SCRIPT,
  GA_WINDOW
} from "../constants/gtmScripts";
import "../styles/ourStuff.css";
import "../styles/index.css";

class Portfolio extends React.Component {
  constructor() {
    super();
    this.state = {
      filter: "All",
      open: false,
      clicked: false,
      activeMenu: 0,
      openMenu: false,
      clickedSection: null,
      clickedIndex: null,
      productOrService: "",
      campaigned: false,
      url: ""
    };
  }

  close = event => {
    this.setState({
      clicked: false,
      clickedSection: null,
      clickedIndex: null
    });
  };

  onToggleMenu = () => {
    this.setState({ openMenu: !this.state.openMenu });
  };

  clicked = (ind, sec, eve, prodOrserv, camp, urls) => {
    Event(prodOrserv ? prodOrserv : "AI", urls, "PORTFOLIO_PAGE");
    this.setState({
      clicked: true,
      clickedSection: sec,
      clickedIndex: ind,
      productOrService: prodOrserv,
      campaigned: camp,
      url: urls
    });
  };

  checkFilter = (menuIndex, event, filter) => {
    Event("TOP_BUTTONS", filter, "PORTFOLIO_PAGE");
    this.setState({
      filter: filter,
      activeMenu: menuIndex,
      clickedSection: null
    });
  };

  componentDidMount = () => {
    GA_SCRIPT();
    GA_WINDOW();
  };

  render() {
    const filteredProjects = PORTFOLIO_CONSTANTS.filter(
      (filter, index) => filter[this.state.filter] === true
    );
    let remainingObjects = filteredProjects.length;
    let counter = 1;
    let eachArray = [];
    const containerOfArrays = [];
    filteredProjects.reduce(function(initialProject, project) {
      if (remainingObjects !== 2 && remainingObjects !== 1 && counter <= 3) {
        eachArray.push(project);
        counter++;
        if (counter === 4) {
          containerOfArrays.push(eachArray);
          counter = 1;
          remainingObjects = remainingObjects - 3;
          eachArray = [];
        }
      } else {
        eachArray.push(project);
      }
    }, containerOfArrays);
    eachArray.length !== 0 && containerOfArrays.push(eachArray);
    const projectDetailedDataKey =
      this.state.clickedSection !== null &&
      containerOfArrays[this.state.clickedSection][this.state.clickedIndex];

    return (
      <div>
        <div className="ourStuffMainDiv">
          <Head>
            {GTM_SCRIPT_TAG.tag}
            <meta charSet="utf-8" />
            <title>High Peak Software - Portfolio</title>
            <meta
              name="description"
              content="A portfolio of all our products and client projects in one place, categorized by AI, products, and services."
            ></meta>
            <meta
              name="keywords"
              content="Portfolio management, portfolio website, products portfolio, projects portfolio, services portfolio, brand portfolio"
            ></meta>
            <meta
              property="og:title"
              content="High Peak Software - Portfolio"
            />
            <meta property="og:site_name" content="High Peak Software" />
            <meta
              property="og:description"
              content="A portfolio of all our products and client projects in one place, categorized by AI, products, and services."
            />
            <meta
              property="og:url"
              content="https://highpeaksw.com/portfolio"
            />
            <meta property="og:image" content="../assets/scarlet.png" />
            <link
              href="https://fonts.googleapis.com/css?family=Montserrat"
              rel="stylesheet"
            />
          </Head>
          <GTM_NO_SCRIPT />
          <div className="ourStuffHead">
            <a href="/">
              <Images
                source="../assets/logo-color.png"
                altText="logo"
                class="logoImageOurStuff"
              />
            </a>
            <span
              className="leftNavigationBarOurStuff"
              onClick={this.onToggleMenu}
            >
              <MenuSvg navigationColor={NAVIGATION_ARRAY[0].navigationColor} />
            </span>
          </div>
          <div className="ourstuffWrap">
            <div className="our">
              Our
              <span className="stuffSpan"> Stuff </span>
              <span style={{ width: "100%" }}>
                <Images
                  class="titleImage"
                  source="../assets/titleelement.png"
                  altText="title-element"
                />
              </span>
            </div>
            <div className="navigationSlider">
              <Modal
                open={this.state.openMenu}
                onClose={this.onToggleMenu}
                styles={{
                  overlay: { padding: "0px" },
                  modal: {
                    maxWidth: "1500px",
                    width: "25%",
                    marginRight: "0%",
                    float: "right",
                    height: "94%",
                    fontSize: "24px",
                    animation: "slide 0.5s forwards",
                    outline: "none"
                  }
                }}
              >
                <ul className="ulRight">
                  {NAVIGATION_ARRAY.map((navObj, index) => (
                    <li key={index} className="liMenuSlide">
                      <Link
                        href={{
                          pathname: "./home",
                          query: { section: index }
                        }}
                      >
                        <a className="menulink">
                          {navObj.title !== "We Deliver Free AI Stuff" &&
                            navObj.title}
                        </a>
                      </Link>
                    </li>
                  ))}
                  <li className="blackColor">
                    <a href="/about" className="menulink">
                      Who Are We?
                    </a>
                  </li>
                </ul>
              </Modal>
            </div>

            <div className="ourStuffMenus">
              {FILTERS.map((navigationNames, index) => (
                <button
                  key={index}
                  className={
                    index === this.state.activeMenu
                      ? "activeMenuOurStuff"
                      : "Menu"
                  }
                  onClick={e => {
                    this.checkFilter(index, e, navigationNames);
                  }}
                >
                  {navigationNames.charAt(0).toUpperCase() +
                    navigationNames.slice(1)}
                </button>
              ))}
            </div>
          </div>

          <div className="wrappedDiv">
            {containerOfArrays.map((wrappedProjects, section) => (
              <div className="wrapSection">
                <div key={section} className="flexedDiv">
                  {wrappedProjects.map((projects, index) => (
                    <div
                      key={index}
                      className={
                        section === this.state.clickedSection &&
                        index === this.state.clickedIndex
                          ? "eachDivWidthColor"
                          : "eachDivWidth"
                      }
                    >
                      <span
                        onClick={event => {
                          this.clicked(
                            index,
                            section,
                            event,
                            projects.productOrService,
                            projects.campaign,
                            projects.url
                          );
                        }}
                      >
                        <Images
                          class={
                            section === this.state.clickedSection &&
                            index === this.state.clickedIndex
                              ? "ProjectColorImages"
                              : "ProjectImages"
                          }
                          source={projects.projectImage}
                          altText="projectImages"
                        />

                        <div
                          className={
                            section === this.state.clickedSection &&
                            index === this.state.clickedIndex
                              ? "IncreasedHeight"
                              : "titleheight"
                          }
                        >
                          <span className="title">
                            {projects.projectName.toUpperCase()}
                          </span>
                          <span className="subTitleOurStuff">
                            {projects.subTitle}
                          </span>
                        </div>
                      </span>
                    </div>
                  ))}
                </div>

                {this.state.clicked && this.state.clickedSection === section && (
                  <div className="description">
                    {this.state.clickedSection !== null && (
                      <div>
                        <div className="descriptionLists">
                          {projectDetailedDataKey.projectDescription.map(
                            (descriptions, index) => (
                              <div key={index}>{descriptions}</div>
                            )
                          )}
                        </div>

                        <div className="client-overviewStuff">
                          {projectDetailedDataKey.clientOverViews.length !==
                            0 && (
                            <div className="featuresOurStuff">
                              Client Overview
                            </div>
                          )}
                          {projectDetailedDataKey.clientOverViews.map(
                            (overview, index) => (
                              <div key={index}>{overview}</div>
                            )
                          )}
                        </div>

                        {projectDetailedDataKey.features.length !== 0 && (
                          <div className="featuresOurStuff featuresWrap">
                            Features
                            <ul className="industriesUlList">
                              {projectDetailedDataKey.features.map(
                                (feature, index) => (
                                  <li key={index}>{feature}</li>
                                )
                              )}
                            </ul>
                          </div>
                        )}

                        {projectDetailedDataKey.industries.length !== 0 && (
                          <div className="featuresOurStuff">
                            Industries
                            <ul className="industriesUlList">
                              {projectDetailedDataKey.industries.map(
                                (industry, index) => (
                                  <li key={index}>{industry}</li>
                                )
                              )}
                            </ul>
                          </div>
                        )}
                      </div>
                    )}
                    <span
                      className="closeSpan"
                      onClick={event => {
                        this.close(event);
                      }}
                    >
                      X
                    </span>

                    <div style={{ marginTop: "62px" }}>
                      <div className="descriptionList">
                        <ul className="descriptionLinks">
                          {projectDetailedDataKey.tags.map((tag, index) => (
                            <li key={index} className="ourStuffTags">
                              {tag}
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>

                    <Link
                      class="centerAlign"
                      prefetch
                      as={
                        this.state.productOrService === "products" &&
                        !this.state.campaigned
                          ? `/products/${this.state.url}`
                          : this.state.productOrService === "services" &&
                            !this.state.campaigned
                          ? `/case-studies/${this.state.url}`
                          : `/blogs/${this.state.url}`
                      }
                      href={{
                        pathname:
                          this.state.productOrService === "products" &&
                          !this.state.campaigned
                            ? "/products/[this.state.url.js]"
                            : this.state.productOrService === "services" &&
                              !this.state.campaigned
                            ? "/case-studies/[this.state.url.js]"
                            : "/blogs/[this.state.url.js]",
                        query: {
                          url: this.state.url
                        }
                      }}
                    >
                      <button className="viewDetails">
                        <a target="_blank">View case study</a>
                      </button>
                    </Link>
                  </div>
                )}
              </div>
            ))}
          </div>
        </div>
        <div className="footerPortfolioWrap">
          <Footer footer={true} />
        </div>
      </div>
    );
  }
}
export default Portfolio;
