import Modal from "react-responsive-modal";
import "@babel/polyfill";
// import { Player } from "video-react";
// import "../node_modules/video-react/dist/video-react.css";
import Head from "next/head";
import Images from "../../Forms/images";
import Footer from "../../components/Footer/footer";
import HitUsUp from "../../components/HitUsUp/hitUsUp";
import { CASESTUDY_CONSTANTS } from "../../constants/caseStudyConstants";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_SCRIPT,
  GA_WINDOW
} from "../../constants/gtmScripts";
import "../../components/HitUsUp/hitUsUp.css";
import "../../styles/index.css";
import "../../styles/caseStudy.css";
import "../../styles/servicePage.css";

class servicePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      pageName: "invoice-factoring-automation"
    };
  }

  onToggleHitUsUpModal = () => {
    this.setState(prevState => ({
      open: !prevState.open
    }));
  };

  componentDidMount = () => {
    GA_SCRIPT();
    window.addEventListener("scroll", this.listenToScroll, true);
    GA_WINDOW();
  };

  render() {
    const filteredCaseStudy = CASESTUDY_CONSTANTS[this.state.pageName];
    const { open } = this.state;
    return (
      <div>
        <div className="servicePage casestudy-page">
          <Head>
            {GTM_SCRIPT_TAG.tag}
            <link
              href="https://fonts.googleapis.com/css?family=Montserrat"
              rel="stylesheet"
            />
            <title>Scarlet - Intelligent Document Processing Tool</title>
          </Head>
          <GTM_NO_SCRIPT />
          <div className="serviceStickyHeader casestudies">
            <a href="/" target="_self">
              <Images
                class="servicepageLogo casestudylogo"
                source="../assets/logo-white.png"
                altText="logo"
              />
            </a>

            <a
              href={filteredCaseStudy["knowMoreLink"]}
              className="hitusupServicePage"
              target="_self"
            >
              Know more
            </a>
            <button
              onClick={this.onToggleHitUsUpModal}
              className="servicepageTopButtons ourStuffButtonService demobutton"
            >
              <strong> Request demo</strong>
            </button>
          </div>

          <div className="modalTransition">
            <Modal
              open={open}
              onClose={this.onToggleHitUsUpModal}
              showCloseIcon={true}
              center
              styles={{
                modal: {
                  outline: "none",
                  animation: "0.2s zoomIn",
                  width: "628px",
                  borderRadius: "5px"
                }
              }}
            >
              <HitUsUp
                togglehitusup={this.onToggleHitUsUpModal}
                headerName="Demo!"
              />
            </Modal>
          </div>

          <div className="CTAbuttons headerCTA">
            <a
              href={filteredCaseStudy["knowMoreLink"]}
              className="hitusupServicePage casestudiesOurStuff knowMoreCTA"
              target="_self"
            >
              Know more
            </a>
            <button
              onClick={this.onToggleHitUsUpModal}
              className="servicepageTopButtons ourStuffButtonService freeDemoCTA"
            >
              <strong> Request demo</strong>
            </button>
          </div>

          <div className="ServicePageTitle casestudyTitleHeader">
            <div className="servicePageTitle">
              {filteredCaseStudy["projectName"].map((titles, indexes) => (
                <div className="casestudyTitle">{titles}</div>
              ))}
            </div>

            <div>
              <Images
                class="titleImageServicePage"
                source="../assets/titleelement.png"
                altText="title-element"
              />
            </div>
          </div>

          <div className="OverviewProblem">
            <div className="clientOverviewService">
              <div className="clientOverviewTitle">CLIENT OVERVIEW</div>
              {filteredCaseStudy["clientOverview"].map(
                (overviews, overviewIndex) => (
                  <div className="overViews"> {overviews} </div>
                )
              )}
            </div>

            <div className="Problem">
              <div class="clientOverviewTitle">PROBLEM</div>
              {filteredCaseStudy["problems"].map((problems, ind) => (
                <div className="overViews">{problems}</div>
              ))}
            </div>
          </div>

          {/* video */}

          {/* <div className="videoPlayer">
              <Player
                  playsInline
                  //   poster="../assets/titleelement.png"
                  src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
              />
            </div> */}

          {/* video */}

          <div className="ImpactWrap">
            {filteredCaseStudy["impactNumbers"].map((impacts, index) => (
              <div className="eachImpacts">
                {impacts.impactNumber}
                <div className="eachImpactNumbers">
                  {impacts.impactDescription}
                </div>
              </div>
            ))}
          </div>

          <div className="Challenges">
            <div className="clientOverviewTitle">CHALLENGES</div>
            {filteredCaseStudy["challenges"].map((data, ind) => (
              <div className="overViewWrap">
                <div className="overViews">{data}</div>
              </div>
            ))}
          </div>

          <div className="solutionsServicePageCase">
            <div class="clientOverviewTitle">SOLUTIONS</div>
            <div className="thirdSectionContentCase">
              {filteredCaseStudy["solutions"].map((data, index) => (
                <div id={index} className="thirdSectionDataCase">
                  <div className="imageWrapDivCase">
                    <img className="actionImagesCase" src={data.img} />
                  </div>
                  <div className="thirdSectionDescriptionWrapCase">
                    {data.solutionsDescription.map(datas => (
                      <div className="thirdSectionDescriptionCase">
                        <span style={{ float: "left" }}>{datas}</span>
                        <hr />
                      </div>
                    ))}
                  </div>
                </div>
              ))}
            </div>

            <div className="CTAbuttons">
              <a
                href={filteredCaseStudy["knowMoreLink"]}
                className="hitusupServicePage casestudiesOurStuff knowMoreCTA"
                target="_self"
              >
                Know more
              </a>
              <button
                onClick={this.onToggleHitUsUpModal}
                className="servicepageTopButtons ourStuffButtonService freeDemoCTA"
              >
                <strong> Request demo</strong>
              </button>
            </div>
          </div>
        </div>
        <div className="servicesFooterWrap">
          <Footer footer={true} />
        </div>
      </div>
    );
  }
}

export default servicePage;
