import Modal from "react-responsive-modal";
import "@babel/polyfill";
import Head from "next/head";
// import { Player } from "video-react";
// import "../node_modules/video-react/dist/video-react.css";
import Images from "../../Forms/images";
import HitUsUp from "../../components/HitUsUp/hitUsUp";
import Footer from "../../components/Footer/footer";
import { SERVICE_CONSTANTS } from "../../constants/servicesConstants";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_SCRIPT,
  GA_WINDOW
} from "../../constants/gtmScripts";
import "../../styles/index.css";
import "../../styles/servicePage.css";
import "../../styles/caseStudy.css";

class servicePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      scrollPosition: 0
    };
  }

  static getInitialProps({ query }) {
    return { query };
  }

  onToggleHitUsUpModal = () => {
    this.setState(prevState => ({
      open: !prevState.open
    }));
  };

  componentDidMount = () => {
    GA_SCRIPT();
    window.addEventListener("scroll", this.listenToScroll, true);
    GA_WINDOW();
  };

  componentWillUnmount = () => {
    window.removeEventListener("scroll", this.listenToScroll);
  };

  listenToScroll = () => {
    const windowScroll =
      document.body.scrollTop || document.documentElement.scrollTop;
    this.setState({
      scrollPosition: windowScroll
    });
  };

  render() {
    const filteredProject = SERVICE_CONSTANTS[this.props.query.service];
    const { open } = this.state;
    return (
      <div>
        <div className="servicePage">
          <Head>
            {GTM_SCRIPT_TAG.tag}

            <link
              href="https://fonts.googleapis.com/css?family=Montserrat"
              rel="stylesheet"
            />
            <title>{filteredProject.metaTitle}</title>

            <meta
              name="description"
              content={filteredProject.metaDescription}
            ></meta>
            <meta name="keywords" content={filteredProject.metaKeywords}></meta>
            <meta
              property="og:title"
              content={filteredProject.metaTitle}
            ></meta>
            <meta
              property="og:description"
              content={filteredProject.metaDescription}
            ></meta>
            <meta
              property="og:url"
              content={filteredProject.openGraphURL}
            ></meta>
            <meta property="og:site_name" content="High Peak Software" />
            <meta
              property="og:image"
              content={filteredProject.projectImage}
            ></meta>
          </Head>

          <GTM_NO_SCRIPT />
          <div className="serviceStickyHeader">
            <a href="/" target="_self">
              <Images
                class="servicepageLogo"
                source="../assets/logo-white.png"
                altText="logo"
              />
            </a>
            <span
              className={
                this.state.scrollPosition > 100
                  ? "scrollProjectServices"
                  : "hideproject"
              }
            >
              {filteredProject.projectName}
              <Images
                class="titleImageServices"
                source="../assets/titleelement.png"
                altText="title-element"
              />
            </span>
            <button
              onClick={this.onToggleHitUsUpModal}
              className="hitusupServicePage"
            >
              Hit us up!
            </button>

            <a
              href="/portfolio"
              target="_blank"
              className="servicepageTopButtons ourStuffButtonService"
            >
              <strong> Our stuff</strong>
            </a>
          </div>

          <div className="modalTransition">
            <Modal
              open={open}
              onClose={this.onToggleHitUsUpModal}
              showCloseIcon={true}
              center
              styles={{
                modal: {
                  outline: "none",
                  animation: "0.2s zoomIn",
                  width: "628px",
                  borderRadius: "5px"
                }
              }}
            >
              <HitUsUp
                togglehitusup={this.onToggleHitUsUpModal}
                headerName="Hit Us Up!"
              />
            </Modal>
          </div>

          <div className="ServicePageTitle">
            <div className="servicePageTitle">
              {filteredProject.projectName}
            </div>
            <div>
              <Images
                class="titleImageServicePage"
                source="../assets/titleelement.png"
                altText="title-element"
              />
            </div>
          </div>

          <div className="OverviewProblem">
            <div className="clientOverviewService">
              <div className="clientOverviewTitle">CLIENT OVERVIEW</div>
              {filteredProject.clientOverViews.map((overviews, index) => (
                <div key={index} className="overViews">
                  {overviews}
                </div>
              ))}
            </div>

            <div className="Problem">
              <div class="clientOverviewTitle">PROBLEM</div>
              {filteredProject.problems.map((problems, index) => (
                <div key={index} className="overViews">
                  {problems}
                </div>
              ))}
            </div>
          </div>

          {/* video */}

          {/* <div className="videoPlayer">
              <Player
                  playsInline
                  //   poster="../assets/titleelement.png"
                  src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
              />
            </div> */}

          {/* video */}

          <div className="Challenges">
            <div className="clientOverviewTitle">CHALLENGES</div>
            {filteredProject.challenges.map((challenges, index) => (
              <div key={index} className="overViews">
                {challenges}
              </div>
            ))}
          </div>

          <div className="solutionsServicePage">
            <div class="clientOverviewTitle">SOLUTIONS</div>
            <div>
              {
                <div className="solutionDescription">
                  {filteredProject.solutionsShortDescription.map(
                    (description, index) => (
                      <div key={index}>{description} </div>
                    )
                  )}

                  <div className="solutionsWrapTest">
                    {filteredProject.solutions.map((solutions, index) => (
                      <div key={index} className="thirdSectionDataWrapTest">
                        {solutions.solutionsDescription.map(
                          (datas, indexes) => (
                            <div
                              key={indexes}
                              className="thirdSectionDescription"
                            >
                              <span className="mobileSolutions">{datas}</span>
                            </div>
                          )
                        )}
                      </div>
                    ))}
                  </div>
                </div>
              }
            </div>
          </div>

          {filteredProject.technologies.length !== 0 && (
            <div className="technology">
              <div class="clientOverviewTitle">TECHNOLOGIES USED</div>
              <div className="technologiesWrap">
                {filteredProject.technologies.map((tech, index) => (
                  <div key={index} className="mobileViewServices">
                    <section className="techDomain">{tech.domain}</section>
                    {tech.technology.map((technologies, techIndex) => (
                      <section className="technologyUsedService">
                        {technologies}
                      </section>
                    ))}
                  </div>
                ))}
              </div>
            </div>
          )}
        </div>
        <div className="servicesFooterWrap">
          <Footer footer={true} />
        </div>
      </div>
    );
  }
}

export default servicePage;
