import Modal from "react-responsive-modal";
import Link from "next/link";
import Head from "next/head";
import Images from "../Forms/images";
import ReactPageScroller from "../components/pageScroller";
import MenuSvg from "../components/menu_svg";
import ScrollSvg from "../components/scroll_svg";
import HitUsUp from "../components/HitUsUp/hitUsUp";
import WhoWeAre from "../components/WhoWeAre/whoWeAre";
import WeArePartner from "../components/WeArePartner/weArePartner";
import OurResident from "../components/OurResident/ourResident";
import WeDeliverFreeAI from "../components/WeDeliverFreeAI/weDeliverFreeAI";
import {
  NAVIGATION_ARRAY,
  ABOUT_NAVIGATION_ARRAY
} from "../constants/constants";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_WINDOW,
  GA_SCRIPT
} from "../constants/gtmScripts";
import { GOOGLE_ANALYTICS_CONSTANTS } from "../constants/googleAnalyticsConstants";
import { Event } from "../utils/analytics";
import "../styles/about.css";
import "../styles/dynamiccls.css";
import "../styles/index.css";

class AboutUsPage extends React.Component {
  constructor() {
    super();
    this.state = {
      Responsivewidth: 0,
      currentPage: 0,
      active: null,
      open: false,
      openMenu: false,
      joinUs: false,
      showPage: false,
      pageName: "aboutUs"
    };
    this._pageScroller = null;
  }
  static getInitialProps({ query }) {
    return { query };
  }
  updateWindowDimensions = () => {
    this.setState({
      Responsivewidth: window.innerWidth
    });
  };
  componentDidMount = () => {
    GA_SCRIPT();
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
    GA_WINDOW();
  };
  componentWillUnmount = () => {
    window.removeEventListener("resize", this.updateWindowDimensions);
  };
  onToggleHitUsUpModal = event => {
    this.setState(prevState => ({
      open: !prevState.open,
      joinUs: event === "join" ? true : false
    }));
  };

  onToggleMenu = () => {
    this.setState(prevState => ({
      openMenu: !prevState.openMenu
    }));
  };

  changePage = index => {
    const pagesAnalytics =
      GOOGLE_ANALYTICS_CONSTANTS[this.state.pageName][index];
    Event(pagesAnalytics.event, pagesAnalytics.page, pagesAnalytics.details);
    this.setState(
      {
        active: this.state.active === index ? null : index,
        currentPage: index,
        openMenu: false
      },
      this.scrollToPage
    );
  };

  scrollToPage = () => {
    const { currentPage } = this.state;
    this._pageScroller.goToPage(currentPage);
  };
  pageOnChange = number => {
    this.setState({ currentPage: number });
    this.changePage(number - 1);
  };
  popIt = () => {
    this.changePage(this.state.currentPage + 1);
  };
  popModal = () => {
    this.onToggleHitUsUpModal("join");
  };

  popThis = () => {
    Event("ABOUT_CURIOUS", "ABOUT", "ABOUT_PAGE");
    this.setState({ showPage: !this.state.showPage });
  };
  render() {
    const { open } = this.state;
    const renderedData = ABOUT_NAVIGATION_ARRAY[this.state.currentPage];
    return (
      <div className="mainDiv">
        <Head>
          {GTM_SCRIPT_TAG.tag}

          <link
            href="https://fonts.googleapis.com/css?family=Montserrat"
            rel="stylesheet"
          />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          ></meta>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1, width=device-width, height=device-height, viewport-fit=cover"
          />
          <title>Overview of High Peak</title>
          <meta name="url" content="https://www.highpeaksw.com/about-us"></meta>
          <meta name="title" content="Overview of High Peak"></meta>
          <meta
            name="description"
            content="We are a leading technology, web development, and mobile app development company that specializes in delivering quality technology services."
          ></meta>
          <meta
            name="keywords"
            content="High Peak Software, High Peak, AI solutions provider, Product outsourcing, ML solutions, AI products, Bangalore AI services provider, Data science solutions provider, Deep learning services provider, IT software outsourcing"
          ></meta>
        </Head>
        <GTM_NO_SCRIPT />
        <div className="headConatiner">
          <div className="middleContainerSpaceBetween coverIt">
            <a href="/" target="_self">
              <Images
                source={renderedData.colorImage}
                altText="logo"
                class="logoPositionition"
              />
            </a>
            <a
              href="/portfolio"
              className={renderedData.ourStuffClassName}
              target="_blank"
            >
              Our stuff
            </a>
            <button
              onClick={this.onToggleHitUsUpModal}
              className={renderedData.hitUsUpClassName}
            >
              Hit us up!
            </button>
          </div>
          <div
            className="leftNavigationBarAbout coverIt"
            onClick={this.onToggleMenu}
          >
            <MenuSvg navigationColor={renderedData.navigationColor} />
          </div>
        </div>
        <div className="modalTransition">
          <Modal
            open={open}
            onClose={this.onToggleHitUsUpModal}
            showCloseIcon={true}
            center
            styles={{ modal: { outline: "none", animation: "0.2s zoomIn" } }}
          >
            <HitUsUp
              headerName={this.state.joinUs ? "Join Us!" : "Hit Us Up!"}
              toggleHitUsUp={this.onToggleHitUsUpModal}
            />
          </Modal>
        </div>
        <div className="navigationSlider">
          <Modal
            open={this.state.openMenu}
            onClose={this.onToggleMenu}
            styles={{
              overlay: { padding: "0px" },
              modal: {
                maxWidth: "1500px",
                width: "25%",
                marginRight: "0%",
                float: "right",
                height: "94%",
                fontSize: "24px",
                animation: "slide 0.5s forwards",
                outline: "none"
              }
            }}
          >
            <ul className="ulRight">
              {NAVIGATION_ARRAY.map((navObj, index) => (
                <li key={index} className="liMenuSlide">
                  <Link
                    href={{
                      pathname: "./home",
                      query: { section: index }
                    }}
                  >
                    <a className="menuLink">{navObj.title}</a>
                  </Link>
                </li>
              ))}
              <li className="blackColor">
                <a href="/about">Who Are We?</a>
              </li>
            </ul>
          </Modal>
        </div>
        <div className="scrollPart">
          {renderedData.scroll && (
            <div className="scrollButton coverIt">
              <div
                onClick={e => {
                  this.changePage(this.state.currentPage + 1);
                }}
              >
                <ScrollSvg
                  rectangleColor={renderedData.stateScrollRectangle}
                  circleColor={renderedData.stateScrollCircle}
                />
                <div
                  style={{
                    fontSize: "11px",
                    color: renderedData.scrollButtonColor
                  }}
                >
                  Scroll
                </div>
              </div>
            </div>
          )}
        </div>
        <div className="webViewAbout">
          <ReactPageScroller
            ref={c => (this._pageScroller = c)}
            pageOnChange={this.pageOnChange}
          >
            <WhoWeAre />
            <WeArePartner scrollDown={this.popIt} addResume={this.popModal} />
            <OurResident />
            <WeDeliverFreeAI
              navigationArray={NAVIGATION_ARRAY}
              changePage={this.changePage}
            />
          </ReactPageScroller>
        </div>
        <div className="mobileViewAbout">
          <WhoWeAre
            onToggleHitUsUpModal={this.onToggleHitUsUpModal}
            onToggleMenu={this.onToggleMenu}
          />

          {this.state.showPage === true ? (
            <OurResident />
          ) : (
            <WeArePartner scrollDown={this.popThis} addResume={this.popModal} />
          )}
          {this.state.showPage === false ? (
            <OurResident />
          ) : (
            <WeArePartner scrollDown={this.popThis} addResume={this.popModal} />
          )}
          <WeDeliverFreeAI
            navigationArray={NAVIGATION_ARRAY}
            changePage={this.changePage}
          />
        </div>
      </div>
    );
  }
}
export default AboutUsPage;
