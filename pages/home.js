import React from "react";
import Head from "next/head";
import HomePageScroll from "../components/homePageNormalScroll/homePageScroll";
import HomePage from "../components/homePages/homePage";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_SCRIPT,
  GA_WINDOW
} from "../constants/gtmScripts";
import "../styles/index.css";
import "../styles/homePage.css";
import "../styles/dynamiccls.css";
import "../styles/animations.css";

export default class FullPage extends React.Component {
  constructor() {
    super();
    this.state = {
      Responsivewidth: 0
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind();
  }

  static getInitialProps({ query }) {
    return { query };
  }
  updateWindowDimensions = () => {
    this.setState({
      Responsivewidth: window.innerWidth
    });
  };

  componentDidMount = () => {
    GA_SCRIPT();
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
    GA_WINDOW();
  };

  componentWillUnmount = () => {
    window.removeEventListener("resize", this.updateWindowDimensions);
  };

  render() {
    return (
      <div>
        <Head>
          {GTM_SCRIPT_TAG.tag}
          <link
            href="https://fonts.googleapis.com/css?family=Montserrat"
            rel="stylesheet"
          />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          ></meta>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1, width=device-width, height=device-height, viewport-fit=cover"
          />
          <title>High Peak Software - Home</title>
          <meta
            name="description"
            content="High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
          ></meta>
          <meta
            name="keywords"
            content="High Peak Software, High Peak, AI solutions provider, Product outsourcing, ML solutions, AI products, Bangalore AI services provider, Data science solutions provider, Deep learning services provider, IT software outsourcing"
          ></meta>
          <meta property="og:title" content="High Peak Software - Home" />
          <meta property="og:site_name" content="High Peak Software" />
          <meta
            property="og:description"
            content="High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
          />
          <meta property="og:url" content="https://highpeaksw.com" />
          <meta property="og:image" content="../assets/logo-color.png" />
        </Head>
        <GTM_NO_SCRIPT />

        {this.state.Responsivewidth > 1000 && this.state.Responsivewidth !== 0 && (
          <div>
            <HomePage
              querysection={
                this.props.query != undefined ? this.props.query.section : 0
              }
            />
          </div>
        )}
        {this.state.Responsivewidth < 1000 && this.state.Responsivewidth !== 0 && (
          <div>
            <HomePageScroll
              querysection={
                this.props.query != undefined ? this.props.query.section : 0
              }
            />
          </div>
        )}
      </div>
    );
  }
}
