import ReactGA from "react-ga";
import Head from "next/head";
import Images from "../Forms/images";
import Footer from "../components/Footer/footer";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_SCRIPT,
  GA_WINDOW
} from "../constants/gtmScripts";
import "../styles/privacyPolicy.css";

class PrivacyPolicy extends React.Component {
  componentDidMount = () => {
    GA_SCRIPT();
    GA_WINDOW();
  };

  render() {
    return (
      <div className="PrivacyPolicyWrap">
        <Head>{GTM_SCRIPT_TAG.tag}</Head>
        <GTM_NO_SCRIPT />
        <div className="stickyHeaderPolicy">
          <a href="/">
            <Images
              source="../assets/logo-white.png"
              altText="logo"
              class="logoImage"
            />
          </a>

          <div className="headerWrap">
            Privacy Policy
            <div>
              <Images
                class="titleImage"
                source="../assets/titleelement.png"
                altText="title-element"
              />
            </div>
          </div>
        </div>
        <div className="policiesWrap">
          <h1>Welcome to our Privacy Policy</h1>
          <h3>Your privacy is critically important to us.</h3>
          High Peak Software is located at:
          <br />
          <address>
            High Peak Software
            <br />
            4, 17th Cross Rd, Siddanna Layout, Banashankari Stage II, Bangalore
            <br />
            560070 - Karnataka , India
            <br />
            08026760510
          </address>
          <p>
            It is High Peak Software's policy to respect your privacy regarding
            any information we may collect while operating our website. This
            Privacy Policy applies to
            <ReactGA.OutboundLink
              eventLabel="send mail"
              to="https://www.highpeaksw.com/"
            >
              https://www.highpeaksw.com/
            </ReactGA.OutboundLink>
            ( hereinafter, "us", "we", or "https://www.highpeaksw.com/"). We
            respect your privacy and are committed to protecting personally
            identifiable information you may provide us through the Website. We
            have adopted this privacy policy ("Privacy Policy") to explain what
            information may be collected on our Website, how we use this
            information, and under what circumstances we may disclose the
            information to third parties. This Privacy Policy applies only to
            information we collect through the Website and does not apply to our
            collection of information from other sources.
          </p>
          <p>
            This Privacy Policy, together with the Terms and conditions posted
            on our Website, set forth the general rules and policies governing
            your use of our Website. Depending on your activities when visiting
            our Website, you may be required to agree to additional terms and
            conditions.
          </p>
          <br />
          <h2>Website Visitors</h2>
          <p>
            Like most website operators, High Peak Software collects
            non-personally-identifying information of the sort that web browsers
            and servers typically make available, such as the browser type,
            language preference, referring site, and the date and time of each
            visitor request. High Peak Software's purpose in collecting
            non-personally identifying information is to better understand how
            High Peak Software's visitors use its website. From time to time,
            High Peak Software may release non-personally-identifying
            information in the aggregate, e.g., by publishing a report on trends
            in the usage of its website.
          </p>
          <p>
            High Peak Software also collects potentially personally-identifying
            information like Internet Protocol (IP) addresses for logged in
            users and for users leaving comments on https://www.highpeaksw.com/
            blog posts. High Peak Software only discloses logged in user and
            commenter IP addresses under the same circumstances that it uses and
            discloses personally-identifying information as described below.
          </p>
          <br />
          <h2>Gathering of Personally-Identifying Information</h2>
          <p>
            Certain visitors to High Peak Software's websites choose to interact
            with High Peak Software in ways that require High Peak Software to
            gather personally-identifying information. The amount and type of
            information that High Peak Software gathers depends on the nature of
            the interaction. For example, we ask visitors who sign up for a blog
            at https://www.highpeaksw.com/ to provide a username and email
            address.
          </p>
          <br />
          <h2>Security</h2>
          <p>
            The security of your Personal Information is important to us, but
            remember that no method of transmission over the Internet, or method
            of electronic storage is 100% secure. While we strive to use
            commercially acceptable means to protect your Personal Information,
            we cannot guarantee its absolute security.
          </p>
          <br />
          <h2>Advertisements</h2>
          <p>
            Ads appearing on our website may be delivered to users by
            advertising partners, who may set cookies. These cookies allow the
            ad server to recognize your computer each time they send you an
            online advertisement to compile information about you or others who
            use your computer. This information allows ad networks to, among
            other things, deliver targeted advertisements that they believe will
            be of most interest to you. This Privacy Policy covers the use of
            cookies by High Peak Software and does not cover the use of cookies
            by any advertisers.
          </p>
          <br />
          <h2>Links To External Sites</h2>
          <p>
            Our Service may contain links to external sites that are not
            operated by us. If you click on a third party link, you will be
            directed to that third party's site. We strongly advise you to
            review the Privacy Policy and terms and conditions of every site you
            visit.
          </p>
          <p>
            We have no control over, and assume no responsibility for the
            content, privacy policies or practices of any third party sites,
            products or services.
          </p>
          <br />
          <h2>Aggregated Statistics</h2>
          <p>
            High Peak Software may collect statistics about the behavior of
            visitors to its website. High Peak Software may display this
            information publicly or provide it to others. However, High Peak
            Software does not disclose your personally-identifying information.
          </p>
          <br />
          <h2>Cookies</h2>
          <p>
            To enrich and perfect your online experience, High Peak Software
            uses "Cookies", similar technologies and services provided by others
            to display personalized content, appropriate advertising and store
            your preferences on your computer.
          </p>
          <p>
            A cookie is a string of information that a website stores on a
            visitor's computer, and that the visitor's browser provides to the
            website each time the visitor returns. High Peak Software uses
            cookies to help High Peak Software identify and track visitors,
            their usage of https://www.highpeaksw.com/, and their website access
            preferences. High Peak Software visitors who do not wish to have
            cookies placed on their computers should set their browsers to
            refuse cookies before using High Peak Software's websites, with the
            drawback that certain features of High Peak Software's websites may
            not function properly without the aid of cookies.
          </p>
          <p>
            By continuing to navigate our website without changing your cookie
            settings, you hereby acknowledge and agree to High Peak Software's
            use of cookies.
          </p>
          <br />
          <h2>Privacy Policy Changes</h2>
          <p>
            Although most changes are likely to be minor, High Peak Software may
            change its Privacy Policy from time to time, and in High Peak
            Software's sole discretion. High Peak Software encourages visitors
            to frequently check this page for any changes to its Privacy Policy.
            Your continued use of this site after any change in this Privacy
            Policy will constitute your acceptance of such change.
          </p>
        </div>
        <div className="footerTermsWrap">
          <Footer footer={true} />
        </div>
      </div>
    );
  }
}

export default PrivacyPolicy;
