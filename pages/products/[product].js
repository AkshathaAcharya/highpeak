import ReactGA from "react-ga";
import "@babel/polyfill";
import Modal from "react-responsive-modal";
import Carousel from "react-multi-carousel";
import Head from "next/head";
import Images from "../../Forms/images";
import HitUsUp from "../../components/HitUsUp/hitUsUp";
import Footer from "../../components/Footer/footer";
import { PRODUCT_CONSTANTS } from "../../constants/productsConstants";
import {
  GTM_NO_SCRIPT,
  GTM_SCRIPT_TAG,
  GA_SCRIPT,
  GA_WINDOW
} from "../../constants/gtmScripts";
import { Event } from "../../utils/analytics";
import "../../styles/productPage.css";
import "../../components/HitUsUp/hitUsUp.css";
import "react-multi-carousel/lib/styles.css";
class productPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      opendemo: false,
      key: null,
      index: 0,
      height: 0,
      theposition: 0
    };
  }
  static getInitialProps({ query }) {
    return { query };
  }

  onToggleHitUsUpModal = () => {
    this.setState(prevState => ({
      open: !prevState.open
    }));
  };

  onrequestHitUsUpModal = () => {
    this.setState(prevState => ({
      opendemo: !prevState.opendemo
    }));
  };

  componentDidMount = () => {
    GA_SCRIPT();
    window.scrollTo(0, 0);
    const keys =
      PRODUCT_CONSTANTS[this.props.query.product].industries.length != 0
        ? PRODUCT_CONSTANTS[this.props.query.product].industries[0]["industry"]
        : null;
    this.setState({
      index: 0,
      key: keys
    });

    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
    window.addEventListener("scroll", this.listenToScroll, true);
    GA_WINDOW();
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
    window.removeEventListener("scroll", this.listenToScroll);
  }

  updateWindowDimensions = () => {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    });
  };

  listenToScroll = () => {
    const windowScroll =
      document.body.scrollTop || document.documentElement.scrollTop;
    this.setState({
      theposition: windowScroll
    });
  };

  getData = (descriptionIndex, event, detail) => {
    Event("Description", detail.industry, "PRODUCT_PAGE");
    const key =
      PRODUCT_CONSTANTS[this.props.query.product]["industries"][
        descriptionIndex
      ].industry;

    this.setState({
      index: descriptionIndex,
      key: key
    });
  };

  render() {
    const responsive = {
      superLargeDesktop: {
        breakpoint: { max: 4000, min: 3000 },
        items: 5
      },
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
      }
    };
    const filteredProduct = PRODUCT_CONSTANTS[this.props.query.product];
    const { open } = this.state;
    const { opendemo } = this.state;

    return (
      <div className="productPage">
        <Head>
          {GTM_SCRIPT_TAG.tag}

          <link
            href="https://fonts.googleapis.com/css?family=Montserrat"
            rel="stylesheet"
          />
          <title>{filteredProduct.metaTitle}</title>

          <meta
            name="description"
            content={filteredProduct.metaDescription}
          ></meta>
          <meta name="keywords" content={filteredProduct.metaKeywords}></meta>
          <meta property="og:title" content={filteredProduct.metaTitle}></meta>
          <meta
            property="og:description"
            content={filteredProduct.metaDescription}
          ></meta>
          <meta property="og:url" content={filteredProduct.openGraphURL}></meta>
          <meta property="og:site_name" content="High Peak Software" />
          <meta
            property="og:image"
            content={filteredProduct.productImage}
          ></meta>
        </Head>
        <GTM_NO_SCRIPT />
        <div className="StickyDiv">
          <a href="/" target="_self">
            <Images
              source="../assets/logo-white.png"
              altText="logo"
              class="logoImage"
            />
          </a>
          <span
            className={
              this.state.theposition > this.state.height
                ? "scrollProject"
                : "hideproject"
            }
          >
            {filteredProduct.productName}
            <Images
              class="titleImageProduct"
              source="../assets/titleelement.png"
              altText="title-element"
            />
          </span>
          <button
            onClick={this.onrequestHitUsUpModal}
            className="requestForDemo"
          >
            Request demo
          </button>
        </div>
        <div className="modalTransition">
          <Modal
            open={open}
            onClose={this.onToggleHitUsUpModal}
            showCloseIcon={true}
            center
            styles={{ modal: { outline: "none", animation: "0.2s zoomIn" } }}
          >
            <HitUsUp
              togglehitusup={this.onToggleHitUsUpModal}
              headerName="Hit Us Up!"
            />
          </Modal>
        </div>
        <div className="modalTransition">
          <Modal
            open={opendemo}
            onClose={this.onrequestHitUsUpModal}
            showCloseIcon={true}
            center
            styles={{ modal: { outline: "none", animation: "0.2s zoomIn" } }}
          >
            <HitUsUp
              togglehitusup={this.onToggleHitUsUpModal}
              headerName="Demo!"
            />
          </Modal>
        </div>
        <div
          className="productionMobileview"
          style={{
            height: this.state.height + 400 + "px",
            position: "relative"
          }}
        >
          <div className="firstproductDiv">
            <div className="titleDiv">
              <section className="projectName">
                {filteredProduct.productName}
              </section>
              <section className="subTitle">{filteredProduct.subTitle}</section>
              <Images
                class="titleImageProduct"
                source="../assets/titleelement.png"
                altText="title-element"
              />
            </div>
            <img className="productImage" src={filteredProduct.productImage} />
          </div>

          <div className="secondDiv">
            <div className="alignCenterContainer">
              {filteredProduct.productDescription}
            </div>
          </div>
        </div>
        {filteredProduct.flowChartSteps.length > 0 && (
          <div className="secondSectionMainDiv">
            {filteredProduct.flowChartHeading}
            <Images
              class="titleImageProduct"
              source="../assets/titleelement.png"
              altText="title-element"
            />
            <div className="secondSection">
              {filteredProduct.flowChartSteps.map((datas, index) => (
                <div key={index} className="secondSectiondata">
                  <div className="innerDivData">
                    <section className="innerDataSection">
                      <span className="numbers">{datas.stepNumber}</span>
                      <span className="dataSteps">{datas.stepDetails}</span>
                    </section>
                    <div className="pseudoDiv"></div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
        <div className="thirdSectionProduct">
          <div className="thirdSectionHead">
            {filteredProduct.productInAction}
          </div>
          <Images
            class="titleImageAction"
            source="../assets/titleelement.png"
            altText="title-element"
          />
          <div className="thirdSectionContentProduct">
            {filteredProduct.actions.map((data, index) => (
              <div key={index} className="thirdSectionDataProduct">
                <div
                  className={
                    filteredProduct.mobileOrWebsite === "Website"
                      ? "imageWrapDivProduct"
                      : "mobimageWrapDivProduct"
                  }
                >
                  <img className="actionImagesProduct" src={data.img} />
                </div>
                <div className="thirdSectionDescriptionWrapProduct">
                  {data.action.map((datas, ind) => (
                    <div key={ind} className="thirdSectionDescriptionProduct">
                      <span style={{ float: "left" }}>{datas}</span>
                      <hr />
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </div>
        {filteredProduct.benefits.length != 0 && (
          <div className="fourthSectionEnables">
            {filteredProduct.productEnablesYouTo}
            <Images
              class="titleImageProduct"
              source="../assets/titleelement.png"
              altText="title-element"
            />
            <div className="productEnablesDiv">
              {filteredProduct.benefits.map((description, index) => (
                <div key={index} className="productEnablesEachDiv">
                  {description.benefit}
                  <div className="fourthSectionDescription">
                    {description.benefitDescription}
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
        {filteredProduct.industries.length != 0 && (
          <div className="fifthSection">
            <span className="fifthHead">
              {filteredProduct.productCanAccuratelyCaptureData}
              <Images
                class="titleImageProduct"
                source="../assets/titleelement.png"
                altText="title-element"
              />
            </span>
            <div className="fifthSectionwrap">
              <div className="fifthSectionLeft">
                {filteredProduct.industries.map((key, ind) => (
                  <div
                    key={ind}
                    className={
                      this.state.key === key.industry
                        ? "activeDiv fifthSectionEachDiv"
                        : "fifthSectionEachDiv"
                    }
                    key={key.industry}
                    onClick={e => {
                      this.getData(ind, e, key);
                    }}
                  >
                    {key.industry}
                  </div>
                ))}
              </div>
              <div className="fifthSectionRight">
                <ul>
                  {filteredProduct.industries[this.state.index]["useCases"].map(
                    (data, index) => (
                      <div key={index} className="useCases">
                        <li>{data}</li>
                      </div>
                    )
                  )}
                </ul>
              </div>
            </div>
          </div>
        )}
        <div className="seventhSection">
          {filteredProduct.productRunsOn}
          <div className="seventhSectionImages">
            {filteredProduct.technologies.map((technologies, index) => (
              <div key={index}>
                <div className="runsOnTechnology technologyUsed">
                  {technologies.technology}
                </div>
              </div>
            ))}
          </div>
        </div>
        {filteredProduct.carousel.length != 0 && (
          <div className="prevIconDown">
            <div style={{ marginTop: "196px" }}>
              <div className="eightssectionSlider">
                <div className="sliderWrap">
                  <Carousel responsive={responsive} infinite={true}>
                    {filteredProduct.carousel.map((corousel, index) => (
                      <ReactGA.OutboundLink
                        key={index}
                        eventLabel={corousel.label}
                        to={corousel.url}
                        target="_blank"
                        className="removeLines"
                      >
                        <div className="eachCorosel">
                          <img src={corousel.img} />
                          <div className="corouselTitle">{corousel.title}</div>
                        </div>
                      </ReactGA.OutboundLink>
                    ))}
                  </Carousel>
                </div>
              </div>
            </div>
          </div>
        )}
        <div className="footerProductWrap">
          <Footer footer={true} />
        </div>
      </div>
    );
  }
}

export default productPage;
