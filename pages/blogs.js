import React from "react";
import Modal from "react-responsive-modal";
import { connect } from "react-redux";
import Link from "next/link";
import MenuSvg from "../components/menu_svg";
import Images from "../Forms/images";
import Footer from "../components/Footer/footer";
import {
  getBlogs,
  getCategories,
  setFlag
} from "./../constants/getBlogsActions";
import { NAVIGATION_ARRAY } from "../constants/constants";
import "../styles/blogs.css";
import highpeakImg from "../public/assets/highpeakLogo.png";

export const FILTERS = ["All", "AI", "products", "services"];

const throttle = (fn, wait) => {
  var time = Date.now();
  return function() {
    if (time + wait - Date.now() < 0) {
      fn();
      time = Date.now();
    }
  };
};

class Blogs extends React.Component {
  constructor() {
    super();
    this.state = {
      openMenu: false,
      activeMenu: 0,
      selectedFilter: "All",
      perPage: 9,
      pageNo: 1
    };
    this.handleScrollEvent = throttle(this.handleScroll, 500);
  }
  componentDidMount() {
    this.props.setFlag(true);
    this.props.getCategories();
    this.props.getBlogs({
      perPage: this.state.perPage,
      pageNo: this.state.pageNo
    });
    document.body.addEventListener("scroll", this.handleScrollEvent, true);
  }

  handleScroll = () => {
    const { isFetch } = this.props;
    const { perPage, pageNo } = this.state;
    let pixelsFromWindowBottomToBottom =
      0 +
      document.body.scrollHeight -
      document.body.scrollTop -
      document.body.offsetHeight;

    if (pixelsFromWindowBottomToBottom < 500 && isFetch) {
      let data = {
        perPage: perPage,
        pageNo: pageNo + 1
      };

      this.setState(
        prevState => ({
          perPage: prevState.perPage,
          pageNo: prevState.pageNo + 1
        }),
        () => this.props.getBlogs(data)
      );
    }
  };

  onToggleMenu = () => {
    this.setState({ openMenu: !this.state.openMenu });
  };

  checkFilter = (menuIndex, event, filter) => {
    let data = {};

    if (filter === "All") {
      data = {
        selectedFilter: "All",
        activeMenu: 0
      };
    } else {
      data = {
        selectedFilter: filter,
        activeMenu: menuIndex
      };
    }
    this.setState(data);
  };

  render() {
    const { posts, categories, groupedByCategories } = this.props;
    const { activeMenu, selectedFilter } = this.state;

    let postsToDisplay = [];

    if (selectedFilter === "All") {
      groupedByCategories &&
        Object.keys(groupedByCategories).map(post => {
          if (post === "posts") {
            postsToDisplay = groupedByCategories[post];
          }
        });
    } else {
      groupedByCategories &&
        Object.keys(groupedByCategories).map(post => {
          if (parseInt(post) === activeMenu) {
            postsToDisplay = groupedByCategories[activeMenu].blogs;
          }
        });
    }

    return (
      <div className="blogsWrap">
        <div className="blogMainDiv">
          <div className="navigationSlider">
            <Modal
              open={this.state.openMenu}
              onClose={this.onToggleMenu}
              styles={{
                overlay: { padding: "0px" },
                modal: {
                  maxWidth: "1500px",
                  width: "25%",
                  marginRight: "0%",
                  float: "right",
                  height: "94%",
                  fontSize: "24px",
                  animation: "slide 0.5s forwards",
                  outline: "none"
                }
              }}
            >
              <ul className="ulRight">
                {NAVIGATION_ARRAY.map((navObj, index) => (
                  <div key={index}>
                    <li className="liMenuSlide">
                      <Link
                        href={{
                          pathname: "./home",
                          query: { section: index }
                        }}
                      >
                        <a key={index} className="menulink">
                          {navObj.title !== "We Deliver Free AI Stuff" &&
                            navObj.title}
                        </a>
                      </Link>
                    </li>
                  </div>
                ))}
              </ul>
            </Modal>
          </div>
          <div className="blogHead">
            <a href="/">
              <Images
                source="../assets/logo-color.png"
                altText="logo"
                class="logoImg"
              />
            </a>

            <span className="leftNavigationBarBlog" onClick={this.onToggleMenu}>
              <MenuSvg navigationColor={NAVIGATION_ARRAY[0].navigationColor} />
            </span>
          </div>

          <div className="blogsWrapHead">
            <div className="ourblog">
              Our
              <span className="blogSpan"> Blogs </span>
              <span className="blogTitleImage">
                <Images
                  class="titleImageBlog"
                  source="../assets/titleelement.png"
                  altText="title-element"
                />
              </span>
            </div>
          </div>
          <div className="ourStuffMenus">
            {postsToDisplay.length > 0 &&
              categories &&
              categories.map((navigation, index) => {
                return (
                  <button
                    key={index}
                    className={
                      navigation.id === activeMenu ? "activeTab" : "Menu"
                    }
                    onClick={e => {
                      this.checkFilter(navigation.id, e, navigation.name);
                    }}
                  >
                    {navigation.name.charAt(0).toUpperCase() +
                      navigation.name.slice(1)}
                  </button>
                );
              })}
          </div>
          <div className="wrappedDivBlogs">
            {postsToDisplay.map((post, index) => {
              return (
                <div key={index} className="eachBlogWidth">
                  <a
                    href={post.redirectLink}
                    className="linkedBlog"
                    target="_blank"
                  >
                    <span>
                      <Images
                        class="postImage"
                        source={post.featuredImage || highpeakImg}
                        altText="postImage"
                      />

                      <div className="blogTitleheight">
                        <section className="title">{post.title}</section>
                      </div>
                    </span>
                  </a>
                </div>
              );
            })}
            {!postsToDisplay.length && (
              <div className="message">We will be shortly with you..</div>
            )}
          </div>
        </div>
        <div className="footerPortfolioWrap">
          <Footer footer={true} />
        </div>
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {
    getBlogs: (...category) => dispatch(getBlogs(...category)),
    getCategories: () => dispatch(getCategories()),
    setFlag: data => dispatch(setFlag(data))
  };
};

const stateToProps = state => {
  return {
    categories: state.BlogsReducer.categories,
    groupedByCategories: state.BlogsReducer.groupedByCategories,
    isFetch: state.BlogsReducer.isFetch
  };
};

export default connect(stateToProps, dispachToProps)(Blogs);
