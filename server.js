const express = require("express");
const next = require("next");
const multer = require("multer");
const bodyParser = require("body-parser");
const port = parseInt(process.env.PORT, 10) || 3090;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const cors = require("cors");
const nodemailer = require("nodemailer");
const smtpTransport = require("nodemailer-smtp-transport");
const storage = multer.diskStorage({
  destination: function(req, file, callBack) {
    callBack(null, "public/upload/");
  },
  filename: function(req, file, callBack) {
    callBack(null, Date.now() + "-" + file.originalname);
  }
});
const upload = multer({ storage: storage }).single("file");
const transporter = nodemailer.createTransport(
  smtpTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
      user: "noreply@highpeaksw.com",
      pass: "HP$W1234"
    }
  })
);

const transporterUser = nodemailer.createTransport(
  smtpTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
      user: "marketing@highpeaksw.com",
      pass: "HP$W1234"
    }
  })
);
const transporterUserHr = nodemailer.createTransport(
  smtpTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
      user: "hrteam@highpeaksw.com",
      pass: "HRH!ghP3@k"
    }
  })
);
app
  .prepare()
  .then(() => {
    const server = express();

    server.use(bodyParser.urlencoded({ extended: true }));
    server.use(bodyParser.json());

    server.get("*", (req, res) => {
      handle(req, res);
    });

    server.use(cors());
    server.post("/upload", function(req, res) {
      upload(req, res, function(err) {
        if (err instanceof multer.MulterError) {
          return res.status(500).json(err);
        } else if (err) {
          return res.status(500).json(err);
        }
        return res.status(200).send(req.file);
      });
    });
    server.post("/send", (req, res) => {
      if (
        `${req.body.Filename}` !== "noFile" &&
        `${req.body.Path}` !== "noPath"
      ) {
        const mailOptions = {
          from: "noreply@highpeaksw.com",
          to: `hr@highpeaksw.com`,
          subject: "New candidate resume",
          text: `Hi team,

You have a new candidate. Please see the below message for details. The resume is attached with the mail.

Name: ${req.body.Name}
Email ID:  ${req.body.Email}
Applying for the position of: ${req.body.Comments}

Regards,
The High Peak Team
`,
          attachments: [
            {
              filename: `${req.body.Filename}`,
              path: `${req.body.Path}`
            }
          ]
        };
        const mailOptionsUserHr = {
          from: '"The High Peak Team" <hrteam@highpeaksw.com>',
          to: `${req.body.Email}`,
          subject: `${req.body.Name}, we will get in touch with you shortly.`,
          html: `Hi ${req.body.Name},<br/><br/>
  Thank you for expressing interest in High Peak. We have received your message. <br/>
  We will get in touch with you shortly. <br/><br/>

  In case of any follow-up queries, please write back to us on this same mail.
  <br/><br/>
  Looking forward to partnering with you, <br/>
  High Peak Software, <br/>
  Your partner-in-AI.  <br/>
  file:${req.body.Comments}
  `
        };

        transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent: " + info.response);
          }
        });
        transporterUserHr.sendMail(mailOptionsUserHr, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent: " + info.response);
          }
        });
      }
      if (
        `${req.body.Filename}` === "noFile" &&
        `${req.body.Path}` === "noPath"
      ) {
        const mailOptions = {
          from: "noreply@highpeaksw.com",
          to: `marketing@highpeaksw.com`,
          subject: "You’ve Got Mail!",
          text: `Hi team,

Congratulations! You have a new message from

Name: ${req.body.Name}
Email ID:  ${req.body.Email}
Message:  ${req.body.Comments}

Chase this prospect,
High Peak.
`
        };
        const mailOptionsUser = {
          from: '"The High Peak Team" <marketing@highpeaksw.com>',
          to: `${req.body.Email}`,
          subject: `${req.body.Name}, we will get in touch with you shortly.`,
          html: `Hi ${req.body.Name},<br/><br/>
  Thank you for expressing interest in High Peak. We have received your message. <br/>
  We will get in touch with you shortly. <br/><br/>

  In case of any follow-up queries, please write back to us on this same mail.
  <br/><br/>
  Looking forward to partnering with you, <br/>
  High Peak Software, <br/>
  Your partner-in-AI.  <br/>
  file:${req.body.Comments}
  `
        };

        transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent: " + info.response);
          }
        });
        transporterUser.sendMail(mailOptionsUser, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent: " + info.response);
          }
        });
      }
    });

    server.listen(port, () => console.log(`server stated on port ${port}`));
  })
  .catch(ex => {
    console.log(ex.stack);
    process.exit(1);
  });
