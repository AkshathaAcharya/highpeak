const Image = props => {
  return (
    <img className={props.class || ""} src={props.source} alt={props.altText} />
  );
};

export default Image;
