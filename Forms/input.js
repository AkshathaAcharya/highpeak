const Input = props => {
  const { onBlur, type, classname, placeholder, value, onChange } = props;
  return (
    <input
      type={type}
      className={classname}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      required
    />
  );
};

export default Input;
