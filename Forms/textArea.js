import React from "react";
 function Textarea(props) {
    const {classname,placeholder} = props;
    return (
	 <textarea
	   className={classname}
	   placeholder={placeholder}
	 > </textarea>
    );

}

export default Textarea;
