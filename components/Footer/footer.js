import Modal from "react-responsive-modal";
import ReactGA from "react-ga";
import Link from "next/link";
import HitUsUp from "../HitUsUp/hitUsUp";
import { FOOTER_ICONS, NAVIGATION_ARRAY } from "../../constants/constants";
import "../Footer/footer.css";
import "../../styles/index.css";

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  onToggleHitUsUpModal = () => {
    this.setState({ open: !this.state.open });
  };

  render() {
    const { open } = this.state;
    const footer = NAVIGATION_ARRAY.map(
      (navObj, index) =>
        navObj.title !== "" &&
        navObj.title !== "Home" &&
        (this.props.homeFooter ? (
          <a
            className="footerRemoveLines"
            key={index}
            onClick={e => {
              this.props.changePage(index);
            }}
          >
            {navObj.title}
          </a>
        ) : (
          <Link
            as="/"
            key={index}
            href={{ pathname: "/home", query: { section: index } }}
            target="_blank"
          >
            {navObj.title}
          </Link>
        ))
    );
    return (
      <>
        {this.props.footer && (
          <div className="footerWrap">
            <div className="footerNavigationLinks">
              {footer}
              <a href="/about" target="_self">
                Who are we?
              </a>
            </div>
            <div className="CTAButtonsFooter">
              <a
                href="/portfolio"
                target="_blank"
                className="ourStuffButtonFooter"
              >
                Our stuff
              </a>

              <button
                className="hitUsUpLink menuHitUsUpLink"
                onClick={this.onToggleHitUsUpModal}
              >
                <b className="footerHoverLine footerHitUsUp">Hit us up!</b>
              </button>
            </div>

            <div className="modalTransition">
              <Modal
                open={open}
                onClose={this.onToggleHitUsUpModal}
                showCloseIcon={true}
                center
                styles={{
                  modal: {
                    outline: "none",
                    animation: "0.2s zoomIn"
                  }
                }}
              >
                <HitUsUp
                  togglehitusup={this.onToggleHitUsUpModal}
                  headerName="Hit Us Up!"
                />
              </Modal>
            </div>

            <div className="weAreEveryWhere">
              We are everywhere. Come, say hi!
            </div>
            <div className="footerFaIconsWrap">
              {Object.values(FOOTER_ICONS).map((item, index) => (
                <ReactGA.OutboundLink
                  className="iconLinks"
                  key={index}
                  eventLabel={item.label}
                  to={item.navigationURL}
                  target="_blank"
                >
                  <img src={item.img} />
                </ReactGA.OutboundLink>
              ))}
            </div>
            <div className="horizontalDiv">
              <span className="copyRights">
                Copyright 2020, High Peak Software. All rights reserved
              </span>
              <span className="PrivacyPolicyTermsConditions">
                <a
                  href="/termsConditions"
                  target="_self"
                  className="termsConditions"
                >
                  Terms and Conditions
                </a>
                <a href="/privacyPolicy" target="_self">
                  Privacy Policy
                </a>
              </span>
            </div>
          </div>
        )}
      </>
    );
  }
}

export default Footer;
