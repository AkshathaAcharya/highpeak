import Modal from "react-responsive-modal";
import ReactPageScroller from "../pageScroller";
import HomeLanding from "../HomeLanding/homeLanding";
import WeMakeCoolAI from "../WeMakeCoolAI/weMakeCoolAI";
import WeSolveToughAI from "../WeSolveToughAI/weSolveToughAI";
import GetFreeAI from "../GetFreeAI/getFreeAI";
import ScrollSvg from "../scroll_svg";
import MenuSvg from "../menu_svg";
import HitUsUp from "../HitUsUp/hitUsUp";
import WeDeliverFreeAI from "../WeDeliverFreeAI/weDeliverFreeAI";
import Image from "../../Forms/images";
import { NAVIGATION_ARRAY } from "../../constants/constants";
import { GOOGLE_ANALYTICS_CONSTANTS } from "../../constants/googleAnalyticsConstants";
import { GA_SCRIPT } from "../../constants/gtmScripts";
import { Event } from "../../utils/analytics";
import "../../styles/index.css";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
      active: null,
      open: false,
      openMenu: false,
      hoveredIndex: "",
      sectionScroll: 0,
      scrollPageStart: false,
      pageName: "home"
    };
    this._pageScroller = null;
  }

  componentWillMount() {
    GA_SCRIPT();
  }

  onToggleHitUsUpModal = () => {
    this.setState(prevState => ({
      open: !prevState.open
    }));
  };

  onToggleMenu = () => {
    this.setState(prevState => ({
      openMenu: !prevState.openMenu
    }));
  };

  changePage = index => {
    if (index) {
      const pagesAnalytics =
        GOOGLE_ANALYTICS_CONSTANTS[this.state.pageName][index];
      Event(pagesAnalytics.event, pagesAnalytics.page, pagesAnalytics.details);
    }
    this.setState(
      {
        active: this.state.active === index ? null : index,
        currentPage: index,
        openMenu: false
      },
      this.scrollToPage
    );
  };
  scrollToPage = () => {
    const { currentPage } = this.state;
    this._pageScroller.goToPage(currentPage);
  };
  navColor = position => {
    const { active, currentPage } = this.state;
    if (active === position || position === currentPage) {
      return "2px solid #FECE2B";
    }
    return " ";
  };

  pageOnChange = number => {
    this.setState({ currentPage: number });
    this.changePage(number - 1);
    this.navColor(number - 1);
  };

  componentDidMount = () => {
    const keyToScroll = this.props.querysection;
    const one = String(keyToScroll).charAt(0);
    this.changePage(Number(one));
  };

  navMenuMouseOver = (event, index) => {
    this.setState({ hoveredIndex: index });
  };

  navMenuMouseLeave = () => {
    this.setState({ hoveredIndex: "" });
  };

  render() {
    const { open } = this.state;
    const renderedArray = NAVIGATION_ARRAY[this.state.currentPage];
    const logo = (
      <Image
        class="logoPosition"
        source={renderedArray.colorImage}
        altText="logo"
      />
    );
    return (
      <div>
        <div className="mainDiv">
          <div className="footerNavigation">
            <div className="vl"> </div>
            <div style={{ height: "100%" }}>
              <ul className="OrderNavigation">
                {NAVIGATION_ARRAY.map(
                  (navObj, index) =>
                    navObj.title != "" && (
                      <div
                        key={index}
                        className="eachListDiv"
                        onClick={e => {
                          this.changePage(index);
                        }}
                        onMouseOver={e => {
                          this.navMenuMouseOver(e, index);
                        }}
                        onMouseLeave={this.navMenuMouseLeave}
                      >
                        <li className="NavList">
                          <a
                            href={navObj.hashurl}
                            style={{ borderLeft: this.navColor(index) }}
                            className="anchorNavigation"
                          >
                            {this.state.hoveredIndex === index && navObj.title}
                          </a>
                        </li>
                      </div>
                    )
                )}
              </ul>
            </div>

            <div className="bottomVlDiv">
              <div className="bottomVl"></div>
            </div>
          </div>
          {renderedArray.scroll && (
            <div
              className="scrollButtonHome"
              onClick={e => {
                this.changePage(this.state.currentPage + 1);
              }}
            >
              <ScrollSvg
                rectangleColor={renderedArray.stateScrollRectangle}
                circleColor={renderedArray.stateScrollCircle}
              />
              <div
                style={{
                  marginLeft: "-3px",
                  fontSize: "11px",
                  color: renderedArray.scrollButtonColor
                }}
              >
                Scroll
              </div>
            </div>
          )}
          <div className="topNavigation">
            <a href="/" target="_self">
              {logo}
            </a>
            <a
              className={renderedArray.classNameOurStuffButton}
              href="/portfolio"
              target="_blank"
            >
              Our stuff
            </a>
            <button
              onClick={this.onToggleHitUsUpModal}
              className={renderedArray.classNameHitUsUp}
            >
              Hit us up!
            </button>

            <div className="leftNavigationBar" onClick={this.onToggleMenu}>
              <MenuSvg navigationColor={renderedArray.navigationColor} />
            </div>
          </div>
          <div className="modalTransition">
            <Modal
              open={open}
              onClose={this.onToggleHitUsUpModal}
              showCloseIcon={true}
              center
              styles={{ modal: { outline: "none", animation: "0.2s zoomIn" } }}
            >
              <HitUsUp
                headerName="Hit Us Up!"
                togglehitusup={this.onToggleHitUsUpModal}
              />
            </Modal>
          </div>

          <div className="navigationSlider">
            <Modal
              open={this.state.openMenu}
              onClose={this.onToggleMenu}
              styles={{
                overlay: { padding: "0px" },
                modal: {
                  maxWidth: "1500px",
                  width: "25%",
                  marginRight: "0%",
                  float: "right",
                  height: "94%",
                  fontSize: "24px",
                  animation: "slide 0.5s forwards",
                  outline: "none"
                }
              }}
            >
              <ul className="ulRight">
                {NAVIGATION_ARRAY.map((navObj, index) => (
                  <li key={index} className="liMenuSlide">
                    <a
                      className={
                        this.state.currentPage === index
                          ? "menulink activeMenu"
                          : "menulink"
                      }
                      onClick={e => {
                        this.changePage(index);
                      }}
                    >
                      {navObj.title}
                    </a>
                  </li>
                ))}
                <li className="blackColor">
                  <a href="/about" target="_self">
                    Who Are We?
                  </a>
                </li>
              </ul>
            </Modal>
          </div>

          <ReactPageScroller
            ref={c => (this._pageScroller = c)}
            pageOnChange={this.pageOnChange}
          >
            <HomeLanding />
            <WeSolveToughAI />
            <WeMakeCoolAI />
            <GetFreeAI />
            <WeDeliverFreeAI
              navigation_Array={NAVIGATION_ARRAY}
              changePage={this.changePage}
              homeFooter={true}
            />
          </ReactPageScroller>
        </div>
      </div>
    );
  }
}

export default HomePage;
