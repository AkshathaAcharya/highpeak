import AwesomeSlider from "react-awesome-slider";
import withAutoplay from "react-awesome-slider/dist/autoplay";
import Head from "next/head";
import Image from "../../Forms/images";
import { PROJECTS_ARRAY, ROTATING_WORDS } from "../../constants/constants.js";
import "../WeMakeCoolAI/weMakeCoolAI.css";
import "react-awesome-slider/dist/styles.css";
const AutoplaySlider = withAutoplay(AwesomeSlider);

class WeMakeCoolAI extends React.Component {
  render() {
    return (
      <div className="component thirdComponent">
        <div className="thirdCompMobile">
          <Head>
            <title>High Peak Software - Home</title>
            <meta
              name="description"
              content="High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
            ></meta>
            <meta
              name="keywords"
              content="AI products, ML products, ML solutions. Web applications. Mobile applications. Emerging technologies, Saas products, "
            />
            <meta property="og:title" content="High Peak Software - Home" />
            <meta property="og:site_name" content="High Peak Software" />
            <meta
              property="og:description"
              content="High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
            />
            <meta property="og:url" content="https://highpeaksw.com" />
            <meta property="og:image" content="../assets/logo-color.png" />
            <meta
              name="viewport"
              content="width=device-width,height=device-height,initial-scale=1.0"
            />
          </Head>
          <div className="columnWise">
            <div className="thirdComponentSlider">
              <AutoplaySlider
                play={true}
                infinite={true}
                cancelOnInteraction={false}
                interval={3000}
              >
                {PROJECTS_ARRAY.map((proj, index) => (
                  <div key={index} className="groupContainer">
                    <div className="ImageWrapCoolAI">
                      <a href={proj.url}>
                        <Image
                          source={proj.projScreen}
                          altText="project-screenshots"
                        />
                      </a>
                    </div>
                    <div className="slideProjTitle">
                      <strong>{proj.title}</strong>
                      <div className="projSubTitle">{proj.subTitle}</div>
                    </div>
                    <div className="responsiveNumber">
                      {proj.responsiveNumber}
                    </div>
                  </div>
                ))}
              </AutoplaySlider>
            </div>
            <div className="ourIntraAtWrk">
              Our
              <div class="rotatingWords rotatingWordsOne rwIntratwork">
                {ROTATING_WORDS.map((words, index) => (
                  <span key={index}>
                    <pre>{words.weMakeCoolAI}</pre>
                  </span>
                ))}
              </div>
              <section>at work</section>
            </div>
          </div>
        </div>
        <div className="weMakeCool">
          <span>
            We make <strong>cool</strong> AI stuff
          </span>
          <Image
            class="weMakeCoolTitleImage"
            source="../assets/titleelement.png"
            altText="title-element"
          />
        </div>
      </div>
    );
  }
}

export default WeMakeCoolAI;
