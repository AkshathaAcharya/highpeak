import ReactCardFlip from "react-card-flip";
class Sixcards extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="marketingWrap">
        <div className="marketingStuff MoreDivs">
          {this.props.filteredSolutions.flipCards.map((marketing, index) => (
            <ReactCardFlip
              key={index}
              isFlipped={this.props.indexToFlip === index}
              flipDirection="horizontal"
            >
              <div
                className="marketingStuffEachDiv"
                style={{ backgroundImage: marketing.backGroundImage }}
                onClick={event => {
                  this.props.handleClick(event, index);
                }}
              >
                <span className="marketingSpan">{marketing.frontCard}</span>
              </div>
              <div
                className="marketingStuffEachDiv backCardFlip"
                onClick={event => {
                  this.props.handleClick(event, index);
                }}
              >
                <ul className="ulLists">
                  {marketing.backCard.map((array, indexes) => (
                    <li key={indexes}>{array}</li>
                  ))}
                </ul>
              </div>
            </ReactCardFlip>
          ))}
        </div>
      </div>
    );
  }
}

export default Sixcards;
