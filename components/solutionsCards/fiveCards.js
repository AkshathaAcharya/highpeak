import ReactCardFlip from "react-card-flip";
class Fivecards extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="marketingWrap">
        <div className="marketingStuff">
          {this.props.filteredSolutions.flipCards.map(
            (solutions, index) =>
              index !== 4 && (
                <ReactCardFlip
                  key={index}
                  isFlipped={this.props.indexToFlip === index}
                  flipDirection="horizontal"
                >
                  <div
                    className="marketingStuffEachDiv"
                    style={{
                      backgroundImage: solutions.backGroundImage
                    }}
                    onClick={event => {
                      this.props.handleClick(event, index);
                    }}
                  >
                    <span className="marketingSpan">{solutions.frontCard}</span>
                  </div>
                  <div
                    className="marketingStuffEachDiv backCardFlip"
                    onClick={event => {
                      this.props.handleClick(event, index);
                    }}
                  >
                    <ul
                      className="ulLists"
                      onClick={event => {
                        this.props.handleClick(event, index);
                      }}
                    >
                      {solutions.backCard.map((array, indexes) => (
                        <li key={indexes}>{array}</li>
                      ))}
                    </ul>
                  </div>
                </ReactCardFlip>
              )
          )}
        </div>
        <div className="contentMarketingWrap">
          {this.props.filteredSolutions.flipCards.map(
            (solutions, index) =>
              index === 4 && (
                <ReactCardFlip
                  key={index}
                  isFlipped={this.props.indexToFlip === index}
                  flipDirection="horizontal"
                >
                  <div
                    className="contentMarketing"
                    style={{
                      backgroundImage: solutions.backGroundImage
                    }}
                    onClick={event => {
                      this.props.handleClick(event, index);
                    }}
                  >
                    <span className="marketingSpan">{solutions.frontCard}</span>
                  </div>
                  <div
                    className="contentMarketing backCardFlip"
                    onClick={event => {
                      this.props.handleClick(event, index);
                    }}
                  >
                    <ul className="ulLists">
                      {solutions.backCard.map((array, indexes) => (
                        <li key={indexes}>{array}</li>
                      ))}
                    </ul>
                  </div>
                </ReactCardFlip>
              )
          )}
        </div>
      </div>
    );
  }
}

export default Fivecards;
