import Slider from "react-slick";
import SlickNextArrow from "../slickNextArrow";
import SlickPrevArrow from "../slickPrevArrow";
import Image from "../../Forms/images";
import { FIRSTSLIDECONTENT, SECSLIDECONTENT } from "../../constants/constants";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
class WeHaveProof extends React.Component {
  constructor() {
    super();
    this.state = {
      nav1: null,
      nav2: null
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }
  render() {
    return (
      <div className="component fourth-component">
        <div className="weHaveProof">
          We have <strong>proof</strong>!
          <Image
            class="weHaveProofTitleImage "
            source="../assets/titleelement.png"
            altText="title-element"
          />
        </div>
        <div className="slickWrap">
          <div className="testimonialQuote">
            <Image
              source="../assets/testimonials-quote.png"
              altText="testimonial-quote"
            />
          </div>
          <div className="fourthSlick">
            <div className="fourthComponentSlider">
              <Slider
                nextArrow={<SlickNextArrow topAlign="" />}
                prevArrow={<SlickPrevArrow topAlign="" />}
                asNavFor={this.state.nav2}
                ref={slider => (this.slider1 = slider)}
                autoplay={false}
              >
                {FIRSTSLIDECONTENT.map((content, index) => (
                  <div key={index} className="testimonialContentTwoent">
                    {content.testimonial}
                  </div>
                ))}
              </Slider>
            </div>

            <div className="fourthComponentSlickSlider">
              <Slider
                asNavFor={this.state.nav1}
                ref={slider => (this.slider2 = slider)}
                slidesToShow={5}
                swipeToSlide={true}
                focusOnSelect={true}
                centerMode={true}
                autoplay={true}
                infinite={true}
                centerPadding={"0px"}
              >
                {SECSLIDECONTENT.map((content, index) => (
                  <div key={index}>
                    <Image
                      class="slickSlideImage"
                      source={content.image}
                      altText="Images"
                    />
                    <div className="theirName" style={{ display: "none" }}>
                      {content.clientName}
                    </div>
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>

        <div className="thatsWhatTheySay">
          That's what <span style={{ color: "#FED600" }}>he</span>&nbsp;said
        </div>
      </div>
    );
  }
}

export default WeHaveProof;
