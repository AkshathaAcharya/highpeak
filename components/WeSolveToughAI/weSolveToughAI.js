import Head from "next/head";
import Link from "next/link";
import Image from "../../Forms/images";
import { WE_SOLVE_TOUGH_AI, ROTATING_WORDS } from "../../constants/constants";
import "../WeSolveToughAI/weSolveToughAI.css";

export default () => {
  return (
    <div className="component secondComponent">
      <Head>
        <title>High Peak Software - Home</title>
        <meta
          name="description"
          content="High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
        ></meta>
        <meta
          name="keywords"
          content="AI solutions, Machine Learning, Predictive Analytics, Deep Learning, Data Science, Natural Language Processing, NLP, ML, DL, Artificial Intelligence, Digital Marketing, UX/UI design"
        />
        <meta property="og:title" content="High Peak Software - Home" />
        <meta property="og:site_name" content="High Peak Software" />
        <meta
          property="og:description"
          content="High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
        />
        <meta property="og:url" content="https://highpeaksw.com" />
        <meta property="og:image" content="../assets/logo-color.png" />
      </Head>
      <div className="wesolveMobileView">
        <div className="weSolveTough">
          <span>
            We solve <strong>tough</strong> AI stuff
          </span>
          <Image
            class="titleImageSolve"
            source="../assets/titleelement.png"
            altText="title-element"
          />
        </div>
        <div className="rightDiv">
          <div className="tableStyle">
            {WE_SOLVE_TOUGH_AI.map((title, index) => (
              <div key={index} className="solveAI">
                <Link
                  prefetch
                  as={`/services/${title.url}`}
                  href={{
                    pathname: "/services/[solution.js]",
                    query: { solution: title.title }
                  }}
                >
                  <a className="weSolveAnchor">
                    <img
                      src={title.img}
                      altText="images"
                      className="solutionsImages"
                      onMouseOver={e =>
                        (e.currentTarget.src = title.hoverImage)
                      }
                      onMouseOut={e => (e.currentTarget.src = title.img)}
                    />
                    <div className="serviceTitle">{title.title}</div>
                  </a>
                </Link>
              </div>
            ))}
          </div>
          <div className="weBuildBetter">
            We build
            <div className="rotatingWords rotatingWordsOne rwBuildbetter">
              {ROTATING_WORDS.map((words, index) => (
                <span key={index}>{words.weSolveToughAI}</span>
              ))}
            </div>
            <section>AI solutions</section>
          </div>
        </div>
      </div>
    </div>
  );
};
