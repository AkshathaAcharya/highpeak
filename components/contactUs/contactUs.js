import "../contactUs/contactUs.css";
const ContactUs = () => {
  return (
    <div className="contactUs">
      <div className="mapDiv columnContainer">
        <span>Drop in for </span>
        <span>
          some <span className="boldStyle"> tea </span>
        </span>
        <span> or </span>
        <span>a game of</span>
        <span className="boldStyle"> TT!</span>
      </div>
      <div className="addressWrap columnContainer">
        <div className="addressBar">
          <span className="addressHeading">Atlanta Office</span>
          <span>6055 Southard Trace, Cumming, GA, USA</span>
        </div>
        <div className="addressBar">
          <span className="addressHeading">Development center</span>
          <span>4, 17th Cross, K R Road, BSK Stage II,</span>
          <span>Bengaluru, Karnataka, India - 560070 </span>
        </div>
        <div className="addressBar">
          <span className="addressHeading">E-mail</span>
          <span>marketing@highpeaksw.com</span>
        </div>
        <div className="addressBar">
          <span className="addressHeading">Phone</span>
          <span>+91 80 2676 0510</span>
        </div>
      </div>
    </div>
  );
};
export default ContactUs;
