import ReactGA from "react-ga";
import Image from "../../Forms/images";
import OUR_RESIDENT_CONSTANTS from "../../constants/ourResidentConstants";
import "../OurResident/ourResident.css";
const details = OUR_RESIDENT_CONSTANTS.map((image, index) => {
  return (
    <div key={index} className="wrappedDivResident coloumContainer">
      <div>
        <ReactGA.OutboundLink
          eventLabel={image.label}
          to={image.url}
          target="_blank"
        >
          <img src={image.imageFeatures} className="roundImage" />
        </ReactGA.OutboundLink>
      </div>
      <div className="nameOfIntrapreneurs textContainer">
        {image.nameIntrapreneurs}
      </div>
      <div className="detailsOfIntrapreneurs textContainer">{image.detail}</div>
    </div>
  );
});
const OurResident = () => {
  return (
    <div className="ourResidentMainDiv coloumContainer">
      <div className="headContainer coloumContainer">
        <span className="ourResidentHeading">
          Our resident
          <strong className="intrapreneurs"> intrapreneurs </strong>
        </span>
        <Image
          class="ourResidentImage"
          source="../assets/titleelement.png"
          altText="title element"
        />
      </div>
      <div className="align">{details}</div>
    </div>
  );
};
export default OurResident;
