import { PopupboxManager, PopupboxContainer } from "react-popupbox";
import { connect } from "react-redux";
import Input from "../../Forms/input";
import Image from "../../Forms/images";
import { sendData } from "../../constants/hitUsUpNewConstants";
import { uploadData } from "../../constants/uploadFileAction";
import { EMAIL_REGEX } from "../../constants/constants";
import { GOOGLE_ANALYTICS_CONSTANTS } from "../../constants/googleAnalyticsConstants";
import { GA_SCRIPT } from "../../constants/gtmScripts";
import { Event } from "../../utils/analytics";
import "../HitUsUp/hitUsUp.css";
class HitUsUp extends React.Component {
  constructor() {
    super();
    this.state = {
      name: null,
      email: null,
      comments: null,
      isChecked: false,
      validName: false,
      validEmail: false,
      data: null,
      submitted: false,
      beforesubmit: true,
      openMenu: false,
      open: false,
      joinus: false,
      file: null,
      fileData: null,
      uploadResume: null
    };
  }
  componentWillMount() {
    GA_SCRIPT();
  }
  handleNameChange = event => {
    this.setState({
      name:
        event && event.target && event.target.value ? event.target.value : null
    });
  };
  handleEmailChange = event => {
    this.setState({
      email:
        event && event.target && event.target.value ? event.target.value : null,
      validEmail:
        /\s/.test(event.target.value) === false &&
        EMAIL_REGEX.regexObject.test(event.target.value) &&
        true
    });
  };
  handleCommentsChange = event => {
    this.setState({
      comments:
        event && event.target && event.target.value ? event.target.value : null
    });
  };
  handleCheckboxChange = () => {
    this.setState({ isChecked: !this.state.isChecked });
  };
  onToggleMenu = () => {
    this.setState(prevState => ({
      openMenu: !prevState.openMenu
    }));
  };
  uploadFile = event => {
    const formData = new FormData();
    formData.append("file", event.target.files[0]);
    this.setState({ uploadResume: this.props.store, fileData: formData });
    return true;
  };
  handleSubmit = async event => {
    event.preventDefault();
    const headerVariable = this.props.headerName
      .replace(/[^a-zA-Z0-9]/g, "")
      .replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
        return index == 0 ? word.toLowerCase() : word.toUpperCase();
      })
      .replace(/\s+/g, "");
    const filteredAnalytics = GOOGLE_ANALYTICS_CONSTANTS[headerVariable];
    Event(
      filteredAnalytics.event,
      filteredAnalytics.details,
      filteredAnalytics.page
    );
    if (
      this.state.validEmail &&
      this.state.name &&
      this.state.isChecked &&
      this.state.comments
    ) {
      this.setState({
        beforesubmit: false,
        submitted: true,
        data: {
          Name: this.state.name,
          Email: this.state.email,
          Comments: this.state.comments,
          Filename:
            this.props.store === undefined
              ? "noFile"
              : this.props.store.filename,
          Path:
            this.props.store === undefined ? "noPath" : this.props.store.path
        }
      });
      return false;
    } else {
      const content = (
        <p className="quotes displaymessage redColorContainer">
          * Please fill all fields!!.
        </p>
      );
      PopupboxManager.open({ content });
    }
  };
  render() {
    const fieldFills =
      this.state.validEmail &&
      this.state.name &&
      this.state.isChecked &&
      this.state.comments &&
      "Filled";

    const submitCheck =
      this.props.headerName === "Join Us!"
        ? this.state.fileData && fieldFills
        : fieldFills;

    if (this.state.data) {
      this.props.sendData(this.state.data);
      this.state.data = null;
    }
    this.props.store !== this.state.uploadResume
      ? null
      : this.state.fileData && this.props.uploadData(this.state.fileData);
    return (
      <div className="hitUsUpWrap">
        <div className="hitUsUpTitle">
          <span>{this.props.headerName}</span>
          <Image
            class="hitusupTitleelement"
            source="/assets/titleelement.png"
            altText="title-element"
          />
        </div>
        <div className="mobileFlex">
          <Image class="iconSize" source="/assets/user.svg"></Image>
          <span className="redColorContainer"> * </span>
          <Input
            value={this.state.name}
            onChange={this.handleNameChange}
            type="text"
            name="name"
            classname="hitUsUpInputOld"
            placeholder="Tell us your name"
            required
          />
        </div>
        <div className="mobileFlex">
          <Image class="iconSize" source="/assets/envelope.svg"></Image>
          <span className="redColorContainer"> * </span>

          <Input
            value={this.state.email}
            onChange={this.handleEmailChange}
            type="email"
            name="email"
            classname="hitUsUpInputOld"
            placeholder="We don't write much"
            required
          />
        </div>

        {!this.state.validEmail && this.state.email !== null && (
          <div className="hpsError redColorContainer displaymessage">
            Invalid e-mail
          </div>
        )}
        {this.props.headerName === "Join Us!" ? (
          <div className="mobileFlex">
            <Image class="iconSize" source="/assets/edit.svg"></Image>
            <span className="redColorContainer"> * </span>

            <Input
              value={this.state.comments}
              onChange={this.handleCommentsChange}
              classname="hitUsUpInputOld"
              name="comments"
              placeholder="Applying for the position of"
              required
            ></Input>
          </div>
        ) : (
          <div className="mobileFlex">
            <span className="moveIconTop">
              <Image class="iconSize" source="/assets/edit.svg"></Image>
              <span className="redColorContainer"> * </span>
            </span>
            <textarea
              value={this.state.comments}
              onChange={this.handleCommentsChange}
              className="hitUsUpTextArea"
              name="comments"
              placeholder={
                "Drop us a note! \n\n(So we know what you want to talk about. Otherwise, we like rambling a lot)"
              }
              required
            ></textarea>
          </div>
        )}
        {this.props.headerName === "Join Us!" && (
          <div className="widthContainer mobileFlex">
            <span className="iconsUpdate">
              <Image class="iconSize" source="/assets/file.svg"></Image>
              <span className="redColorContainer"> * </span>
            </span>
            <input
              type="file"
              name="file"
              className="fileStyle"
              onChange={this.uploadFile}
              required
            />
          </div>
        )}
        <div className="AgreeTerms">
          <input
            className="hitusupCheckbox"
            type="checkbox"
            onChange={this.handleCheckboxChange}
            checked={this.state.isChecked}
            required
          />
          <span className="redColorContainer">*</span>
          <span className="termsPrivacy">
            I agree to the
            <a href="/termsConditions" target="_blank" className="termsLink">
              terms and conditions
            </a>
            and
            <a href="/privacyPolicy" target="_blank" className="termsLink">
              privacypolicy
            </a>
          </span>
        </div>
        {this.state.submitted ? (
          <input
            disabled={submitCheck && this.state.submitted}
            type="submit"
            value="Submitted!"
            onClick={event => this.handleSubmit(event)}
            className="disablesubmit buttonContainer"
          ></input>
        ) : (
          <input
            type="submit"
            value="Submit"
            onClick={event => this.handleSubmit(event)}
            className={
              this.state.beforesubmit && submitCheck === "Filled"
                ? "hitUsUpSubmit buttonContainer"
                : "disablesubmit buttonContainer"
            }
          ></input>
        )}

        {submitCheck !== "Filled" && <PopupboxContainer />}
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {
    sendData: (...test) => dispatch(sendData(...test)),
    uploadData: (...test) => dispatch(uploadData(...test))
  };
};
const stateToProps = state => {
  return {
    store: state.SendMailReducer.store,
    sentMail: state.SendMailReducer.sentMail
  };
};
export default connect(stateToProps, dispachToProps)(HitUsUp);
