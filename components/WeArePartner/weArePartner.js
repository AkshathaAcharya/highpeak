import "../../components/WeArePartner/weArePartner.css";

const WeArePartner = props => {
  return (
    <div>
      <div className="weArePartnerDiv">
        <div className="weArePartnerHeading colorContainer">
          We are
          <strong className="nextLine"> your partner-in-AI </strong>
        </div>
        <div className="hiFunContainer">
          <div className="sayHi colorContainerHi ">
            <span className="paragraphButton hideIt">
              Say hi to our resident intrapreneurs.
            </span>
            <button
              className="curious WeArePartnerButton"
              onClick={props.scrollDown}
            >
              Curious?
            </button>
          </div>
          <div className="weHaveFun  colorContainerHi">
            <span className="paragraphButton hideIt">
              We are fun, and we have snacks!
            </span>
            <button
              onClick={props.addResume}
              className="joinUs WeArePartnerButton"
            >
              Join Us
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default WeArePartner;
