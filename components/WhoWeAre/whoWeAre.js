import Images from "../../Forms/images";
import MenuSvg from "../../components/menu_svg";
import "../WhoWeAre/whoWeAre.css";
const WhoWeAre = props => {
  return (
    <div className="aboutMainDiv">
      <div className="mobileHeadConatiner">
        <div className="logoImageWhoWeAre">
          <a href="/">
            <Images
              source="../assets/logo-color.png"
              altText="logo"
              class="logoPositionition"
            />
          </a>
        </div>
        <div className="menuImageWhoWeAre " onClick={props.onToggleMenu}>
          <MenuSvg navigationColor="black" />
        </div>
      </div>
      <span className="whoAreWe"> Who are we?</span>
    </div>
  );
};
export default WhoWeAre;
