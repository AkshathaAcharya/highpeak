import SVG from "../SVG/svg";
const ScrollSvg = props => {
  return (
    <SVG>
      <g id="01" transform="translate(-673.000000, -656.000000)">
        <g id="Group-8" transform="translate(668.000000, 656.000000)">
          <g id="Group-6" transform="translate(5.000000, 0.000000)">
            <rect
              id="Rectangle"
              x="0"
              y="0"
              width="21.0000014"
              height="31.5000021"
              rx="10.5000007"
              className={props.rectangleColor}
            ></rect>
            <circle
              id="Oval"
              cx="10.5000007"
              cy="7.70000052"
              r="2.80000019"
              className={props.circleColor}
            ></circle>
          </g>
        </g>
      </g>
    </SVG>
  );
};

export default ScrollSvg;
