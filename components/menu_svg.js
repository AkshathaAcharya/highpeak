import SVG from "../SVG/svg";

const MenuSvg = props => {
  return (
    <React.Fragment>
      <SVG width="42px">
        <g
          id="01"
          transform="translate(-1269.000000, -55.000000)"
          fill={props.navigationColor}
          fill-rule="nonzero"
        >
          <g id="menu(1)" transform="translate(1269.000000, 55.000000)">
            <path
              d="M38.7500292,0 L1.24997078,0 C0.55994119,0 0,0.44804655 0,1 C0,1.55195345 0.560045095,2 1.24997078,2 L38.7500292,2 C39.4400588,2 40,1.55195345 40,1 C40,0.44804655 39.4400588,0 38.7500292,0 Z"
              id="Path"
            ></path>
            <path
              d="M38.7727551,9 L14.2272449,9 C13.5497608,9 13,9.44804655 13,10 C13,10.5520366 13.5498628,11 14.2272449,11 L38.7727551,11 C39.4502392,11 40,10.5519534 40,10 C40,9.44796342 39.4502392,9 38.7727551,9 Z"
              id="Path"
            ></path>
          </g>
        </g>
      </SVG>
    </React.Fragment>
  );
};

export default MenuSvg;
