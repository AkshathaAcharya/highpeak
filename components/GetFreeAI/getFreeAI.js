import ReactGA from "react-ga";
import Head from "next/head";
import Image from "../../Forms/images";
import { FREE_AI_IMG } from "../../constants/constants";
import "../GetFreeAI/getFreeAI.css";

const GetFreeAI = () => {
  return (
    <div className="component fifthComponent">
      <div className="fifthCompMble">
        <Head>
          <title>High Peak Software - Home</title>
          <meta
            name="description"
            content="High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
          ></meta>
          <meta
            name="keywords"
            content="latest technology blogs, artificial intelligence blogs, technology blogs, automation blogs, blogs about technology, marketing tech blog, IT blogs, technology ebooks, product case study, project case study"
          />
          <meta property="og:title" content="High Peak Software - Home" />
          <meta property="og:site_name" content="High Peak Software" />
          <meta
            property="og:description"
            content="High Peak Software is an end-to-end AI product solutions provider, specializing in artificial intelligence, machine learning, data science, NLP, etc."
          />
          <meta property="og:url" content="https://highpeaksw.com" />
          <meta property="og:image" content="../assets/logo-color.png" />
        </Head>
        <div className="getFreeAIStuff">
          Get <strong>free</strong> AI stuff
          <Image
            class="weHaveProofTitleImage"
            source="../assets/titleelement.png"
            altText="title element"
          />
        </div>
        <div className="freeImageWrap">
          {FREE_AI_IMG.map((val, index) => (
            <ReactGA.OutboundLink
              className="horizontal"
              eventLabel={val.label}
              to={val.url}
              target="_blank"
            >
              <div key={index} className="verticalDiv">
                <div
                  className={`${val.className} ${
                    index % 2 === 0 ? "" : "hideMobile"
                  }`}
                />
                <div className="titleDiv">
                  <strong className={val.titleClass}>{val.title}</strong>
                  <span className={val.subtitleClass}>{val.subTitle}</span>
                </div>
                {index % 2 == 1 && (
                  <div className={`${val.className} showMobile`} />
                )}
              </div>
            </ReactGA.OutboundLink>
          ))}
        </div>
      </div>
    </div>
  );
};
export default GetFreeAI;
