import Modal from "react-responsive-modal";
import HomeLanding from "../HomeLanding/homeLanding";
import WeSolveToughAI from "../WeSolveToughAI/weSolveToughAI";
import WeMakeCoolAI from "../WeMakeCoolAI/weMakeCoolAI";
import GetFreeAI from "../GetFreeAI/getFreeAI";
import Image from "../../Forms/images";
import MenuSvg from "../menu_svg";
import WeDeliverFreeAI from "../WeDeliverFreeAI/weDeliverFreeAI";
import { NAVIGATION_ARRAY } from "../../constants/constants";
import { GA_SCRIPT } from "../../constants/gtmScripts";
import "../../styles/about.css";
import "../Footer/footer.css";
import "../../styles/homePage.css";
class HomePageScroll extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      openMenu: false,
      homePages: "homePageLinks"
    };
    this.homeDivToFocus = React.createRef();
    this.WeSolveToughAIToFocus = React.createRef();
    this.weMakeCoolAIToFocus = React.createRef();
    this.GetFreeAIToFocus = React.createRef();
    this.DropInToFocus = React.createRef();
  }

  onToggleHitUsUpModal = () => {
    this.setState(prevState => ({
      open: !prevState.open
    }));
  };

  onToggleMenu = () => {
    this.setState(prevState => ({
      openMenu: !prevState.openMenu
    }));
  };

  componentDidMount = () => {
    GA_SCRIPT();
    const keyToScroll = this.props.querysection;
    const key = String(keyToScroll).charAt(0);
    if (key == 1) {
      this.focusScrollMethod("", this.WeSolveToughAIToFocus);
    }
    if (key == 2) {
      this.focusScrollMethod("", this.weMakeCoolAIToFocus);
    }
    if (key == 3) {
      this.focusScrollMethod("", this.GetFreeAIToFocus);
    }
    if (key == 4) {
      this.focusScrollMethod("", this.DropInToFocus);
    }
  };
  footerScroll = event => {
    if (event == 1) {
      this.focusScrollMethod("", this.WeSolveToughAIToFocus);
    }
    if (event == 2) {
      this.focusScrollMethod("", this.weMakeCoolAIToFocus);
    }
    if (event == 3) {
      this.focusScrollMethod("", this.GetFreeAIToFocus);
    }
    if (event == 4) {
      this.focusScrollMethod("", this.DropInToFocus);
    }
  };
  focusScrollMethod = (event, reference) => {
    this.state.openMenu &&
      this.setState({
        openMenu: false
      });
    if (reference["current"]) {
      reference["current"].scrollIntoView({
        top: reference["offsetTop"],
        behavior: "auto",
        block: "center",
        inline: "center"
      });
    }
  };

  render() {
    const navigationArray = [
      {
        menu: "Home",
        reference: this.homeDivToFocus
      },
      {
        menu: "We Solve Tough AI Stuff",
        reference: this.WeSolveToughAIToFocus
      },
      {
        menu: "We Make Cool AI Stuff",
        reference: this.weMakeCoolAIToFocus
      },
      {
        menu: "Get Free AI Stuff",
        reference: this.GetFreeAIToFocus
      },
      { menu: "Drop In", reference: this.DropInToFocus }
    ];

    const { open } = this.state;

    return (
      <div>
        <div className="mobileHeaderSticky">
          <a href="/" target="_self">
            <Image
              class="logoPosition"
              source="/assets/logo-color.png"
              altText="logo"
            />
          </a>
          <div className="leftNavigationBar" onClick={this.onToggleMenu}>
            <MenuSvg navigationColor="black" />
          </div>
          <div className="navigationSlider">
            <Modal
              open={this.state.openMenu}
              onClose={this.onToggleMenu}
              styles={{
                overlay: { padding: "0px" },
                modal: {
                  maxWidth: "1500px",
                  width: "0%",
                  marginRight: "0%",
                  float: "right",
                  height: "100%",
                  fontSize: "24px",
                  animation: "slide-in 0.5s forwards",
                  outline: "none"
                }
              }}
            >
              <ul className="ulRight">
                <li className="liMenuSlide">
                  {navigationArray.map((navigation, array) => (
                    <a
                      onClick={e => {
                        this.focusScrollMethod(e, navigation.reference);
                      }}
                    >
                      {navigation.menu}
                    </a>
                  ))}
                  <a href="/about" className="menulink">
                    Who Are We?
                  </a>
                </li>
              </ul>
            </Modal>
          </div>
        </div>
        <div className="homelanding" ref={this.homeDivToFocus}>
          <HomeLanding />
        </div>
        <div className="wesolvetoughAI" ref={this.WeSolveToughAIToFocus}>
          <WeSolveToughAI />
        </div>
        <div className="wemakecoolAI" ref={this.weMakeCoolAIToFocus}>
          <WeMakeCoolAI />
        </div>
        <div className="getfreeAI" ref={this.GetFreeAIToFocus}>
          <GetFreeAI />
        </div>
        <div className="wedeliverfreeAI" ref={this.DropInToFocus}>
          <WeDeliverFreeAI
            footer={true}
            navigation_Array={NAVIGATION_ARRAY}
            changePage={this.footerScroll}
            homeFooter={true}
          />
        </div>
      </div>
    );
  }
}

export default HomePageScroll;
