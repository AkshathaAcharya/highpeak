import ContactUs from "../../components/contactUs/contactUs";
import Footer from "../Footer/footer";
import "../WeDeliverFreeAI/weDeliverFreeAI.css";
class WeDeliverFreeAI extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      footer: true,
      homeFooter: this.props.homeFooter || false
    };
  }
  componentDidMount = () => {
    this.props.footer === false &&
      this.setState({
        footer: false
      });
  };

  render() {
    return (
      <div className="mainDivdlvestf">
        <ContactUs />
        <Footer
          homeFooter={this.state.homeFooter}
          footer={this.state.footer}
          navigationArray={this.props.navigationArray}
          changePage={this.props.changePage}
        />
      </div>
    );
  }
}

export default WeDeliverFreeAI;
