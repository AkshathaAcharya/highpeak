import Head from "next/head";
import Image from "../../Forms/images";
import { ROTATING_WORDS } from "../../constants/constants";
import "../HomeLanding/homeLanding.css";
const HomeLanding = () => {
  return (
    <div className="component firstComponent">
      <Head>
        <title>High Peak Software - Home</title>
      </Head>
      <div className="weAreHighAI">
        We Are
        <section className="rotatingWordsMargin">
          High-
          <span class="rotatingWords rotatingWordsOne homepageRwwords">
            {ROTATING_WORDS.map((words, index) => (
              <span key={index}>{words.homePage}</span>
            ))}
          </span>
        </section>
        AI
        <Image
          class="titleImage"
          source="../assets/titleelement.png"
          altText="title-element"
        />
        <a href="/portfolio" target="_blank" className="checkOutOurStuff">
          Check out our stuff
        </a>
      </div>
    </div>
  );
};

export default HomeLanding;
