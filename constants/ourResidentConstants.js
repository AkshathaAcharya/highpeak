const OUR_RESIDENT_CONSTANTS = [
  {
    imageFeatures: "../assets/vinay.png",
    url: "https://www.linkedin.com/in/chandravinay/",
    nameIntrapreneurs: "Vinay Chandra ",
    detail: "Our resident intrapreneur-in-chief",
    label: "Vinay Linkedin"
  },
  {
    imageFeatures: "../assets/keerthi.png",
    url: "https://www.linkedin.com/in/keerthishekar/",
    nameIntrapreneurs: "Keerthi Shekar G",
    detail: "Our resident technology chief",
    label: "Keerthi Linkedin"
  }
];
export default OUR_RESIDENT_CONSTANTS;
