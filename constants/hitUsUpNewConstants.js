export const SEND_DATA_TO_SERVER = "SEND_DATA_TO_SERVER";

export const sendData = data => {
  return {
    type: SEND_DATA_TO_SERVER,
    payload: data
  };
};
