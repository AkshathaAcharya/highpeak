export const SERVICE_CONSTANTS = {
  "chemical-management-solution": {
    projectName: "Chemical Management Solution",
    metaTitle: "High Peak Software - Chemical Management Solution",
    metaDescription:
      "An automated end-to-end chemical management system for digitizing the chemical management inventory and logistics processes in health and safety companies.",
    metaKeywords:
      "Chemical logistics management. Logistics and transport management, Integrated logistics management, Chemical management system, Operations management & logistics for chemicals, Chemical data management system, Chemical inventory system, Chemical management database",
    projectImage: "../assets/1_Chemical.png",
    openGraphURL:
      "https://highpeaksw.com/case-studies/chemical-management-solution",
    clientOverViews: [
      "Our client is a leading environmental health and safety (EH&S) software company in the US.",
      "The company leverages cloud computing technology to create solutions that are used by major corporations and governments globally to gauge, manage, record, and report mission critical EH&S data."
    ],
    problems: [
      "Chemical management involves the manual processes of handling, distributing, and storing of chemicals. Further, monitoring and tracking the chemicals, process control, distribution control, and complying with safety regulations and maintaining safety standards are also part of the tedious process.",
      "Our client wanted to digitize and automate this entire lifecycle for enterprises and pharmacies handling various chemicals."
    ],
    challenges: [
      "Each chemical comes with its own regulatory compliances and safety standards set as per global regulations, such as RECH, RoHS, WEEE, CA65, and so on. Accurately mapping existing and new chemicals against these standards was a major challenge, owing to large datasets and centralising data from various data sources. ",
      "Because every chemical has different regulatory compliances depending on the properties of that chemical, the team had to create different templates for request forms. Further, customizing these forms for different chemicals was another challenge."
    ],

    solutionsShortDescription: [
      "High Peak helped design and develop a web application for digitizing the chemical management inventory and logistics process."
    ],
    solutions: [
      {
        solutionsDescription: [
          "Safety data sheet (SDS) builder",
          "This web application allows users to create and export safety data sheets for every chemical. These data sheets typically include regulatory compliance information, chemical and physical properties, etc. for the corresponding chemicals. Users can also search for any existing SDS if required. ",
          "The drag-and-drop feature of the SDS builder makes for a highly configurable system to create customized new formats for safety data sheets"
        ],
        img: "../assets/Safety_Data_Sheet_1.jpg"
      },
      {
        solutionsDescription: [
          "Label generator",
          "Every chemical container comes with its own set of labels, warning potential handlers of its physical and chemical properties. Labels are important elements of safety data sheets. ",
          "Along with the SDS builder, High Peak has built a highly configurable label generator, which allows for creation of new labels according to compliatory and regulatory requirements. ",
          "The label generator also comes with predefined templates for thousands of widely used standardized labels."
        ],
        img: "../assets/Label_2.jpg"
      },
      {
        solutionsDescription: [
          "SDS library",
          "SDS library is essentially a secure database of all the chemicals, along with their corresponding information, including safety data sheets. ",
          "When a vendor puts in a request for an existing chemical, the corresponding safety sheet is pulled up and the vendor’s orders are processed accordingly. ",
          "If a new order comes in for a chemical that is not yet registered in the SDS library, the chemical’s corresponding information is created along with a new safety data sheet using the SDS builder, and this new information is automatically stored in the SDS Library."
        ],
        img: "../assets/Library_3.jpg"
      },
      {
        solutionsDescription: [
          "Intelligent text viewer",
          "Because the safety data sheets come in a plethora of templates, according to geography, compliance and other regulatory requirements, it is crucial to adhere to them. ",
          "Once the safety data sheet is built for a certain chemical, the intelligent text viewer allows the user to view the information in any of the several standardized formats available and choose the required format to adhere to."
        ],
        img: "../assets/Text_4.jpg"
      },
      {
        solutionsDescription: [
          "Chemical order management system",
          "This system allows a vendor to search for a particular chemical available in the vicinity of their location. ",
          "If the product is available, they can raise a request for the transportation and consumption of the chemical. This new order goes through multiple approval processes. The Chemical order management system enables the sellers and distributors to create a configurable workflow to accommodate these approval processes.",
          "If the product is not available on the system, a request for creating and storing the corresponding information for the new chemical, and the above order management process is carried out, once the product’s information is created in the SDS builder and stored in the SDS library."
        ],
        img: "../assets/Order_5.jpg"
      },
      {
        solutionsDescription: [
          "Chemical inventory & logistics management system",
          "The inventory and logistics management system stores updated information on the availability and transportation of all chemicals stored in the SDS library.",
          "This information is particularly useful when a chemical is out of stock or its transportation details are under scrutiny during an audit."
        ],
        img: "../assets/Logistics_6.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["Javascript", "HTML"]
      },
      {
        domain: "Backend",
        technology: ["ColdFusion"]
      },
      {
        domain: "Database",
        technology: ["MySQL"]
      },
      {
        domain: "Hosted on",
        technology: ["Google Cloud Platform"]
      }
    ]
  },
  "lawyer-networking-platform": {
    projectName: "Lawyer Networking Platform",
    metaTitle: "High Peak Software - Lawyer Networking Platform",
    metaDescription:
      "A lawyer-to-lawyer networking platform that facilitates lawyers to communicate, exchange knowledge, and practice law virtually.",
    metaKeywords:
      "Legal tech platform, Digital platforms for lawyers, Legal tech software, Legal case management platform, Legal case management platform, Legal matter management software, Case management software for lawyers, Case management software for law firms",
    openGraphURL:
      "https://highpeaksw.com/case-studies/lawyer-networking-platform",
    projectImage: "../assets/2_Lawyer.png",
    clientOverViews: [
      "Our client is an established law firm with more than twenty years of experience that spans across three continents. ",
      "The firm has extensive expertise in corporate and commercial law, general corporate, technology, immigration, conflict resolution, intellectual property and employment law."
    ],
    problems: [
      "Traditionally, lawyers have always had brick-and-mortar work environments with four-walled offices and courtrooms. Most domains today have introduced virtual work environments to encourage employees to pick and choose their work location and timings. ",
      "In addition, certain lawyers such as those returning to work after a break, fresh law graduates, retired lawyers, and even sole practitioners are faced with challenges in the ever evolving profession of law.",
      "Our client wanted to develop a virtual platform for all such lawyers to communicate, work together, and practice law. ",
      "The platform would ideally serve as an avenue for lawyers across geographies to exchange knowledge and practice law at their own convenience."
    ],
    challenges: [
      "Because the field of law was completely new territory for the technical team, the major challenge was learning and understanding the day-to-day work routines of lawyers, and the modus operandi of the law profession.",
      "The team needed to create a platform that could cater to a target audience comprising lawyers from distinct domains of law. With that in mind, the team focused on developing this business model to allow the transition of the users of the platform and help them understand how to work with it. ",
      "Yet another challenge was to ensure the overall security of the platform, maintain user privacy, and protect user assets like confidential client documents, legal contract documents and so on."
    ],

    solutionsShortDescription: [
      "High Peak designed and developed a lawyer-to-lawyer networking platform that facilitates lawyers to communicate, exchange knowledge, and practice law virtually. The platform can be used by law professionals ranging from law graduates and sole practitioners to lawyers who are looking to work after a break."
    ],
    solutions: [
      {
        solutionsDescription: [
          "Lawyer discovery system",
          "The main feature of the platform, the lawyer discovery system, caters to discovering lawyers that wield expertise in specific areas of law. ",
          "During our user testing phase and while monitoring user behavior on the platform, we found that the lawyers were actively looking for junior lawyers, associates, and co-lawyers to help out with their workload. ",
          "Therefore, the lawyer discovery system enables lawyers to find other lawyers using filters such as location, areas of practice, and so on. ",
          "The system also allows lawyers to filter names based on the lawyer’s bar admission. Members can also mark certain lawyers as favorites for easy and immediate access for task allocation."
        ],
        img: "../assets/Lawyerr_Networking_1.jpg"
      },
      {
        solutionsDescription: [
          "Task management system",
          "High Peak designed and developed  a task management system which provides an overview of cases and tasks for a lawyer. It enables lawyers to delegate cases and tasks to other lawyers, accept cases from other lawyers, assign favorites for easy task delegation, and more. ",
          "An inventory of all assigned tasks can be maintained on the dashboard, along with time reports to keep track of work progression. Members can also view their invoices on the platform."
        ],
        img: "../assets/Task_2.jpg"
      },
      {
        solutionsDescription: [
          "User management system",
          "The platform operates on an invite-only policy. Lawyers can become members on the platform after obtaining an invite from an existing member. ",
          "As a member, the user can create a profile on the dashboard. The dashboard is an overview of the lawyer’s profile containing assigned tasks, client information, and invoices.",
          "The system also allows for tracking of subscriptions, and upgrading and downgrading of users of the platform."
        ],
        img: "../assets/User_3.jpg"
      },
      {
        solutionsDescription: [
          "Feedback management system",
          "The platform includes a feedback management system, which allows members to report bugs, log in issues on the platform, raise requests, and provide feedback. ",
          "The HPS team actively tracks the feedback received on the platform and follows up with the users. Members of the platform are continually updated about necessary bug fixes, feature additions, and other related updates."
        ],
        img: "../assets/Feedback_4.jpg"
      },
      {
        solutionsDescription: [
          "Invoice management system",
          "The platform also includes an invoice management system that allows lawyers to generate invoices for their clients. ",
          "Lawyers can receive or make payments via a payment gateway that is integrated with the platform."
        ],
        img: "../assets/Invoice_5.jpg"
      },
      {
        solutionsDescription: [
          "Knowledge centre",
          "A section of the platform is dedicated to sharing and exchanging of information on the latest trends in the law. ",
          "This section contains web links, videos, law updates, events, and podcasts. Users can also share and upload their own articles or blogs for other members to read."
        ],
        img: "../assets/Knowledge_6.jpg"
      },
      {
        solutionsDescription: [
          "Event management system",
          "High Peak designed an event management system to keep members on the platform abreast of law-related events such as the lawyer round table sessions. ",
          "Such events are also conducted for the members across India to come together to network, share knowledge, and discuss trends."
        ],
        img: "../assets/Event_7.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["HTML", "CSS", "JavaScript", "Redis", "Redux"]
      },
      {
        domain: "Backend",
        technology: [
          "Java",
          "Spring Boot framework",
          "MySQL",
          "REDIS",
          "Websockets"
        ]
      },
      {
        domain: "Hosted on",
        technology: ["AWS"]
      },
      {
        domain: "Encryption",
        technology: ["Elastic Block Store"]
      }
    ]
  },
  "smart-enterprise-payroll-system": {
    projectName: "Smart Enterprise Payroll System",
    metaTitle: "High Peak Software - Enterprise Payroll System",
    metaDescription:
      "An automated enterprise payroll system for data collection and validation of payroll and tax-related information.  ",
    metaKeywords:
      "Enterprise payroll system, Payroll automation software, Automated payroll system, Smart payroll software, KYC document processing software",
    openGraphURL:
      "https://highpeaksw.com/case-studies/smart-enterprise-payroll-system",
    projectImage: "../assets/3_Smart.png",
    clientOverViews: [
      "Our client is a leading provider of scalable and customizable payroll and statutory compliance solutions across the globe with 68 years of industry expertise. ",
      "They pay specific attention to continuous needs for scalability, data security, disaster recovery, and uninterrupted business continuity."
    ],
    problems: [
      "Manual data entry and processing of forms specific to definitive tasks is time and resource-consuming. Sectors like BFSI, IT, HR, legal, and insurance deal with enormous amounts of paperwork on a daily basis. ",
      "Because of the long drawn-out tasks that manual paperwork involves, it is prone to human errors.",
      "Our client was dealing with mountains of physical KYC documents relating to employee payroll, loan, and salary data. During the peak tax seasons, the organization caters to about 3000 new end customers, which means more paperwork. ",
      "Therefore, to digitize this process, the client wanted to streamline and automate manual tasks."
    ],
    challenges: [
      "Since the client works with different industries, they face a challenge in managing the growing customer base. One of the major challenges was engaging with the customers daily for ad-hoc requirement gathering.",
      "Another challenge was the digitization of KYC forms. Since KYC documents are long and time consuming, digitizing them involves creating a lot of fields and digital pages. ",
      "In addition, the system deals with large quantities of data on a daily basis. This required load balancing and ensuring that the servers are always up to manage data processing, which posed a major challenge."
    ],

    solutionsShortDescription: [],
    solutions: [
      {
        solutionsDescription: [
          "Automated information warehouse",
          "The High Peak team built a data collection and validation web app to digitize the manual workflow of entering heavy payroll and tax-related information that usually spans several physical pages. ",
          "This web app makes the data entry more interactive and less time consuming by laying out prefilled fields."
        ],
        img: "../assets/Info_Warehouse_1.jpg"
      },
      {
        solutionsDescription: [
          "Interactive chatbot",
          "The High Peak team first designed and developed an interactive chatbot by automating the process of form filling and digitizing the KYC documents. ",
          "The chatbot could interact with customers and prompt them to answer specific questions. However, during the testing phase, the team found out from user feedback that this process was exhausting although the users enjoyed the interaction. ",
          "The team quickly found another less-time consuming solution, which is explained in the next section."
        ],
        img: "../assets/Chatbot_2.jpg"
      },
      {
        solutionsDescription: [
          "Card-based UI",
          "Although the process of form filling was digitized using the chatbot, it was still a tiresome process for users to answer several questions while filling the form. ",
          "In order to alleviate this problem, the team developed a card-based UI. ",
          "This improved UI comprises pre-filled fields, which reduces the time taken to gather data and shortens user navigation."
        ],
        img: "../assets/Card_3.jpg"
      },
      {
        solutionsDescription: [
          "Three-tiered information access",
          "This automated data collection and validation platform serves three kinds of users: ",
          "Our client, the payment solutions provider",
          "The client’s immediate clientele, the firms",
          "The end users, the employees of the firms",
          "Because the above-mentioned three different types of users have different requirements, the system must be able to adapt to the same. Therefore, the High Peak team built three different UIs to cater to these different requirements, with role-based, secure information access."
        ],
        img: "../assets/Tiered_info_4.jpg"
      },
      {
        solutionsDescription: [
          "Secure communication platform",
          "Because tax and payroll information is highly sensitive, the client required a platform which enables secure communication between users such that they could securely exchange, transfer and consume sensitive information."
        ],
        img: "../assets/Communication_5.jpg"
      },
      {
        solutionsDescription: [
          "Secure transmission and sharing of files",
          "Before the secure communication system was in place, users had to use emails and other less secure electronic means to send their information to the client. ",
          "The client was concerned about the information safety and breach. Therefore, in order to alleviate this problem, High Peak enabled a secure feature that would enable the users to transfer and consume their sensitive payroll and tax information."
        ],
        img: "../assets/File_Transmission_6.jpg"
      },
      {
        solutionsDescription: [
          "Effective ticketing system",
          "This communication platform also includes a ticketing system, which allows the customer support to effectively deal with issues and requests raised by the users."
        ],
        img: "../assets/Ticketing_7.jpg"
      },
      {
        solutionsDescription: [
          "Whitelabeling payroll solutions",
          "Whitelabeling happens when a product or service removes their brand and logo from their own product and uses the branding requested by the purchaser or its end client.",
          "The automated information warehouse web app enables the end users of our client to rebrand the platform according to their brand requirements and market or campaign the client’s original payment solutions with ease."
        ],
        img: "../assets/Campaign_8.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["React.js"]
      },
      {
        domain: "Backend",
        technology: ["Spring Boot", "MySQL"]
      },
      {
        domain: "Hosted on",
        technology: ["AWS"]
      }
    ]
  },
  "office-ergonomic-software": {
    projectName: "Office Ergonomic Software",
    metaTitle: "High Peak Software - Office Ergonomic Software",
    metaDescription:
      "A behavioral biometric-based intelligent safety system for improving employee productivity and workstyle in organizations.",
    metaKeywords:
      "Office ergonomic software, Ergonomic software for IT, Ergonomics desktop application, Work station assessment, Biometric software, Intelligent ergonomic safety system",
    openGraphURL:
      "https://highpeaksw.com/case-studies/office-ergonomic-software",
    projectImage: "../assets/4_Office.png",
    clientOverViews: [
      "Our client is a leading environmental health and safety (EH&S) software company in the US. ",
      "The company leverages cloud computing technology to create solutions that are used by major corporations and governments in the world to gauge, manage, record, and report mission critical EH&S data."
    ],
    problems: [
      "Employees with desk jobs typically spend a large part of their day working on a computer system at a desk. Due to the strain of sitting for long periods of time, they are prone to various ergonomic injuries, such as lumbar complications. ",
      "Perennial typing habits could also lead to injuries like carpal tunnel syndrome. This is only one part of the problem, however. Others include monitoring and maintaining workplace safety and employee health, according to standard industry regulations.",
      "Our client wanted to automate parts of the process of ensuring and monitoring health and safety regulations. The High Peak team helped digitize this operation."
    ],
    challenges: [
      "A part of this automation process involved dealing with large data sets, including personal and ergonomic data of large-enterprise workforce. For instance, monitoring employee break durations, number of keystrokes, number of mouse clicks, and so on, for thousands of employees contributed to big data.",
      "For self-assessment and awareness creation, the client wanted to create training materials, specific to each industry. However, since every industry has its own set of safety regulations and standards, the training materials had to be customized accordingly.",
      "Further, in addition to logging existing problems in the health and safety department of an organization, the system had to incorporate a way for users to create new tickets as and when they arise."
    ],

    solutionsShortDescription: [
      "High Peak helped design and develop the office ergonomic software (OES), a behavioral biometric-based intelligent safety system."
    ],
    solutions: [
      {
        solutionsDescription: [
          "Office ergonomics software",
          "This OES collects extensive information about employee behavioral biometrics such as typing habits, and device use. This enables organizations to gain insights into employee productivity and workstyle, org-wide.",
          "The core OES was designed to create and manage training materials, track employee biometric behavior, generate reports and assessments, collect feedback, and record surveys."
        ],
        img: "../assets/Ergonomics_1.jpg"
      },
      {
        solutionsDescription: [
          "Behavioural biometrics tracking system",
          "High Peak integrated a behavioural biometrics tracking system to track various aspects of employee ergonomic behavior. This tool tracks device use, keystroke habits, typing habits, and mouse clicks.",
          "Based on such observations, the system provides warnings and trigger alerts so that employees can estimate the risk levels and make suitable amendments. ",
          "For instance, the system will prompt an employee to take a break or perform simple exercises if the employee spends too much time staring at the monitor."
        ],
        img: "../assets/Bio_2.jpg"
      },
      {
        solutionsDescription: [
          "Interactive learning management system",
          "Training and ergonomic awareness videos",
          "The team integrated an interactive learning management system (LMS) which contains information on ergonomic safety, best practices for employees, and training schedules.",
          "The interactive LMS also allows employees to create new training materials customized for the organization, which are then exported to the core OES."
        ],
        img: "../assets/Learning_Management_3.jpg"
      },
      {
        solutionsDescription: [
          "Survey management tool",
          "The interactive survey management tool enables organizations to create surveys and questionnaires for employees. ",
          "The surveys and questionnaires are conducted to gauge the employee’s comfort levels, risk levels, and any other issues they may be facing at the workplace.",
          "The responses obtained from these surveys and questionnaires are stored and exported to the core OES."
        ],
        img: "../assets/Survey_4.jpg"
      },
      {
        solutionsDescription: [
          "Issue management system",
          "The issue management system (IMS) enables employees and teams to submit ergonomic problems or other issues faced by them such as improper working conditions, temperature suitability, inadequate lighting, and more. ",
          "The IMS also allows enterprise workers to log issues for new problems. Consequently, these new problems will be evaluated by a supervisor and new training materials and/or safety equipment and manuals would be added to the LMS."
        ],
        img: "../assets/Issue_5.jpg"
      },
      {
        solutionsDescription: [
          "Customer relationship management system (CRM)",
          "The CRM facilitates customization of the OES in accordance with the organization’s requirements and the industry it belongs to."
        ],
        img: "../assets/CRM_6.jpg"
      },
      {
        solutionsDescription: [
          "Intelligent reporting system",
          "High Peak developed an intelligent reporting system to generate reports based on responses obtained from the surveys and questionnaires.",
          "In addition, the data from the behavioural biometrics tracking system is synced to the intelligent reporting system for report generation.",
          "These reports contain information regarding attendance of employees during training, comfort levels at work, risk levels at work, training schedules of employees, and other metrics."
        ],
        img: "../assets/Report_7.jpg"
      },
      {
        solutionsDescription: [
          "Comprehensive dashboard",
          "High Peak designed and developed a comprehensive dashboard that provides an overview of all the data in the core OES. ",
          "The dashboard can also be used for setting role-based access depending on the employee’s role in the organization."
        ],
        img: "../assets/Dashboard_8.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["Javascript"]
      },
      {
        domain: "Backend",
        technology: ["Java", "ColdFusion"]
      },
      {
        domain: "Database",
        technology: ["Microsoft SQL Server Database"]
      }
    ]
  },
  "legal-contracts-services-platform": {
    projectName: "Legal Contracts Services Platform",
    metaTitle: "High Peak Software - Legal Contracts Services Platform",
    metaDescription:
      "A legal contract management and annotation system that enables companies to understand and regulate their contracts for efficient decision making in business. ",
    metaKeywords:
      "Legal contract mamagement software, Legal document management system, Legal contract management solution for enterprises, Legal annotation software, Software for legal contracts",
    openGraphURL:
      "https://highpeaksw.com/case-studies/legal-contracts-services-platform",
    projectImage: "../assets/5_LegalContracts.png",
    clientOverViews: [
      "The client is a leading software company providing enterprise SaaS that leverages machine learning to accurately extract critical information from legal contracts.",
      "The company mainly focuses on helping organizations to maximize profits and minimize risks in their commercial relationships by understanding their legal business contracts."
    ],
    problems: [
      "The need for maintaining and enforcing business contracts has grown in the past decade, owing to frequent regulatory reforms. ",
      "Because contract documents are increasingly verbose and detailed, they are overwhelming and difficult to comprehend.",
      "A leading software firm wanted to simplify this process by helping companies understand and manage their business contracts through automated annotation and summarization of contracts. Owing to inadequate results obtained by a previous vendor, the project was handed over to High Peak."
    ],
    challenges: [
      "The project was partially developed by another vendor and comprised a lot of bugs. A major challenge for High Peak was to evaluate the already existing design and make significant  improvements to the application. The team also developed new applications with more features and better technologies. ",
      "Another challenge was to improve the speed of annotation of contracts. The team at High Peak had to gather data to be fed to a machine learning model for training. ",
      "The team also had to commit to a UI design that was simple and user-friendly to enable attorneys to use and understand the application with ease."
    ],

    solutionsShortDescription: [
      "High Peak Software operates as the client’s offshore development center and has assisted in the development of the legal contracts management and annotation system. It comprises:"
    ],
    solutions: [
      {
        solutionsDescription: [
          "Automated document identification",
          "The platform uses machine learning models to automatically detect the various types of contract documents when they are uploaded to the platform. For instance, if the document uploaded to the platform is a non-disclosure agreement contract, or a licensing contract, the platform is capable of identifying it as such automatically. ",
          "High Peak uses machine learning models that are intelligent enough to learn from the data they consume and more accurately identify document types with time."
        ],
        img: "../assets/Doc_Classification_1.jpg"
      },
      {
        solutionsDescription: [
          "Automated data extraction",
          "High Peak implemented automated data extraction for image files by employing Optical Character Recognition (OCR) technology.  ",
          "The platform allows the user to upload any kind of document including text, pdf, images etc. The textual content in some of these document types are not readable by the machine because they are images. In order to extract information from such document types, the platform uses OCR technology and retrieves relevant information. ",
          "This extracted data is then accordingly categorised by our ML algorithms. For example, the algorithms can understand the extracted information presented in a DD/MM/YYYY format as a date."
        ],
        img: "../assets/Data_extraction_2.jpg"
      },
      {
        solutionsDescription: [
          "Automated configurable annotation tool",
          "The High Peak team designed and developed a configurable annotation tool using machine learning algorithms that are capable of identifying the document type and predicting annotations by extracting key-value pairs. ",
          "The ML model learns to identify and label annotations within the document. For instance, start and end date of the contract, party names, monetary value, and so on. ",
          "In addition, the user can search for a specific value within the document using keywords and create an annotation manually."
        ],
        img: "../assets/Annotation_3.jpg"
      },
      {
        solutionsDescription: [
          "Intelligent document summarization",
          "High peak designed and developed an intelligent document summarization tool integrated within the platform. This tool is capable of identifying important sections of text in a contract document and summarizing the information in a way that can be easily understood and consumed by a human intelligently. ",
          "Document summarization contains different important highlights of the document categorised by annotation types such as dates, events, and so on. Once the user clicks on a particular category, the annotated segment belonging to that category is pulled up and presented for the user’s view."
        ],
        img: "../assets/Summary_4.jpg"
      },
      {
        solutionsDescription: [
          "Document validation",
          "The documents identified, processed, extracted, and annotated by our intelligent machine language algorithms are manually verified and validated by attorneys in order to ensure they are highly error-free and fool-proof."
        ],
        img: "../assets/Doc_Validation_5.jpg"
      },
      {
        solutionsDescription: [
          "Intelligent search management system",
          "High Peak integrated the platform with an intelligent search management system that includes a global search and local search feature.",
          "The platform uses elasticsearch to enable the user to search for documents by type, by date and so on. In addition, the user can also search for certain elements within the documents as well."
        ],
        img: "../assets/Search_6.jpg"
      },
      {
        solutionsDescription: [
          "User management system",
          "The Legal Contracts Services platform serves three kinds of users: tagger, reviewer, and publisher. ",
          "The tagger annotates the uploaded contract document. The reviewer is responsible for reviewing the annotated contract and to ensure that the annotation is accurate. The publisher is in charge of publishing the annotated contract document. ",
          "Because the above-mentioned three different types of users have different requirements, the system must be able to adapt to the same. Therefore, the High Peak team built the platform in a way that caters to these different requirements, with role-based, secure information access."
        ],
        img: "../assets/User_7.jpg"
      },
      {
        solutionsDescription: [
          "Content management system",
          "High Peak developed a content management system that is integrated within the platform for secure storage of contracts and other relevant documents. These documents are categorized according to the contract type. ",
          "This content management system also enables the manager to assign attorneys based on the type of contract document received from the customer portal (featured in the next section). The manager also regulates the key considerations that attorneys must include in the annotated contract document."
        ],
        img: "../assets/Content_Management_8.jpg"
      },
      {
        solutionsDescription: [
          "Customer portal",
          "Customers can upload their contract documents on the platform using Google Drive or the file upload option from their device.",
          "Depending on the availability of attorneys and the uploaded file size, the users get a processed contract document which is annotated and summarized."
        ],
        img: "../assets/Customer_Portal_9.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["React.js"]
      },
      {
        domain: "Backend",
        technology: ["Node.js", "Python"]
      },
      {
        domain: "Database",
        technology: ["MySQL"]
      },
      {
        domain: "Hosted on",
        technology: ["AWS"]
      }
    ]
  },
  "manufacturing-process-optimization-solution": {
    projectName: "Manufacturing Process Optimization Solution",
    metaTitle: "High Peak Software - Manufacturing Process Optimization",
    metaDescription:
      "A process optimization system which works as a microservice architecture for a leading footwear/apparel manufacturing firm. ",
    metaKeywords:
      "Process optimzation, Production engineering, Manufacturing, Smart manufacturing, Business process optimization, Process analysis",
    openGraphURL:
      "https://highpeaksw.com/case-studies/manufacturing-process-optimization-solution",
    projectImage: "../assets/6_Footwear.png",
    clientOverViews: [
      "Our client is a subsidiary of a leading provider of process optimization, quality management and control services based in the U.S.",
      "The company provides SaaS platform services that digitize retailer and brands manufacturing ecosystems by providing real-time quality, productivity, speed, and order status data."
    ],
    problems: [
      "Production processes and quality control are important functions for any business. These processes are time-consuming, complex, and costly when carried out manually. ",
      "They are also highly error-prone, owing to human limitations. In addition, these manual processes are inefficient and productivity is low. ",
      "Our client wanted to automate some of these processes. High Peak undertook the project and helped digitize these processes."
    ],
    challenges: [
      "One of the major challenges for High Peak was catering to ad hoc requirements of the client, owing to frequent change requests. HPS designed the project from the initial stages including development of the proof of concept.",
      "The HPS team developed a system comprising five modules. Handling various elements in each of the modules such as RFID tags, custom forms, etc and managing the entire system was challenging.",
      "Because the project was vast and comprised of software and hardware attributes, High Peak  extended its services and assisted the client with their hardware requirements as well."
    ],

    solutionsShortDescription: [
      "High Peak helped design and develop the entire system which works as a microservice architecture. The system comprises five modules:"
    ],
    solutions: [
      {
        solutionsDescription: [
          "Intuitive learning management system",
          "The learning management system (LMS) has been built exclusively for artisans of varying literacy levels, who specialize in making shoes, apparel, and more. ",
          "Because the client’s factory outlets are located in many developing countries, HPS plans to localize the entire LMS application, including the video courses, in the near future to cater to many types of users. ",
          "This system enables artisans to learn and implement standard adherence. It comprises an overview dashboard, curriculum details, progress tracker, certifications taken, and employees’ resumes. This LMS platform offers courses in six categories, including standards, technology, quality, etc.  ",
          "The LMS is currently available in two languages: English and Mandarin. We are currently in the process of implementing a facial recognition software for the LMS, which will be used for identification and security purposes."
        ],
        img: "../assets/Learning_manahent_footwear_1.jpg"
      },
      {
        solutionsDescription: [
          "Shop floor live control",
          "The shop floor live control (SFLC) system has four components:",
          "RFID configurator, which allows the user to tag individual products in pairs or bundles for identification. ",
          "A master database with an infrastructure that stores extensive information pertaining to workstations, personnel, products, factory settings, etc. ",
          "Core SFLC, which provides an overview of the entire process, including defective workstations, production stalling, potential issues, and so on. This is used by factory supervisors, floor control managers, production managers, and line managers. This helps implement root cause analysis of issues, cataloging problem assumptions using the fishbone and 5why strategies, and proposed corrective action plans. ",
          "A quality control platform, which enables quality control personnel to digitally log defects from a prepopulated set of categories corresponding to a process or product.",
          "SFLC comprises language localization capabilities, including Portuguese and Tamil. The language models are developed in-house at HPS."
        ],
        img: "../assets/Shop_Floor_2.jpg"
      },
      {
        solutionsDescription: [
          "Digital quality inspection platform",
          "The digital quality inspection platform equips a factory inspector with an exhaustive checklist of inspection points, categorised by development, commercialization, and production. ",
          "This enables the surveyors and inspectors to collect information on factory environment conditions, workstation health, and other related problems and catalog them in forms. The platform also acts as a process sequence designer whose data is fed to the SFLC."
        ],
        img: "../assets/Quality_Footwear_3.jpg"
      },
      {
        solutionsDescription: [
          "IT asset management system",
          "The IT asset management system (ITAM) doubles up as a pulse monitor for tablets and other workstations, tracking vital components such as the memory, battery usage, etc. This system alerts the users in case of any system failure. ",
          "The data collected in the system would be fed to a predictive analytics model in the near future to predict the failure of systems in advance."
        ],
        img: "../assets/Asset_Management_4.jpg"
      },
      {
        solutionsDescription: [
          "Form design system",
          "The form design system leverages Form.IO for custom-designing forms and questionnaires for survey and feedback collection by factory and quality inspectors. ",
          "HPS chose Form.IO because it is an open source tool and enabled the development team to design forms easily with minimal coding efforts."
        ],
        img: "../assets/Forms_footwear_5.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["React.js", "Next.js"]
      },
      {
        domain: "Backend",
        technology: ["Java", "Spring Boot"]
      },
      {
        domain: "Database",
        technology: ["MySQL", "MongoDB"]
      },
      {
        domain: "Excel parsing",
        technology: ["Apache Poi"]
      }
    ]
  },
  "lawyer-intelligence-augmentation-system": {
    projectName: "Lawyer Intelligence Augmentation System",
    metaTitle: "High Peak Software - Decision Making Platform for Lawyers",
    metaDescription:
      "A lawyer intelligence software enabling attorneys to handle case related documents, and built-in features such as report generation and email autosummarization.",
    metaKeywords:
      "Lawyer intelligence software, Legal solutions software, Enterprise legal management software, Lawyer case management software, Legal document management software",
    openGraphURL:
      "https://highpeaksw.com/case-studies/lawyer-intelligence-augmentation-system",
    projectImage: "../assets/7_LawyerIntelligence.png",
    clientOverViews: [
      "Our client, a law firm in the U.S, delivers legal, risk and compliance solutions to enterprises. ",
      "The company mainly focuses on enabling lawyers to have access to critical case-related information that would allow them to make informed decisions quickly."
    ],
    problems: [
      "Law firms and attorneys face a major challenge in dealing with large amounts of paperwork and documentation. This could include conducting extensive research, documentation, paralegals, paperwork, and more. This process is usually time-consuming and not qualitative. ",
      "Our client  wanted to create a software for lawyers that would allow them to retrieve case relevant information, highlight important information from documents, and obtain intelligently prepared reports, so that they would be able to arrive at factual decisions quickly."
    ],
    challenges: [
      "The client was dealing with paperwork comprising scanned PDF files and other related documentation. Extracting data from a large quantity of files was time-consuming and inefficient. ",
      "A major challenge for High Peak was to make this process scalable and robust. The team achieved this by implementing optical character recognition (OCR) technology for data extraction and natural language processing algorithms for analyzing large amounts of data. ",
      "Implementing visualizations such as heat maps and circos charts for large scale data also presented as a challenge for the team. ",
      "Because the application was built for non-technical users i.e., lawyers, High Peak had to commit to a UI design that was simple for lawyers to use and understand.",
      "Yet another challenge was catering to ad hoc requirements of the client, owing to frequent change requests. The team designed multiple versions for implementation of features and each stage of the design went through a rigorous approval process on the client side."
    ],

    solutionsShortDescription: [
      "High Peak developed a lawyer intelligence augmentation software that includes:"
    ],
    solutions: [
      {
        solutionsDescription: [
          "Case management system",
          "The lawyer intelligence augmentation application includes a smart and interactive case management system. This system operates on a role-based access, catering to three types of users: global admin, practice admin, and case owner.  ",
          "The case management system enables the user to create cases with attributes such as case titles, dates, jurisdiction, opposing attorneys, judges, etc. Users can invite collaborating attorneys to participate in, contribute to, and access details and documents of a particular case. ",
          "Users can upload raw and native files or processed files pertaining to a case using the platform. How the platform processes these files is provided in detail in the upcoming sections."
        ],
        img: "../assets/Case_management_lawyer_1.jpg"
      },
      {
        solutionsDescription: [
          "Automated document classification",
          "The platform uses machine learning models to automatically detect the various types of documents when they are uploaded to the platform. For instance, the user can upload contract documents, invoices, images to the platform, and the platform is capable of identifying it as such automatically. ",
          "High Peak uses machine learning models that are intelligent enough to learn from the data they consume and more accurately identify document types with time."
        ],
        img: "../assets/Doc_Classification_2.jpg"
      },
      {
        solutionsDescription: [
          "Automated data extraction",
          "Among the files that are uploaded to the intelligence augmentation system, images are a popular type. ",
          "High Peak implemented automated data extraction for extracting data from image files by employing OCR technology.  ",
          "The platform allows the user to upload any kind of document including text, pdf, images etc. The textual content in some of these document types are not readable by the machine because they are images. In order to extract information from such document types, the platform uses OCR technology and retrieves relevant information. ",
          "This extracted data is then accordingly categorised by our DL algorithms. For example, the algorithms can understand the extracted information presented in a DD/MM/YYYY format as a date."
        ],
        img: "../assets/data_extraction_Lawyer_3.jpg"
      },
      {
        solutionsDescription: [
          "Autosummarization of emails",
          "The platform uses machine learning models and natural language processing to summarize the text in emails. This summarized text is presented as a snippet. ",
          "The lawyer can use the auto summarization feature to view a summarized version of all the emails in a thread. This saves time and effort of the lawyers.",
          "In addition, information such as email threads, number of emails can be viewed by the lawyer working on the case."
        ],
        img: "../assets/Summary_Lawyer_4.jpg"
      },
      {
        solutionsDescription: [
          "Sentiment analysis",
          "The platform includes sentiment analysis which gauges the sentiment of text as positive, neutral, or negative in the email bodies. Each sentence within the email body is tagged as positive or negative. ",
          "High Peak used pre-trained language models and natural language processing for implementing sentiment analysis. These language models learn to predict the sentiment of the text based on the context of the sentence. ",
          "This application is currently in the data collection and model training phase."
        ],
        img: "../assets/Sentiment_5.jpg"
      },
      {
        solutionsDescription: [
          "Content management system",
          "The content management system (CMS) enables lawyers to organize and maintain all case-related information. The CMS comprises four main categories:",
          "Facts: Any insights or conclusions derived from the case documents can be stored as a fact by the lawyer. ",
          "Notes: The notes feature allows lawyers to maintain reference notes, case highlights, and any information that is significant to the case they are working on. ",
          "Issues: High-level and incident-level legal issues pertinent to the case can be logged in this section of the CMS.",
          "Source: Lawyers can organize and store all the case-related documents with the source feature. For instance, the number of processed files, unprocessed files, email threads, and so on. This feature enables lawyers to have quick and immediate access to all necessary files in one place."
        ],
        img: "../assets/Content_Manaegement_6.jpg"
      },
      {
        solutionsDescription: [
          "Interactive data visualization",
          "The platform stores information such as client documents, emails, and case-related files in various visual formats–Heat maps, Circos chart. ",
          "Lawyers can use the heat maps to view events (emails and documents) pertinent to any given date concerning a particular case in a timeline format. Lawyers can also access suggested files and documents relating to cases similar in nature to their case.",
          "The circos chart enables lawyers to access email exchanges between the two parties. This interactive chart also allows lawyers to filter email exchanges by subject, timeline, sender, recipient, and so on.",
          "This visual representation of data enables lawyers to access the most relevant and necessary information pertaining to the case and helps them in arriving at a suitable decision. The case timeline provides the lawyer a quick overview of the case and relevant documents."
        ],
        img: "../assets/Data_Visualisation_7.jpg"
      },
      {
        solutionsDescription: [
          "Intelligent search mechanism",
          "The platform includes an intelligent search mechanism with a global search and local search feature.",
          "The platform uses elasticsearch to enable the lawyer to search for keywords, names, dates, emails and more. The synonyms search feature allows the lawyer to type in a word and access lists of all the files containing the keyword and its synonyms. ",
          "In addition, the user can also search for specific elements on a given tab/page using the local search feature."
        ],
        img: "../assets/Search_lawyer_8.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["React.js"]
      },
      {
        domain: "Backend",
        technology: ["Java"]
      },
      {
        domain: "Database",
        technology: ["MySQL"]
      },
      {
        domain: "NLP",
        technology: ["Python"]
      },
      {
        domain: "Searching",
        technology: ["Elasticsearch"]
      }
    ]
  },
  "healthcare-referral-platform": {
    projectName: "Healthcare Referral Platform",
    metaTitle: "High Peak Software - Healthcare Referral Platform",
    metaDescription:
      "A healthcare referral platform that enables doctors to refer patients to specialists for better healthcare information and access. ",
    metaKeywords:
      "Referral program, healthcare tech, healthcare referral, healthcare management system, healthcare management services, referral management system, referral system, referral services",
    openGraphURL:
      "https://highpeaksw.com/case-studies/healthcare-referral-platform",
    projectImage: "../assets/8_Healthcare.png",
    clientOverViews: [
      "Our client, a leading software company in the US, develops IT solutions for healthcare service providers. ",
      "The company aims to deliver optimal healthcare services to all individuals through a referral system for physicians to refer specialised treatments and other physicians to their patients."
    ],
    problems: [
      "The need for quality healthcare is a global concern. Healthcare institutions are increasingly looking to technology to empower doctors and medical companies to provide exceptional healthcare services to patients. ",
      "It is a common practice for general physicians to refer their patients to specialist doctors or hospitals. Oftentimes, these specialised healthcare providers may not work in the same clinic or location as the referring doctor and may lead to misconceptions. ",
      "Our client wanted to build an all-inclusive application that enables healthcare practitioners to refer patients to specialists for better specialized healthcare access and information."
    ],
    challenges: [
      "The client had an application previously developed by a former vendor. This version of the application made it difficult to onboard individual physicians to the application and wasn’t yielding satisfactory results. ",
      "A major challenge for the High Peak team was to make significant improvements to the existing application and make it scalable. ",
      "Owing to the lack of an admin website, onboarding and managing hospitals and individual physicians was challenging. Additionally, reaching out to physicians and medical professionals was difficult. ",
      "Because the application deals with sensitive healthcare data, it had to be compliant with the Health Insurance Portability and Accountability Act. This ensures that the application adheres to the regulations and sets a standard for protecting sensitive patient data."
    ],

    solutionsShortDescription: [
      "High Peak built this healthcare referral application for hospitals, doctors, physicians, healthcare specialists, and pharmaceutical companies.",
      "This platform aims to help doctors establish a referral network of hospitals, physicians, and medical companies in order to provide patients with more accessible healthcare. It comprises:"
    ],
    solutions: [
      {
        solutionsDescription: [
          "Referral discovery system",
          "The referral discovery system enables physicians to search for other physicians, medical facilities, clinical trials, medical devices, and more. This information allows physicians to refer their patients to other physicians accordingly."
        ],
        img: "../assets/Referral_HelthCare_1.jpg"
      },
      {
        solutionsDescription: [
          "Referral management system",
          "The referral management system allows each physician to create a detailed profile indicating their level of medical expertise and their specialty. It also enables physicians to refer their patients to other physicians based on his/her medical requirements so the patient can receive suitable medical treatment."
        ],
        img: "../assets/Management_referral_2.jpg"
      },
      {
        solutionsDescription: [
          "Role-based access",
          "The global admin is responsible for administrative tasks such as sending out invites to hospitals, individual doctors, and medical companies. The system is built so that users can gain access to the application through an invite-only basis. ",
          "The practice admin operates on the hospital side. Practice admins can add information such as location, facilities, branch locations of hospitals, and invite physicians. ",
          "In addition to this, the system automatically schedules operations and sends out push notifications to the doctors in the hospital. Based on multiple factors such as availability and specialization, doctors pick patients to be treated."
        ],
        img: "../assets/Role_Based_Access_health_3.jpg"
      },
      {
        solutionsDescription: [
          "Effective team communication platform",
          "Physicians can communicate with other physicians via a group chat platform once they have accepted the referral. Because every physician has a supporting team comprising junior doctors, interns, and nurses, they can be added to the group chat as well. ",
          "In addition, other communication exchanges within the teams happen via a general group chat platform on the system"
        ],
        img: "../assets/Communication_health_4.jpg"
      },
      {
        solutionsDescription: [
          "Healthcare marketing & campaigning platform",
          "High Peak developed a supporting web application integrated with the healthcare referral platform to carry out healthcare marketing and campaigning. ",
          "This marketing platform is used by medical and pharmaceutical companies to market and sell pharmaceutical drugs and medical devices to hospitals and clinics.",
          "Further, non-sensitive medical data such as high frequency patient visits, regions for maximum patient referrals, and hospitals facilities are analysed by these companies to determine their marketing and sales strategies."
        ],
        img: "../assets/Marketing_HealthCare_5.jpg"
      },
      {
        solutionsDescription: [
          "Logistics & inventory management system",
          "The medical companies can create an inventory of their products on the application and associate them to respective doctors. This inventory can be accessed by the doctors and hospitals who are authorized to recommend the product to their patients."
        ],
        img: "../assets/Logistics_Health_6.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["React.js", " Redux"]
      },
      {
        domain: "Backend",
        technology: ["Node.js", "Java", "Spring Boot"]
      },
      {
        domain: "Database",
        technology: ["MySQL"]
      },
      {
        domain: "Hosted on",
        technology: ["AWS"]
      },
      {
        domain: "App development",
        technology: ["Flutter", "Dart"]
      }
    ]
  },
  "professional-translation-service-platform": {
    projectName: "Professional Translation Services Platform",
    metaTitle: "High Peak Software - Translation Services Platform",
    metaDescription:
      "A Professional Translation Services Platform for organizations to transcribe and translate local languages into English text for qualitative research purposes. ",
    metaKeywords:
      "Translation software, Annotation software, Professional Translation and annotation software, Online Audio transaltion and annotation software, Bangla to English translation and annotation platform",
    openGraphURL:
      "https://highpeaksw.com/case-studies/professional-translation-service-platform",
    projectImage: "../assets/9_Professional.png",
    clientOverViews: [
      "Our client works with the UN, several NGOs, and the government in South East Asia, specifically focused on automating translation of research surveys and interviews. ",
      "They aim to connect the government and non-profit organizations with dedicated retainers of translators, transcribers, and editors, and improve the turn-around time and quality control of the end-to-end translation services process."
    ],
    problems: [
      "INGOs conduct and document interviews and surveys as a part of their research process. Oftentimes, the interviewers need to have knowledge of the local languages spoken in the particular area so that they are able to communicate with the local residents. ",
      "The current challenge for INGOs operating in developing countries is the lack of a mature qualitative and quantitative research value chain, especially around translation and transcription of the local languages into English. They often lack the technical and organizational capacity to overcome such challenges. ",
      "Our client wanted to alleviate these problems by providing a platform to provide transcription and translation services to INGOs with a primary focus on quality control."
    ],
    challenges: [
      "One of the major challenges for the team was to process the large quantities of data in audio and video formats. ",
      "Owing to the scarcity of training data, the team faced a challenge in building machine learning models to incorporate multiple languages. ",
      "Ensuring quality control of all annotated data was yet another challenge."
    ],

    solutionsShortDescription: [
      "High Peak designed and developed a platform that enables INGOs and research organizations to transcribe and translate local languages into English text. ",
      "The platform aims to serve as a tool for INGOs to manage their qualitative research processes."
    ],
    solutions: [
      {
        solutionsDescription: [
          "Professional translation services platform",
          "With the current translation services platform, users can: ",
          "Upload audio and video files to the platform for transcription and translation.",
          "Access free file storage. High Peak used AWS Elastic File Storage (EFS) for storing and securing user-related files.   ",
          "Make payments via an online payment gateway. ",
          "Store uploaded files and download processed files. ",
          "Get support. The FAQs section on the platform allows users to understand the workings of the platform.",
          "High Peak encrypted all communications and transactions with SSL encryption."
        ],
        img: "../assets/Translation_Annotation_1.jpg"
      },
      {
        solutionsDescription: [
          "Annotation platform",
          "High Peak designed and developed a native android application that works like an annotation tool. ",
          "It is essentially a crowdsourcing platform for gathering annotation data and enabling transcribers and translators to collaborate. ",
          "High Peak is working to make this into a progressive web application in the future. ",
          "With the current annotation platform annotators can perform the following actions based on their roles and responsibilities:",
          "Access audio and video files for transcription or translation. ",
          "Transcribe audio and/or video files.",
          "Translate audio and/or video files.",
          "Edit and review the annotated files. ",
          "Upload processed documents to the platform to perform either of the above mentioned actions.",
          "Receive payment for completion of work."
        ],
        img: "../assets/Annoitation_Translation_2.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["Kotlin", "React.js"]
      },
      {
        domain: "Backend",
        technology: ["Java", "Spring Boot"]
      },
      {
        domain: "Database",
        technology: ["MySQL"]
      }
    ]
  },
  "health-and-fitness-monitoring-application": {
    projectName: "Health & Fitness Monitoring Application",
    metaTitle: "High Peak Software - Health & Fitness Monitoring Application",
    metaDescription:
      "The health & fitness monitoring application acts as a personal guide that helps sports personnel, fitness enthusiasts, and users understand their health. ",
    metaKeywords:
      "Health monitoring application, fitness monitoring application, health tracker, fitness tracker, health tracking app, fitness tracking app, heart rate monitor ",
    openGraphURL:
      "http://highpeaksw.com/case-studies/health-and-fitness-monitoring-application",
    projectImage: "../assets/10_Health.png",
    clientOverViews: [
      "Our client is a global transformation leadership organization in Germany enabling members to transform themselves, their organizations, their relationships, and ecosystems.",
      "Their mission encompasses business and personal development, social and sustainability activism, as well as the arts, both in for-profit or not-for-profit endeavors."
    ],
    problems: [
      "A global healthcare report by WHO reveals that 70% of worldwide deaths occur due to non-communicable diseases like diabetes, cancers, and heart disease. ",
      "Factors such as air pollution, climate change, incompetent primary healthcare, and others result in a myriad of health problems globally.",
      " ",
      "Our client wanted to develop a mobile application which acts as a personal guide and helps users optimize their lifestyle and keep track of fitness."
    ],
    challenges: [
      "The client already had a working application in place which was developed by another vendor previously. Gathering data was a challenge for the team owing to the transfer of project from another vendor to High Peak. ",
      "Another challenge for the team was to update the application’s existing framework on both iOS and Android."
    ],

    solutionsShortDescription: [
      "High Peak developed a personal guide that helps users understand their health. The application has been developed for sports personnel, fitness enthusiasts, and individual users."
    ],
    solutions: [
      {
        solutionsDescription: [
          "User management system",
          "Users can create their profile on the mobile application. Each user profile comprises three sections:",
          "General section",
          "Users can add their personal information–height, weight, gender, age, drinking and smoking habits etc.",
          "Activity section",
          "This section is for recording information such as number of steps and metabolism rate. This data is pulled from Apple Health and Google Fit for iOS and Android users respectively.",
          "Healthcare section",
          "This is for recording medical data–heart rate, blood pressure, and resting pulse."
        ],
        img: "../assets/User_Fitness_1.jpg"
      },
      {
        solutionsDescription: [
          "Fitness monitoring",
          "Users can add their personal information–height, weight, gender, age, drinking and smoking habits etc.",
          "In addition, users can add information such as number of steps walked and metabolism rate. This data is pulled from Apple Health and Google Fit for iOS and Android users respectively."
        ],
        img: "../assets/Fitness_Fitness_2.jpg"
      },
      {
        solutionsDescription: [
          "Personalized suggestions",
          "Users receive suggestions for personalized medical information and products specific to individual health.",
          "Users can browse and search for health-related articles on various topics such as sleep, nutrition, fitness, lifestyle, alternative therapy, e-health, psychology and more.",
          "The application is integrated with and pulls vital data from Apple Health and Google Fit for iOS and Android users respectively."
        ],
        img: "../assets/Personalised_Fitness_3.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["Swift for iOS", "Kotlin for Android"]
      },
      {
        domain: "Backend",
        technology: ["Node.js"]
      },
      {
        domain: "Database",
        technology: ["MongoDB"]
      }
    ]
  },
  "ecommerce-logistics-application": {
    projectName: "ECommerce Logistics Application",
    metaTitle: "High Peak Software - Ecommerce Logistics",
    metaDescription:
      "An intelligent construction matrerials delivery system that allows construction workers to deliver construction supplies to customers efficiently.",
    metaKeywords:
      "Ecommerce logistics, ecommerce delivery, construction ecommerce. construction materials management, order fulfillment, ecommerce delivery service, delivery service, delivery management system",
    openGraphURL:
      "http://highpeaksw.com/case-studies/ecommerce-logistics-application",
    projectImage: "../assets/11_Ecommerce.png",
    clientOverViews: [
      "Our client is an app-based and web-interface e-commerce logistics services provider connecting the driver network to the construction & building material supply chain.",
      "They aim to solve the frustrations that builders and suppliers face when they need to get materials from one location to the next."
    ],
    problems: [
      "Construction services in the U.S. are often costly, and construction workers spend a lot of time transporting and delivering construction materials. A client survey predicts that construction workers spend upwards of 200 hours in gathering construction materials, leading to an annual loss of $10K per individual.",
      "Our client in the U.S. wanted to combat such construction-related revenue loss. They wanted to build a robust and intelligent system that allows construction workers to remotely outsource locating and delivering building supplies to a job site. ",
      "This system was required to manage the delivery of heavy materials to customers in the shortest possible time, while reducing the overall cost of transportation and delivery."
    ],
    challenges: [
      "The client was already working with a vendor and was not satisfied with the outcome. They had invested a lot of time and resources. Owing to this, the go-to-market timeline was very aggressive when the project was handed over to High Peak.",
      "The High Peak team had a very short learning curve. The team had to step up to redesign and develop an effective and usable product in a limited amount of time. ",
      "Logic computation for handling operations involved several factors, including allocation of trucks in proportion to the bulk of the order, computing shortest possible delivery routes, locating more than one store in case of product unavailability, and so on."
    ],

    solutionsShortDescription: [
      "High Peak designed and developed three solutions for the e-commerce logistics application platform: ",
      "1. A customer-facing construction materials delivery app on both iOS and android operating systems, ",
      "2. A driver-facing app for the drivers to communicate with the users and customer satisfaction managers,",
      "3. A configurable admin console web app "
    ],
    solutions: [
      {
        solutionsDescription: [
          "Intelligent product ecommerce ecosystem",
          "The customer app enables customers to search for and discover products, add items to the cart, and place orders. The app also allows customers to select different delivery options based on the size of the order they are placing. For instance, customers may choose a bigger truck for delivery if they order more items. ",
          "The app also facilitates push notifications and allows users to edit orders and upload invoices.",
          "High Peak integrated the app with a robust payment gateway to process secure payments and transactions."
        ],
        img: "../assets/Product_Discovery_1.jpg"
      },
      {
        solutionsDescription: [
          "Real-time delivery tracking",
          "High Peak implemented real-time delivery tracking on the customer app that allows customers to track their delivery after having placed an order.",
          "Once the customer places an order on the customer app, the details of the order and delivery status are shared with the customer via SMS, emails, and in-app notifications.",
          "In addition, the invoice is shared with the customer once the order is delivered."
        ],
        img: "../assets/Delivery_Tracking_ecommerce_2.jpg"
      },
      {
        solutionsDescription: [
          "Driver assignment system",
          "After a customer places an order, the driver assignment system assigns the order to a driver. The driver is notified of orders on the driver-facing app. The driver assignment is based on other parameters as well, including vehicle make and size, bulk size of order, etc."
        ],
        img: "../assets/Driver_Management_3.jpg"
      },
      {
        solutionsDescription: [
          "Order processing system",
          "After a customer places an order, the order processing system finds the nearest stores where the ordered products are available. After buying the materials, the assigned driver can deliver the products to the required location.  ",
          "Verification processes are introduced at various stages. For instance, the driver can capture images of ordered products and send them to a Customer Satisfaction Manager (CSM) for verification. The driver can also attach an invoice for the purchases made and send it via email. ",
          "Additionally, drivers can raise issues through the driver-facing app that will alert the CSM, who can quickly communicate with the end customer to resolve the issue."
        ],
        img: "../assets/Orders_Commerce_4.jpg"
      },
      {
        solutionsDescription: [
          "Route management system",
          "High Peak designed the route management system to facilitate drivers to travel the shortest route possible for their delivery, thereby saving their time and effort."
        ],
        img: "../assets/Route_commerce_5.jpg"
      },
      {
        solutionsDescription: [
          "Configurable admin console",
          "The admin console enables CSMs to control the entire workflow and override automation of certain features. ",
          "This includes user actions such as changing the store location, checking for quantity of products, allocating/changing drivers according to the shortest route possible, deciding trucks size in proportion to the order, and checking for availability of products. ",
          "The portal also facilitates real-time tracking of trucks and drivers, direct communication with end customers, and quickly issue resolution."
        ],
        img: "../assets/Admin_console_Commerce_6.jpg"
      },
      {
        solutionsDescription: [
          "Automated order management system",
          "High Peak designed and developed an automated order management system that enables the CSMs to process orders, assign stores, assign trucks to drivers, and manage orders. ",
          "Additionally, if there is any discrepancy in the order, or issues such as non availability of an item, the CSM can communicate with the customers and resolve their issues."
        ],
        img: "../assets/Order_Processing_Commerce_7.jpg"
      },
      {
        solutionsDescription: [
          "User management system",
          "The user management system enables CSMs to manage the drivers and orders. The system also allows for tracking and updating of order statuses. Notifications about the status of the orders are sent to drivers via emails and SMSes through the user management system."
        ],
        img: "../assets/User_Commerce_8.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["Cordova", "Swift", "Kotlin", "React.js"]
      },
      {
        domain: "Backend",
        technology: ["C#", ".NET", "Java"]
      }
    ]
  },
  "automated-payment-processing-solutions": {
    projectName: "Automated Payment Processing Solutions",
    metaTitle: "High Peak Software - Payment Processing Services",
    metaDescription:
      "An end-to-end payment processing solution for automation of  client and vendor payment operations such as invoicing and payment processing.",
    metaKeywords:
      "Automated payment processing system, Payment processing services, Automated check processing, Invoice processing automation, Credit score assignment using ML models",
    openGraphURL:
      "https://highpeaksw.com/case-studies/automated-payment-processing-solutions",
    projectImage: "../assets/12Automated.png",
    clientOverViews: [
      "Our client is a provider of payment processing and information management services to the United States commercial and government vehicle fleet industry, Canada, South America, Europe, Asia, and Australia.",
      "They simplify the complexities of payment systems across continents and industries, including fleet, corporate payments, and health."
    ],
    problems: [
      "A leading payment processing and information services company working across the globe  processes over 8000 invoices on a daily basis. This entire process was being carried out manually and required a substantial amount of time and effort. In addition, the amount of resources and manual labor costs involved in the process posed a major challenge for the organization. ",
      "The organization wanted to automate the client and vendor payment operations of invoicing and payment processing through the development of an end-to-end ecosystem that would allow them to seamlessly automate and manage the complete process. ",
      "In order to overcome this challenge, they approached High Peak."
    ],
    challenges: [
      "Vendors send invoice and check payment information in several different formats, including digital attachments (CSV files, PDFs, etc.) and scanned copy attachments (images files such as TIFF, PNG, etc) over emails. Data extraction, such as retrieving invoice numbers, bill amounts, vendor names, and so on, from these files posed a heavy challenge, owing to this difference in document formats.",
      "Another challenge was to accurately process blurred scanned copy. Automated data extraction from such blurred images demanded intelligent scanning and information identifying mechanisms. Initially, the team used Tesseract OCR engine to perform the above-mentioned process. However, they quickly found out that the output accuracy was less than 50%. Therefore, the team had to find an alternate tool to perform this operation. ",
      "Because image files were sometimes heavy, processing big data was hampered, owing to memory and performance limitations of the client’s IT infrastructure. The High Peak team had to overcome this challenge to improve processing time, while reducing memory and processing efforts."
    ],

    solutionsShortDescription: [
      "High Peak helped design and develop four applications for the client:"
    ],
    solutions: [
      {
        solutionsDescription: [
          "Automating inbound payments",
          "The role of this application is autonomously and securely detecting whether an email has an attachment of either an invoice or a payment check and accurately extracting relevant payment information. HPS effectively automated this process. ",
          "This implementation is particularly challenging because the email attachments come in various formats, including digital attachments (CSV files, PDFs) and scanned copy attachments (images files such as TIFF, PNG).  ",
          "Initially, HPS used the Tesseract OCR engine to perform data extraction. However, they soon found that the accuracy level of Tesseract was not up to the mark. Therefore, in order to improve the accuracy of extracted data, HPS tested out various other third party engines and chose to employ Google Vision OCR for its high accuracy. In order to speed up performance and to handle increase in inbound traffic, HPS employed multithreading for handling several documents at once. ",
          "This automation essentially helped us reduce the number of manual hours and save overhead costs. In addition, we learned that by automating this process and increasing processing time, productivity improved because it allowed the employees to focus on the core operations rather than long drawn repetitive tasks."
        ],
        img: "../assets/Payment_Inbound_1.jpg"
      },
      {
        solutionsDescription: [
          "Automating client payment operations",
          "HPS built the Remittance system to securely and automatically extract payment information from the clients’ checks. The system validates the extracted data against a master spreadsheet and produces two outputs: fully-matched information and close-match as CSV files.",
          "The close-match CSV file may contain 500-600 rows of partial information extracted from blurry in images that would later be filled and validated manually. The entire process is carried out without any manual intervention, thereby preventing confidential data leaks. ",
          "High Peak is currently working with the client’s team to improve both the speed and accuracy of data extraction."
        ],
        img: "../assets/Client_Payments_2.jpg"
      },
      {
        solutionsDescription: [
          "Interactive email engagement",
          "HPS designed an automated email system that allows for a two-way communication between the organization and their clients. ",
          "Automated notification emails are sent to the clients to remind them of routine debtor payment. The email scheduling process involves a series of complex flows. HPS automated the scheduling of emails and created email templates for each flow. ",
          "HPS designed the UX in a way that is simple and easy to use without complications or clutter."
        ],
        img: "../assets/Email_Payments_3.jpg"
      },
      {
        solutionsDescription: [
          "Assigning credit score",
          "Initially, our client used the Cadence software to manually rate and review clients and vendors. Because this process was carried out manually, the client decided to automate this process. ",
          "HPS is in the process of developing machine learning (ML) models to determine and assign credit scores for clients based on various factors, such as payment history, due date adherence, and more. ",
          "The ML models will be using the data collected from inbound and client payment operations applications, and certain predefined metrics. The credit score would enable our client to assess whether a client would make payments on time or not."
        ],
        img: "../assets/Credit_Payment_4.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["React.js"]
      },
      {
        domain: "Backend",
        technology: ["Java", "Spring Boot"]
      },
      {
        domain: "Database",
        technology: ["MySQL"]
      },
      {
        domain: "ML models",
        technology: ["Python"]
      },
      {
        domain: "OCR",
        technology: ["Google Vision"]
      }
    ]
  },
  "multi-currency-digital-wallet": {
    projectName: "Multi Currency Digital Wallet",
    metaTitle: "High Peak Software - Multi-currency Digital Wallet",
    metaDescription:
      "An intelligent multi currency digital wallet that allows users to send, transfer, and exchange international money, irrespective of their location.",
    metaKeywords:
      "Digital payment software, Multi-currency digital payment software, Multi currency digital wallet, ",
    openGraphURL:
      "https://highpeaksw.com/case-studies/multi-currency-digital-wallet",
    projectImage: "../assets/13_MultiCurrency.png",
    clientOverViews: [
      "Our client wields 25 years of experience in financial markets with a focus on payments and foreign exchange at foreign and domestic financial institutions."
    ],
    problems: [
      "Traditional bank money exchanges and online payments have many hidden costs termed as transaction fees that users may lose out on. This is particularly a big challenge for frequent travelers, expatriates, immigrants, and students studying abroad.  ",
      "To alleviate this problem, our client in the U.S. wanted to develop an intelligent digital wallet, which would mitigate these hidden costs and facilitate users to instantaneously transfer or exchange international money."
    ],
    challenges: [
      "In the U.S., obtaining proper licenses for setting up the digital wallet business poses a heavy challenge, owing to strict financial regulations. ",
      "Further, partnering up with banks in the U.S. was also quite demanding because they are wary of implementing cryptocurrency."
    ],

    solutionsShortDescription: [
      "High Peak Software developed a multi currency digital wallet that allows users to send, transfer, and exchange international money, irrespective of their location. In addition, the digital wallet allows use of various cryptocurrencies for exchange and transfer. ",
      "Currently, however, the digital wallet supports transfers to Mexico and the Philippines. The client is intent on adding new markets in 2020. ",
      "The client is also partnering with Ripple to employ the blockchain technology and cryptocurrencies for securing international money transfers and payments. Users are now able to get the best current FX rates, and money transfers and payments are done instantaneously. ",
      "This digital wallet is available as iOS, Android apps."
    ],
    solutions: [],
    technologies: [
      {
        domain: "Frontend",
        technology: ["Kotlin", "Swift"]
      },
      {
        domain: "Backend",
        technology: ["Java"]
      },
      {
        domain: "Database",
        technology: ["MySQL"]
      },
      {
        domain: "Blockchain and cryptocurrency",
        technology: ["RippleNet"]
      }
    ]
  },
  "intelligent-ad-campaign-optimization-solution": {
    projectName: "Intelligent Ad Campaign Optimization Solution",
    metaTitle: "High Peak Software -  Intelligent Ad Campaign Optimization",
    metaDescription:
      "An intelligent predictive ad campaign optimization application that enhances and improves the efficiency of of ad publishing for their end customers.",
    metaKeywords:
      "Predictive analytics, campaign management system, advertising campaign optimization, campaign optimization, media tech, campaign management tools, display campaigns",
    openGraphURL:
      "https://highpeaksw.com/case-studies/intelligent-ad-campaign-optimization-solution",
    projectImage: "../assets/14_IntelligentAd.png",
    clientOverViews: [
      "Our client is a leading online financial marketing platform with an audience of 25 million influential decision makers, including individual investors, active traders, financial advisors, institutional investors and so on.",
      "The organisation empowers the financial ecosystem of publishers, brands and audiences through highly effective digital solutions designed specifically for the financial services industry using a unique mix of data and technology."
    ],
    problems: [
      "Our client is a leading financial media publishing company in the U.S dealing with ad campaigns. ",
      "They were able to publish only one ad at a time on one website. Because the client publishes ads on nearly 130 websites, this process required a significant amount of manual effort and time. In addition, the administrative processes were slow. ",
      "They were using an outdated system that prevented them from customizing the features for their use. The client approached High Peak to develop an intelligent campaign optimization solution."
    ],
    challenges: [
      "High Peak had to make significant changes and add customized features to the application.",
      "A major challenge for the team was to rewrite the code to improve its robustness and security by making the system invulnerable to outside attacks. ",
      "The codebase obtained from the existing system was not clean. So the HPS team had to clean up the code and ensure that best practices were followed with respect to code standardization. The team also had to enhance the entire codebase for scalability and consistency."
    ],

    solutionsShortDescription: [],
    solutions: [
      {
        solutionsDescription: [
          "Enhanced campaign management system",
          "The campaign management system allows bulk addition of ads across several hundred websites at a time, thereby saving time and effort. ",
          "With the previous system, bulk additions were not possible and took a considerable amount of time to publish ads. ",
          "Users can clone campaigns and replicate the settings and functionalities of one campaign for other campaigns. In addition, campaigns can also be customized according to different requirements."
        ],
        img: "../assets/Campaign_Intelligent_1.jpg"
      },
      {
        solutionsDescription: [
          "Intelligent report generation",
          "The intelligent report generation tool generates reports showcasing various metrics such as ad viewability, click-through rate, cost-per-click, and so on. These reports are generated in near real-time and on demand. ",
          "These reports help in determining the top-performing websites as well as the non-performing ones. With this information, the client is able to make informed decisions regarding the campaigns. ",
          "For instance, the campaigns are optimized to include only the top-performing publishers and non performing campaigns are eliminated."
        ],
        img: "../assets/Report_Intelligent_2.jpg"
      },
      {
        solutionsDescription: [
          "Interactive dashboard",
          "High Peak designed and developed an interactive dashboard that provides users an overview of all the campaigns and other important information such as top-performing websites enabling them to take quick actions."
        ],
        img: "../assets/Dashboard_Intelligent_3.jpg"
      },
      {
        solutionsDescription: [
          "Improved backend security",
          "The HPS team re-architected the backend of the application for improved security. Code standardization was carried out by rewriting the code to improve the quality and structure. ",
          "Because the Spring Framework has an extensive online community and support, the team decided to include the latest version of the framework within the application. The Spring Framework comes with Spring Security, which improves backend data safety and access."
        ],
        img: "../assets/Security_Intelligent_4.jpg"
      },
      {
        solutionsDescription: [
          "User management system",
          "High Peak designed and developed a user management system for assigning roles and responsibilities to users; for instance, campaign managers, team members, and so on. ",
          "In order to cater to these different requirements, the High Peak team built the platform with role-based, secure information access."
        ],
        img: "../assets/User_Copy_5.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["Keras.js"]
      },
      {
        domain: "Backend",
        technology: ["Java", "Spring Boot"]
      },
      {
        domain: "Hosted on",
        technology: ["AWS"]
      },
      {
        domain: "Pipeline management",
        technology: ["Apache AirFlow"]
      },
      {
        domain: "Data Processing",
        technology: ["Apache Spark", "Amazon RedShift Spectrum"]
      }
    ]
  },
  "multilingual-agriculture-advisory-system": {
    projectName: "Multilingual Agriculture Advisory System",
    metaTitle: "High Peak Software - Agriculture Advisory",
    metaDescription:
      "An intelligent agriculture advisory system providing agricultural agents access to accurate weather forecasts, data, and crop advisory for specific locations.",
    metaKeywords:
      "agricultural advisory system, crop advisory services, weather prediction system, agriculture development advisory, crop weather advisory, crop management system, advisory services",
    openGraphURL:
      "https://highpeaksw.com/case-studies/multilingual-agriculture-advisory-system",
    projectImage: "../assets/15_Multilingual.png",
    clientOverViews: [
      "Our client is a 50-year-old non-profit international agricultural research and training organization focusing on cereal grains and related cropping systems.",
      "The organization’s research has helped tens of millions of farmers grow more nutritious, resilient and productive crops, using methods that nourish the environment and combat climate change.",
      "They collaborate with hundreds of partners in 50 countries and are part of the global network of CGIAR research centers, delivering science for a food-secure future."
    ],
    problems: [
      "Our client is a non-profit government organization that partners with the Department of Agricultural Extension (DAE) and Sub Assistant Agricultural Officers (SAAO) in Bangladesh. The client provides advice to farmers on the appropriate crops to grow according to Bangladeshi weather conditions such that crop wastage owing to natural disasters is mitigated. ",
      "The SAAOs work as field agents in collaboration with the DAE, to educate the farmers on a daily basis based on the weather and crop information pertinent to their region or upazila.",
      "Our client was using an existing agriculture advisor application, which was slow, inaccurate, and heavyweight. The client wanted to rebuild the entire application and accommodate more accurate weather and crop advisory, language localization, and easy-to-use interface."
    ],
    challenges: [
      "The application required the SAAOs to login to use it. The onboarding of 14,000 SAAOs into the application posed a major challenge. In addition, establishing a communication channel to engage with the SAAOs and intimate them about the launch of the application posed another challenge. ",
      "The weather forecast data is obtained from the Bangladesh Meteorological Department (BMD) every three hours. This data has to be processed every 15 mins, including critical information such as minimum and maximum temperature, and so on. This requires a lot of time and processing effort consumption, making the application a heavyweight application and slowing down its performance.  High Peak had to figure out a way to make the application lightweight and ensure that the system performance wasn't affected. ",
      "The existing application was coded in R. The development team had to migrate the application to Java to facilitate faster and less complicated processing. ",
      "The weather data obtained from the BMD covered 140*140 grid points which wasn’t enough to obtain accurate weather conditions for smaller locations, also called upazilas. To obtain more accuracy on weather data for smaller locations, the team used an algorithm that interpolated location data."
    ],

    solutionsShortDescription: [
      "High Peak designed and developed a responsive web application for agricultural agents in Bangladesh. This application, available both in English and Bangla, aids SAAOs in accessing relevant weather information for a five-day forecast for select crops in every upazila. ",
      "The intelligent agriculture advisory application includes:"
    ],
    solutions: [
      {
        solutionsDescription: [
          "Weather prediction system",
          "This intelligent agricultural advisory application includes a complex weather prediction logic that takes into consideration the active weather conditions, temperature, rainfall occurrences, humidity, moisture levels, and so on, and predicts the weather in the near future. ",
          "This prediction logic is highly accurate, considering that it was built on the data obtained from the BMD and then interpolated for locations smaller than the 140*140 grids that the BMD provides. This means that, the smaller the locale focus, the more accurate the weather prediction.    ",
          "According to these predicted conditions, advisories are built up with the help of agricultural experts and are stored in the advisory management system."
        ],
        img: "../assets/Weather_Prediction_System.jpg"
      },
      {
        solutionsDescription: [
          "Simulation of real-time geographical view",
          "This application offers a geographical view of Bangladesh that can be zoomed into focus on a particular region, district or upazila of choice. ",
          "The user can easily access the crop and weather data for that specific location by simply clicking on the chosen area. The geographical view also displays alerts in the form of pointers where advisories have been updated. ",
          "The user can also bookmark locations to save corresponding data by using the ‘Starred Locations’ feature for faster and easier information retrieval at a later stage."
        ],
        img: "../assets/Geography.jpg"
      },
      {
        solutionsDescription: [
          "Crop management system",
          "The application includes a smart crop management system that catalogs many kinds of crops that are indigenous to Bangladesh and those that can potentially be nurtured under Bangladeshi weather and soil conditions. ",
          "This information also includes important properties of these crops, such as their ideal climatic conditions, water intake level, and so on.",
          "Using this data and accurate prediction of weather conditions, the application provides useful crop advisories to mitigate crop damage."
        ],
        img: "../assets/Crop.jpg"
      },
      {
        solutionsDescription: [
          "Advisory management system",
          "The advisory management system is another important part of the application. Based on present and predicted weather conditions, advisories are provided for each type of crop available in the crop management system.",
          "These advisories are available on the platform as well as sent to the SAAOs in the form of detailed automated emails.",
          "The SAAOs use these advisories to relay important information to Bangladeshi farmers and advise them on the most suitable crops to grow in the given climatic conditions."
        ],
        img: "../assets/Advisory.jpg"
      },
      {
        solutionsDescription: [
          "User management system",
          "The application is built for users spanning upwards of 14,000 in number. The two main users here are the DAE and SAAO agents. ",
          "The guided tour feature facilitates onboarding of the SAAO agents into the application allowing them to understand and learn navigation through the application."
        ],
        img: "../assets/User.jpg"
      }
    ],
    technologies: [
      {
        domain: "Frontend",
        technology: ["React.js"]
      },
      {
        domain: "Backend",
        technology: ["Java"]
      },
      {
        domain: "Database",
        technology: ["MongoDB"]
      },
      {
        domain: "Hosted on",
        technology: ["Google Cloud Platform"]
      }
    ]
  },
  "operations-security": {
    projectName: "Operations Security",
    metaTitle: "High Peak Software - Operations Security",
    metaDescription:
      "Operations security system with built-in functionalities including security assets maintenance, standards—assets compliance mapping, and security analysis.",
    metaKeywords:
      "Operations security, Security operations centre, OPSEC security, OPSEC solutions, OPSEC, Security report",
    projectImage: "../assets/OperationsSecurity.png",
    openGraphURL: "https://highpeaksw.com/case-studies/operations-security",
    clientOverViews: [
      "Our client’s mission is to simplify and automate how the full stack of stakeholders from fiduciaries to practitioners engage with cybersecurity activity, reducing costs and improving response time and confidence."
    ],
    problems: [
      "Operations security (OPSEC) has always been a grave concern in both military and enterprise businesses. In order to avoid information breach, defining levels of access, maintaining, and monitoring OPSEC are of prime importance. As the complexity of the organizational network increases, so do the maintenance and monitoring of OPSEC."
    ],
    challenges: [
      "Organizations must adhere to established OPSEC standards such as NIST, CSF, etc, primarily to assess their level of security. The OPSEC data visualization system is designed and developed to feature and provide insights on real-time data. Helping businesses adhere to all recognized OPSEC standards is a huge challenge, owing to lack of centralized data."
    ],

    solutionsShortDescription: [
      "The system’s core functionalities include visualisations for OPSEC standard adherence, security assets maintenance, standards—assets compliance mapping, and security analysis."
    ],
    solutions: [],
    technologies: [
      {
        domain: "Frontend",
        technology: ["Javascript", "HTML", "CSS", "jQuery", "D3.js"]
      },
      {
        domain: "Backend",
        technology: ["Java", "Spring Boot", "MySQL"]
      }
    ]
  },
  "it-security-service-management": {
    projectName: "It Security & Service Management",
    metaTitle: "High Peak Software - It Security & Service Management",
    metaDescription:
      "An IT Security & Service Management tool powered by machine learning algorithms for efficient management of big data.",
    metaKeywords:
      "ITSM systems, IT security & risk management, IT vulnerability management, IT security incident management, IT security and service level management, IT security threat management, IT security management and risk assessment",
    projectImage: "../assets/ITsecurity.png",
    openGraphURL:
      "https://highpeaksw.com/case-studies/it-security-service-management",
    clientOverViews: [
      "Our client is a provider of data quality management software, which ensures the integrity and quality of IT and operational data.",
      "They transform, align, and unify data across a range of sources in the industry, enabling businesses to improve IT and OT strategies and recommendations, while driving downstream value across organizations."
    ],
    problems: [
      "The IT security & service management (ITSM) deals with big data. It previously took 36 hours to process and clean one day's worth of data on system status and performance. This hindered several follow-up and linked processes, such as retrieving information on system anomalies and analyzing and fixing them in time."
    ],
    challenges: [
      "As the number of customers/tenants and data volume expanded exponentially, the data cleansing and processing needed to become more scalable and robust. In addition, the processing time needed to be reduced dramatically in order to evoke faster response time for fixing anomalies in the system."
    ],

    solutionsShortDescription: [
      "The ITSM Engine built by High Peak is a high performance tool powered by machine learning algorithms. It accelerates the big data cleansing process and reduces the processing time from 36 hours to 3 hours."
    ],
    solutions: [],
    technologies: [
      {
        domain: "Frontend",
        technology: [
          "Javascript ES6",
          "AngularJS 1.3",
          "gulp.js",
          "Babel",
          "Node.js"
        ]
      },
      {
        domain: "Backend",
        technology: ["Java", "Cassandra", "Redis", "Spark"]
      }
    ]
  },
  "health-fitness-app-for-wearable": {
    projectName: "Health & Fitness App For Wearable",
    metaTitle: "High Peak Software - Health & Fitness App For Wearable",
    metaDescription:
      "An app designed to provide real-time insights, analyze user data, and track user performance. It pairs with a SmartShirt controller to process biometric data. ",
    metaKeywords:
      "Wearable technology, Fitness wearables, Smart wearable technology, Health and fitness wearable, Smartshirt, Wearable tech",
    projectImage: "../assets/Health&fitnesswearable.png",
    openGraphURL:
      "https://highpeaksw.com/case-studies/health-fitness-app-for-wearable",
    clientOverViews: [
      "Our client is a manufacturer of smart shirts aimed to be used by military personnel, athletes, and fitness enthusiasts."
    ],
    problems: [
      "The client wanted to expand their wearable technology, a SmartShirt, used by military forces to track soldiers on the battlefield to a larger, civilian consumer base. ",
      "The objective was to develop a system that could seamlessly connect to the SmartShirt and monitor and improve the daily exercises and fitness activities of athletes."
    ],
    challenges: [
      "The amount of data received from just one user was very large--about 10,000 packets per second. ",
      "Some of the heavy challenges were parsing this large, raw data to retrieve inferences for providing real- time insights, optimizing the processing time and storage in order to skirt the level that can be handled by a Bluetooth connection, etc."
    ],

    solutionsShortDescription: [
      "The Sarvint mobile app is an iOS application, which pairs with a SmartShirt controller device to process biometric and other related data. ",
      "This app provides real - time insights on workouts.It is designed to analyze these data during workout sessions to track and improve user performance and help prevent injuries."
    ],
    solutions: [],
    technologies: []
  }
};
