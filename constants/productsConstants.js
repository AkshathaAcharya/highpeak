export const PRODUCT_CONSTANTS = {
  scarlet: {
    productName: "Scarlet",
    metaTitle: "Scarlet - Intelligent Document Processing Tool",
    metaDescription:
      "Scarlet is a document processing system, powered by intelligent deep learning algorithms. It is a customizable solution capable of processing any document. ",
    metaKeywords:
      "Intelligent document processing, intelligent text processing, data extraction, intelligent document capture, document extraction, AI document processing, OCR image processing, document recognition",
    openGraphURL: "https://www.highpeaksw.com/products/scarlet",
    productImage: "../assets/scarlet.png",
    mobileOrWebsite: "Website",
    subTitle: "Intelligent document processing system",
    productDescription: [
      "Scarlet is an automated document processing system, powered by intelligent deep learning algorithms.",
      "It carries out a geometrical analysis of a document and accurately extracts relevant data. ",
      "Scarlet is a customizable solution capable of processing any kind of document, including images and non-searchable PDF files, specifically tailored for your business requirements."
    ],

    flowChartHeading: "PROCESS RELEVANT DATA IN FOUR EASY STEPS",

    flowChartSteps: [
      {
        stepNumber: "01",
        stepDetails: "Upload & manage documents"
      },
      {
        stepNumber: "02",
        stepDetails: "Select document(s) for processing"
      },
      {
        stepNumber: "03",
        stepDetails: "Automated intelligent data capture"
      },
      {
        stepNumber: "04",
        stepDetails: "Validate extracted data"
      }
    ],
    productInAction: "SCARLET IN ACTION",
    actions: [
      {
        action: [
          "Automated document analysis",
          "Scarlet first carries out an automated geometrical analysis of the document via a complex combination of convolutional neural networks, recurrent neural networks, and segmentation to analyze and extract relevant data.",
          "Scarlet is capable of processing any type of document, including PDFs, tables, freeform text, scanned images, and so on."
        ],
        img: "../assets/Doc_Analysis.png"
      },
      {
        action: [
          "Intelligent data capture",
          "Scarlet employs intelligent OCR for data extraction. The platform can accurately capture data from even scanned images and non-readable PDFs.",
          "Because every document has a different structure, the extraction of data depends on the type of document being processed. Scarlet can extract data from a document in three formats–tables, sections, and key-value pairs."
        ],
        img: "../assets/Data_Capture.png"
      },
      {
        action: [
          "Document review",
          "Scarlet enables you to validate and update the extracted data to ensure there are no errors in the final output. ",
          "For instance, if a particular column of a table is missing from the extracted output, you can rectify this by selecting this column to be extracted. The final output will include the extracted column."
        ],
        img: "../assets/Data_Review_3.png"
      }
    ],
    productEnablesYouTo: "SCARLET ENABLES YOU TO",
    benefits: [
      {
        benefit: "Save overhead costs",
        benefitDescription:
          "Manually processing large documents adds to cost and resource overheads, including paper cost, employee salary, storage and shredding costs, and more. Scarlet helps you save such overhead costs along with an improved ROI."
      },
      {
        benefit: "Improve processing time",
        benefitDescription:
          "Scarlet scans a large number of documents within a short amount of time, thereby improving processing and data extraction time."
      },
      {
        benefit: "Increase data accuracy",
        benefitDescription:
          "Humans are prone to errors. Manually processing large data leads to many errors, such as wrong mapping of data, and a defective tracking mechanism."
      },
      {
        benefit: "Focus on core operations",
        benefitDescription:
          "Scarlet automates document processing with an improved Optical Character Recognition pipeline that has significantly improved data extraction accuracy and reduced processing time, freeing up your time to focus on core business operations, rather than long-drawn, repetitive tasks."
      }
    ],
    productCanAccuratelyCaptureData: "SCARLET CAN ACCURATELY CAPTURE DATA FOR",
    industries: [
      {
        industry: "BFSI",
        useCases: [
          "KYC document verification",
          "Loan application management",
          "Audit documents",
          "Compliance-related processes",
          "Mortgage documents",
          "Customer onboarding",
          "Claims handling"
        ]
      },
      {
        industry: "Tax & Insurance",
        useCases: [
          "Invoice processing ",
          "Claims processing ",
          "Tax documents",
          "Tax & insurance audit processing"
        ]
      },
      {
        industry: "Healthcare",
        useCases: [
          "Medical bills ",
          "Patient records ",
          "Invoice records ",
          "Insurance processing"
        ]
      },
      {
        industry: "HR",
        useCases: [
          "Employee payroll ",
          "Compensation claims ",
          "Employee onboarding",
          "HR records processing"
        ]
      },
      {
        industry: "Legal",
        useCases: [
          "Legal contracts",
          "Non-disclosure agreements",
          "Invoice processing",
          "Lease agreements"
        ]
      },
      {
        industry: "Manufacturing",
        useCases: [
          "Sales order processing",
          "Accounts payable/ receivable",
          "Parts requests from customers",
          "Remittance processing"
        ]
      }
    ],

    productRunsOn: "SCARLET RUNS ON",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Google Vision"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Tesseract"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "TensorFlow"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Pytorch"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Keras"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Python"
      }
    ],
    carousel: [
      {
        img: "../assets/scarletcasestudy1.png",
        url: "/blogs/invoice-factoring-automation",
        label: "Case study",
        title:
          "Invoice Factoring Automation: Scarlet Enables 5,200% Increase in Documents Processed Per Hour"
      },
      {
        img: "../assets/scarletcasestudy2.jpg",
        label: "Beginner’s Guide",
        title: "A Beginner’s Guide to Intelligent Document Processing (Ebook)",
        url:
          "https://medium.com/high-peak-ai/a-beginners-guide-to-intelligent-document-processing-e1061c4363ec"
      },
      {
        img: "../assets/IDP1.jpg",
        label: "Real-time Applications",
        title: "Real-time Applications of Intelligent Document Processing",
        url:
          "https://medium.com/high-peak-ai/real-time-applications-of-intelligent-document-processing-993e314360f9"
      },
      {
        img: "../assets/scarletcasestudy1.png",
        url: "/blogs/invoice-factoring-automation",
        label: "Case study",
        title:
          "Invoice Factoring Automation: Scarlet Enables 5,200% Increase in Documents Processed Per Hour"
      },
      {
        img: "../assets/scarletcasestudy2.jpg",
        label: "Beginner’s Guide",
        title: "A Beginner’s Guide to Intelligent Document Processing (Ebook)",
        url:
          "https://medium.com/high-peak-ai/a-beginners-guide-to-intelligent-document-processing-e1061c4363ec"
      },
      {
        img: "../assets/IDP1.jpg",
        label: "Real-time Applications",
        title: "Real-time Applications of Intelligent Document Processing",
        url:
          "https://medium.com/high-peak-ai/real-time-applications-of-intelligent-document-processing-993e314360f9"
      }
    ]
  },
  almanac: {
    productName: "Almanac",
    metaTitle: "Almanac - Project and Task Management Software",
    metaDescription:
      "Almanac is a smart project and task management tool that enables you to add workstreams, manage teams, track employee productivity effectively. ",
    metaKeywords:
      "Project management software, daily progress tracker, employee task tracking software, project tracking software, team management software, task management system, production progress tracking, project planning tool, team progress tracking, work progress tracker, project management and time tracking software, project monitoring and tracking tool",
    openGraphURL: "https://www.highpeaksw.com/products/almanac",
    productImage: "../assets/almanac.png",
    subTitle: "Project and team management system",
    mobileOrWebsite: "Website",
    productDescription: [
      "Almanac is an intelligent project and team management system that enables you to communicate, collaborate, and delegate tasks. ",
      "The platform also includes a comprehensive dashboard, task manager, time tracker, attendance tracker, and an automated report generator."
    ],

    flowChartHeading: "TRACK YOUR DAILY PRODUCTIVITY",
    flowChartSteps: [
      {
        stepNumber: "01",
        stepDetails: "Create domain"
      },
      {
        stepNumber: "02",
        stepDetails: "Add workstreams/ projects/ teams"
      },
      {
        stepNumber: "03",
        stepDetails: "Create and assign tasks/ events"
      },
      {
        stepNumber: "04",
        stepDetails: "Set up task metrics"
      },
      {
        stepNumber: "05",
        stepDetails: "Submit/ review automated reports"
      }
    ],
    productInAction: "ALMANAC IN ACTION",
    actions: [
      {
        action: [
          "Interactive dashboard",
          "The dashboard provides an overview of the teams’ projects and tasks. It also contains metrics such as list of projects, assigned tasks, and task recipients."
        ],
        img: "../assets/Dashboard_1.png"
      },
      {
        img: "../assets/Project_2.png",
        action: [
          "Project management suite",
          "In the project management suite, you can create projects, tasks, and add new members to the team. Metrics such as estimated task completion time, task type (billable and non-billable), task priority, and levels of difficulty can be added while creating the tasks. Tasks can be allocated to a specific member/s of the team."
        ]
      },
      {
        action: [
          "Secure layered access",
          "Almanac introduces several access and responsibility hierarchies, such as admin, manager, and regular user. Depending on the assigned role, you will have varying levels of access within the application."
        ],
        img: "../assets/Secure_Access_3.png"
      },
      {
        action: [
          "Attendance tracker",
          "The attendance tracker is integrated with the biometric device entry of the organization to record attendance of an employee. The admins alone have view access to the attendance data of every employee in the organization such as punch-in and punch-out timings. ",
          "The tracker enables admins to view filtered data on the basis of date, project, team member, and so on. The application calculates the total time duration a team member has worked in the day, for each project, and overall."
        ],
        img: "../assets/Attendance_4.png"
      },
      {
        action: [
          "Report generation",
          "Once a task is completed, an automated report is generated and submitted to the manager and team members. The report comprises metrics such as time taken to complete the task, task priority, and level of difficulty."
        ],
        img: "../assets/Reports_5.png"
      },
      {
        action: [
          "Google—Almanac calendar sync",
          "Almanac includes an interactive calendar that is also integrated with Google calendar. Events created on either calendars are synced and displayed on both calendars."
        ],
        img: "../assets/Tools_Integration_6.png"
      },
      {
        action: [
          "Almanac mobile applications",
          "High Peak recently introduced mobile applications for Almanac on both iOS and android systems. ",
          "These mobile applications comprise all functionalities available in the web application. Additionally, they allow you to create multiple domains on the mobile application that can be utilized across organizations, departments, projects, and teams."
        ],
        img: "../assets/Mobile_7.png"
      }
    ],
    productEnablesYouTo: "ALMANAC ENABLES YOU TO",

    benefits: [
      {
        benefit: "Effectively manage projects",
        benefitDescription:
          "Almanac provides you with an overview of all the projects, assigned tasks, task duration, events, and so on. Project managers and team leads have access to project details of their team members, and can assign tasks accordingly."
      },
      {
        benefit: "Prioritize important tasks",
        benefitDescription:
          "With Almanac, you can view, prioritise, and track all your tasks for a project. You can also delegate tasks to other team members, set task deadlines, and track progress."
      },
      {
        benefit: "Track daily productivity",
        benefitDescription:
          "Almanac allows you to track overall and individual work productivity by tracking employee attendance and generating reports that calculate how much time is spent on specific types of tasks, their levels of difficulty, and so on. In addition, Almanac also allows you to realign employees to other projects based on availability."
      },
      {
        benefit: "Make informed decisions",
        benefitDescription:
          "With report generation, Almanac allows you to track progress on a daily basis. The reports provide an overview of various metrics that serve as valuable data during employee appraisals and feedback sessions."
      }
    ],
    productCanAccuratelyCaptureData: "",
    industries: [],
    productRunsOn: "ALMANAC RUNS ON",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "React"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Redux"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "ES6"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Webpack"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Spring Boot"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Redis"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "ActiveMQ"
      }
    ],
    carousel: []
  },
  iiot: {
    productName: "IIOT",
    metaTitle: "IIOT - Secure Remote Access Software",
    metaDescription:
      "IIOT is your secure one-stop shop to access, control, and monitor and record remote system usage.",
    metaKeywords:
      "Remote access software, privileged access management, parallel access, privileged account management, remote control access management, Remote Desktop access, parallel remote access management, secure remote access management",
    openGraphURL: "https://www.highpeaksw.com/products/iiot",
    productImage: "../assets/IIOT.png",
    subTitle: "Secure remote access",
    mobileOrWebsite: "Website",
    productDescription: [
      "IIOT is a secure one-stop shop to access, control, and monitor all remote systems, including IOT assets. ",
      "You can now provide your users secure access to specific remote systems or groups of systems, assign appropriate roles and responsibilities, effectively communicate and monitor system usage, and so on."
    ],

    flowChartHeading: "SECURELY ACCESS YOUR REMOTE SYSTEMS",
    flowChartSteps: [
      {
        stepNumber: "01",
        stepDetails: "Privileged, secure remote login"
      },
      {
        stepNumber: "02",
        stepDetails: "Customize configuration settings"
      },
      {
        stepNumber: "03",
        stepDetails: "Add assets and users"
      },
      {
        stepNumber: "04",
        stepDetails: "Approve /deny requests"
      },
      {
        stepNumber: "05",
        stepDetails: "Request /start /stop sessions"
      },
      {
        stepNumber: "06",
        stepDetails: "View live sessions"
      },
      {
        stepNumber: "07",
        stepDetails: "Conduct session and application audits"
      }
    ],

    productInAction: "IIOT IN ACTION",
    actions: [
      {
        action: [
          "Privileged role-based access",
          "IIOT makes it possible for users to set up all their system and user information in one place. ",
          "You can access relevant data, assign roles and access to trusted technicians, store and access all system and user-related files in one global repository, all remotely from the comfort of their home or workplace."
        ],
        img: "../assets/Role_Based_Access_1.png"
      },
      {
        action: [
          "Intelligent auditing & reporting",
          "IIOT guarantees a complete and secure control over all remote systems and monitoring of their usage. ",
          "You can remotely assign, revoke, and limit access to sensitive systems and data accordingly. With IIOT’s screen and session recordings, users can monitor usage in real time. In addition,  they will also be able to use these recordings and log information for future audit or repair sessions."
        ],
        img: "../assets/Auditing_2.png"
      },
      {
        action: [
          "Secure communications and data transfer",
          "Using IIOT, you can securely chat with technicians and exchange useful information. ",
          "You can raise alarms or notify in real time via chat if they encounter any problems with the remote systems or deduce suspicious activities. ",
          "IIOT allows you to transfer files securely from your remote device during remote access usage into a user file repository. ",
          "The application performs automatic security checks such as virus scans on all the files during upload. Once the files are scanned, they are sent to the admin for approval. Approved files are exported to the public repository where they can be easily accessed through remote assets."
        ],
        img: "../assets/Secure_Communications_3.png"
      },
      {
        action: [
          "Configuration manager",
          "IIOT is integrated with Lightweight Directory Access Protocol (LDAP) and Active Directory. These directories enable you to directly add users to the application from your existing employee database, thereby saving time and eliminating the need for manual entry. ",
          "The admin maintains control over the configuration of communication mediums such as emails and SMS. For instance, the admin can set constraints on the number of password characters, choosing the email ID for communication exchange, and so on. ",
          "The admin can also configure storage settings in the application for storing files and recordings."
        ],
        img: "../assets/Configuration_4.png"
      },
      {
        action: [
          "Data security",
          "IIOT ensures data privacy and security during live recording sessions by encrypting all forms of communication with the SSL encryption."
        ],
        img: "../assets/Security_5.png"
      },
      {
        action: [
          "Intuitive UI",
          "IIOT is designed with an intuitive UI that enables you to securely access systems and data remotely in a simple manner without any complications."
        ],
        img: "../assets/UI_6.png"
      }
    ],

    productEnablesYouTo: "IIOT ENABLES YOU TO",
    benefits: [
      {
        benefit: "Eliminate overhead costs",
        benefitDescription:
          "IIOT enables you to remotely access systems and data from anywhere, thereby eliminating cost overheads such as travel costs, worker/technician costs, maintenance costs, and employee health and safety expenses."
      },
      {
        benefit: "Improve end-to-end security",
        benefitDescription:
          "IIOT provides end-to-end security by encrypting all communications with the SSL encryption, enabling you to securely communicate and share information. IIOT users are guaranteed secure control over all remote systems and monitoring of their usage. With IIOTs privileged role-based access, you can remotely assign, revoke, or limit another user’s access to sensitive data accordingly."
      },
      {
        benefit: "Enhance business continuity & productivity",
        benefitDescription:
          "IIOT enables you to work across geographies facilitating you to coordinate with your team, all in one virtual place. Because you can work from anywhere independent of your travel, IIOT also helps in mitigating time lag."
      },
      {
        benefit: "Securely access devices anytime, anywhere",
        benefitDescription:
          "With IIOT, you can log in to your systems and securely access sensitive data, assign roles and provide access to technicians, and access all your files in one global repository, anytime and from  anywhere."
      }
    ],
    productCanAccuratelyCaptureData: "IIOT CAN SECURE YOUR REMOTE ACCESS IN",
    industries: [
      {
        industry: "IT sector",
        useCases: [
          "Remote desktop access",
          "Secure remote access to customer systems",
          "Quick customer issue resolution"
        ]
      },
      {
        industry: "Manufacturing",
        useCases: [
          "Real-time health monitoring for machines",
          "Prediction of system downtime or process failure",
          "Real-time alarm notification and chat feature"
        ]
      },
      {
        industry: "Oil & gas",
        useCases: [
          "Real-time health monitoring for machines",
          "Prediction of system downtime or process failure",
          "Real-time alarm notification and chat feature"
        ]
      },
      {
        industry: "Energy & utilities",
        useCases: [
          "Real-time health monitoring for machines",
          "Prediction of system downtime or process failure",
          "Real-time alarm notification and chat feature"
        ]
      },
      {
        industry: "Smart cities and countries",
        useCases: [
          "Real-time health monitoring for machines",
          "Prediction of system downtime or process failure",
          "Real-time alarm notification and chat feature"
        ]
      }
    ],

    productRunsOn: "IIOT RUNS ON",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Redis"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Apache Guacamole"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Elasticsearch"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Kubernetes"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Graylog"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Prometheus"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Java"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "React.js"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Cloudwatch"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Docker"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Docker Swarm"
      }
    ],
    carousel: []
  },
  peakspeak: {
    productName: "PeakSpeak",
    metaTitle: "PeakSpeak - Anonymous Group Chat Application",
    metaDescription:
      "PeakSpeak is an anonymous group chat application that facilitates user privacy through anonymous messaging, and feedback gathering. ",
    metaKeywords:
      "Anonymous group chat app, anonymous employee feedback, user data privacy, anonymous messaging application, secure and anonymous messaging application, anonymous messaging app for feedback gathering",
    openGraphURL: "https://www.highpeaksw.com/products/peakspeak",
    productImage: "../assets/peakspeak.png",
    subTitle: "Anonymous group chat application",
    mobileOrWebsite: "Mobile",
    productDescription: [
      "PeakSpeak is an anonymous group chat application designed and developed using React Native, a cross-platform development framework. ",
      "PeakSpeak allows you to exchange messages anonymously on a group chat.",
      "It can be used for org-wide feedback gathering, anonymous employee reviews, and surveys."
    ],

    flowChartHeading: "CHAT ANONYMOUSLY",
    flowChartSteps: [
      {
        stepNumber: "01",
        stepDetails: "Register"
      },
      {
        stepNumber: "02",
        stepDetails: "Login"
      },
      {
        stepNumber: "03",
        stepDetails: "Create groups"
      },
      {
        stepNumber: "04",
        stepDetails: "Exchange messages anonymously"
      }
    ],
    productInAction: "PEAKSPEAK IN ACTION",
    actions: [
      {
        action: [
          "Easy registration",
          "You can register with your mobile phone number and invite others to join via an SMS invite."
        ],
        img: "../assets/Register_Peakspeak_1.png"
      },
      {
        action: [
          "Anonymous chatting",
          "PeakSpeak allows you to exchange anonymous messages by creating and adding members to groups. Messages sent by any user on the group stays hidden."
        ],
        img: "../assets/Chat_peakspeak_2.png"
      },
      {
        action: [
          "User privacy",
          "As a PeakSpeak user, you have the option to mute a group chat, block a contact, and report abusive messages in a group chat."
        ],
        img: "../assets/Privacy_Peakspeak_3.png"
      },
      {
        action: [
          "Admin actions",
          "PeakSpeak allows group admins to add, remove, block, or report other members accordingly."
        ],
        img: "../assets/Admin_peakspeak.png"
      }
    ],
    productEnablesYouTo: "PEAKSPEAK ENABLES YOU TO",
    benefits: [
      {
        benefit: "Provide anonymous feedback",
        benefitDescription:
          "PeakSpeak can be used to gather anonymous feedback about assets organization-wide. In addition, it can be used for group discussions on any topic where the participants wish to remain anonymous."
      },
      {
        benefit: "Submit anonymous surveys",
        benefitDescription:
          "Organizations can use PeakSpeak to conduct anonymous reviews for employees, allowing them to share their opinions freely. It can also be used for surveys and questionnaires."
      }
    ],
    productCanAccuratelyCaptureData: "",
    industries: [],
    productRunsOn: "PEAKSPEAK RUNS ON",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Firebase"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "VSCode"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "React Native"
      }
    ],
    carousel: []
  },
  peakcruit: {
    productName: "PeakCruit",
    metaTitle: "PeakCruit - Smart Recruitment Application",
    metaDescription:
      "PeakCruit is a smart recruitment application that can be used for recruitment and hiring purposes. ",
    metaKeywords:
      "Smart recruitment application, recruitment application, employment decision, recruitment management system, online recruitment app, online applicant recruitment and tracking app, candidate management system, smart recruitment and employment tool",
    openGraphURL: "https://www.highpeaksw.com/products/peakcruit",
    productImage: "../assets/peakcruit.png",
    subTitle: "Smart hiring and recruitment application",
    mobileOrWebsite: "Mobile",
    productDescription: [
      "PeakCruit is a mobile application designed and developed by High Peak for hiring and recruitment. ",
      "PeakCruit allows you to register and create a detailed academic profile and upload resumes. You can also register for aptitude tests as a prerequisite for the interview."
    ],

    flowChartHeading: "AUTOMATED TALENT ACQUISITION",
    flowChartSteps: [
      {
        stepNumber: "01",
        stepDetails: "User registration"
      },
      {
        stepNumber: "02",
        stepDetails: "Upload a resume"
      },
      {
        stepNumber: "03",
        stepDetails: "Take online aptitude tests"
      },
      {
        stepNumber: "04",
        stepDetails: "Receive test results"
      },
      {
        stepNumber: "05",
        stepDetails: "Get interview & venue details via mail"
      }
    ],

    productInAction: "PEAKCRUIT IN ACTION",
    actions: [
      {
        action: [
          "User registration",
          "You can register on PeakCruit using your mail ID. After signing up, the app enables you to create a profile on the application by filling in your personal and academic details. It also allows you to upload your resumes."
        ],
        img: "../assets/Register_peakcruit_1.png"
      },
      {
        action: [
          "Online test management",
          "As a part of the preliminary interview process, potential candidates take an aptitude test on PeakCruit after being registered. ",
          "Tests are customized for various entry-level roles and tests the candidates aptitude and computational skills and provides them results right on the app."
        ],
        img: "../assets/Test_peakcruit_2.png"
      },
      {
        action: [
          "Automated candidature communications",
          "The shortlisted candidates will receive details about interviews such as date, venue, and other prerequisites on the PeakCruit app. ",
          "Further, the app triggers a personalized email to be sent to the candidate’s mail ID for reference and reminder purposes."
        ],
        img: "../assets/communication_peakcruit_.png"
      }
    ],
    productEnablesYouTo: "PEAKCRUIT ENABLES YOU TO",
    benefits: [
      {
        benefit: "Digitize talent acquisition",
        benefitDescription:
          "Recruitment is a largely time consuming process involving heavy paperwork. With PeakCruit, you can digitize the process of acquiring talent by shortlisting candidates based on resumes."
      },
      {
        benefit: "Make informed decisions",
        benefitDescription:
          "Customized tests for various entry-level roles can be conducted to test the candidate’s aptitude and skills on the PeakCruit application, thereby allowing you to make informed decisions in the recruitment process."
      },
      {
        benefit: "Automate communications",
        benefitDescription:
          "Once a candidate is shortlisted, all personalized communications in the form of emails are automated by the PeakCruit app. Candidates receive recruitment related information such as interview details, date, and venue."
      }
    ],
    productCanAccuratelyCaptureData: "",
    industries: [],
    productRunsOn: "PEAKCRUIT RUNS ON",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Flutter"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Kotlin"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Spring Boot"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Spring Security"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Firebase"
      }
    ],

    carousel: []
  },

  sharpic: {
    productName: "Sharpic",
    metaTitle: "Sharpic - Intelligent Image Enhancement Software",
    metaDescription:
      "Sharpic is an intelligent image enhancement software powered by deep learning algorithms. It enables users to enhance low-resolution images up to six times",
    metaKeywords:
      "Image enhancement software, image enhancement and restoration software, automatic image enhancement, image detail enhancement, photo enhancement, visual media enhancement software, digital photo restoration software",
    openGraphURL: "https://www.highpeaksw.com/products/sharpic",
    productImage: "../assets/sharpic.png",
    subTitle: "Intelligent image upscaling system",
    mobileOrWebsite: "Website",
    productDescription: [
      "Sharpic is an image enhancement tool that uses complex deep learning technologies to enhance, refine, and upscale low resolution images upto six times their original resolution."
    ],

    flowChartHeading: "",
    flowChartSteps: [],
    productInAction: "SHARPIC IN ACTION",
    actions: [
      {
        action: [
          "Intelligent image upscaling",
          "For upscaling images, Sharpic uses custom layers for stencils, such as edges, color, light and darkness components, gradient maps of different features, etc. ",
          "Sharpic models decide the combination of features that provide the highest accuracy and then deliver an upscaled image."
        ],
        img: "../assets/SHARPIC_INTELLIGENT_SCALING.png"
      },
      {
        action: [
          "6X image enhancement",
          "While most image enhancement applications are able to enhance images by only four times the original size, Sharpic can enhance the image six times the original size.",
          "Sharpic achieves this by marrying the best components from GAN and bilinear interpolation, and then builds higher-dimensional matrices, which are complex dense vectors that act as stencils of layers of a particular image."
        ],
        img: "../assets/SHARPIC_6X.png"
      },
      {
        action: [
          "Minimal loss of ",
          "Sharpic allows you to upload low-resolution images via a drag-and-drop method or by physically uploading the file on the application. Sharpic then converts these low resolution images into sharp, refined, and enhanced image outputs with minimal loss to structural integrity."
        ],
        img: "../assets/SHARPIC_MinimlLoss.png"
      },
      {
        action: [
          "Intuitive UI",
          "Sharpic’s simple and intuitive UI enables you to upload images and view the enhanced version of the image in real-time in an effortless manner without any complications."
        ],
        img: "../assets/SHARPIC_SharpIt4.png"
      }
    ],
    productEnablesYouTo: "SHARPIC ENABLES YOU TO",
    benefits: [
      {
        benefit: "Upscale images with minimal loss",
        benefitDescription:
          "Photo editors can use the Sharpic application to upscale and increase the density of the image. The application also allows them to upload and enhance their images."
      },
      {
        benefit: "Crime identification & prevention",
        benefitDescription:
          "Sharpic can be used in crime prevention by enhancing low quality images in CCTV footages for suspect identification."
      }
    ],
    productCanAccuratelyCaptureData: "SHARPIC CAN UPSCALE IMAGES FOR",
    industries: [
      {
        industry: "Textile & manufacturing",
        useCases: [
          "Resolution enhancement of images without loss of data.",
          "Defect identification through enhanced images in real-time."
        ]
      },
      {
        industry: "Policing & security",
        useCases: ["Suspect identification from CCTV footage."]
      },
      {
        industry: "Media",
        useCases: [
          "Improved quality and resolution of images and videos in various media campaigns."
        ]
      },
      {
        industry: "Photography",
        useCases: [
          "Enhancement of photographs. ",
          "Recovery of old, distorted images for reuse."
        ]
      }
    ],
    productRunsOn: "SHARPIC RUNS ON",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Python"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "TensorFlow"
      },
      {
        technologyIcon: "../assets/tensorflow.png",
        technology: "Keras framework"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Deep Learning"
      }
    ],
    carousel: []
  },
  lendstash: {
    productName: "LendStash",
    metaTitle: "LendStash - Rental platform",
    metaDescription:
      "LendStash is a blockchain-based book rental platform that enables users to create a virtual library for lending and borrowing books. ",
    metaKeywords:
      "Virtual library, book rental platform, digital library, virtual library system, virtual book rental service, online book rental platform",
    openGraphURL: "https://www.highpeaksw.com/products/lendstash",
    productImage: "../assets/lendstash.png",
    subTitle: "A book rental platform",
    mobileOrWebsite: "Mobile",
    productDescription: [
      "LendStash is a peer-to-peer book rental platform, which aims to contribute to the sharing economy. You can lend and borrow books by creating a virtual library and receiving payments for the same. SnLendstash uses a blockchain technology named ‘Chronicles’ to keep your transactional records (ledger keeping) safe and tamper-proof."
    ],

    flowChartHeading: "BUILD YOUR VIRTUAL LIBRARY",
    flowChartSteps: [
      {
        stepNumber: "01",
        stepDetails: "Enter book details"
      },
      {
        stepNumber: "02",
        stepDetails: "Find books"
      },
      {
        stepNumber: "03",
        stepDetails: "Lend/ rent books"
      },
      {
        stepNumber: "04",
        stepDetails: "Make/ receive payment"
      },
      {
        stepNumber: "05",
        stepDetails: "Return books"
      }
    ],
    productInAction: "LENDSTASH IN ACTION",
    actions: [
      {
        action: [
          "Build your book stash",
          "You can create a list of books and upload details to build a virtual library in LendStash. You can scan/enter the ISBN name of the book and LendStash pre-populates information, such as book title, description, etc. ",
          "LendStash also suggests a lending price and deposit amount. Therefore, this becomes a two-step process to upload a book, instead of manually entering 8-10 fields for every book you want to lend.",
          "Alternatively, if you don’t have the ISBN code, you could still search the book by its name, and relevant details would be pre-populated."
        ],
        img: "../assets/Build_stash_test.png"
      },
      {
        action: [
          "Lend books",
          "You can lend books from your stash to borrowers on LendStash by accepting their request to borrow. A safety deposit is collected from the borrower."
        ],
        img: "../assets/Lend_books_2.png"
      },
      {
        action: [
          "Receive payment",
          "Once the rental period ends, you get the book back from the borrower and also get paid for it. The amount of payment depends on the time duration for which the book was rented."
        ],
        img: "../assets/Pyment_3.png"
      },
      {
        action: [
          "Find and rent books",
          "You can search for any particular book of your choice on the application. If the book is available on the stash, you can borrow the book from the lender for the time period you desire by paying a safety deposit. As a borrower, you can also chat with the lender to collect the book."
        ],
        img: "../assets/Find_books_4.png"
      },
      {
        action: [
          "Return books",
          "You can return the book after having read it before the renting period ends. Once the book is returned, you get back the full safety deposit amount."
        ],
        img: "../assets/Return_books_5.png"
      }
    ],
    productEnablesYouTo: "LENDSTASH ENABLES YOU TO",
    benefits: [],
    productCanAccuratelyCaptureData: "",
    industries: [],
    productRunsOn: "LENDSTASH RUNS ON",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Kotlin (Android)"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Swift (iOS)"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Firebase"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Elasticsearch"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Tendermint"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Node.js"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "GoLang"
      }
    ],
    carousel: []
  },
  "vision-ai": {
    productName: "Vision AI",
    metaTitle: "High Peak Software - Automated AI Defect Identification",
    metaDescription:
      "An automated anomaly detection software application developed using machine learning models and computer vision for detecting anomalies on unmarked surfaces.",
    metaKeywords:
      "Defect identification systems, Target defect identification, Automated defect recognition and identification, Anomaly detection, Machine learning algorithms, Machine learning models, Autoencoder, Anomaly detection and prediction, Machine learning, Computer Vision",
    openGraphURL: "https://www.highpeaksw.com/products/vision-ai",
    productImage: "../assets/VisionAIportfolio.png",
    mobileOrWebsite: "Website",
    subTitle: "Visual Based Anomaly Detection",
    productDescription: [
      "Vision AI is an automated anomaly detection software, powered by intelligent machine-learning algorithms and computer vision.The platform carries out surface-level anomaly detection in products, such as auto body parts, screws, leather, footwear, apparel etc., by identifying the region of interest on the surface of the product.The anomalies detected in the product are then represented on heat maps and error detection masks for easy defect representation."
    ],

    flowChartHeading: "Detect Anomalies In Real Time",

    flowChartSteps: [
      {
        stepNumber: "01",
        stepDetails: "Real-time anomaly detection"
      },
      {
        stepNumber: "02",
        stepDetails: "Create image and model repositories"
      },
      {
        stepNumber: "03",
        stepDetails: "Automate model training and tuning"
      },
      {
        stepNumber: "04",
        stepDetails: "Validate model"
      },
      {
        stepNumber: "05",
        stepDetails: "View and calibrate results"
      }
    ],
    productInAction: "Vision AI In Action",
    actions: [
      {
        action: [
          "Real-time anomaly detection",
          "The Vision AI platform performs surface-level anomaly detection in products, such as auto body parts, screws, apparel, footwear, etc., by leveraging intelligent machine-learning algorithms whose accuracy improves with time and refined datasets.",
          "The platform trains the models with just 10-30 error-free images of the product. These images are fed as training data to the model. By learning the normal or optimal appearance of an object, the model identifies unpredictable defects in products in real-time.",
          "Identified defects, if any, in the product are represented on heat maps and error detection masks."
        ],
        img: "../assets/Annotation_view.png"
      },
      {
        action: [
          "Image repository",
          "Both error-free and defective product images are stored in an image repository. These images are tagged and labelled accordingly for training purposes. Each time a new image is fed to the platform, a new model is generated for that particular image."
        ],
        img: "../assets/Images_view.png"
      },
      {
        action: [
          "Model repository",
          "The model repository contains every new model that is generated in the process and its respective training results obtained after running the models through the application."
        ],
        img: "../assets/Images_view_add_type.png"
      },
      {
        action: [
          "Automated model tuning and training",
          "After identification of defects on the object, the model is tuned with various hyperparameters. Once the model is tuned, model training is carried out for real-time model confidence score, accuracy, and precision."
        ],
        img: "../assets/Training_accuracy.png"
      },
      {
        action: [
          "Model validation",
          "The models with the best accuracy/precision/confidence score can be selected and validated manually. The platform allows manual intervention for identification of objects when a model is unable to classify the object as defective.",
          "Users can annotate each defect individually in multiple ways, namely by using a marker, or drawing various shapes such as rectangle, circle, or a polygon around the region. Users can also annotate using free form drawing. The platform also provides users an eraser for editing the annotation. Additionally, users can use the zoom in/zoom out feature for greater clarity.",
          "In addition, the original dimensions of images are retained and each defect is provided with a different color for easy classification. ",
          "Based on the user’s selected image a unique model is generated for every defect that is identified. This user validation data is then captured and used for generating model training data."
        ],
        img: "../assets/Training_loss.png"
      }
    ],
    productEnablesYouTo: "",
    benefits: [],
    productCanAccuratelyCaptureData:
      "Vision AI Can Accurately Detect Anomalies In",
    industries: [
      {
        industry: "Manufacturing",
        useCases: [
          "Real-time anomaly detection in manufacturing process",
          "Visual inspection of surface defects on products"
        ]
      },
      {
        industry: "Leather",
        useCases: [
          "Real-time anomaly detection in leather manufacturing",
          "Detection of dark spots on raw leather materials"
        ]
      },
      {
        industry: "Footwear",
        useCases: [
          "Real-time anomaly detection in footwear material manufacturing "
        ]
      },
      {
        industry: "Textile and apparel",
        useCases: [
          "Real-time anomaly detection in textile and apparel manufacturing "
        ]
      },
      {
        industry: "Auto body parts manufacturing",
        useCases: ["Real-time anomaly detection in auto parts manufacturing"]
      },
      {
        industry: "Healthcare equipment",
        useCases: [
          "Real-time anomaly detection in healthcare equipment manufacturing"
        ]
      },

      {
        industry: "Hardware production",
        useCases: ["Real-time anomaly detection in hardware manufacturing"]
      }
    ],

    productRunsOn: "Vision AI Runs On",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Keras"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Python"
      }
    ],
    carousel: []
  },
  aistrology: {
    productName: "AIstrology",
    metaTitle: "High Peak Software - Personalized Pocket Astrologer",
    metaDescription:
      "The AIstrology app is an AI-powered app powered by technology and Vedic astrology, enabling users to schedule appointments and get astrological predictions.",
    metaKeywords:
      "The AIstrology app is an AI-powered app powered by technology and Vedic astrology, enabling users to schedule appointments and get astrological predictions.",
    openGraphURL: "https://www.highpeaksw.com/products/aistrology",
    productImage: "../assets/AIstrologyPortfolio.png",
    mobileOrWebsite: "Website",
    subTitle: "Personalized Pocket Astrologer",
    productDescription: [
      "The AIstrology app is an AI-powered application, bridging advanced technology and thousands of years of Vedic knowledge in astrology. ",
      "The application enables you to book an appointment with an astrologer and avail solutions and predictions to various career and work- related problems.",
      "The application also comprises a personal chatbot Ri Shi, serving as your pocket astrologer."
    ],

    flowChartHeading: "Get Your Astrological Predictions",
    flowChartSteps: [
      {
        stepNumber: "01",
        stepDetails: "Create user and family profiles"
      },
      {
        stepNumber: "02",
        stepDetails: "Chat with Ri Shi bot "
      },
      {
        stepNumber: "03",
        stepDetails: "Schedule an appointment"
      },
      {
        stepNumber: "04",
        stepDetails: "Meet/ Chat with astrologer "
      },
      {
        stepNumber: "05",
        stepDetails: "Get personalized solutions to your problems"
      }
    ],
    productInAction: "AIstrology AI In Action",
    actions: [
      {
        action: [
          "Interactive chatbot: Ri Shi",
          "High Peak integrated the methodologies of astrology with modern technologies like Natural Language Processing to develop an interactive chatbot, Ri Shi.",
          "Ri Shi analyzes the users’ charts based on date, time, and place of birth and is designed to answer select career-related questions. "
        ],
        img: "../assets/RiShi1.png"
      },
      {
        action: [
          "Easy user registration",
          "With this app, you can create a profile providing necessary details for your astrological consultation and schedule appointments with an astrologer at your own convenience. "
        ],
        img: "../assets/Schedule2.png"
      },
      {
        action: [
          "Intelligent payment processing",
          "The app also comes with a payment gateway, which facilitates payments via UPI, netbanking, digital wallet, or debit/credit cards. "
        ],
        img: "../assets/Charts3.png"
      }
    ],
    productEnablesYouTo: "",
    benefits: [],
    productCanAccuratelyCaptureData: "",
    industries: [],

    productRunsOn: "AIstrology Runs On",
    technologies: [
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Keras"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Flutter"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "ReactJS"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Dart"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Firebase"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Java"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "MongoDB"
      },
      {
        technologyIcon: "../assets/google-vision.png",
        technology: "Natural language processing"
      }
    ],
    carousel: []
  }
};
