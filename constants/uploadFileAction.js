export const UPLOAD_FILE_DATA = "UPLOAD_FILE_DATA";

export const uploadData = data => {
  return {
    type: UPLOAD_FILE_DATA,
    payload: data
  };
};
