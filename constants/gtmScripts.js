import { initGA, logPageView } from "../utils/analytics";

export const GTM_SCRIPT_TAG = {
  tag: (
    <script>
      {function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          "gtm.start": new Date().getTime(),
          event: "gtm.js"
        });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != "dataLayer" ? "&l=" + l : "";
        j.async = true;
        j.src = "https://www.googletagmanager.com/gtm.js?id=" + i + dl;
        f.parentNode.insertBefore(j, f);
      }}
      (window,document,'script','dataLayer','GTM-PM5VGBL');
    </script>
  )
};
export const GTM_NO_SCRIPT = () => {
  return (
    <noscript>
      <iframe
        src="https://www.googletagmanager.com/ns.html?id=GTM-PM5VGBL"
        height="0"
        width="0"
        style={{ display: "none", visibility: "hidden" }}
      ></iframe>
    </noscript>
  );
};

export const GA_SCRIPT = () => {
  const script = document.createElement("script");
  script.src = "//js.hs-scripts.com/7251170.js";
  script.id = "hs-script-loader";
  script.type = "text/javascript";
  script.async = true;
  document.body.appendChild(script);
  const iframescript = document.createElement("iframe");
  iframescript.src = "https://www.googletagmanager.com/ns.html?id=GTM-PM5VGBL";
  iframescript.height = "0";
  iframescript.width = "0";
  iframescript.style.visibility = "hidden";
  iframescript.style.display = "none";
};

export const GA_WINDOW = () => {
  if (!window.GA_INITIALIZED) {
    initGA();
    window.GA_INITIALIZED = true;
  }
  logPageView();
};
