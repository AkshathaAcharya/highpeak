import { GET_BLOGS, GET_CATEGORY, STORE_CATEGORIES, GROUPED_BY_CATEGORY, SET_FLAG } from "./getBlogsConstants" 

export const getBlogs = (page) => {
    return {
      type: GET_BLOGS,
      page : page
    };
};

export const getCategories = () => {
  return {
    type: GET_CATEGORY
  };
};

export const storeCategories = (data) => {
  return {
    type: STORE_CATEGORIES,
    payload : data
  };
};

export const groupedByCategories = data => {
  return {
    type:  GROUPED_BY_CATEGORY,
    payload: data
  };
};

export const setFlag = data => {
  return {
    type:  SET_FLAG,
    payload: data
  };
};