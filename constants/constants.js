export const PROJECTS_ARRAY = [
  {
    projScreen: "../assets/scarlet.png",
    title: "Scarlet",
    url: "/products/scarlet",
    subTitle: "Intelligent Document Processing",
    responsiveNumber: "01/07"
  },
  {
    projScreen: "../assets/IIOT.png",
    title: "IIOT",
    url: "/products/iiot",
    subTitle: "Secure Remote Access Software",
    responsiveNumber: "02/07"
  },
  {
    projScreen: "../assets/almanac.png",
    title: "Almanac",
    url: "/products/almanac",
    subTitle: "Smart Project Management Tool",
    responsiveNumber: "03/07"
  },
  {
    projScreen: "../assets/peakcruit.png",
    title: "PeakCruit",
    url: "/products/peakcruit",
    subTitle: "Smart Recruitment Application",
    responsiveNumber: "04/07"
  },
  {
    projScreen: "../assets/sharpic.png",
    title: "Sharpic",
    url: "/products/sharpic",
    subTitle: "Image Upscaling Tool",
    responsiveNumber: "05/07"
  },
  {
    projScreen: "../assets/peakspeak.png",
    title: "PeakSpeak",
    url: "/products/peakspeak",
    subTitle: "Anonymous Chat Application",
    responsiveNumber: "06/07"
  },
  {
    projScreen: "../assets/lendstash.png",
    title: "LendStash",
    url: "/products/lendstash",
    subTitle: "Secure Rental Platform",
    responsiveNumber: "07/07"
  }
];

export const WE_SOLVE_TOUGH_AI = [
  {
    img: "../assets/AI_home.svg",
    hoverImage: "../assets/AI_hover.svg",
    title: "Artificial Intelligence",
    url: "artificial-intelligence"
  },
  {
    img: "../assets/MachineLearning_home.svg",
    hoverImage: "../assets/MachineLearning_hover.svg",
    title: "Machine Learning",
    url: "machine-learning"
  },
  {
    img: "../assets/DataScience_home.svg",
    hoverImage: "../assets/DataScience_hover.svg",
    title: "Data Science",
    url: "data-science"
  },
  {
    img: "../assets/ProductDevelopment_home.svg",
    hoverImage: "../assets/ProductDevelopment_hover.svg",
    title: "Product Development",
    url: "product-development"
  },
  {
    img: "../assets/UXDesign_home.svg",
    hoverImage: "../assets/UXDesign_hover.svg",
    title: "UI & UX Design",
    url: "ui-&-ux-design"
  },
  {
    img: "../assets/DigitalMarketing_home.svg",
    hoverImage: "../assets/DigitalMarketing_hover.svg",
    title: "Marketing",
    url: "marketing"
  }
];

export const FREE_AI_IMG = [
  {
    className: "freeAiImages aiBlog",
    url: "/blogs",
    title: "AI Blog",
    titleClass: "TitleAi GetfreeAITitle ",
    subtitleClass: "freeAiTitle GetfreeAITitle ",
    subTitle: "Our research and ramblings",
    label: "AiBlogsImage"
  },
  {
    className: "freeAiImages caseStudies",
    url: "/portfolio",
    title: "Case studies",
    titleClass: "TitleAi GetfreeAITitle ",
    subtitleClass: "freeAiTitle GetfreeAITitle ",
    subTitle: "This is why you should give us your business",
    label: "AiCaseStudies"
  },
  {
    className: "freeAiImages eBooks",
    url: "https://medium.com/high-peak-ai",
    title: "E-Books",
    titleClass: "TitleAi GetfreeAITitle ",
    subtitleClass: "freeAiTitle GetfreeAITitle ",
    subTitle: "Our research and ramblings but with more words",
    label: "AiEbooks"
  }
];

export const FOOTER_ICONS = {
  facebook: {
    img: "../assets/facebook.svg",
    navigationURL: "https://www.facebook.com/highpeaksw/",
    label: "facebookIcon"
  },
  twitter: {
    img: "../assets/twitter.svg",
    navigationURL: "https://twitter.com/highpeaksw",
    label: "twitterIcon"
  },
  linkedin: {
    img: "../assets/linkedin.svg",
    navigationURL: "https://www.linkedin.com/company/high-peak-software-inc./",
    label: "linkedinIcon"
  }
};

export const ROTATING_WORDS = [
  {
    weSolveToughAI: "better",
    homePage: "Performance",
    weMakeCoolAI: "intrapreneurs"
  },
  {
    weSolveToughAI: "smarter",
    homePage: "Precision",
    weMakeCoolAI: "AI innovators"
  },
  {
    weSolveToughAI: "efficient",
    homePage: "Impact",
    weMakeCoolAI: "AI experts"
  }
];

export const NAVIGATION_ARRAY = [
  {
    title: "Home",
    logo: true,
    colorImage: "../assets/logo-color.png",
    navigationColor: "#222222",
    classNameOurStuffButton: "topButtons ourStuffHome",
    classNameHitUsUp: "topButtons hitUsUpHome hitUsUpBackground",
    scroll: true,
    stateScrollRectangle: "scrollHome",
    stateScrollCircle: "scrollHomeCircle",
    scrollButton: { rectangleColor: "#B2B2B2", circleColor: "#ffffff" },
    scrollButtonColor: "#B2B2B2",
    autoHideShowHeader: "topNavigation active activeBackground-home"
  },
  {
    title: "We Solve Tough AI Stuff",
    logo: true,
    colorImage: "../assets/logo-white.png",
    classNameOurStuffButton: "topButtons ourStuffWeSolveToughAI",
    classNameHitUsUp: "topButtons hitUsUpWeSolveToughAI hitUsUpBackground",
    scroll: true,
    stateScrollRectangle: "scrollWeSolveToughAI",
    stateScrollCircle: "scrollWeSolveToughAICircle",
    navigationColor: "#ffffff",
    scrollButton: { rectangleColor: "#565656", circleColor: "#2a2a2a" },
    scrollButtonColor: "#565656",
    autoHideShowHeader: "topNavigation active activeBackground-weSolveToughAI"
  },
  {
    title: "We Make Cool AI Stuff",
    logo: true,
    colorImage: "../assets/logo-white.png",
    classNameOurStuffButton: "topButtons ourStuffWeMAkeCoolAI",
    classNameHitUsUp: "topButtons hitUsUpWeMakeCoolAI hitUsUpBackground",
    scroll: true,
    stateScrollRectangle: "scrollWeMAkeCoolAI",
    stateScrollCircle: "scrollWeMAkeCoolAICircle",
    navigationColor: "#ffffff",
    scrollButton: { rectangleColor: "#7169cc", circleColor: "#2f2f8a" },
    scrollButtonColor: "#7169cc",
    autoHideShowHeader: "topNavigation active activeBackground-weMakeCoolAI"
  },

  {
    title: "Get Free AI Stuff",
    logo: true,
    colorImage: "../assets/logo-color.png",
    classNameOurStuffButton: "topButtons ourStuffGetFreeAI",
    classNameHitUsUp: "topButtons hitUsUpFreeAI hitUsUpBackground",
    scroll: true,
    stateScrollRectangle: "scrollFreeAI",
    stateScrollCircle: "scrollFreeAICircle",
    navigationColor: "#222222",
    scrollButton: { rectangleColor: "#B2B2B2", circleColor: "#2a2a2a" },
    scrollButtonColor: "B2B2B2",
    autoHideShowHeader: "topNavigation active activeBackground-getFreeAI"
  },
  {
    title: "Drop In!",
    logo: true,
    colorImage: "../assets/logo-color.png",
    classNameOurStuffButton: "topButtons ourStuffGetFreeAI",
    classNameHitUsUp: "topButtons hitUsUpFreeAI hitUsUpBackground",
    scroll: false,
    stateScrollRectangle: "scrollFooter",
    stateScrollCircle: "scrollFooterCircle",
    navigationColor: "#222222",
    scrollButton: {
      rectangleColor: "antiquewhite",
      circleColor: "antiquewhite"
    },
    scrollButtonColor: "antiquewhite",
    autoHideShowHeader: "topNavigation active activeBackground-weDeliverFreeAI"
  },
  {
    title: "",
    logo: true,
    colorImage: "../assets/logo-color.png",
    classNameOurStuffButton: "topButtonsAbout ourStuffGetFreeAI",
    classNameHitUsUp: "topButtonsAbout hitUsUpFreeAI hitUsUpBackground",
    scroll: false,
    stateScrollRectangle: "scrollFooter",
    stateScrollCircle: "scrollFooterCircle",
    navigationColor: "#222222",
    scrollButton: {
      rectangleColor: "antiquewhite",
      circleColor: "antiquewhite"
    },
    scrollButtonColor: "antiquewhite",
    autoHideShowHeader: "topNavigation active activeBackground-weDeliverFreeAI"
  }
];
export const ABOUT_NAVIGATION_ARRAY = [
  {
    title: "Who we are ?",
    hashurl: "#we",
    logo: true,
    colorImage: "../assets/logo-color.png",
    navigationColor: "#222222",
    ourStuffClassName: "topButtonsAbout ourStuffButtonHome fontContainer",
    hitUsUpClassName:
      "topButtonsAbout hitUsUpHome dynamichitUsUp fontContainer",
    scroll: true,
    stateScrollRectangle: "scrollHome",
    stateScrollCircle: "scrollHomeCircle",
    scrollButton: { rectangleColor: "#B2B2B2", circleColor: "#ffffff" },
    scrollButtonColor: "black",
    autoHideShowHeader: "topNavigation active activeBackground-Who_Are_We"
  },
  {
    title: "We are your partner",
    hashurl: "#partner",
    logo: true,
    colorImage: "../assets/logo-white.png",
    navigationColor: "#FFFFFF",
    ourStuffClassName:
      "topButtonsAbout ourStuffButtonWeArePartners fontContainer",
    hitUsUpClassName:
      "topButtonsAbout hitUsUpWestay dynamichitUsUp fontContainer",
    scroll: true,
    stateScrollRectangle: "scrollHome",
    stateScrollCircle: "scrollHomeCircle",
    scrollButton: { rectangleColor: "#B2B2B2", circleColor: "#ffffff" },
    scrollButtonColor: "white",
    autoHideShowHeader: "topNavigation active activeBackground-we_Are_Partner"
  },
  {
    title: "Our Resident ?",
    hashurl: "#resident",
    logo: true,
    colorImage: "../assets/logo-color.png",
    navigationColor: "#222222",
    ourStuffClassName: "topButtonsAbout ourStuffButtonHome fontContainer",
    hitUsUpClassName:
      "topButtonsAbout hitUsUpHome dynamichitUsUp fontContainer",
    scroll: true,
    stateScrollRectangle: "scrollHome",
    stateScrollCircle: "scrollHomeCircle",
    scrollButton: { rectangleColor: "#B2B2B2", circleColor: "#ffffff" },
    scrollButtonColor: "black",
    autoHideShowHeader: "topNavigation active activeBackground-Our_Resident"
  },
  {
    title: "Drop In!",
    hashurl: "#dropIn",
    logo: true,
    colorImage: "../assets/logo-color.png",
    navigationColor: "#222222",
    ourStuffClassName: "topButtonsAbout ourStuffButtonHome fontContainer",
    hitUsUpClassName:
      "topButtonsAbout hitUsUpHome dynamichitUsUp fontContainer",
    scroll: false,
    stateScrollRectangle: "scrollHome",
    stateScrollCircle: "scrollHomeCircle",
    scrollButton: { rectangleColor: "#B2B2B2", circleColor: "#ffffff" },
    scrollButtonColor: "black",
    autoHideShowHeader: "topNavigation active activeBackground-Our_Resident"
  },
  {
    title: "",
    hashurl: "",
    logo: false,
    colorImage: "",
    ourStuffClassName: "",
    hitUsUpClassName: "",
    scroll: false,
    stateScrollRectangle: "",
    stateScrollCircle: "",
    navigationColor: "",
    stateScroll: { rectangleColor: "", circleColor: "" },
    scrollColor: "",
    autoHideShowHeader: ""
  }
];
export const EMAIL_REGEX = {
  regexObject: /[\w-]+@([\w-]+\.)+[\w-]+/i
};
