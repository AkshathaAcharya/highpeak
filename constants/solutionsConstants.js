export const SOLUTIONS = {
  "artificial-intelligence": {
    solutionsName: "Artificial Intelligence",
    Solution_url: "ArtificialIntelligence",
    ogURL: "https://highpeaksw.com/services/Artificial-Intelligence",
    ogImage: "../assets/AI_home.svg",
    metaTitle: "High Peak Software - Artificial Intelligence",
    metaDescription:
      "Custom AI-powered software development for small, medium, and big enterprises. Find Artificial Intelligence solutions for your business with High Peak.",
    metaKeywords:
      "AI solutions, Artificial Intelligence solutions, Artificial Intelligence systems, Artificial Intelligence applications, AI technologies, AI for business, AI projects, AI services, AI development",
    componentToInclude: "Sixcards",
    flipCards: [
      {
        frontCard: "Machine Learning",
        backGroundImage: 'url("/assets/MachineLearning.svg")',
        backCard: [
          "Supervised ML",
          "Unsupervised ML",
          "Semi-supervised ML",
          "Reinforcement learning"
        ]
      },
      {
        frontCard: "Deep Learning",
        backGroundImage: 'url("/assets/DeepLearning.svg")',
        backCard: [
          "Fully connected neural networks",
          "Convolutional neural networks",
          "Recurrent neural networks",
          "Generative adversarial networks",
          "Deep reinforcement learning"
        ]
      },
      {
        frontCard: "Computer Vision",
        backGroundImage: 'url("/assets/ComputerVision.svg")',
        backCard: [
          "Image upscaling",
          "Object identification",
          "Facial recognition",
          "Defect identification",
          "Optical character recognition"
        ]
      },
      {
        frontCard: "Predictive Analytics",
        backGroundImage: 'url("/assets/PredictiveAnalytics.svg")',
        backCard: [
          "Data collection",
          "Data analysis ",
          "Statistical analysis ",
          "Predictive modelling ",
          "Model deployment ",
          "Model monitoring"
        ]
      },
      {
        frontCard: "Robotic Process Automation",
        backGroundImage: 'url("/assets/RoboticProcessAutomation.svg")',
        backCard: [
          "Business process automation",
          "Automated document classification",
          "Automated data extraction",
          "Automated report generation",
          "Virtual system integration",
          "Information auditing",
          "Invoice processing"
        ]
      },
      {
        frontCard: "Natural Language Processing",
        backGroundImage: 'url("/assets/NaturalLanguageProcessing.svg")',
        backCard: [
          "Speech recognition",
          "Natural language understanding",
          "Natural language generation",
          "Syntax and semantic analysis",
          "Sentiment analysis",
          "Automatic summarization"
        ]
      }
    ]
  },
  "machine-learning": {
    solutionsName: "Machine Learning",
    Solution_url: "Machine Learning",
    ogURL: "https://highpeaksw.com/services/Machine-Learning",
    ogImage: "../assets/MachineLearning_home.svg",
    metaTitle: "High Peak Software - Machine Learning",
    metaDescription:
      "Explore opportunities for your business with our machine learning models built to perform NLP, predictive analysis and more. ",
    metaKeywords:
      "Machine Learning models, Machine Learning services, Machine Learning projects, Predictive Analytics, ML model generation, Machine Learning development, ML algorithms, Neural networks, General Adversarial Networks",
    componentToInclude: "Fourcards",
    flipCards: [
      {
        frontCard: "Supervised ML",
        backGroundImage: 'url("/assets/SupervisedML.svg")',
        backCard: [
          "Support vector machines",
          "Linear regression",
          "Logistic regression",
          "Naive Bayes",
          "Linear discriminant analysis",
          "Decision trees",
          "K-nearest neighbor algorithm"
        ]
      },
      {
        frontCard: "Unsupervised ML",
        backGroundImage: 'url("/assets/UnsupervisedML.svg")',
        backCard: [
          "Clustering",
          "Anomaly detection",
          "Neural networks",
          "Autoencoders",
          "Generative adversarial networks",
          "Expectation–maximization algorithm",
          "Method of moments",
          "Blind signal separation"
        ]
      },
      {
        frontCard: "Semi-supervised ML",
        backGroundImage: 'url("/assets/SemiSupervisedML.svg")',
        backCard: [
          "Generative models",
          "Low-density separation",
          "Graph-based methods",
          "Heuristic approaches"
        ]
      },
      {
        frontCard: "Reinforcement Learning",
        backGroundImage: 'url("/assets/ReinforcementLearning.svg")',
        backCard: [
          "Deep reinforcement learning",
          "Inverse reinforcement learning",
          "Apprenticeship learning"
        ]
      }
    ]
  },
  "data-science": {
    solutionsName: "Data Science",
    Solution_url: "DataScience",
    ogURL: "https://highpeaksw.com/services/Data-Science",
    ogImage: "../assets/DataScience_home.svg",
    metaTitle: "High Peak Software - Data Science",
    metaDescription:
      "Data science technologies with AI expertise that will add value to your business ",
    metaKeywords:
      "Data Science, Data preparation, Data analysis, Data analytics, Data transformation, Model generation, Model training, Data visualization, Predictive analytics, Prescriptive analytics, Descriptive analytics",
    componentToInclude: "Sixcards",
    flipCards: [
      {
        frontCard: "Data Preparation",
        backGroundImage: 'url("/assets/DataPreparation.svg")',
        backCard: [
          "Data discovery ",
          "Data assessment",
          "Data cleansing",
          "Data validation",
          "Data transformation",
          "Data enrichment",
          "Data storage and delivery"
        ]
      },
      {
        frontCard: "Data Analysis",
        backGroundImage: 'url("/assets/DataAnalysis.svg")',
        backCard: [
          "Descriptive analysis",
          "Exploratory analysis",
          "Inferential analysis",
          "Predictive analysis",
          "Causal analysis",
          "Mechanistic analysis"
        ]
      },
      {
        frontCard: "Data Transformation",
        backGroundImage: 'url("/assets/DataTransformation.svg")',
        backCard: [
          "Data discovery",
          "Data mapping",
          "Code generation",
          "Code execution",
          "Data review"
        ]
      },
      {
        frontCard: "Model Generation & Training",
        backGroundImage: 'url("/assets/ModelGeneration.svg")',
        backCard: [
          "Structuralist modelling",
          "Functionalist modelling",
          "Data training"
        ]
      },
      {
        frontCard: "Data Visualization and Reporting",
        backGroundImage: 'url("/assets/DataViz.svg")',
        backCard: [
          "Data charts",
          "Data plots",
          "Heat maps",
          "Network graphs",
          "Animated graphics"
        ]
      },
      {
        frontCard: "Data Analytics",
        backGroundImage: 'url("/assets/DataAnalytics.svg")',
        backCard: [
          "Descriptive analytics",
          "Diagnostic analytics",
          "Predictive analytics",
          "Prescriptive analytics"
        ]
      }
    ]
  },
  "product-development": {
    solutionsName: "Product Development",
    Solution_url: "ProductDevelopment",
    ogURL: "https://highpeaksw.com/services/Product-Development",
    ogImage: "../assets/ProductDevelopment_home.svg",
    metaTitle: "High Peak Software - Product Development",
    metaDescription:
      "Software solutions built using emerging advanced technologies for optimum business success.",
    metaKeywords:
      "Product development, Product development roadmap, Web development, Mobile development, Progressive web app development, Product management, Product marketing",
    componentToInclude: "Sixcards",
    flipCards: [
      {
        frontCard: "Product Design",
        backGroundImage: 'url("/assets/ProductDesign.svg")',
        backCard: [
          "User analysis",
          "UI/UX design",
          "Design testing",
          "Usability testing",
          "User testing"
        ]
      },
      {
        frontCard: "Web App Development",
        backGroundImage: 'url("/assets/WebDeevelopment.svg")',
        backCard: [
          "Static web apps",
          "Dynamic web apps",
          "Single and multi-page apps",
          "Rich internet apps",
          "Responsive web apps",
          "Progressive web apps"
        ]
      },
      {
        frontCard: "Mobile App Development",
        backGroundImage: 'url("/assets/MobileDevelopment.svg")',
        backCard: [
          "Native iOS apps",
          "Native android apps",
          "Hybrid apps",
          "Cross-platform apps"
        ]
      },
      {
        frontCard: "Testing & QA",
        backGroundImage: 'url("/assets/QA.svg")',
        backCard: [
          "Automation testing",
          "Manual testing",
          "Functional testing",
          "Non-functional testing",
          "Quality assurance",
          "Quality control"
        ]
      },
      {
        frontCard: "Deployment/DevOps",
        backGroundImage: 'url("/assets/devops.svg")',
        backCard: [
          "Infrastructure as code",
          "CI/CD",
          "Test automation",
          "Containerization",
          "Orchestration",
          "Software deployment",
          "Software measurement"
        ]
      },
      {
        frontCard: "Go-to-Market",
        backGroundImage: 'url("/assets/GoToMarket.svg")',
        backCard: [
          "Product--market fit analysis",
          "User analysis",
          "Go-to-market strategies",
          "Minimum viable product",
          "Proof-of-concept",
          "Sprint releases and updates"
        ]
      }
    ]
  },
  "ui-&-ux-design": {
    solutionsName: "UI & UX Design",
    Solution_url: "UI & UX Design",
    ogURL: "https://highpeaksw.com/services/UI-&-UX-Design",
    ogImage: "../assets/UXDesign_home.svg",
    metaTitle: "High Peak Software - UX and UI Design",
    metaDescription:
      "Helping you elevate your web presence by producing user friendly interactive designs built with extensive user research.",
    metaKeywords:
      "User experience design, User interface design, Web design, UX design, Product design, UI design, UI/UX design, Web page design, Lean UX, App UI design",
    componentToInclude: "Fivecards",
    flipCards: [
      {
        frontCard: "Requirement Analysis",
        backGroundImage: 'url("/assets/RequirementAnalysis.svg")',
        backCard: [
          "User analysis",
          "Brand analysis",
          "Requirement gathering",
          "Feature analysis"
        ]
      },
      {
        frontCard: "User Research",
        backGroundImage: 'url("/assets/UserResearch.svg")',
        backCard: [
          "One-on-one interviews",
          "Focus groups",
          "Surveys",
          "Questionnaires"
        ]
      },
      {
        frontCard: "User Analysis",
        backGroundImage: 'url("/assets/UserAnalysis.svg")',
        backCard: [
          "User personas",
          "User journey maps",
          "Workflow",
          "Navigation flow",
          "Storyboarding",
          "Information architecture"
        ]
      },
      {
        frontCard: "Interactive Prototyping",
        backGroundImage: 'url("/assets/Prototyping.svg")',
        backCard: ["Moodboards", "Sitemaps", "Wireframes", "Visual mockups"]
      },
      {
        frontCard: "Design Testing",
        backGroundImage: 'url("/assets/DesignTesting.svg")',
        backCard: ["Usability testing", "User testing", "Feedback loop"]
      }
    ]
  },
  marketing: {
    solutionsName: "Marketing",
    Solution_url: "Marketing",
    ogURL: "https://highpeaksw.com/services/Marketing",
    ogImage: "../assets/DigitalMarketing_home.svg",
    metaTitle: "High Peak Software - Marketing",
    metaDescription:
      "Powering your marketing efforts through content, SEO, email, social media, and more. ",
    metaKeywords:
      "Digital marketing, Email marketing, Content marketing, Social media marketing, Social media advertising, Social media analytics, Content analytics, Search engine optimization, Product promotion, Brand promotion, Product positioning, Market research",
    componentToInclude: "Fivecards",
    flipCards: [
      {
        frontCard: "Digital Marketing",
        backGroundImage: 'url("/assets/DigitalMarketing.svg")',
        backCard: [
          "Content marketing",
          "Social media marketing",
          "Email marketing",
          "Inbound marketing",
          "SEO",
          "Organic and paid promotions"
        ]
      },
      {
        frontCard: "Social Media Marketing",
        backGroundImage: 'url("/assets/SocialMediaMarketing.svg")',
        backCard: [
          "Strategy",
          "Scheduling",
          "Analytics",
          "Creative & copy",
          "Organic and paid promotions"
        ]
      },
      {
        frontCard: "Search Engine Optimization",
        backGroundImage: 'url("/assets/SEO.svg")',
        backCard: [
          "On-page SEO",
          "Off-page SEO",
          "SERP ranking",
          "Content audit"
        ]
      },
      {
        frontCard: "Email Marketing",
        backGroundImage: 'url("/assets/EmailMarketing.svg")',
        backCard: [
          "Personalized campaigns",
          "Automated campaigns",
          "Drip campaigns",
          "Action-triggered campaigns",
          "Recurrent emails",
          "Newsletter campaigns"
        ]
      },
      {
        frontCard: "Content Marketing",
        backGroundImage: 'url("/assets/ContentMarketing.svg")',
        backCard: [
          "Strategy",
          "Ideation",
          "Research",
          "Design",
          "Curation",
          "Development",
          "Distribution",
          "Audit",
          "Analytics"
        ]
      }
    ]
  }
};
