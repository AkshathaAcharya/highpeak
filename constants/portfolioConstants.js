export const FILTERS = ["All", "AI", "products", "services"];
export const PORTFOLIO_CONSTANTS = [
  {
    projectName: "Scarlet",
    url: "scarlet",
    projectImage: "../assets/scarlet.png",
    subTitle: "Intelligent document processing",
    projectDescription: [
      "Scarlet is an automated document processing system, powered by intelligent deep-learning algorithms. It carries out a geometrical analysis of a document and accurately extracts relevant data. It is a customizable solution capable of processing any kind of document, including images and non-searchable PDF files, specifically tailored for your business requirements."
    ],
    clientOverViews: [],
    features: [
      "Automated document analysis",
      "Intelligent data capture ",
      "Document review"
    ],
    industries: [
      "Finance ",
      "Banking ",
      "Tax & Insurance ",
      "Healthcare",
      "HR",
      "Legal ",
      "Manufacturing"
    ],
    tags: ["ML", "OCR", "Product development"],
    productOrService: "products",
    All: true,
    AI: true,
    products: true,
    services: false,
    campaign: false
  },
  {
    projectName: "Vision AI",
    url: "vision-ai",
    projectImage: "../assets/VisionAIportfolio.png",
    subTitle: "Automated AI Defect Identification System",
    projectDescription: [
      "Vision AI is an automated AI defect identification system. It is an intelligent platform that leverages machine-learning models and computer vision for anomaly detection from surface images. The application records and identifies unmarked surface-level defects in real-time for products, such as auto body parts, healthcare equipment, apparel, footwear, etc. "
    ],
    clientOverViews: [],
    features: [
      "Real-time anomaly detection",
      "Image repository",
      "Model repository",
      "Automated model tuning and training",
      "Model validation"
    ],
    industries: [
      "Manufacturing",
      "Textiles/footwear industries",
      "Auto body parts manufacturing",
      "Healthcare equipment",
      "Hardware production"
    ],
    tags: ["Machine learning", "Product development"],
    productOrService: "products",
    All: true,
    AI: true,
    products: true,
    services: false,
    campaign: false
  },
  {
    projectName: "IIOT",
    url: "iiot",
    projectImage: "../assets/IIOT.png",
    subTitle: "Secure remote access platform",
    projectDescription: [
      "IIOT is a secure one-stop shop to access, control, and monitor all remote systems, including IOT assets. ",
      "You can now provide your users secure access to specific remote systems or groups of systems, assign appropriate roles and responsibilities, effectively communicate and monitor system usage, and so on."
    ],
    clientOverViews: [],
    features: [
      "Privileged, role-based access",
      "Intelligent auditing & reporting",
      "Secure communications and data transfer",
      "Secure encryption",
      "Intuitive UI"
    ],
    industries: [
      "IT ",
      "Logistics and transportation",
      "Manufacturing",
      "Oil & gas",
      "Energy & utility",
      "Smart cities and countries"
    ],
    tags: ["Product development", "Web app", "Data security"],
    productOrService: "products",
    All: true,
    AI: false,
    products: true,
    services: false,
    campaign: false
  },

  {
    projectName: "Almanac",
    url: "almanac",
    projectImage: "../assets/almanac.png",
    subTitle: "Project and team management system",
    clientOverViews: [],
    projectDescription: [
      "Almanac is an intelligent project and team management system that enables you to communicate, collaborate, and delegate tasks. The platform also includes a comprehensive dashboard, task manager, time tracker, and an automated report generator. ",
      "Using Almanac, you can monitor and track employee productivity, oversee time taken for each task for individual users, realign resources to projects based on availability, and so on."
    ],
    clientOverViews: [],
    features: [
      "Comprehensive dashboard",
      "Project management suite",
      "Secure layered access",
      "Attendance tracker",
      "Report generation",
      "Google--Almanac calendar sync"
    ],
    industries: [],
    tags: ["Product development", "iOS", "Android"],
    productOrService: "products",
    All: true,
    AI: false,
    products: true,
    services: false,
    campaign: false
  },
  {
    projectName: "PeakCruit",
    url: "peakcruit",
    projectImage: "../assets/peakcruit.png",
    subTitle: "Hiring & talent acquisition app",
    projectDescription: [
      "PeakCruit is a mobile application for hiring & talent acquisition, available on both iOS and android. ",
      "It allows you to register and create a detailed academic profile and upload resumes. You can also register for aptitude tests as a prerequisite for the interview."
    ],
    clientOverViews: [],
    features: [
      "User registration",
      "Online test management ",
      "Automated candidature communications"
    ],
    industries: [],
    tags: ["Product development", "iOS", "Android"],
    productOrService: "products",
    All: true,
    AI: false,
    products: true,
    services: false,
    campaign: false
  },
  {
    projectName: "LendStash",
    url: "lendstash",
    projectImage: "../assets/lendstash.png",
    subTitle: "Book rental platform",
    projectDescription: [
      "LendStash is a peer-to-peer book rental platform, which aims to contribute to the sharing economy.  You can lend and borrow books by creating a virtual library and receiving payments for the same.",
      "Lendstash uses a blockchain technology named ‘Chronicles’ to keep your transactional records (ledger keeping) safe and tamper-proof."
    ],
    clientOverViews: [],
    features: [
      "Build your book stash",
      "Lend books",
      "Receive payment",
      "Find and rent books",
      "Return books"
    ],
    industries: [],
    tags: ["Blockchain", "Product development", "iOS", "Android"],
    productOrService: "products",
    All: true,
    AI: false,
    products: true,
    services: false,
    campaign: false
  },
  {
    projectName: "PeakSpeak",
    url: "peakspeak",
    projectImage: "../assets/peakspeak.png",
    subTitle: "Anonymous group chat application",
    projectDescription: [
      "PeakSpeak is an anonymous group chat application designed and developed using React Native, a cross-platform development framework. It allows you to exchange messages anonymously on a group chat. It can be used for org-wide feedback gathering, anonymous employee reviews, and surveys."
    ],
    clientOverViews: [],
    features: [
      "Easy registration",
      "Anonymous chatting",
      "User privacy",
      "Admin actions"
    ],
    industries: [],
    tags: ["Product development", "iOS", "Android"],
    productOrService: "products",
    All: true,
    AI: false,
    products: true,
    services: false,
    campaign: false
  },
  {
    projectName: "Sharpic",
    url: "sharpic",
    projectImage: "../assets/sharpic.png",
    subTitle: "Intelligent image upscaling system",
    projectDescription: [
      "Sharpic is an image enhancement tool that uses technologies such as deep learning, Generative Adversarial Networks, and bilinear interpolations to enhance and refine low resolution images upto six times the original resolution with minimal loss of data."
    ],
    clientOverViews: [],
    features: [
      "Intelligent image upscaling",
      "6X image enhancement",
      "Minimal loss of data",
      "Intuitive UI"
    ],
    industries: [
      "Textile & manufacturing",
      "Policing & security",
      "Media & publishing",
      "Photography"
    ],
    tags: ["ML", "CNN", "Product development"],
    productOrService: "products",
    All: true,
    AI: true,
    products: true,
    services: false,
    campaign: false
  },
  {
    projectName: "AIstrology",
    url: "aistrology",
    projectImage: "../assets/AIstrologyPortfolio.png",
    subTitle: "Personalized Pocket Astrologer",
    projectDescription: [
      "The AIstrology app is an AI-powered application, bridging advanced technology and thousands of years of Vedic knowledge in astrology. The application enables you to book an appointment with an astrologer and avail solutions and predictions to various career and work-related problems. The application also comprises a personal chatbot Ri Shi, serving as your pocket astrologer."
    ],
    clientOverViews: [],
    features: [
      "Interactive chatbot",
      "Schedule appointments",
      "Personalized predictions"
    ],
    industries: [],
    tags: ["Product development", "iOS", "Android"],
    productOrService: "products",
    All: true,
    AI: true,
    products: true,
    services: false,
    campaign: false
  },

  {
    projectName: "Lawyer Networking Platform",
    url: "lawyer-networking-platform",
    projectImage: "../assets/2_Lawyer.png",
    subTitle: "",
    projectDescription: [
      "The Lawyer-to-lawyer Networking Platform is a web application designed for lawyers to enable them to work across locations, exchange knowledge, and virtually practice law at their own convenience."
    ],
    clientOverViews: [
      "The company is an established law firm that spans across three continents. The firm has extensive expertise in corporate and commercial law, general corporate, technology, immigration, conflict resolution, intellectual property and employment law."
    ],
    features: [
      "Lawyer discovery system",
      "Task management system ",
      "User management system ",
      "Feedback management system",
      "Invoice management system",
      "Knowledge centre",
      "Event management system"
    ],
    industries: [],
    tags: ["Legal", "Product development", "Web app"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Chemical Management Solution",
    url: "chemical-management-solution",
    projectImage: "../assets/1_Chemical.png",
    subTitle: "",
    projectDescription: [
      "The Chemical Management Solution is a web application that automates chemical management, inventory, and logistics processes. The application enables users to monitor regulatory compliance, safety standards, process control, distribution control, track accountability and audit trail."
    ],
    clientOverViews: [
      "The company is an environmental health and safety (EH&S) software company in the US that leverages cloud computing technology. It specializes in the development of software solutions that are used by major corporations and governments in the world to gauge, manage, record, and report mission critical EH&S data."
    ],
    features: [
      "Safety data sheet (SDS) builder ",
      "Label generator",
      "SDS library",
      "Intelligent text viewer",
      "Chemical order management system ",
      "Chemical inventory & logistics management system"
    ],
    industries: [],
    tags: ["Healthcare", "Product development", "Web app"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Smart Enterprise Payroll System",
    url: "smart-enterprise-payroll-system",
    projectImage: "../assets/3_Smart.png",
    subTitle: "",
    projectDescription: [
      "The Smart Enterprise Payroll System is an intelligent web app that is designed to automate the enterprise payroll workflow in organizations. ",
      "The system ensures user data security and information safety through a secure communication platform where users can securely exchange, transfer and consume sensitive information.",
      "In addition, the web app facilitates whitelabelling of the payroll platform by enabling organizations to rebrand the platform according to their brand requirements."
    ],
    clientOverViews: [
      "The company is a leading provider of scalable and customizable payroll and statutory compliance solutions across the globe with 68 years of industry expertise. The company pays specific attention to continuous needs for scalability, data security, disaster recovery, and uninterrupted business continuity."
    ],
    features: [
      "Automated information warehouse",
      "Interactive chatbot ",
      "Card-based UI ",
      "Three-tiered information access",
      "Secure communication platform",
      "Secure transmission and file sharing",
      "Effective ticketing system",
      "Whitelabeling payroll solutions"
    ],
    industries: [],
    tags: ["Tax & insurance", "Product development", "Progressive Web app"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Office Ergonomic Software",
    url: "office-ergonomic-software",
    projectImage: "../assets/4_Office.png",
    subTitle: "",
    projectDescription: [
      "The Office Ergonomic Software is a behavioral biometric-based, intelligent safety system that is integrated with an ergonomic suite. The software is designed to analyze potential risks of ergonomic injuries that could be sustained by enterprise workers and provide them with  tested, preventive measures.",
      "The application measures and records employees’ behavioral biometric data to enable organizations to gain insights into employee productivity and workstyle, org-wide."
    ],
    clientOverViews: [
      "A leading environmental health and safety (EH&S) software company in the US, which leverages cloud computing technology to create software solutions to gauge, manage, record, and report mission critical EH&S data."
    ],
    features: [
      "Office ergonomics solution",
      "Behavioural biometrics tracking system",
      "Interactive learning management system",
      "Training and ergonomic awareness videos",
      "Survey management tool",
      "Issue management system ",
      "Customer relationship management system ",
      "Intelligent reporting system ",
      "Comprehensive dashboard"
    ],
    industries: [],
    tags: ["Healthcare & safety", "Product development", "Web app"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Legal Contracts Services Platform",
    url: "legal-contracts-services-platform",
    projectImage: "../assets/5_LegalContracts.png",
    subTitle: "",
    projectDescription: [
      "The Legal Contract Services Platform uses machine-learning algorithms to classify documents, extract critical data relative to a legal case, and annotate important information for ready use. The platform enables businesses and individuals to better understand legal contracts by processing, annotating, and summarizing important legal points in the document"
    ],
    clientOverViews: [
      "The company is a leading software company delivering enterprise SaaS solutions that leverages machine learning to accurately extract critical information from legal contracts. They focus on helping organizations maximize profits and minimize risks in their commercial relationships by understanding their legal business contracts."
    ],
    features: [
      "Contracts management system",
      "Automated document identification ",
      "Automated data extraction",
      "Automated configurable annotation tool ",
      "Intelligent document summarization  ",
      "Document validation",
      "Intelligent search management system",
      "User management system",
      "Content management system ",
      "Customer portal"
    ],
    industries: [],
    tags: ["Legal", "ML", "Product development", "Web app"],
    productOrService: "services",
    All: true,
    AI: true,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Lawyer Intelligence Augmentation System",
    url: "lawyer-intelligence-augmentation-system",
    projectImage: "../assets/7_LawyerIntelligence.png",
    subTitle: "",
    projectDescription: [
      "The Lawyer Intelligence Augmentation System uses natural language processing and machine learning algorithms to automatically classify documents and extract data pertinent to a legal case. Further, it automatically annotates/highlights important information for ready use. ",
      "It autosummarizes contents of case emails and uses sentiment analysis to identify context. It also delivers intelligently visualized reports and analytics to enable attorneys to quickly arrive at informed decisions."
    ],
    clientOverViews: [
      "A law firm in the U.S, which delivers legal, risk, and compliance solutions to enterprises. The company mainly focuses on enabling lawyers to have access to critical case-related information that would allow them to make informed decisions quickly."
    ],
    features: [
      "Case management system",
      "Automated document classification",
      "Automated data extraction",
      "Autosummarization of emails",
      "Sentiment analysis",
      "Content management system",
      "Interactive data visualization",
      "Intelligent search mechanism"
    ],
    industries: [],
    tags: ["Legal", "ML", "NLP", "Sentiment Analysis"],
    productOrService: "services",
    All: true,
    AI: true,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Healthcare Referral Platform",
    url: "healthcare-referral-platform",
    projectImage: "../assets/8_Healthcare.png",
    subTitle: "",
    projectDescription: [
      "The Healthcare Referral Platform is available as a web app, iOS and android apps. This all-inclusive application enables healthcare practitioners establish a referral network of hospitals, physicians, and medical companies in order to provide patients better specialized healthcare access and information."
    ],
    clientOverViews: [
      "A leading software company in the US, which develops IT solutions for healthcare service providers. The company aims to deliver optimal healthcare services to all individuals through a referral system for physicians to refer specialised treatments and other physicians to their patients."
    ],
    features: [
      "Referral discovery system",
      "Referral management system",
      "Admin services",
      "Role-based access",
      "Effective communication platform",
      "Healthcare marketing & campaigning platform",
      "Logistics & inventory management system"
    ],
    industries: [],
    tags: ["Healthcare", "Product development", "iOS", "Android"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Multilingual Agriculture Advisory System",
    url: "multilingual-agriculture-advisory-system",
    projectImage: "../assets/15_Multilingual.png",
    subTitle: "",
    projectDescription: [
      "The Multilingual Agriculture Advisory System is available as a webapp, which includes intelligent weather prediction and crop management systems. It is available in two languages: Bangla and English.",
      "It provides advisories to farmers on the appropriate crops to grow according to Bangladeshi weather conditions such that crop wastage owing to natural disasters is mitigated."
    ],
    clientOverViews: [
      "The organization is a 50-year-old non-profit international agricultural research and training organization focusing on cereal grains and related cropping systems. The company’s research has helped tens of millions of farmers grow more nutritious, resilient and productive crops, using methods that nourish the environment and combat climate change. They collaborate with hundreds of partners in 50 countries and are part of the global network of CGIAR research centers, delivering science for a food-secure future."
    ],
    features: [
      "Weather prediction system",
      "Simulation of real-time geographical view ",
      "Crop management system",
      "Advisory management system ",
      "User management system"
    ],
    industries: [],
    tags: ["Climate & weather", "Product development"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "ECommerce Logistics Application",
    url: "ecommerce-logistics-application",
    projectImage: "../assets/11_Ecommerce.png",
    subTitle: "",
    projectDescription: [
      "The ECommerce Logistics Application is available as webapp, iOS and android apps. The platform ecosystem comprises three Product catering to three users: an ecommerce app for construction workers, a driver app for truck drivers, and an admin console for customer managers. ",
      "With the ecommerce app, users can get order construction materials delivered to their job sites. The system automatically assigns trucks and drivers according to the bulk size of the order. Users can track their delivery in real time, while admins can step in through the console if they need to modify orders, store locations, and product prices, etc."
    ],
    clientOverViews: [
      "The organization is an app-based and web-interface e-commerce logistics services provider connecting the driver network to the construction and building material supply chain. They aim to solve the frustrations that builders and suppliers face when they need to get materials from one location to the next."
    ],
    features: [
      "Intelligent product ecommerce ecosystem ",
      "Real-time delivery tracking",
      "Order processing system",
      "Route management system",
      "Automated order management system",
      "User management system",
      "Configurable admin console"
    ],
    industries: [],
    tags: ["Ecommerce", "Product development", "iOS", "Android"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Health & Fitness Monitoring Application",
    url: "health-and-fitness-monitoring-application",
    projectImage: "../assets/10_Health.png",
    subTitle: "",
    projectDescription: [
      "The Health & Fitness Monitoring Application is available as iOS and android apps. It acts as a personal guide that helps users understand their health. The application has been developed for sports personnel, fitness enthusiasts, and individual users. ",
      "With this app, users also have access to health-related articles on various topics such as sleep, nutrition, fitness, lifestyle, alternative therapy, e-health, psychology, and more."
    ],
    clientOverViews: [
      "Our client is a global transformation leadership organization in Germany enabling members to transform themselves, their organizations, their relationships, and ecosystems.",
      "Their mission encompasses business and personal development, social and sustainability activism, as well as the arts, both in for-profit or not-for-profit endeavors."
    ],
    features: [
      "User management system",
      "Healthcare monitoring system",
      "Personalized suggestions"
    ],
    industries: [],
    tags: ["Healthcare", "Product development", "iOS", "Android"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Professional Translation Services Platform",
    url: "professional-translation-service-platform",
    projectImage: "../assets/9_Professional.png",
    subTitle: "",
    projectDescription: [
      "The Professional Translation Services & Annotation Platform is available as web app, iOS and android apps. It enables INGOs and research organizations to automate the process of transcribing and translating local languages into English. The platform aims to serve as a tool for INGOs to manage their qualitative research processes."
    ],
    clientOverViews: [
      "The organization works with the UN, several NGOs, and the government in South East Asia, specifically focused on automating translation of research surveys and interviews. It aims to connect the government and non-profit organizations with dedicated retainers of translators, transcribers and editors, and improve the turn-around time and quality control of the end-to-end translation services process."
    ],
    features: [
      "Translation services platform",
      "Annotation platform ",
      "Content management system",
      "Payment gateway"
    ],
    industries: [],
    tags: ["Product development", "iOS", "Android"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Multi Currency Digital Wallet",
    url: "multi-currency-digital-wallet",
    projectImage: "../assets/13_MultiCurrency.png",
    subTitle: "",
    projectDescription: [
      "The Multi Currency Digital Wallet is available as iOS and android apps. It allows users to send, transfer, and exchange international money, irrespective of their location. In addition, it allows the use of various cryptocurrencies for exchange and transfer. ",
      "The company is also partnering with Ripple to employ the blockchain technology and cryptocurrencies for securing international money transfers and payments."
    ],
    clientOverViews: [
      "The organization wields 25 years of experience in financial markets with a focus on payments and foreign exchange at foreign and domestic financial institutions."
    ],
    features: [
      "Digital wallet",
      "International money transfer",
      "Multiple currencies ",
      "Cryptocurrency"
    ],
    industries: [],
    tags: ["Finance", "Mobile development", "iOS", "Android"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Automated Payment Processing Solutions",
    url: "automated-payment-processing-solutions",
    projectImage: "../assets/12Automated.png",
    subTitle: "",
    projectDescription: [
      "The Automated Payment Processing Solutions Platform automates the client and vendor payment operations of invoicing and payment processing through the development of an end-to-end ecosystem that would allow them to seamlessly automate and manage the complete process."
    ],
    clientOverViews: [
      "The organization is a provider of payment processing and information management services to the United States commercial and government vehicle fleet industry, Canada, South America, Europe, Asia, and Australia. They simplify the complexities of payment systems across continents and industries, including fleet, corporate payments, and health."
    ],
    features: [
      "Automated document classification",
      "Automated data extraction",
      "Automated inbound payments ",
      "Automated payment validation",
      "Credit risk assessment & validation",
      "Automated report generation ",
      "Interactive email engagement"
    ],
    industries: [],
    tags: ["Finance", "Product development", "ML", "OCR"],
    productOrService: "services",
    All: true,
    AI: true,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Intelligent Ad Campaign Optimization Solution",
    url: "intelligent-ad-campaign-optimization-solution",
    projectImage: "../assets/14_IntelligentAd.png",
    subTitle: "",
    projectDescription: [
      "The Intelligent Ad Campaign Optimization Solution is available as a robust web app, which enables users to create multiple ad campaigns simultaneously.  It allows cloning of campaigns by replicating the settings and functionalities. ",
      "The reports delivered by the system help in determining the top performing websites as well as the non-performing ones. With this information, users are able to make informed decisions regarding the campaigns"
    ],
    clientOverViews: [
      "The company is a leading online financial marketing platform with an audience of 25 million influential decision makers, including individual investors, active traders, financial advisors, institutional investors and so on. It empowers the financial ecosystem of publishers, brands and audiences through highly effective digital solutions designed specifically for the financial services industry using a unique mix of data and technology."
    ],
    features: [
      "Enhanced campaign management system",
      "Intelligent report generation",
      "Interactive dashboard",
      "Improved backend security",
      "User management system"
    ],
    industries: [],
    tags: ["Media & Publishing", "Product development", "Web app"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Manufacturing Process Optimization Solution",
    url: "manufacturing-process-optimization-solution",
    projectImage: "../assets/6_Footwear.png",
    subTitle: "",
    projectDescription: [
      "The Manufacturing Process Optimization Solution comprises various modules working together as a microservice architecture. The system records real-time data to enable factory supervisors, floor control managers, production managers, and line managers to maintain an overview of all the processes occurring in the factory outlet. It includes an intuitive learning management system for the workers to take lessons, complete certifications and track progress.",
      "The digital quality inspection platform enables the factory inspectors to collect information on factory environment conditions, workstation health, and other related problems."
    ],
    clientOverViews: [
      "Our client is a subsidiary of a leading provider of process optimization, quality management and control services based in the U.S.",
      "The company provides SaaS platform service that digitize retailer and brands manufacturing ecosystems by providing real-time quality, productivity, speed, and order status data."
    ],
    features: [
      "Intuitive learning management system",
      "Shop floor live control ",
      "Digital quality inspection platform",
      "IT asset management system",
      "Form design system"
    ],
    industries: [],
    tags: ["Manufacturing", "Product development", "Web app"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },

  {
    projectName: "Invoice Factoring Automation",
    url: "invoice-factoring-automation",
    projectImage: "../assets/scarletcasestudy1.png",
    subTitle: "",
    projectDescription: [
      "Scarlet is an automated document processing system, powered by intelligent deep-learning algorithms. It is a customisable solution capable of processing any kind of document, and can be specifically tailored for your business. In this case study, we explain how we enabled our client to automate invoice factoring processes, save 10K business hours, and achieve a 5,200% increase in document processing speed."
    ],
    clientOverViews: [
      "Our client is a provider of payment processing and information management services in the United States commercial and government vehicle fleet industry, Canada, South America, Europe, Asia, and Australia.",
      "They simplify the complexities of payment systems across continents and industries, including fleet, corporate payments, and health."
    ],
    features: [
      "Automated document analysis",
      "Intelligent data capture ",
      "Document review"
    ],
    industries: [
      "Finance ",
      "Banking ",
      "Tax & Insurance ",
      "Healthcare",
      "HR",
      "Legal ",
      "Manufacturing"
    ],
    tags: ["ML", "OCR", "Product development"],
    productOrService: "",
    All: true,
    AI: true,
    products: false,
    services: false,
    campaign: true
  },
  {
    projectName: "Operations Security",
    url: "operations-security",
    projectImage: "../assets/OperationsSecurity.png",
    subTitle: "",
    projectDescription: [
      "Operations security (OPSEC) has always been a grave concern in both military and enterprise businesses. In order to avoid information breach, defining levels of access, maintaining, and monitoring OPSEC are of prime importance. The OPSEC data visualization system is designed and developed to feature and provide insights on real-time data."
    ],
    clientOverViews: [
      "Our client’s mission is to simplify and automate how the full stack of stakeholders from fiduciaries to practitioners engage with cybersecurity activity, reducing costs and improving response time and confidence."
    ],
    features: [
      "Visualizations for OPSEC standard adherence",
      "Maintenance of security assets",
      "Standards—assets compliance mapping",
      "Security analysis"
    ],
    industries: [],
    tags: ["Cybersecurity", "Product development"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "It Security & Service Management",
    url: "it-security-service-management",
    projectImage: "../assets/ITsecurity.png",
    subTitle: "",
    projectDescription: [
      "The ITSM Engine is a high performance tool powered by machine learning algorithms. It accelerates the big data cleansing process and reduces the processing time from 36 hours to 3 hours. "
    ],
    clientOverViews: [
      "Our client is a provider of data quality management software, which ensures the integrity and quality of IT and operational data.",
      "They transform, align, and unify data across a range of sources in the industry, enabling businesses to improve IT and OT strategies and recommendations, while driving downstream value across organizations."
    ],
    features: [
      "Big data cleansing",
      "Improved processing time",
      "Faster system response time"
    ],
    industries: [],
    tags: ["Machine Learning", "IT Security", "Web app"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  },
  {
    projectName: "Health & Fitness Wearable Technology",
    url: "health-fitness-app-for-wearable",
    projectImage: "../assets/Health&fitnesswearable.png",
    subTitle: "",
    projectDescription: [
      "The health & fitness wearable is an iOS application, which pairs with a SmartShirt controller device to process biometric and other related data. ",
      "This app provides real - time insights on workouts.It is designed to analyze these data during workout sessions to track and improve user performance and help prevent injuries"
    ],
    clientOverViews: [
      "Our client is a manufacturer of smart shirts aimed to be used by military personnel, athletes, and fitness enthusiasts."
    ],
    features: [
      "Processing of biometric data",
      "Real-time insights on user activity",
      "Data analysis",
      "Tracking user performance "
    ],
    industries: [],
    tags: ["Health & Fitness", "Mobile App", "iOS"],
    productOrService: "services",
    All: true,
    AI: false,
    products: false,
    services: true,
    campaign: false
  }
];
