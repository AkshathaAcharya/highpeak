export const CASESTUDY_CONSTANTS = {
  "invoice-factoring-automation": {
    projectName: [
      "Invoice Factoring Automation: A Case Study",
      "Scarlet Enables 5,200% Increase in Documents Processed Per Hour"
    ],
    metaTitle: "Scarlet - Invoice Factoring Automation",
    metaDescription:
      "An automated end-to-end chemical management system for digitizing the chemical management inventory and logistics processes in health and safety companies.",
    metaKeywords:
      "Chemical logistics management. Logistics and transport management, Integrated logistics management, Chemical management system, Operations management & logistics for chemicals, Chemical data management system, Chemical inventory system, Chemical management database",
    ogURL: "https://www.highpeaksw.com/blogs/invoice-factoring-automation",
    clientOverviewHeader: "CLIENT OVERVIEW",
    clientOverview: [
      "Our client is a provider of payment processing and information management services in the United States commercial and government vehicle fleet industry, Canada, South America, Europe, Asia, and Australia.",
      "They simplify the complexities of payment systems across continents and industries, including fleet, corporate payments, and health."
    ],
    knowMoreLink: "/products/scarlet",
    problems: [
      "Our client has thousands of existing customers. They processed over 8000 invoices on a daily basis.",
      "Creditors and debtors send payment information over e-mails via different avenues, including invoices, bank checks, drafts, etc. This information is either included in the body of the email or as an email attachment.",
      "Because these invoices came from disparate sources, they were in various formats and did not have a standardized template. This made it even more difficult to identify.",
      "The client employed a large manual workforce to process these invoices and identify their source and format. This entire process required a substantial amount of time, effort, and resource overheads.",
      "For instance, processing 8000 invoices took more than 8 hours and at least 5-8 employees on a given business day.",
      "In addition, our client was facing other problems, including:",
      "Compromised productivity: Manual classification of documents received in different formats from different sources was time-consuming and counter-productive.",
      "Highly error-prone processes: Because the email attachments contained sensitive payment-related data, even the smallest error could prove to be catastrophic to business. Further, the manual processing of these emails was a highly error-prone process.",
      " Overhead costs: Manually processing such large volumes of payment-related information required a significant amount of resources and was an expensive affair.",
      "The client wanted to automate these manual processes and approached High Peak to facilitate business process automation. To achieve this, we introduced our product Scarlet for automating invoice factoring."
    ],
    challenges: [
      "Automation of document classification",
      "The client workforce had to manually read thousands of emails per day, check for attachments, and classify documents accordingly. Automating this operation to improve the processing speed of emails and efficiency of the overall process was a challenge",
      "Data centralization",
      "Unifying disparate sources to include multiple formats and templates especially when there is no definite standardization, posed a major challenge for the team.",
      "Data extraction",
      "Scarlet carries out data extraction for two kinds of data: structured and unstructured. These would include free text and tables present in the email body and digital attachments. Further, the digital attachments are of various types, including CSV files, non-readable PDFs, TIFF, PNG, etc. Extracting data from a plethora of scanned and image file formats accurately posed a great challenge.",
      "Data accuracy",
      "Because the financial information is extremely sensitive, the High Peak team had to ensure that the system is capable of extracting data with the highest levels of accuracy.",
      "Data security",
      "As mentioned above, our client was dealing with banking and financial data, which is extremely sensitive. The High Peak team had to ensure that extraction of data from different sources was performed in such a way that such sensitive data was secure and tamper-proof."
    ],
    impactNumbers: [
      {
        impactNumber: "5,200%",
        impactDescription: "Increase in document processing/hour"
      },
      {
        impactNumber: "3,250",
        impactDescription: "E-mails automatically processed/hour"
      },
      {
        impactNumber: "90%",
        impactDescription: "Data extraction accuracy"
      },
      {
        impactNumber: "10,560 h",
        impactDescription: "Manual hours saved/year"
      }
    ],
    solutionsHeader: "Scarlet Automates Invoice Factoring",
    solutions: [
      {
        img: "../assets/WordsListViewCase.png",
        solutionsDescription: [
          "Automated document processing for structured and unstructured data",
          "Creditors and debtors send payment information via different avenues, including invoices, bank checks, drafts, etc. This information is either included in the body of the email or as an email attachment. Further, the email attachments are of various types, including CSV files, non-readable PDFs, TIFF, PNG, etc.",
          "Scarlet first carries out a geometrical analysis of the document. This process analyzes the document and draws out the shape of the content in the document.",
          "For example, if the document contains a table, it would be identified as such by its even spacing, rows and columns, etc., whereas unstructured data would be identified by its free flow of text.",
          "Each content or data in the document is associated with attributes such as font size, etc. Scarlet analyzes whether the text is bold, italicized, or indented, and performs a hierarchical mapping of all contents in the document, mapping parent data with child data, and so on, in the form of a tree.",
          "This way, even unstructured data is given a specific structure and is made configurable. The required output can be customized by setting certain rules for the data to be extracted. For example, if the user requires only the invoice number and the bill amount from an invoice document, the system extracts only those, leaving out other trivial data, and presents them as the final, structured output.",
          "Scarlet achieves this by employing a complex combination of convolutional neural networks, recurrent neural networks, and segmentation, to analyze and extract required data."
        ]
      },
      {
        img: "../assets/DocumentDetailsCase.png",
        solutionsDescription: [
          "Accurate data extraction with improved OCR pipeline",
          "Scarlet can extract data from a document in three formats–tables, sections, and key-value pairs.",
          "A key--value pair is a set of data items that are directly linked to one another.",
          "For example, in an invoice document, the term “Invoice Number” could be a key, and the actual invoice number this term refers to, say, “G1234TY509” could be its value.",
          "Because every document has a different structure, the extraction of data depends on the type of document being processed.",
          "For instance, accounting and billing documents contain row and column values such as serial number, item of purchase, quantity, bill amount, and so on. The extraction of data from such a document will be in the table format.",
          "For structured data, the extraction is simple. However, analyzing unstructured, free-form data is a slightly complex process.",
          "For image files, Tesseract and Google Vision are then employed for performing Optical Character Recognition (OCR) for data extraction. With the help of OCR, Scarlet can scan a large number of documents, thereby improving the processing speed and data extraction time."
        ]
      },
      {
        img: "../assets/FilterViewCase.png",
        solutionsDescription: [
          "Improved data validation",
          " Validation of payment information between the vendor and debtor was being carried out manually. The client workforce had to identify partially extracted information from low-quality images and fill in the data manually. This process was time-consuming and highly error-prone.",
          "With the help of Scarlet, the entire process was automated and is now carried out without any manual intervention, thereby securing confidential data and preventing potential data leaks."
        ]
      },
      {
        img: "../assets/HomePageCase.png",
        solutionsDescription: [
          "90% data accuracy",
          "Data accuracy is of absolute significance because the client dealt with highly sensitive data that, if compromised or misrepresented, would result in heavy losses.",
          "On average, Scarlet is able to extract data from email attachments with a 90% accuracy. ",
          " In addition, the initial development was carried out on test data to monitor and improve Scarlet’s accuracy.  Once the High Peak team was able to improve the accuracy, Scarlet began processing actual data.",
          "The accuracy of the extracted data, however, is dependent on the source. For example, if the image file is quite blurred, it is possible to only partially extract data."
        ]
      },
      {
        img: "../assets/5_WordsListViewCase.png",
        solutionsDescription: [
          "Enhanced data security",
          "To enable data security, High Peak employed privileged access to client data and servers.  Access to critical data on the server was limited only to a few select members of the team",
          "For instance, the High Peak team had access to only one email account for parsing and extracting data for automating inbound payment operations.",
          "The team’s access to the client server was restricted by whitelisted IPs to the client’s VPN."
        ]
      }
    ]
  }
};
