export const GET_BLOGS = "GET_BLOGS";

export const GET_CATEGORY = "GET_CATEGORY";

export const STORE_CATEGORIES = "STORE_CATEGORIES";

export const GROUPED_BY_CATEGORY = "GROUPED_BY_CATEGORY";

export const SET_FLAG = "SET_FLAG";

