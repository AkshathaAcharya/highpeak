export const GOOGLE_ANALYTICS_CONSTANTS = {
  hitUsUp: {
    event: "HITUSUP_SUBMIT",
    page: "HITUSUP",
    details: "HITUSUP_PAGE"
  },
  demo: {
    event: "REQUEST FOR DEMO",
    page: "DEMO_SUBMIT",
    details: "DEMO_PAGE"
  },
  joinUs: {
    event: "JOINUS_SUBMIT",
    page: "UPLOAD RESUME",
    details: "JOINUS_PAGE"
  },
  home: {
    0: {
      event: "HOME_PAGE_SCROLL",
      page: "HOMELANDING",
      details: "HOME_PAGE"
    },
    1: {
      event: "HOME_PAGE_SCROLL",
      page: "WE_SOLVE_TOUGH_AI",
      details: "HOME_PAGE"
    },
    2: {
      event: "HOME_PAGE_SCROLL",
      page: "WE_MAKE_COOL_AI",
      details: "HOME_PAGE"
    },
    3: {
      event: "HOME_PAGE_SCROLL",
      page: "GET_FREE_AI",
      details: "HOME_PAGE"
    },
    4: {
      event: "HOME_PAGE_SCROLL",
      page: "DROP_IN",
      details: "HOME_PAGE"
    }
  },
  aboutUs: {
    0: {
      event: "ABOUT_PAGE_SCROLL",
      page: "WHO_ARE_WE",
      details: "ABOUT_PAGE"
    },
    1: {
      event: "ABOUT_PAGE_SCROLL",
      page: "WE_ARE_PARTNERS",
      details: "ABOUT_PAGE"
    },
    2: {
      event: "ABOUT_PAGE_SCROLL",
      page: "OUR_RESIDENT",
      details: "ABOUT_PAGE"
    },
    3: {
      event: "ABOUT_PAGE_SCROLL",
      page: "DROP_IN",
      details: "ABOUT_PAGE"
    }
  }
};
