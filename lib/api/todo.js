import axios from 'axios';
import { ROOT_URL } from './index';

export function getRandomData(url) {
    return axios.get(ROOT_URL+url);
}