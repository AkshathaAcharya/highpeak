import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import Reducer from "./reducers/reducerIndex";
import rootSaga from "./saga/sagaIndex";

const initialState = {};

const bindMiddleware = middleware => {
  if (process.env.NODE_ENV !== "production") {
    const { composeWithDevTools } = require("redux-devtools-extension");
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

export const initializeStore = (preloadedState = initialState) => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    Reducer,
    preloadedState,
    bindMiddleware([sagaMiddleware])
  );

  store.sagaTask = sagaMiddleware.run(rootSaga);

  return store;
};
