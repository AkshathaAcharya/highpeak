import axiosCall from "../../../utils/apiUtils";
import { takeLatest, all, call } from "redux-saga/effects";
import { SEND_DATA_TO_SERVER } from "../../../constants/hitUsUpNewConstants";

function* sendDataToServer(action) {
  try {
    const { payload } = action;
    const response = yield call(
      axiosCall,
      `${window.location.origin}/send`,
      payload,
      "post"
    );
    return false;
  } catch (error) {}
}

function* sendMail() {
  yield takeLatest(SEND_DATA_TO_SERVER, sendDataToServer);
}

export default function* hitUsUpSaga() {
  yield all([sendMail()]);
}
