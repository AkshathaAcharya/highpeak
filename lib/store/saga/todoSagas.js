import { constants, getRandomValueRequested, setRandomValue, getRandomValueFailed } from '../constantsAndActions/todoConstants';
import { all, call, put } from 'redux-saga/effects'
import { getWatcher, getSagaCustomizedObj } from './sagaIndex';
import { TAKE_EVERY, TAKE_LATEST } from '../constantsAndActions/commonConstants';
import * as api from '../../api/todo';

const urls = {
    GET_RANDOM_DATA: '/getData'
}

function* getRandomObj() {
    yield put(getRandomValueRequested());
    try {
        const response = yield call(api.getRandomData, urls.GET_RANDOM_DATA);
        if(response.status === 200 && response.data){
            yield put(setRandomValue(response.data));
        } else {
            throw new Error('Data is empty');
        }
    } catch (error){
        console.log(error);
        yield put(getRandomValueFailed());
    }
}

const sagaObj = {
    [constants.GET_RANDOM_VALUE]: getSagaCustomizedObj(TAKE_EVERY, getRandomObj)
}

export function* todoSagas(){
    const watcherArray = getWatcher(sagaObj);
    yield all(watcherArray.map(watcherFunc => watcherFunc()));
}