import axiosCall from "../../../utils/apiUtils";
import { takeLatest, all, put, call } from "redux-saga/effects";
import { UPLOAD_FILE_DATA } from "../../../constants/uploadFileAction";
const RESPONSE_TO_REDUCER = "RESPONSE_TO_REDUCER";

function* uploadDataToServer(action) {
  try {
    const { payload } = action;
    var response = yield call(
      axiosCall,
      `${window.location.origin}/upload`,
      payload,
      "post"
    );
    yield put({
      type: RESPONSE_TO_REDUCER,
      payload: response.data
    });
    return false;
  } catch (error) {}
}
function* UploadResume() {
  yield takeLatest(UPLOAD_FILE_DATA, uploadDataToServer);
}
export default function* JoinUsSaga() {
  yield all([UploadResume()]);
}
