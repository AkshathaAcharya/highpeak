import { all } from "redux-saga/effects";
import JoinUsSaga from "../saga/JoinUsSaga";
import hitUsUpSaga from "../saga/HitUsUpSaga";
import getBlogsSaga from "../saga/getBlogsSaga";
export default function* rootsaga() {
  yield all([JoinUsSaga(), hitUsUpSaga(),getBlogsSaga()]);
}
