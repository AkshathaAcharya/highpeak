import axiosCall from "../../../utils/apiUtils";
import { takeLatest, all, call, put, fork,takeEvery } from "redux-saga/effects";
import { GET_BLOGS, GET_CATEGORY } from "../../../constants/getBlogsConstants";
import { storeCategories, groupedByCategories, setFlag } from "../../../constants/getBlogsActions";

const url = 'https://blogs.highpeaksw.com/wp-json/wp/v2'

function* getBlogsFromWordpress(action) {
  try {
    const response = yield call(
      axiosCall,
      `${url}/posts?_embed&per_page=${action.page.perPage}&page=${action.page.pageNo}`,
      "get"
    );

    yield put(groupedByCategories(response.data))
    return false;
  } catch (error) {
    yield put(setFlag(false))
  }
}

function* getCategoriesFromWP() {
  try {
    const response = yield call(
      axiosCall,
      `${url}/categories`,
      "get"
    );
    yield put(storeCategories(response.data))
    return false;
  } catch (error) {
    console.log(error);
  }
}

export default function* getBlogsSaga() {
  yield takeEvery(GET_BLOGS, getBlogsFromWordpress);
  yield takeLatest(GET_CATEGORY, getCategoriesFromWP);
}
