import { handleActions } from "redux-actions";
import shortid from 'shortid';

import { constants } from '../constantsAndActions/todoConstants.js'

const initialState = {
    list: {},
    randomRequestStatus: false,
    randomValueRequestSuccess: true,
    randomValue: []
}

const todoReducerObj = {
    [constants.ADD_TODO]: (state, action) => {
        const id = shortid.generate();
        return ({
            ...state,
            list: {...state.list, [id]: {label: action.payload, status:constants.TODO_STATUS_NOT_DONE, id}}
        })
    },
    [constants.DONE_TODO]: (state, action) => ({
        ...state,
        list: {...state.list, [action.payload]: {...state.list[action.payload], status: constants.TODO_STATUS_DONE}}
    }),
    [constants.REMOVE_TODO]: (state, action) => {
        const { list } = state;
        delete list[action.payload]
        return {...state, list: {...list}}
    },
    [constants.UNDO_TODO]: (state, action) => ({
        ...state,
        list: {...state.list, [action.payload]: {...state.list[action.payload], status: constants.TODO_STATUS_NOT_DONE}}
    })
}

const randomReducerObj = {
    [constants.GET_RANDOM_VALUE_REQUESTED] : (state) => ({...state, randomRequestStatus: true}),
    [constants.GET_RANDOM_VALUE_SUCCEEDED] : (state) => ({...state, randomRequestStatus: false, randomValueRequestSuccess: true}),
    [constants.GET_RANDOM_VALUE_SUCCEEDED] : (state) => ({...state, randomRequestStatus: false, randomValueRequestSuccess: false}),
    [constants.SET_RANDOM_VALUE]: (state, action) => ({ ...state, randomValue: action.payload })
}

export const todoReducer = handleActions({...todoReducerObj, ...randomReducerObj}, initialState)