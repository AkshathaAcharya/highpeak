import getReducerFromObject from "../../../utils/reducerUtils";

const initialState = {
  store: []
};

const SendMailReducer = getReducerFromObject([], {
  RESPONSE_TO_REDUCER: (state, action) => {
    return {
      store: action.payload
    };
  }
});

export default SendMailReducer;
