import getReducerFromObject from "../../../utils/reducerUtils";
import {STORE_POSTS, STORE_CATEGORIES} from '../../../constants/getBlogsConstants'

const initialState = {
  categories : [],
  groupedByCategories : [],
  isFetch : true
};

const BlogsReducer = getReducerFromObject([], {

  STORE_CATEGORIES : (state, action) => {
    const allCategory = [{id :0, name : 'All'}]
      action.payload.forEach((category) => {
        allCategory.push(category)
      })

      return {
        groupedByCategories : state.groupedByCategories,
        categories : allCategory,
        isFetch : state.isFetch
      };
  },

  GROUPED_BY_CATEGORY : (state, action) => {
    let groupedByCategories = {posts : []}
    let postList = []
      state.categories.forEach((category, index) => {
        let groupedByCategoriesObj = {
          blogs : [],
          totalBlogs : 0
        };
        action.payload.forEach((post) => {  
          let postObj = {};
            postObj['postId'] = post.id;
            postObj['htmlContent'] = post.content.rendered;
            postObj['title'] = post.title.rendered;
            postObj['status'] = post.status;
            postObj['excerpt'] = post.excerpt.rendered;
            postObj['featureImageId'] = post.featured_media;
            postObj['redirectLink'] = post.link
            postObj['featuredMedia'] = post.featured_media !==0 && post._embedded['wp:featuredmedia'][0];
            postObj['featuredImage'] = post.featured_media !==0 && post._embedded['wp:featuredmedia'][0].source_url;
            postObj['obj'] = post
            postObj['catgories'] = post.categories

            if(index === 0){
              state.groupedByCategories ? state.groupedByCategories.posts.push(postObj) : postList.push(postObj)
            }

          if(post.categories.indexOf(category.id) === 0) {
            if(state.groupedByCategories){
              state.groupedByCategories[category.id].blogs.push(postObj)
            }
            else{  
              groupedByCategoriesObj.blogs.push(postObj)
            }
          }

        })
        if(state.groupedByCategories){
          state.groupedByCategories[category.id].totalBlogs = state.groupedByCategories[category.id].blogs.length
        }
        else{
          groupedByCategoriesObj.totalBlogs = groupedByCategoriesObj.blogs.length
          groupedByCategories[category.id] = groupedByCategoriesObj;
          groupedByCategories['posts'] = postList
        }
      })

      return {
        groupedByCategories : state.groupedByCategories ? state.groupedByCategories : groupedByCategories,
        categories : state.categories,
        isFetch : state.isFetch
      };

  },
  SET_FLAG : (state, action) => {
    return { 
      groupedByCategories : state.groupedByCategories,
      categories : state.categories,
      isFetch : action.payload
    }
  }

})

export default BlogsReducer;
