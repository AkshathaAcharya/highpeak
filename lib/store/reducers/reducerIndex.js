import { combineReducers } from "redux";
import SendMailReducer from "./sendMailReducer";
import BlogsReducer  from "./getBlogsReducer"

const Reducer = combineReducers({
  SendMailReducer,
  BlogsReducer
});

export default Reducer;
