import { createAction } from "redux-actions";

export const constants= {
    ADD_TODO: "ADD_TODO",
    DONE_TODO: "DONE_TODO",
    REMOVE_TODO: "REMOVE_TODO",
    UNDO_TODO: "UNDO_TODO",
    TODO_STATUS_DONE: "TODO_STATUS_DONE",
    TODO_STATUS_NOT_DONE: "TODO_STATUS_NOT_DONE",

    GET_RANDOM_VALUE: "GET_RANDOM_VALUE",
    GET_RANDOM_VALUE_REQUESTED: 'GET_RANDOM_VALUE_REQUESTED',
    GET_RANDOM_VALUE_SUCCEEDED: 'GET_RANDOM_VALUE_SUCCEEDED',
    GET_RANDOM_VALUE_FAILED: 'GET_RANDOM_VALUE_FAILED',
    SET_RANDOM_VALUE: 'SET_RANDOM_VALUE'
}

export const addTodo = createAction(constants.ADD_TODO);
export const doneTodo = createAction(constants.DONE_TODO);
export const removeTodo = createAction(constants.REMOVE_TODO);
export const undoTodo = createAction(constants.UNDO_TODO);

export const getRandomValue = createAction(constants.GET_RANDOM_VALUE);
export const getRandomValueRequested = createAction(constants.GET_RANDOM_VALUE_REQUESTED);
export const getRandomValueSucceeded = createAction(constants.GET_RANDOM_VALUE_SUCCEEDED);
export const getRandomValueFailed = createAction(constants.GET_RANDOM_VALUE_FAILED);
export const setRandomValue = createAction(constants.SET_RANDOM_VALUE);
